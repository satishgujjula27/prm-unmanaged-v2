import { LightningElement, track, api } from 'lwc';
//import getPicklistObjValues from '@salesforce/apex/PLTActionController.getPicklistObjValues';
import getPicklistValuesByRecordType from '@salesforce/apex/LogCallController.getPicklistValuesByRecordType';
export default class SearchComboBox extends LightningElement {
    @api objectName;
    @api fieldApiName;
    @track picklistoptions = [];
    @track serverResult = [];
    @api placeholder;
    @track selItemText;
    @api inpValue = '';
    @track spinner = false;
    connectedCallback() {
        this.serverCall();
    }
   
    get showValues(){
        return this.serverResult.length>0?true:false;
    }

    hideValues(event) {
        var that = this;
        setTimeout(function () {
            that.serverResult = [];
        }, 500);
    }

    searchCall(event) {
        var searchTxt = event.target.value;
        var retObj = this.picklistoptions;
        var text = '';
        if (searchTxt) {
            var text = searchTxt.toUpperCase();
        }
        var retObjArr = [];
        for (var i in retObj) {
            if (retObj[i].label.toUpperCase().indexOf(text) > -1) {
                retObjArr.push(retObj[i]);
            }
        }
        this.serverResult = retObjArr;
      
        if (searchTxt)
            this.fireSelBackEvent(searchTxt);
        else{
            this.inpValue = '';
            this.fireSelBackEvent(this.inpValue);
        }
           
    }

    serverCall() {
        this.spinner = true;
        getPicklistValuesByRecordType({ objName: this.objectName, fieldName: this.fieldApiName }).then(result => {
            var finalres = [];
            for (var eachResult in result) {
                finalres.push({ value: result[eachResult], label: result[eachResult] });
            }
            this.picklistoptions = finalres;
            //console.log(JSON.stringify(this.picklistoptions));
            this.spinner = false;
        })
            .catch(error => {
                alert(JSON.stringify(error));
            })
    }

    itemselected(event) {

        var target = event.target;
        var SelIndex = this.getIndexFrmParent(target, "data-selected-index");
        if (SelIndex) {
            var serverResult = this.serverResult;
            var selItem = serverResult[SelIndex];
            if (selItem.value) {
                this.fireSelBackEvent(selItem.label);
            }
            this.serverResult = [];
        }
    }

    getIndexFrmParent(target, attributeToFind) {
        //User can click on any child element, so traverse till intended parent found
        var SelIndex = target.getAttribute(attributeToFind);
        while (!SelIndex) {
            target = target.parentNode;
            SelIndex = this.getIndexFrmParent(target, attributeToFind);
        }
        return SelIndex;
    }

    fireSelBackEvent(comboText) {
        var combo = {};
        combo.text = comboText;
        this.inpValue = combo.text;
        // call the event
        const selectedEvent = new CustomEvent('valueselect', { detail: combo });
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
    }
    
}