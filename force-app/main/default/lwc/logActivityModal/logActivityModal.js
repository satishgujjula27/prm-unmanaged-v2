import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import saveTask from '@salesforce/apex/LogCallController.saveTask';
import getPicklistValues from '@salesforce/apex/LogCallController.getPicklistValues';
import getProviders2 from '@salesforce/apex/LogCallController.getProviders2';
import getLoggedInUser from '@salesforce/apex/LogCallController.getLoggedInUser';
import getActivityList from '@salesforce/apex/LogCallController.getActivityList';
import namespace from '@salesforce/label/c.BasePkgNamespace';
import pubsub from 'c/pubSub' ;

export default class LogActivityModal extends NavigationMixin(LightningElement) {
    @api recordId;
    mynamespace = namespace;
    modalBool;
    @api isRenderSalesforce1 = false;
    @track providerLocations = [];
    @track newTask = {};
    @track newFollowupTask = {};
    @track mycolumns = [];
    @track isTask = 0;
    @track subjectOptions = [];
    @track intelOptions = [];
    @track priorityOptions = [];
    @track statusOptions = [];
    @track keepRowSelected = true;
    @track isLogCall = true;
    @track isFollowUpTask = false;
    @track providerList = [];
    @track selectedLookUpRecord;
    @track keepSelection = [];
    @track selectedAssignedToLookUpRecord;
    @track timer;
    @track selectedProvider = [];
    @track sortedBy = 'firstName';
    @track sortedDirection = 'asc';
    @track pageNumber;
    @track pageSize;
    @track isLastPage;
    @track dataSize;
    @track selectedRecord;
    @track enableInfiniteLoading = true;
    @track initialRows = 3;
    @track rowsToLoad = 3;
    @track totalNumberOfRows = 11;
    @track defaultLimit = 50;
    @track defaultInitialOffset = 0;
    @track loadMoreStatus = 'Please scroll down to load more data';
    @track rowNumberOffset = 0;
    @track providerEntityName;
    @track showAccompained = false;
    @track today;
    @track message = '';
    @track messageType = '';
    @track selItem = {};
    @track selItem1 = {};
    @track selItem2 = {};
    @track spinner = false;
    @track filterString;
    @track isReminder = false;

    @track activityLists = [];
    @track activityTabFlds = [];
    @track objOptions = [];
    @track activityRequiredFlds = [];
    @track activityDefaultTabs = '';
    @track currentActiveTabName = '';
    @track selectedObject = '';
    @api showActivityTab;
    @api showLoadingActivityFields = false;
    dualListBoxvalues = [];

    filterBy = 'practice';
    @track value = '';
    taskReqFlds = ['OwnerId'];
    fldTypeArr = { 'STRING' : 'showStringField', 'DATE' : 'showDateField', 'BOOLEAN' : 'showBooleanField', 'TEXTAREA' : 'showTextareaField',
                    'PICKLIST' : 'showPicklistField', 'REFERENCE' : 'showReferenceField', 'MULTIPICKLIST' : 'showDualListBox', 'COMBOBOX' : 'showComboBox'
                 };

    get mycolumnslength() {
        return this.mycolumns.length > 0;
    }

    get intelprop() {
        return this.mynamespace + 'Intel__c';
    }

    get departmentprop() {
        return this.mynamespace + 'Department__c';
    }

    get intelDescriptionprop() {
        return this.mynamespace +'IntelDescription__c';
    }

    connectedCallback() {
        this.getProvider('', false);
        this.getLoggedInUser2();
        this.getPicklistValues2();
        this.getActivityLists('');
        this.setParams();
    }

    getActivityLists(tabSelectedVal) {
        getActivityList({
            "selectedTab": tabSelectedVal
        }).then(getActivityListdetail => {
            let detail = JSON.parse(getActivityListdetail);
            if( (typeof detail.actvtListWrapper == "object" || detail.actvtListWrapper.length != 0) && detail.defaultActTabs != null ) {
                this.showActivityTab = true;
                this.activityLists = detail.actvtListWrapper;
                this.activityDefaultTabs = detail.defaultActTabs;

                for (let prop in detail.actvtListWrapper) {
                    if( detail.actvtListWrapper[prop]["Id"] == detail.defaultActTabs ) {
                        this.currentActiveTabName = detail.actvtListWrapper[prop]["title"];
                    }
                }

                for(let key in detail.defaultFldList){
                    if(detail.defaultFldList[key].relObjs != '' && detail.defaultFldList[key].relObjs != null && detail.defaultFldList[key].relObjs.length != null && detail.defaultFldList[key].relObjs.length > 0){
                        this.selectedObject = detail.defaultFldList[key].relObjs[0];
                        break;
                    }                   
                }

                var objOptions = [];
                for(let key in detail.defaultFldList){
                    if(detail.defaultFldList[key].relObjs != '' && detail.defaultFldList[key].relObjs != null && detail.defaultFldList[key].relObjs.length != null && detail.defaultFldList[key].relObjs.length > 0){
                        for(let obj in detail.defaultFldList[key].relObjs){
                            objOptions.push({"value":detail.defaultFldList[key].relObjs[obj], "label":detail.defaultFldList[key].relObjs[obj]});
                        }
                    }                   
                }
                this.objOptions = objOptions;

                for (let key in detail.defaultFldList) {
                    let fldTypeVal = detail.defaultFldList[key].fieldType;
                    detail.defaultFldList[key][this.fldTypeArr[fldTypeVal]] = true;

                    // To make task standard fields mandatory
                    if( this.taskReqFlds.includes(detail.defaultFldList[key].apiName) ) {
                        detail.defaultFldList[key].isRequired = true;
                    }

                    // Create an array to validate required fields later before saving.
                    if(detail.defaultFldList[key].isRequired) {
                        this.activityRequiredFlds.push( detail.defaultFldList[key].apiName);
                    }

                    // Form the picklist array of objects that is accepted by the component.
                    if(fldTypeVal == 'PICKLIST') {
                        let newPickListObjArr = [];
                        for (let picklstValKey in detail.defaultFldList[key].picklistValues) {
                            let newPickListObj = new Object(); 
                            newPickListObj = { label : detail.defaultFldList[key].picklistValues[picklstValKey], value : detail.defaultFldList[key].picklistValues[picklstValKey] };
                            newPickListObjArr.push(newPickListObj);
                        }
                        detail.defaultFldList[key].picklistValues = newPickListObjArr;
                    }
                    if(detail.defaultFldList[key].defaultValue != null) {
                        this.newTask[detail.defaultFldList[key].apiName] = detail.defaultFldList[key].defaultValue;
                    }
                }
                this.activityTabFlds = detail.defaultFldList;
                this.showLoadingActivityFields = false;
            } else { this.showActivityTab = false; }
        }).catch(error => {
            this.errorMessage = 'Could not find the user';
        })
    }

    tabSelected(event) {
        if( event.target.title != null ) {
            this.activityTabFlds = []; this.showLoadingActivityFields = true;
            this.currentActiveTabName = event.target.title;
            this.getActivityLists(event.target.title);
        }
    }

    getProvider(searchKey, isAlphaSearch) {
        this.spinner = true;
        getProviders2({
            "searchKey": searchKey,
            "isAlphaSearch": isAlphaSearch,
            "filterBy": this.filterBy,
            "filterOpt": this.recordId,
            "sortField": this.sortedBy,
            "sortDirection": this.sortedDirection
        }).then(result => {
            let res = JSON.parse(result);
            if (res != null) {
                let fieldObj = res.fieldsWrapper;
                let resultData = res.resultWrapper2;
                let headerFields = [];
                for (let i in fieldObj) {
                    let v = {
                        label: fieldObj[i].label, fieldName: fieldObj[i].linkfieldName, type: fieldObj[i].fieldType, sortable: fieldObj[i].sortable,
                        typeAttributes: {
                            label: { fieldName: fieldObj[i].fieldName }
                        }
                    };
                    headerFields.push(v);
                }
                this.mycolumns = headerFields;

                if (res.providerEntityName != null && res.providerEntityName != 'null') {
                    this.showAccompained = true;
                    this.providerEntityName = res.providerEntityName;
                }
                let providerListIds = this.providerList;
                let idArr = [];
                if (providerListIds != '') {
                    idArr = providerListIds.split(",");
                }

                if (idArr.length > 0) {
                    // start of sorting data on provider selection
                    let tempArray = [];
                    for (let i = 0; i < idArr.length; i++) {
                        let obj = resultData.find(x => x.Id == idArr[i]);
                        if (obj !== undefined)
                            tempArray.push(obj);
                    }

                    tempArray.filter((item, index) => {
                        let tempArrIndex = resultData.indexOf(item);
                        resultData.splice(tempArrIndex, 1)
                    });
                    let newArr = tempArray.concat(resultData);
                    this.providerLocations = newArr;
                    // end of sorting data on provider selection

                    this.keepSelection = idArr;
                    this.selectedProvider = idArr;
                } else {
                    this.providerLocations = resultData;
                    this.sortData( this.sortedBy, this.sortedDirection);
                }
            }
            this.spinner = false;
        })
        .catch(error => {
            console.log('FAILED WITH ERROR :' + error);
        })
    }

    delayedKeyupEvent(event) {
        let timer = this.timer;
        clearTimeout(timer);
        let that = this;
        timer = setTimeout(function () {
            that.quickSearch();
            clearTimeout(timer);
            that.timer = null;
        }, 500);
        this.timer = timer;
    }

    updateColumnSorting(event) {
        this.sortedBy = event.detail.fieldName;
        let fieldName = '';
        if (this.sortedBy === 'fLinkId') {
            fieldName = 'firstName';
        } else if (this.sortedBy === 'lLinkId') {
            fieldName = 'lastName';
        }
        this.sortedDirection = event.detail.sortDirection;
        this.sortData(fieldName, this.sortedDirection);
    }

    sortData(fieldName, sortDirection) {
        let sortResult = Object.assign([], this.providerLocations);
        this.providerLocations = sortResult.sort(function(a,b){
            if(a[fieldName] < b[fieldName]) {
                return sortDirection === 'asc' ? -1 : 1;
            } else if(a[fieldName] > b[fieldName]) {
                return sortDirection === 'asc' ? 1 : -1;
            } else {
                return 0;
            }
        });
    }

    quickSearch() {
        this.getProvider(this.template.querySelector('[data-id="providerSearchBox"]').value, false);
    }

    clickAlphaSearch(event) {
        this.initialRows = this.defaultLimit;
        this.rowNumberOffset = this.defaultInitialOffset;
        let alphaSearchKey = event.currentTarget.getAttribute("data-attr");
        this.getProvider(alphaSearchKey, true);
    }

    handleRowAction(event) {
        let selRows = this.template.querySelector('lightning-datatable').getSelectedRows();
        this.selectedProvider = selRows;
        let allSelectedRows = [];

        selRows.forEach(function (row) {
            if (!allSelectedRows.includes(row.Id)) {
                allSelectedRows.push(row.Id);
            }
        });
        this.keepSelection = allSelectedRows;
    }

    handleKeepSelected() {
        this.keepRowSelected = !this.keepRowSelected;
    }

    handleReset() {
        this.getProvider('', false);
        this.template.querySelector('[data-id="providerSearchBox"]').value = '';
    }

    createNewTask() {
        let currentRecordType;
        if( this.activityLists.length > 0 ) {
            for (let actLsts in this.activityLists) {
                if( this.activityLists[actLsts]["Id"] == this.activityDefaultTabs ) {
                    currentRecordType = this.activityLists[actLsts]["label"];
                }
            }
        } else {
            currentRecordType = '';
        }

        let self = this; let returnVal;
        this.newFollowupTask.WhatId = this.recordId;
        let selectedProvider = this.selectedProvider;
        if (selectedProvider.length > 0) {
            // iterate through the providers that were selected in the table.
            let selectedIdProvider = [];
            for (let key in selectedProvider) {
                // First if condition for checking if it is from Activities component
                if (JSON.parse(this.isLogCall)) {
                    // if activity component, below will work because it returns
                    // location object to front-end.
                    if (typeof selectedProvider[key] == 'object' && selectedProvider[key].Id != "undefined") {
                        selectedIdProvider.push(selectedProvider[key].pId);
                        // this use-case applies when it is called from Bulk Action.
                    } else {
                        selectedIdProvider.push(selectedProvider[key]);
                    }
                } else {
                    if (typeof selectedProvider[key] == 'object' && selectedProvider[key].Id != "undefined") {
                        selectedIdProvider.push(selectedProvider[key].pId);
                        // this use-case applies when it is called from Bulk Action.
                    } else {
                        selectedIdProvider.push(selectedProvider[key]);
                    }
                }
            }
            
            // Check if it is dynamic layout or traditional screen
            // Dynamic layout where showActivityTab boolean is true
            if( this.showActivityTab ) {
                let validateActFlds = this.validateActReqField(this.newTask, this.activityRequiredFlds);
                if( validateActFlds ) { returnVal = true; } 
                else {
                    this.showToast('Please Fill Required Fields And Review Errors On This Page', 'error');
                    returnVal = false;
                }
            } else {
                let valid = this.validateRequiredField(this.newTask, this.isLogCall, this.newFollowupTask, this.isFollowUpTask);
                if (valid) { returnVal = true; } 
                else {
                    this.showToast('Please Fill Required Fields And Review Errors On This Page', 'error');
                    returnVal = false;
                }
            }
            if( returnVal ) {
                this.saveTaskRecord(this.newTask, this.isLogCall, this.newFollowupTask, this.isFollowUpTask, currentRecordType, selectedIdProvider);
            }
        } else {
            this.showToast('Nothing to process', 'warning');
        }
    }

    saveTaskRecord(newTaskObj, isLogCallObj, newFollowupTaskObj, isFollowUpTaskObj, currentRecType, thisProviderList) {
        let state;
        saveTask({
            "task": newTaskObj,
            "followupTask": newFollowupTaskObj,
            "isCreateTask": isLogCallObj,
            "iscreateFollowupTask": isFollowUpTaskObj,
            "thisRecordTypeName" : currentRecType,
            "providerList": thisProviderList
        }).then(result => {
            if( result ) {
                let message = '';
                if (isLogCallObj) {
                    message = "Total new calls logged:" + thisProviderList.length;
                    state = 'success';
                    this.messageType = 'success';
                }
                if (isFollowUpTaskObj) {
                    message += " Follow-up task created:" + thisProviderList.length;
                    state = 'success';
                    this.messageType = 'success';
                }
    
                this.showToast(message, this.messageType);
                if (this.keepRowSelected == false) {
                    this.template.querySelector('lightning-datatable').selectedRows = [];
                    this.selectedProvider = [];
                }
                this.handleCloseModal();
            } else {
                this.showToast('Saving Failed - check configs', 'error');
            }
        })
        .catch(error => {
            console.log('saving Failed');
        })
        return state;
    }

    showToast(message, messageType) {
        const event = new ShowToastEvent({
            title: 'Process Info',
            message: message,
            variant: messageType
        });
        this.dispatchEvent(event);
    }

    validateActReqField(newTask, activityFields) {
        let returnVal;
        let missingReqFields = [];
        activityFields = activityFields.filter((c, index) => {
            return activityFields.indexOf(c) !== index;
        });

        for(let regFld in activityFields ) {
            if (newTask[activityFields[regFld]] === undefined || newTask[activityFields[regFld]] === "") {
                missingReqFields.push(newTask[activityFields[regFld]]); 
            }
        }
        missingReqFields.length !== 0 ? returnVal = false : returnVal = true; 
        return returnVal;
    }

    validateRequiredField(newTask, isLogCall, newFollowUpTask, isFollowUpTask) {
        if (isLogCall) {
            if (newTask.OwnerId === undefined || newTask.OwnerId === "" || 
                newTask.Subject == undefined || newTask.Subject === ""
               ) {
                    return false;
               }
        }
        if (isFollowUpTask) {
            if (newFollowUpTask.OwnerId === undefined || newFollowUpTask.OwnerId === "" ||
                newFollowUpTask.Status == undefined || newFollowUpTask.Status === "" ||
                newFollowUpTask.Priority == undefined || newFollowUpTask.Priority === "" ||
                newFollowUpTask.Subject == undefined || newFollowUpTask.Subject === "") {
                    return false;
            }
        }
        return true;
    }

    getLoggedInUser2() {
        getLoggedInUser().then(result => {
            this.selItem1 = { 'text': result.Name };
            this.newTask.OwnerId = result.Id;
            this.selItem2 = { 'text': result.Name };
            this.newFollowupTask.OwnerId = result.Id;
        })
        .catch(error => {
            this.errorMessage = 'Could not find the user';
        })
    }

    getPicklistValues2() {

        getPicklistValues().then(result => {

            if (result !== null) {

                let subjectoptions = [{ label: 'None', value: '' }];
                for (let i in result.subject) {
                    let v = { label: result.subject[i], value: result.subject[i] };
                    subjectoptions.push(v);
                }
                this.subjectOptions = subjectoptions;

                let inteloptions = [{ label: 'None', value: '' }];
                for (let i in result.intel) {
                    if (result.intel[i] == 'None') {
                        let w = { label: result.intel[i], value: '' };
                        inteloptions.push(w);
                    } else {
                        let v = { label: result.intel[i], value: result.intel[i] };
                        inteloptions.push(v);
                    }
                }
                this.intelOptions = inteloptions;

                let statusoptions = [{ label: 'None', value: '' }];
                for (let i in result.status) {
                    let v = { label: result.status[i], value: result.status[i] };
                    statusoptions.push(v);
                }
                this.statusOptions = statusoptions;

                let priorityoptions = [{ label: 'None', value: '' }];
                for (let i in result.priority) {
                    let v = { label: result.priority[i], value: result.priority[i] };
                    priorityoptions.push(v);
                }
                this.priorityOptions = priorityoptions;

                this.filterString = result.filterString;
            }
        })
    }

    setParams() {
        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1;
        let yyyy = today.getFullYear();

        if (dd < 10)
            dd = '0' + dd;
        if (mm < 10)
            mm = '0' + mm;

        today = mm + '/' + dd + '/' + yyyy;
        this.today = today;

        //accepting isTask from bulk action
        let isTask = this.isTask;
        if (isTask == 1) {
            this.isFollowUpTask = true;
            this.isLogCall = false;
        }
    }

    logCall() { this.isLogCall = !this.isLogCall; }
    createFollowUpTask() { this.isFollowUpTask = !this.isFollowUpTask; }

    insertCallDate() {
        let d = new Date();
        let dd = d.getDate();
        let mm = d.getMonth() + 1;
        let yyyy = d.getFullYear();
        if (dd < 10)
            dd = '0' + dd;
        if (mm < 10)
            mm = '0' + mm;
        d = yyyy + '-' + mm + '-' + dd;
        this.newTask.ActivityDate = d;
    }

    insertFollowupTaskDate() {
        let d = new Date();
        let dd = d.getDate();
        let mm = d.getMonth() + 1;
        let yyyy = d.getFullYear();
        if (dd < 10)
            dd = '0' + dd;
        if (mm < 10)
            mm = '0' + mm;
        d = yyyy + '-' + mm + '-' + dd;
        this.newFollowupTask.ActivityDate = d;
    }

    handleCall(event) {
        if (event.target.name == this.mynamespace + 'Intel__c')
            this.value = event.detail.value;
        this.newTask[event.target.name] = event.target.value;
    }

    onLookupSelection(event) { this.newTask[event.target.name] = event.detail.selectedRecordId; }
    handlePicklistTxtDtChange(event) { this.newTask[event.target.name] = event.target.value; }
    handleCheckBoxChange(event) { this.newTask[event.target.name] = event.target.checked; }
    handleCallSub(event) { this.newTask.Subject = event.getParam("text"); }
    handleCallSub(event) { this.newTask.Subject = event.detail.text; }
    handleTaskSub(event) { this.newFollowupTask.Subject = event.detail.text; }
    handleTask(event) { this.newFollowupTask[event.target.name] = event.target.value; }
    handleIsReminder() { this.isReminder = !this.isReminder; this.newFollowupTask.IsReminderSet = this.isReminder; }
    handleLookupCmpEvent(event) {
        let selectedObj = event.detail.selItem;
        let lookupFieldName = event.detail.lookupFieldName;
        let selVal = '';

        if (selectedObj != null) {
            selVal = selectedObj.val;
        }
        if (lookupFieldName == 'AccompaniedPhysician__c') {
            if (this.mynamespace != null && this.mynamespace != '') {
                this.newTask[this.mynamespace + 'AccompaniedPhysician__c'] = selVal;
            }
            else {
                this.newTask.AccompaniedPhysician__c = selVal;
            }
        }
        if (lookupFieldName == 'AssignedTo') {
            this.newFollowupTask.OwnerId = selVal;
        }
        if (lookupFieldName == 'OwnerId') {
            
            this.newTask.OwnerId = selVal;
        }
        if (lookupFieldName == 'CallAssignedTo') {
            this.newTask.OwnerId = selVal;
        }
        if (lookupFieldName == 'WhatId') {
            this.newTask.WhatId = selVal;
        }
    }

    handleCloseModal(event) {
        if (this.isRenderSalesforce1 == true) {
            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: this.recordId,
                    objectApiName: 'Contact',
                    actionName: 'view'
                },
            });
        } else {
            this.modalBool = false;
            // call the event
            const selectedEvent = new CustomEvent('closemodal', { detail: this.modalBool });
            // Dispatches the event.
            this.dispatchEvent(selectedEvent);
        }
    }
    
    setObjects(event){
        var activityTabFlds = this.activityTabFlds;
        this.selectedObject = event.detail.value;
        
        for(var key in activityTabFlds) {
            if(activityTabFlds[key].relObjs.length <= 0) {
              // No related objects available for processing - continue to next iteration
              continue;
            }
            let message =  'WhatId';
            pubsub.fire('simplevt', message);
        }        
        this.activityTabFlds = activityTabFlds;
    }
}