import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getEventInfo from '@salesforce/apex/UnProcessedEventController.getEventInfo';
import saveTask from '@salesforce/apex/UnProcessedEventController.saveTask';
export default class EventProcessorComp extends LightningElement {
    @api eventId;
    @track WhatName;
    @track WhoName;
    @track StartDateTime;
    @track EndDateTime;
    @track Description;
    @track Subject;
    @track Location;
    @track WhatLinkId;
    @track WhoLinkId;
    @track eventLinkId;
    @track isCreateTask = true;
    @track isCreateFollowTask = false;
    @track newTask = {
        Subject: '',
        Description: '',
        ActivityDate: ''
    };
    @track newFollowupTask = {
        Subject: '',
        Status: '',
        Priority: '',
        Description: '',
        ActivityDate: '',
        OwnerId: ''
    };
    @track priorityOptions = [];
    @track statusOptions = [];
    @track selItem = {};
    @track data;
    @track columns;
    @track selectedRows = [];
    @track eventProcessed = true;
    @track spinner = false;
    
    
    connectedCallback() {
        this.getEventInfo();
    }

    handleCloseModal(event){
        this.closeModal(false);
    }

    handleCancel(event) {
        this.closeModal(false);
    }

    handleProcessEvent(event){
        if(this.eventProcessed == true){
            this.eventProcessed = false;
        }
        else
            this.eventProcessed = true;
    }

    closeModal(isRefreshData){
        var pgEventObj={};
        pgEventObj.isRefresh = isRefreshData;
        const selectedEvent = new CustomEvent('closemodal', { detail: pgEventObj });
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
    }

    handleCreateFollowUpTask(event) {
        if(this.isCreateFollowTask == true){
            this.isCreateFollowTask = false;
        }
        else
            this.isCreateFollowTask = true;
    }

    handleCreateTask(event) {
        if(this.isCreateTask == true){
            this.isCreateTask = false;
        }
        else
            this.isCreateTask = true;
    }

    handleLookupCmpEvent(event) {
        var selectedObj = event.detail.selItem;
        var lookupFieldName = event.detail.lookupFieldName;
        var selVal = '';

        if (selectedObj != null) {
            selVal = selectedObj.val;
        }
        
        if (lookupFieldName == 'AssignedTo') {
            this.newFollowupTask.OwnerId = selVal;
        }
        
    }

    getEventInfo() {
        this.spinner = true;
        getEventInfo({
            eventId : this.eventId
        })
        .then(result => {
            var result = JSON.parse(result);
            if (result != null) {
               if(result.event != null){
                    this.Description = result.event.decription;
                    this.Subject = result.event.subject;
                    this.WhoName = result.event.name;
                    this.WhatName = result.event.whatId;
                    this.StartDateTime = result.event.startdatetime;
                    this.EndDateTime = result.event.enddatetime;
                    this.Location = result.event.location;
                    this.WhoLinkId = '/'+result.event.whoId;
                    this.WhatLinkId = '/'+result.event.whatId;
                    this.eventLinkId = '/'+result.event.Id;

               }
               if(result.picklistMap !=null){
                let statusoptions = [{ label: 'None', value: '' }];
                for (var i in result.picklistMap.status) {
                    let v = { label: result.picklistMap.status[i], value: result.picklistMap.status[i] };
                    statusoptions.push(v);
                }
                this.statusOptions = statusoptions;

                let priorityoptions = [{ label: 'None', value: '' }];
                for (var i in result.picklistMap.priority) {
                    let v = { label: result.picklistMap.priority[i], value: result.picklistMap.priority[i] };
                    priorityoptions.push(v);
                }
                this.priorityOptions = priorityoptions;
               }

               if(result.loggedInUser !=null){
                this.selItem = { 'text': result.loggedInUser.Name };
                this.newFollowupTask.OwnerId = result.loggedInUser.Id;
               }

               if(result.flswrapper != null){
                var columns=[];
                var btnColmn = {type: 'button-icon', label : 'IN MY TERRITORIES', typeAttributes: {name: 'primary', iconName: {fieldName: 'iconName'}, disabled: {fieldName: 'disabledValue'}, alternativeText: {fieldName: 'alternativeTextValue'}}};
                columns.push(btnColmn);
                for(var i=0;i<result.flswrapper.length;i++){
                    var c = {label: result.flswrapper[i].label, fieldName: result.flswrapper[i].linkfieldName, type: result.flswrapper[i].fieldType,
                        typeAttributes: {
                            label: { fieldName: result.flswrapper[i].fieldName },
                            target: '_blank'
                        }
                    };
                    columns.push(c);
                }
                this.columns = columns;
                this.data = result.selectedPLRecords;
                if(result.selectedRecords !=null){
                    this.selectedRows = result.selectedRecords;
                }
                this.setInTerritory();
                this.spinner = false;
               }

            }
            this.spinner = false;
        })
            .catch(error => {
                this.error = error;
                this.spinner = false;
            });

    }

    handleRowSelection(event) {
        var selRows = this.template.querySelector('lightning-datatable').getSelectedRows();
        var plIds = [];
        selRows.forEach(function(row) {
            plIds.push(row.Id);
        });
        this.selectedRows = plIds;
    }

    handleTask(event) {
        this.newTask[event.target.name] = event.target.value;
    }

    handleFollowUpTask(event) {
        this.newFollowupTask[event.target.name] = event.target.value;
    }

    handleFollowUpSub(event) {
        this.newFollowupTask.Subject = event.detail.text;
    }

    handleTaskSub(event) {
        this.newTask.Subject = event.detail.text;
    }

    handleProcess(event){
        this.spinner = true;
        if(this.selectedRows!=null && this.selectedRows.length > 0){
        var valid = this.validateRequiredField(this.newTask, this.isCreateTask, this.newFollowupTask, this.isCreateFollowTask);
        if(valid){
        saveTask({
            "task": this.newTask,
            "followupTask": this.newFollowupTask,
            "isCreateTask": this.isCreateTask,
            "iscreateFollowupTask": this.isCreateFollowTask,
            "providerList": this.selectedRows,
            "eventId": this.eventId,
            "isEventProcessed": this.eventProcessed
        }).then(result => {
            var result = JSON.parse(result);
            if(result.reqResult == true){
                if(this.eventProcessed == true){
                    this.showMessage('success', 'Event processed.', 'success');
                }
                else if(this.newFollowupTask == true){
                    this.showMessage('success', 'Follow-up task created.', 'success');
                }
                else{
                    let message = 'Total new tasks logged: '+this.selectedRows.length;
                    this.showMessage('success', message, 'success');
                }
                this.closeModal(true);
            }
            else{

            }
            this.spinner = false;
        })
            .catch(error => {
                console.log('saving Failed');
                this.spinner = false;
            })
        }
        else{
            this.showMessage('Error', 'Please Fill Required Fields And Review Errors On This Page', 'error');    
            this.spinner = false;
        }
        }
        else{
            this.showMessage('Error', 'Nothing to process.', 'error');
            this.spinner = false;
        }
    }

    setInTerritory(){
        let data = this.data;
        data = data.map(function(rowData) {
            if(rowData.isInTerritory == true){
                rowData.iconName = 'utility:check';
                rowData.alternativeTextValue = 'Checked';
            }
            else{
                rowData.alternativeTextValue = 'Not Checked';
            }
            rowData.disabledValue = true;
            return rowData;
        });
        this.data = data;
    }

    validateRequiredField(newTask, isCreateTask, newFollowUpTask, isCreateFollowTask) {
        if (isCreateTask) {
            if (newTask.Subject == undefined || newTask.Subject === "")
                return false;
        }
        if (isCreateFollowTask) {
            if (newFollowUpTask.OwnerId === undefined || newFollowUpTask.OwnerId === "" || newFollowUpTask.Subject == undefined || newFollowUpTask.Subject === "" ||
                newFollowUpTask.Status == undefined || newFollowUpTask.Status === "" ||
                newFollowUpTask.Priority == undefined || newFollowUpTask.Priority === "")
                return false;
        }
        return true;
    }

    showMessage(messageTitle,messageBody,messageType){
        const event = new ShowToastEvent({
            title: messageTitle,
            message: messageBody,
            variant : messageType
        });
        this.dispatchEvent(event);
    }

}