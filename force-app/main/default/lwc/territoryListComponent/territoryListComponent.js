import { LightningElement,api,track } from 'lwc';

export default class TerritoryListComponent extends LightningElement {
    @api pldata;
    @api columns;
    @api territoryName;
    @api terrId;
    @track showPlTable = false;
    @track plChecked = false;
    @api assocPlIds = [];
    @track activeSections = [];

    connectedCallback() {
        if(this.assocPlIds.length > 0) {
            this.plChecked = true;
        }
    }

    HideShowPlTable(event) {
        if(this.showPlTable == true){
            this.showPlTable = false;
        } else {
            this.showPlTable = true;
        }
    }

    handleRowSelection(event) {
        var selRows = this.template.querySelector('lightning-datatable').getSelectedRows();
        var  assocPlIds = this.assocPlIds;
        var allSelectedRows = [];
        var terrplObj = {};
        var plIds = [];
        var newPlIds = [];
        var removedPlIds = [];

        selRows.forEach(function(row) {
            plIds.push(row.Id);
            if(!assocPlIds.includes(row.Id))
                newPlIds.push(row.Id);
        });
        
        assocPlIds.forEach(function(Id) {
            if(!plIds.includes(Id)) {
                removedPlIds.push(Id);
            }
        });
        terrplObj['terrId'] = this.terrId;
        terrplObj['selPlIds'] = plIds;
        terrplObj['removedPlIds'] = removedPlIds;
        terrplObj['newPlIds'] = newPlIds;
        terrplObj['isNewTerritory'] = false;
        var pgEventObj={};

        pgEventObj.terrplObj=terrplObj;
        const pgEvent = new CustomEvent('addtomyterrevent', { detail: pgEventObj });
        this.dispatchEvent(pgEvent);
    }
}