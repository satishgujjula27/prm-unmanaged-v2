import { LightningElement, api, track, wire } from 'lwc';
import getContactSpeciality from '@salesforce/apex/Util.getContactSpeciality';
import getSpecialityLookup from '@salesforce/apex/Util.getSpecialityLookup';
import getSpecialityDetails from '@salesforce/apex/Util.getSpecialityDetails';
import saveProviderSpeciality from '@salesforce/apex/Util.saveProviderSpeciality';
import baseNamespaceLbl from '@salesforce/label/c.BasePkgNamespace';
import FORM_FACTOR from '@salesforce/client/formFactor';

export default class SpecialityLookupComponent extends LightningElement {
    @api isTerritoryView; @api isProvSpecView; @api isDetailView; 
    @api recordId; groupingOptions = []; classificationOptions = []; specializationOptions = [];
    isViewMode = true; isEditMode = false; isClassificationRequired = false; isSaveDisabled = false;
    currViewCSS = 'slds-form-element slds-form-element_edit slds-hint-parent slds-border_bottom slds-var-p-bottom_x-small';
    groupOptionsValue = ''; classifyOptionsValue = ''; specialOptionsValue = ''; codeOptionsValue = '';
    finalSelectedSpecString = ''; selectedSpecFilterType=''; groupingVal; classificationVal; specializationVal;
    classificationOptionsReadonly = true; specializationOptionsReadonly = true; showToast = false;

    selectedDropDown = [
        {label: 'grouping', value: 'true'},
        {label: 'classification', value: 'false'},
        {label: 'specialization', value: 'false'}
    ];

    specialityColumns = [
        {label: 'Grouping', fieldName: baseNamespaceLbl+'Grouping__c', type: 'text'},
        {label: 'Classification', fieldName: baseNamespaceLbl+'Classification__c', type: 'text'},
        {label: 'Specialization', fieldName: baseNamespaceLbl+'Specialization__c', type: 'text'},
        {label: 'Taxonomy Code', fieldName: baseNamespaceLbl+'Code__c', type: 'text'}
    ];

    specialityData = ''; selectedRows = []; queryLimit;

    connectedCallback() {
        this.loadSpecialities('none', '300');
        this.getSpecialityData(baseNamespaceLbl + 'Speciality__c', baseNamespaceLbl + 'Grouping__c', '', 'groupingOptions');
    }

    get layoutSize() {
        return (FORM_FACTOR === 'Large' ? 6 : 12);
    }

    get layoutSizeForSpecialitySearch() {
        return (FORM_FACTOR === 'Large' ? 6 : 6);
    }

    get infoClassName() {
        return (FORM_FACTOR != 'Large' ? 'slds-media iPad-infomessage-margin' : 'slds-media infomessage-margin');
    }

    get displayRefreshIcon() {
        return ((FORM_FACTOR != 'Large' && !this.isEditMode) ? true : false);
    }

    get showSearchLabel() {
        if (this.isProvSpecView) {
            return 'Search';
        } else {
            return '';
        }
    }

    get notDetailView() {
        if (this.isProvSpecView || this.isTerritoryView) {
            return true;
        } else {
            return false;
        }
    }

    getContactSpecData(thisRecordId) {
        getContactSpeciality({
            parentRecordId: this.recordId
        })
        .then(contactSpecResult => {
            if (contactSpecResult != null) {
                for (var i=0;i<contactSpecResult.length;i++) {
                    if (contactSpecResult[i].fieldAPIName == baseNamespaceLbl + 'Grouping__c') {
                        this.groupingVal = contactSpecResult[i].fieldValue;
                    } else if (contactSpecResult[i].fieldAPIName == baseNamespaceLbl + 'Classification__c') {
                        this.classificationVal = contactSpecResult[i].fieldValue;
                    } else if (contactSpecResult[i].fieldAPIName == baseNamespaceLbl + 'Specialization__c') {
                        this.specializationVal = contactSpecResult[i].fieldValue;
                    }

                    if (this.isViewMode) {
                        this.groupOptionsValue =  (this.groupingVal != undefined) ? this.groupingVal : '';
                        this.classifyOptionsValue = (this.classificationVal != undefined) ? this.classificationVal : '';
                        this.specialOptionsValue =  (this.specializationVal != undefined) ? this.specializationVal : '';
                    }
                }
            }
        })
        .catch(contactSpecError => {
            console.log('contactSpecError*****', contactSpecError);
        });
    }

    changeView() {
        this.isEditMode = true; this.isViewMode = false;
        this.currViewCSS += ' customslds-form-element_readonly slds-border_bottom_none';
        if (this.isDetailView) {
            if (FORM_FACTOR === 'Large') {
                this.template.querySelector('div[data-id="infomessage"]').className = 'slds-media slds-m-top_medium'; 
            } else {
                this.template.querySelector('div[data-id="infomessage"]').className = 'slds-media iPad-infomessage-margin';
            }

            if( this.groupOptionsValue.length <= 0 || this.groupOptionsValue == '' ) {
                this.classificationOptionsReadonly = true; this.specializationOptionsReadonly = true;
            } else {
                this.classificationOptionsReadonly = false; this.specializationOptionsReadonly = false;
            }
            let classificationSearchStringForBackend = this.groupOptionsValue;
            let specializationSearchStringForBackend = this.groupOptionsValue + ' | ' + this.classifyOptionsValue + ' | ' + this.specialOptionsValue;
            this.isClassificationRequired = (this.groupingVal != undefined) ? true : false;
            this.isSaveDisabled = (this.groupingVal != undefined) ? false : true;
            this.getSpecialityData(baseNamespaceLbl + 'Speciality__c', baseNamespaceLbl + 'Classification__c', classificationSearchStringForBackend, 'classificationOptions');
            this.getSpecialityData(baseNamespaceLbl + 'Speciality__c', baseNamespaceLbl + 'Specialization__c', specializationSearchStringForBackend, 'specializationOptions');
            //this.classificationOptionsReadonly = false; this.specializationOptionsReadonly = false;
        }
    }

    handleSpecRowAction(event) { 
        const selectedRows = event.detail.selectedRows;
        const selectEvent = new CustomEvent('selectedRow', {
            detail: {selectedRows}
        });
       this.dispatchEvent(selectEvent);
    }

    handleCancel() {
        this.isViewMode = true; this.isEditMode = false;
        this.currViewCSS = 'slds-form-element slds-form-element_edit slds-hint-parent slds-border_bottom';
        this.groupOptionsValue =  (this.groupingVal != undefined) ? this.groupingVal : '';
        this.classifyOptionsValue = (this.classificationVal != undefined) ? this.classificationVal : '';
        this.classificationOptions = (this.classificationOptions.length > 0) ? this.classificationOptions : [];
        this.specialOptionsValue = (this.specializationVal != undefined) ? this.specializationVal : '';
        this.specializationOptions = (this.specializationOptions.length > 0) ? this.specializationOptions : [];
        this.classificationOptionsReadonly = (this.classificationOptions.length > 0) ? false : true; 
        this.specializationOptionsReadonly = (this.specializationOptions.length > 0) ? false : true;
        if (FORM_FACTOR === 'Large') {
            this.template.querySelector('div[data-id="infomessage"]').className = 'slds-media infomessage-margin'; 
        } else {
            this.template.querySelector('div[data-id="infomessage"]').className = 'slds-media iPad-infomessage-margin';
        }
    }

    comboBoxOptChange(event) {
        let comboboxSelLabel = event.target.options.find(opt => opt.value === event.detail.value).label;
        let nextItem; let nextItemObjName; let comboboxName = event.target.name; let comboboxSelValue = event.detail.value;
        if (comboboxName == "grouping") { 
            this.groupOptionsValue = comboboxSelLabel; 
            this.isClassificationRequired = (this.groupOptionsValue.length > 0) ? true : false;
            this.isSaveDisabled = (this.groupOptionsValue.length == 0) ? true : false;
        }
        else if (comboboxName == "classification") { this.classifyOptionsValue = comboboxSelLabel; this.validateClassificationValue(); }
        else if (comboboxName == "specialization") { this.specialOptionsValue = comboboxSelLabel; }

        let alreadySelectedDropDown = JSON.parse(JSON.stringify(this.selectedDropDown));
        Object.keys(alreadySelectedDropDown).forEach(alrSelDropDwn => {
            if (alreadySelectedDropDown[alrSelDropDwn].label == comboboxName) {
            	if (parseInt(alrSelDropDwn) + 1 < alreadySelectedDropDown.length) {
                    nextItem = alreadySelectedDropDown[parseInt(alrSelDropDwn) + 1].label;
                    nextItemObjName = nextItem.charAt(0).toUpperCase() + nextItem.slice(1);
                }
            }
        });

        if (comboboxName == "grouping") {
            this.classifyOptionsValue = ''; this.specialOptionsValue = '';
        }

        if (typeof nextItem != "undefined") {
            let searchStringForBackend;
            if (nextItem == "classification") { 
                this.classificationOptionsReadonly = false; this.specializationOptionsReadonly = true; this.specialOptionsValue = ''; 
                searchStringForBackend = this.groupOptionsValue;
            }

            if (nextItem == "specialization") {
                this.specializationOptionsReadonly = false; 
                searchStringForBackend = this.groupOptionsValue + ' | ' + this.classifyOptionsValue + ' | ' + this.specialOptionsValue; 
            }

            this.getSpecialityData(baseNamespaceLbl + 'Speciality__c', baseNamespaceLbl+nextItem.charAt(0).toUpperCase() + nextItem.slice(1) + '__c', searchStringForBackend, nextItem + 'Options');
        }

        if (this.isProvSpecView || this.isTerritoryView) {
            this.loadSpecialities(comboboxSelLabel, '0');
        }

        this.finalSelectedSpecString = this.groupOptionsValue + ' | ' + this.classifyOptionsValue + ' | ' + this.specialOptionsValue;
    }

    getSpecialityData(thisObjName, thisFieldName, thisauraAttribFld, fieldAuraName) {
        getSpecialityLookup({
            objName: thisObjName,
            fieldName: thisFieldName,
            groupingValue: thisauraAttribFld
        })
        .then(result => {
            if (result != null) {
                let pickListArr = [];
                for (var i=0;i<result.length;i++) {
                    let pickListArrObj = new Object();
                    if (result.fieldLabel != 'undefined') {
                        pickListArrObj["label"] = result[i].fieldLabel;
                        pickListArrObj["value"] = result[i].fieldLabel;
                    }

                    pickListArr.push(pickListArrObj);
                }

                pickListArr.sort((a, b) => (a.label > b.label) ? 1 : -1);
                pickListArr = pickListArr.reduce((acc, current) => {
                    const x = acc.find(item => item.label === current.label);
                    if (!x) {
                        return acc.concat([current]);
                    } else {
                        return acc;
                    }
                }, []);

                if (fieldAuraName == "classificationOptions") { this.classificationOptions = pickListArr; } 
                else if (fieldAuraName == "specializationOptions") {
                    this.specializationOptions = pickListArr;
                    if (this.specializationOptions.length == 0 && this.groupOptionsValue.length > 0) {
                        this.specializationOptionsReadonly = 'true';
                        this.template.querySelector("lightning-combobox[data-id=specialization]").placeholder = 'N/A';
                    }  else {
                        this.template.querySelector("lightning-combobox[data-id=specialization]").placeholder = '-None-';
                    }
                }
                else if (fieldAuraName == "groupingOptions") { this.groupingOptions = pickListArr; }

                if (this.isViewMode ) {
                    this.getContactSpecData();
                }
            }
        })
        .catch(error => {
            console.log('error*****', error);
        });
    }

    handleSave() {
        this.validateClassificationValue();
        let groupingOptVal = this.finalSelectedSpecString;
        if (groupingOptVal != null && this.recordId != null) {
            saveProviderSpeciality({
                currSObjRecId: this.recordId,
                specialityId: groupingOptVal
            })
            .then(result => {
                if (result != null) {
                    if (result.isSuccess) {
                        this.showToast = true;
                        window.setTimeout(function(){
                            window.location.reload();
                            return false;
                        }, 5000);
                    }
                }
            })
            .catch(error => {
                console.log('error*****', error);
            });
        }
    }

    onSearchBoxtype(event) {
        this.selectedSpecFilterType = event.target.value;
        if (event.target.value != '' && typeof event.target.value != "undefined") {
            this.loadSpecialities(event.target.value, '0');
        } else {
            this.loadSpecialities('none', '100');
        }
    }

    loadSpecialities(filterByTxt, queryLimit) {
        let groupVal = this.groupOptionsValue;
        let classifyVal = this.classifyOptionsValue;
        let specVal = this.specialOptionsValue;
        getSpecialityDetails({
            filterBy: filterByTxt,
            queryLimit: queryLimit
        })
        .then(loadSpecResult => {
            if (loadSpecResult != null) {
                let specResultDataResults = JSON.parse(JSON.stringify(loadSpecResult));

                if (groupVal.length > 0) {
                    specResultDataResults = specResultDataResults.filter(function(item) {
                        if (groupVal.length > 0 && classifyVal.length == 0 && specVal.length == 0) {
                            return item[baseNamespaceLbl+'Grouping__c'] === groupVal;
                        } else if (groupVal.length > 0 && classifyVal.length > 0 && specVal.length == 0) {
                            return (item[baseNamespaceLbl+'Grouping__c'] === groupVal && item[baseNamespaceLbl+'Classification__c'] === classifyVal);
                        } else if (groupVal.length > 0 && classifyVal.length > 0 && specVal.length > 0) {
                            return (item[baseNamespaceLbl+'Grouping__c'] === groupVal && 
                                    item[baseNamespaceLbl+'Classification__c'] === classifyVal &&
                                    item[baseNamespaceLbl+'Specialization__c'] === specVal);
                        }
                    });
                }

                this.specialityData = specResultDataResults;
            }
        })
        .catch(error => {
            console.log('error*****', error);
        });
    }

    // Method to validate classificaion combox required value 
    validateClassificationValue() {
        let classficationComboxCmp = this.template.querySelector("lightning-combobox[data-id=classification]");
        let value = classficationComboxCmp.value;
        if (value === "") {
            classficationComboxCmp.setCustomValidity("Classification is required.");
        } else {
            classficationComboxCmp.setCustomValidity("");
        }
        
        classficationComboxCmp.reportValidity();
    }
}