import { LightningElement, api } from 'lwc';

export default class ConfirmModalComp extends LightningElement {
    @api title;
    @api message = '';
    pgEventObj={};

    handleCancel(event){
        this.pgEventObj.response=false;
        // Creates the event with the selected item data.
        const confirmCancelEvent = new CustomEvent('confirmmodalcallback', { detail: this.pgEventObj });
        
        // Dispatches the event.
        this.dispatchEvent(confirmCancelEvent);
    }

    handleRemove(event){
        this.pgEventObj.response=true;
        // Creates the event with the selected item data.
        const confirmCancelEvent = new CustomEvent('confirmmodalcallback', { detail: this.pgEventObj });

        // Dispatches the event.
        this.dispatchEvent(confirmCancelEvent);
    }
}