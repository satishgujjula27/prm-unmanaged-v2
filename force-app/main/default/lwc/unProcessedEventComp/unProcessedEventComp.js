import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getEvents from '@salesforce/apex/UnProcessedEventController.getEvents';
import removeEvents from '@salesforce/apex/UnProcessedEventController.removeEvents';

export default class UnProcessedEventComp extends LightningElement {
    @track data;
    @track columns;
    @track spinner = false;
    @track showRemoveConfirm = false;
    @track showProcessModal = false;
    @track selectedRows;
    @track eventTimeframe = 'Past';
    @track eventIdToProcess;
    @track timeframeOpt = [
        {'label': 'Past', 'value': 'Past'},
        {'label': 'Current', 'value': 'Current'},
        {'label': 'Future', 'value': 'Future'}
        ];
        @track isProcessDisabled = false;
    
        
    connectedCallback() {
        this.getUnprocessedEvents();
    }
    handleProcess(event){
        if(this.selectedRows!=null && this.selectedRows.length > 0){
            if(this.selectedRows.length > 1){
                this.showMessage('Error', 'Please select only 1 event to process', 'error');
            }
            else{
                    this.showProcessModal = true;
                    this.eventIdToProcess = this.selectedRows[0];
            }

        }
        else
        this.showMessage('Error', 'You must select an event to process', 'error');
    }
    
    handleRemove(event){
            if(this.selectedRows!=null && this.selectedRows.length > 0){
                this.showRemoveConfirm = true;
            }
            else
            this.showMessage('Error', 'Please select events to remove.', 'error');
    }

    handleRowSelection(event){
        var selRows = this.template.querySelector('lightning-datatable').getSelectedRows();
        var allSelectedRows = [];   
        selRows.forEach(function(row) {
            if(!allSelectedRows.includes(row.Id))
                allSelectedRows.push(row.Id);
        });
        if(allSelectedRows.length >1){
            this.isProcessDisabled = true;
        }
        else
            this.isProcessDisabled = false;
        this.selectedRows = allSelectedRows;
    }

    handleConfirmCallBack(event){
        console.log('in confirm call back');
        this.showRemoveConfirm = false;
        console.log(event.detail.response);
        if(event.detail.response == true){
            this.removeUnprocessedEvent();
        }
    }

    handleCloseProcessModal(event){
        this.showProcessModal = false;
        if(event.detail.isRefresh == true){
            this.getUnprocessedEvents();
        }
    }

    handleTimeframeChange(event){
        this.eventTimeframe = event.detail.value;
        this.getUnprocessedEvents();
    }

    getUnprocessedEvents() {
        this.spinner = true;
        getEvents({
            eventTimeframe : this.eventTimeframe
        })
        .then(result => {
            var result = JSON.parse(result);
            if (result != null) {
               if(result.fieldsWrapper != null){
                var columns=[];
                for(var i=0;i<result.fieldsWrapper.length;i++){
                    columns.push({label: result.fieldsWrapper[i].label, fieldName: result.fieldsWrapper[i].fieldName, type: 'text'});
                }
                this.columns = columns;
                this.data = result.ew;
               }

            }
            this.spinner = false;
        })
            .catch(error => {
                this.error = error;
                this.spinner = false;
            });
        
    }

    removeUnprocessedEvent(){
        this.spinner = true;
        removeEvents({
            eventIds : this.selectedRows
        })
        .then(result => {
           this.getUnprocessedEvents();            
        })
            .catch(error => {
                this.error = error;
                this.spinner = false;
            });
    }

    showMessage(messageTitle,messageBody,messageType){
        const event = new ShowToastEvent({
            title: messageTitle,
            message: messageBody,
            variant : messageType
        });
        this.dispatchEvent(event);
    }
}