import { LightningElement, track, api } from 'lwc';
import searchDB from '@salesforce/apex/LookupController.searchDB';
import pubsub from 'c/pubSub' ;

export default class LookupComp extends LightningElement {
    @api selItem;
    @track serverResult;
    @api lookupIcon = 'standard:contact'
    @api objectName;
    @api fieldAPIText;
    @api fieldAPIVal;
    @api fieldAPISearch;
    @api limit = 5;
    @api placeholder;
    @api searchtype = 'partial';
    lastServerResult;
    SAItemsList;
    @api lookupFieldName;
    lastSearchText = '';
    @api showSubText;

    connectedCallback(){
        this.regiser();
    }
    regiser(){
        pubsub.register('simplevt', this.handleEvent.bind(this));
    }
    handleEvent(messageFromEvt){
        this.message = messageFromEvt ? messageFromEvt : 'no message payload';
        if(this.message == this.lookupFieldName){
            this.selItem = {};
        }
    }

    get isEmptySelItem() {
        if (Object.keys(this.selItem).length == 0) {
            return true;
        } else {
            return false;
        }

    }
    get greaterthanorequal() {
        if (this.serverResult)
            return this.serverResult.length >= 1;
        else
            return false;
    }

    hideValues(event) {
        event.target.value = '';
        var that = this;
        setTimeout(function () {
            that.serverResult = [];
        }, 500);
    }

    itemSelected(event) {
        var target = event.target;
        var SelIndex = this.getIndexFrmParent(target, "data-selected-index");
        if (SelIndex) {
            var serverResult = this.serverResult;
            var selItem = serverResult[SelIndex];
            if (selItem.val) {
                this.selItem = selItem;
                this.lastServerResult = serverResult;
                this.fireSelBackEvent();
            }
            this.serverResult = null;
        }
    }

    clearSelection() {
        this.selItem = {};
        this.serverResult = {};
        this.fireSelBackEvent();
    }

    serverCall(event) {
        var target = event.target;
        var searchText = target.value;
        var last_SearchText = this.lastSearchText;
        //Escape button pressed 
        if (event.keyCode == 27 || !searchText.trim()) {
            this.clearSelection();
        } else if (searchText.trim() != last_SearchText) {
            //Save server call, if last text not changed
            //Search only when space character entered
            var objectName = this.objectName;
            var fieldAPItext = this.fieldAPIText;
            var fieldAPIval = this.fieldAPIVal;
            var fieldAPIsearch = this.fieldAPISearch;
            var limit = this.limit;
            searchDB({
                objectName: objectName,
                fldAPIText: fieldAPItext,
                fldAPIVal: fieldAPIval,
                lim: limit,
                fldAPISearch: fieldAPIsearch,
                searchText: searchText,
                searchType: this.searchtype
            })
                .then(result => {
                    var retObj = JSON.parse(result);
                    if (retObj.length <= 0) {
                        var noResult = JSON.parse('[{"text":"No Results Found"}]');
                        this.serverResult = noResult;
                        this.lastServerResult = noResult;
                    } else {
                        this.serverResult = retObj;
                        this.lastServerResult = retObj;
                        if( this.objectName == "User" ) {
                            this.lookupIcon = "standard:user";
                            this.showSubText = false;
                        } else {
                            this.showSubText = true;
                        }
                    }
                })
                .catch(error => {
                    alert(JSON.stringify(error));
                });
            this.last_SearchText = searchText.trim();
        } else if (searchText && last_SearchText && searchText.trim() == last_SearchText.trim()) {
            this.serverResult = this.lastServerResult;
        }
    }
    getIndexFrmParent(target, attributeToFind) {
        //User can click on any child element, so traverse till intended parent found
        var SelIndex = target.getAttribute(attributeToFind);
        while (!SelIndex) {
            target = target.parentNode;
            SelIndex = this.getIndexFrmParent(target, attributeToFind);
        }
        return SelIndex;
    }
    fireSelBackEvent() {
        // call the event
        var selbackobj = {};
        selbackobj.selItem = this.selItem;
        selbackobj.lookupFieldName = this.lookupFieldName;
        // set the Selected sObject Record to the event attribute.  

        // Creates the event with the selected item data.
        const lookupselectedEvent = new CustomEvent('lookupselected', { detail: selbackobj });

        // Dispatches the event.
        this.dispatchEvent(lookupselectedEvent);
    }
}