import { LightningElement, api, track, wire } from 'lwc';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getPlRecords from '@salesforce/apex/AddToMyTerritoryController.getPlRecords';
import saveTerrPlRec from '@salesforce/apex/AddToMyTerritoryController.saveTerrPlRec';
import { loadStyle } from 'lightning/platformResourceLoader';
import cssResource from '@salesforce/resourceUrl/AddToMyTerritoryCss';

export default class AddToMyTerritoryComponent extends NavigationMixin(LightningElement) {
    @api recordId ='';
    @api isRenderSalesforce1 = false;
    @track data;
    @track columns;
    @track spinner = false;
    @track providerName;
    @track showTerrPlTable = false;
    @track terrList;
    @track selTerrPlRec;
    @track selTerrPlRecList = [];
    @track newTerrPlRec;
    @track plNewTerrChecked =false;
    @track territoryNameList = [];
    @track activeSections = [];

    @wire(CurrentPageReference)
    currentPageReference; 
    buttonStatefulState = false;

    get recordIdFromState(){
        return this.currentPageReference && this.currentPageReference.state.c__recordId; 
    }

    connectedCallback() {
        //this.recordId = this.recordIdFromState;
        loadStyle(this, cssResource);
        this.getProvLocationDataWithTerritory();
    }

    handleAddToMyTerrEvent(event) {
        let terrplObj = event.detail.terrplObj;
        let t = [];
        let selTerrPlRecList = this.selTerrPlRecList;
        let canPush = true;
        if(selTerrPlRecList.length > 0){
            for(let i in selTerrPlRecList){
                if(selTerrPlRecList[i].terrId == terrplObj.terrId){
                    canPush = false;
                    t.push(terrplObj);
                } else {
                    t.push(selTerrPlRecList[i]);
                }
            }
        }
        if(canPush == true) {
            t.push(terrplObj);
        }
        this.selTerrPlRecList = t;
    }

    HideShowTerrPlTable(event) {
        if(this.showTerrPlTable == true){
            this.showTerrPlTable = false;
        } else {
            this.showTerrPlTable = true;
        }
        this.buttonStatefulState = !this.buttonStatefulState;
    }

    handlePltSelection(event) {
        let selRows = this.template.querySelector('lightning-datatable').getSelectedRows();
        let plIds = [];
        let terrplObj = {};
        selRows.forEach(function(row) {
            plIds.push(row.Id);
        });
        
        terrplObj['selPlIds'] = plIds;
        terrplObj['newPlIds'] = plIds;
        terrplObj['removedPlIds'] = [];
        terrplObj['isNewTerritory'] = true;
        this.newTerrPlRec = terrplObj;
    }

    handleSave(event) {
        this.spinner = true;
        let isUniqueTerr = false;
        if(this.template.querySelector('[data-id="territoryName"]')!=null && this.template.querySelector('[data-id="territoryName"]').value !=null && this.newTerrPlRec != undefined){
            let newTerrPlObj = this.newTerrPlRec;
            newTerrPlObj['terrId'] = this.template.querySelector('[data-id="territoryName"]').value;
            for(let i=0;i<this.territoryNameList.length;i++){
                if(this.territoryNameList[i].toLowerCase() == newTerrPlObj['terrId'].toLowerCase()){
                    isUniqueTerr = true;
                    break;
                }
            }
            if(isUniqueTerr == false){
                let selTerrPlRecList = this.selTerrPlRecList;
                selTerrPlRecList.push(newTerrPlObj);
                this.selTerrPlRecList = selTerrPlRecList;
            }
        }
        if(isUniqueTerr == false){
        saveTerrPlRec({
            terrPLRec: JSON.stringify(this.selTerrPlRecList)
        })
        .then(result => {
            let parsedResult = JSON.parse(result);
            if(parsedResult != null){
                let msg = 'Created '+parsedResult.created+' removed '+parsedResult.removed+' territory association';
                this.showMessage('Success!', msg, 'success');
                this.showTerrPlTable = false;
                this.selTerrPlRecList = [];
                this.getProvLocationDataWithTerritory();
                setTimeout(() => {
                    this.navigateToProviderPage();
                }, 5000);
            }
            this.spinner = false;
        })
            .catch(error => {
                this.error = error;
                this.spinner = false;
            });
        } else {
            this.spinner = false;
            this.showMessage('Error', 'Territory Name: Must be unique', 'error');
        }
    }

    handleCloseModal(event){
        if (this.isRenderSalesforce1 == true) {
            this.navigateToProviderPage();
        } else {
            const selectedEvent = new CustomEvent('closemodal');
            // Dispatches the event.
            this.dispatchEvent(selectedEvent);
        }
    }
    
    navigateToProviderPage() {
       this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.recordId,
                objectApiName: 'Contact',
                actionName: 'view'
            },
        });
    }

    getProvLocationDataWithTerritory() {
        this.spinner = true;
        getPlRecords({
            recordId: this.recordId
        })
        .then(result => {
            let provLocDataResult = JSON.parse(result);
            if (provLocDataResult != null) {
               if(provLocDataResult.fieldsWrapper != null){
                    let columns=[];
                    for(let i=0;i<provLocDataResult.fieldsWrapper.length;i++) {
                        if(provLocDataResult.fieldsWrapper[i].fieldName == 'Primary') {
                            let btnCol = {type: 'button-icon', label : 'PRIMARY', typeAttributes: {name: 'primary', iconName: {fieldName: 'iconName'}, disabled: {fieldName: 'disabledValue'}, alternativeText: {fieldName: 'alternativeTextValue'}}};
                            columns.push(btnCol);
                        } else {
                            let col = {label: provLocDataResult.fieldsWrapper[i].label, fieldName: provLocDataResult.fieldsWrapper[i].linkfieldName, type: provLocDataResult.fieldsWrapper[i].fieldType,
                                typeAttributes: {
                                    label: { fieldName: provLocDataResult.fieldsWrapper[i].fieldName },
                                    target: '_blank'
                                }
                            };
                            columns.push(col);
                        }
                    }
                    this.columns = columns;
                    this.data = provLocDataResult.provLocWrapper;
                    this.totalRecords = provLocDataResult.totalRec;
                    this.setPrimary();
                    this.terrList = provLocDataResult.territoryList;
                    for(let i=0;i<provLocDataResult.territoryList.length;i++){
                        this.territoryNameList.push(provLocDataResult.territoryList[i].TerritoryName);
                    }
                    this.providerName = provLocDataResult.providerName;
               }
            }
            this.spinner = false;
        })
        .catch(error => {
            this.error = error;
            this.spinner = false;
        });
    }

    setPrimary(){
        let data = this.data;
        data = data.map(function(rowData) {
            if(rowData.isPrimary == true) {
                rowData.iconName = 'utility:check';
                rowData.alternativeTextValue = 'Checked';
            } else {
                rowData.alternativeTextValue = 'Not Checked';
            }
            rowData.disabledValue = true;
            return rowData;
        });
        this.data = data;
    }

    showMessage(messageTitle,messageBody,messageType){
        const event = new ShowToastEvent({
            title: messageTitle,
            message: messageBody,
            variant : messageType
        });
        this.dispatchEvent(event);
    }
}