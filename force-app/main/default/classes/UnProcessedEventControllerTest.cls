/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying 
* or sale of the above may constitute an infringement of copyright 
* and may result in criminal or other legal proceedings.
*
* @Class Name: UnProcessedEventControllerTest
* @description: This is the test class UnProcessedEventController
* @Last Modified Date: 06/10/2020
**/
@isTest
public class UnProcessedEventControllerTest {
    @testSetup 
    static void setup() {
        Account practice= new Account();
        practice.put('Name', 'Test');
        insert practice;
        
        Sobject Location= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Location__c').newSObject();
        Location.put('Name', 'Location 1');
        Location.put(NamespaceSelector.baseNamespace+'PracticeGroup__c', practice.Id);
        insert Location;
        
        Contact Provider= new Contact();
        Provider.put('FirstName', 'Test');
        Provider.put('LastName', 'Test');
        insert Provider;
        
        
        Sobject PractitionerLocation1= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocation__c').newSObject();
        PractitionerLocation1.put('Name', 'PLTest');
        PractitionerLocation1.put(NamespaceSelector.baseNamespace+'Location__c', Location.Id);
        PractitionerLocation1.put(NamespaceSelector.baseNamespace+'Provider__c', Provider.Id);
        insert PractitionerLocation1;
        
        Sobject territory= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Territory__c').newSObject();
        territory.put('Name', 'Test Territory');
        insert territory;
        
        Sobject pltObj= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c').newSObject();
        pltObj.put(NamespaceSelector.baseNamespace+'Territory__c', territory.Id);
        pltObj.put(NamespaceSelector.baseNamespace+'PractitionerLocation__c', PractitionerLocation1.Id);
        insert pltObj;
        
        Event event= new Event();
        event.put('Subject', 'Test Event');
        event.put('WhoId', Provider.Id);
        event.put('WhatId', practice.Id);
        event.put('Description', 'Test Event');
        event.put('StartDateTime', System.now().addDays(1));
        event.put('EndDateTime', System.now().addDays(2));
        event.put('Location', 'Test Location');
        insert event;
        
    }
    
    @isTest
    static void testGetEvents(){
        Map<string,object> gtEventObj = (Map<string,Object>)JSON.deserializeUntyped(UnProcessedEventController.getEvents('Future'));
        system.assertNotEquals(null, gtEventObj.get('ew'));
    }
    
    @isTest
    static void testSaveTask(){
         // Create Task
        Task testTask = new Task ();
        testTask.Subject = 'Test Task';
        testTask.Description = 'Test Description';
        Task testFollowuptask = testTask.clone(false, true, false, false);
        testFollowuptask.status = 'Open';
 		testFollowuptask.Priority = 'High';     
        testFollowuptask.Description = 'Test Followup Description';
        string plName = 'PLTest';
        List<Sobject> plObjs = Database.query('SELECT Id FROM '+NamespaceSelector.baseNamespace+'PractitionerLocation__c LIMIT 1');
        Event ev = [SELECT Id From Event LIMIT 1];
        System.assertNotEquals('',UnProcessedEventController.saveTask(testTask, testFollowuptask, true, true, new List<string>{plObjs[0].Id}, ev.Id, true));
    }
    
    @isTest
    static void testGetEventInfo(){
        Event ev = [SELECT Id From Event LIMIT 1];
        system.assertNotEquals(null, JSON.deserializeUntyped(UnProcessedEventController.getEventInfo(ev.Id)));
    }
}