/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: RestPRM3CallMonthCountControllerTest
* @description: This is the test class for RestPRM3CallMonthCountController
* @Last Modified Date: 04/08/2020
**/
@isTest
public class RestPRM3CallMonthCountControllerTest {

    /**@@
    #DESCRIPTION   : This method is used to create Test records.
    #Paramaters    : None
    @@**/
    @testSetup static void createTestTaskdata() {
        // Create Test Contact
        Contact testContact = new Contact ();
        testContact.LastName = 'Test';
        insert testContact;

        // Create Test Task
        Task testTask = new Task ();
        testTask.Subject = 'Call';
        testTask.Description = 'Test Description';
        testTask.TaskSubtype = 'Call';
        testTask.Status = 'Open';
        Contact lstObjects = [SELECT Id, Name FROM Contact WHERE Id != null LIMIT 1];
        testTask.WhoId = lstObjects.Id;
        testTask.RecordTypeId = [SELECT Id, DeveloperName, SobjectType FROM RecordType WHERE DeveloperName = 'PRM' AND SobjectType = 'Task'].Id;
        insert testTask;
    }

    @isTest
    static void doGetTest(){
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/prm3/v1/Calls/monthlyCall';
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json');
        RestContext.request = req;
        RestContext.response = res;
        List<PRM3uRestController.Result> result = RestPRM3CallMonthCountController.doGet();
        Test.stopTest();
        System.assertEquals(1, result.size(),'');
    }
}