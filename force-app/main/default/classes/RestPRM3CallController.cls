/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: RestPRM3CallController
* @description: This class is to expose the Recent Logged Calls.
* @Author: Healthgrades Inc.
* @Last Modified Date: 10/04/2020
**/
@RestResource(urlMapping='/prm3/v1/Calls/myCalls')
global inherited sharing class RestPRM3CallController {

	/********************************
    * @method: doGet
    * @description: get method for Api
    * @return: List<PRM3uRestController.Activity>
    * *****************************/
    @HttpGet
    global static List<PRM3uRestController.Activity> doGet() {
        try {
            RestRequest request = RestContext.request;
            RestResponse response = RestContext.response;
            response.statusCode = 200;
            response.addHeader('Content-Type', 'application/json');
            Integer recordLimit = PRM3uRestController.maxRecordsParam(request);
            return performGetSearch(recordLimit);
        } catch(Exception RestPRM3CallCtrlEx) {
            CoreControllerException errLogException = new CoreControllerException('RestPRM3CallController.doGet', 
                                                                                          'Could not get activity records ==> '+RestPRM3CallCtrlEx.getStackTraceString());
            return null;
        }
    }

    /********************************
    * @method: performGetSearch
    * @description: performing search to fetch Recent Call
    * @return: List
    * *****************************/
    public static List<PRM3uRestController.Activity> performGetSearch(Integer recordLimit) {
        List<String> conditions = new List<String>();
        conditions.add('TaskSubtype = \'Call\'');
        List<PRM3uRestController.Activity> response = new List<PRM3uRestController.Activity>();
        if( Schema.sObjectType.Task.isAccessible() ) {
            List<Task> result = (List<Task>)Database.query(PRM3uRestController.generateCallSql(conditions, recordLimit));
            for(Task a : result) { response.add(new PRM3uRestController.Activity(a)); }
            return response;
        } else {
            CoreControllerException errLogException = new CoreControllerException('RestPRM3CallController.performGetSearch', 
                                                                                          'User does not have access to Task Object ');
            return null;
        }
    }
}