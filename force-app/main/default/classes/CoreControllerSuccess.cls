global with sharing class CoreControllerSuccess {
  private static final String CLASS_NAME = 'CoreControllerSuccess';
  //private final static String NP = Utility.getNamespacePrefix(false);
  /**
   * Creates a success log entry asynchronously
   * @param  className  Name of the class from where we are firing
   * @param  message    Success message about the operation
   */
  global CoreControllerSuccess(String className, String message) {
    createLog(
      className,
      message,
      '',
      LoggingLevel.INFO
    );
  }

  /**
   * Creates a success log entry asynchronously
   * @param  className  Name of the class from where we are firing
   * @param  message    Success message about the operation
   * @param  detail     Detailed message about the operation
   */
  global CoreControllerSuccess(
    String className,
    String message,
    String detail
  ) {
    createLog(
      className,
      message,
      detail,
      LoggingLevel.INFO
    );
  }

  /**
   * Creates a success log entry asynchronously
   * @param  className  Name of the class from where we are firing
   * @param  message    Success message about the operation
   * @param  detail     Detailed message about the operation
   * @param  logLevel   The custom LoggingLevel for this entry
   */
  global CoreControllerSuccess(
    String className,
    String message,
    String detail,
    LoggingLevel logLevel
  ) {
    createLog(
      className,
      message,
      detail,
      logLevel
    );
  }

  /**
   * Creates a success log entry asynchronously
   * @param  className  Name of the class from where we are firing
   * @param  message    Success message about the operation
   * @param  detail     Detailed message about the operation
   * @param  logLevel   The custom LoggingLevel for this entry
   * @param  statusCode The custom StatusCode for this entry (ErrorLogger.StatusCode enum)
   */
  // Deprecated
  global CoreControllerSuccess(
    String className,
    String message,
    String detail,
    LoggingLevel logLevel,
    ErrorLogger.StatusCode statusCode
  ) {
    createLog(className, message, detail, logLevel);
  }

  /**
   * Creates a success log entry asynchronously
   * @param  className  Name of the class from where we are firing
   * @param  message    Success message about the operation
   * @param  obj        Object containing status information
   */
  global CoreControllerSuccess(String className, String message, Object obj) {
    createLog(
      className,
      message,
      String.valueOf(obj),
      LoggingLevel.INFO
      
    );
  }

  private static void createLog(
    String className,
    String message,
    String detail,
    LoggingLevel logLevel
   
  ) {
    System.debug(
      'Log: ' +
      className +
      ': ' +
      ' ' +
      logLevel +
      ' ' +
      message +
      '\n' +
      detail
    );
  }
}