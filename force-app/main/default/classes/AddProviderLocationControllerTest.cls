/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: AddProviderLocationControllerTest
* @description: This is the test class for AddProviderLocationController
* @Last Modified Date: 07/10/2020
**/
@isTest
public class AddProviderLocationControllerTest {
	@isTest
    public static void testBehavior(){
        
        Account practice = new Account();
        practice.put('Name','Practice 1');
        insert practice;
        
		sObject locationObj = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Location__c').newSObject();
		locationObj.put('Name','Location 1'); 
        locationObj.put(NamespaceSelector.baseNamespace+'PracticeGroup__c',practice.Id);
        insert locationObj;

        Contact provider = new Contact();
        provider.put('LastName','Test');
        insert provider;
		
		sObject provPractObj = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'ProviderPracticeGroup__c').newSObject();
        provPractObj.put(NamespaceSelector.baseNamespace+'PracticeGroup__c', practice.Id);
        provPractObj.put(NamespaceSelector.baseNamespace+'Provider__c', provider.Id);
        insert provPractObj;
       	
        //System.assertNotEquals(null,AddProviderLocationController.getLocations(provider.Id, '50', '1'));
        
        System.assertEquals(true, AddProviderLocationController.saveProviderLocation(provider.Id, new List<String>{locationObj.id}, locationObj.id));
        
        System.assertEquals(true, AddProviderLocationController.saveProviderPractice(provider.Id, new Map<String,string>{'PracticeGroup'=>practice.Id}));
    }
}