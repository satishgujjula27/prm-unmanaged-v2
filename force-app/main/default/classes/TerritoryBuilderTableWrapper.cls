/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: TerritoryBuilderTableWrapper
* @description: This Class is used handle generic the table wrapper
* @Last Modified Date: 10/05/2020
**/
public inherited sharing class TerritoryBuilderTableWrapper {
    public static List<Sobject> lstObjMap;
    
    /********************************
    * @method: getTerrTableMeta
    * @description: get Datatable meta Records for page type and filterBy
    * Param: String, String
    * Return type: String
    * *****************************/
    public static List<SObject> getTerrTableMeta(String pageType, String filterBy){
        try{
            List<String> fields = new List<String>{
                'Id','DeveloperName','MasterLabel',
				NamespaceSelector.hgprmNamespace+'DataType__c',
				NamespaceSelector.hgprmNamespace+'FieldName__c',
				NamespaceSelector.hgprmNamespace+'Ordering__c',
				NamespaceSelector.hgprmNamespace+'FilterBy__c',
				NamespaceSelector.hgprmNamespace+'ExpandDbFieldName__c'
			};
			String metaQuery = 'SELECT '+String.join(fields, ', ')+' FROM '+NamespaceSelector.hgprmNamespace+'DatatableResultColumns__mdt WHERE Id != null AND '+
                				NamespaceSelector.hgprmNamespace+'Active__c = true AND '+NamespaceSelector.hgprmNamespace+'PageType__c= :pageType';
            if(!String.isBlank(filterBy)){
                metaQuery += ' AND '+NamespaceSelector.hgprmNamespace+'FilterBy__c IN (SELECT Id FROM '+NamespaceSelector.hgprmNamespace+'SearchBy__mdt WHERE Id != null AND '+NamespaceSelector.hgprmNamespace+'SearchByType__c = :filterBy)';
            }
            metaQuery += ' ORDER BY '+NamespaceSelector.hgprmNamespace+'Ordering__c';
            lstObjMap = Database.query(metaQuery);
            return lstObjMap;
        } catch(Exception getTerrTbMetaEx) {
            CoreControllerException methodException = new CoreControllerException('TerritoryBuilderTableWrapper', 
                                                                                          'Exception occured while getTerrTableMeta'+getTerrTbMetaEx.getStackTraceString());
            return new List<SObject>();
        }
    }
    
    /********************************
    * @method: getQueryFields
    * @description: get Query fields from datatable meta to fetch records
    * Param: none
    * Return type: List
    * *****************************/
    public static List<String> getQueryFields() {
        List<String> queryFields = new List<String>();
        for(SObject metaObj:lstObjMap){
            if(metaObj.get(NamespaceSelector.hgprmNamespace+'FieldName__c') != null) { queryFields.add((String)metaObj.get(NamespaceSelector.hgprmNamespace+'FieldName__c')); }
        }
        if(!queryFields.contains('Id') && !queryFields.contains('ID')) { queryFields.add('Id'); }
        return queryFields;
    }
    
    /********************************
    * @method: getExpandQueryFields
    * @description: get Expand Query fields from datatable meta to fetch records
    * Param: none
    * Return type: List
    * *****************************/
    public static List<String> getExpandQueryFields() {
        List<String> queryFields = new List<String>();
        for(Sobject metaObj:lstObjMap) { queryFields.add((String)metaObj.get(NamespaceSelector.hgprmNamespace+'ExpandDbFieldName__c')); }
        if(!queryFields.contains('Id') && !queryFields.contains('ID')) { queryFields.add('Id'); }
        return queryFields;
    }
    
    /********************************
    * @method: setMapResult
    * @description: set Datatable result into Map
    * Param: Sobject
    * Return type: Map
    * *****************************/
    public static Map<String, Object> setMapResult(SObject sObj){
        Map<String, Object> resMap = new Map<String, Object>();
        for(SObject metaObj:lstObjMap){
            if(metaObj.get(NamespaceSelector.hgprmNamespace+'FieldName__c') != null){
                String dbField = (String)metaObj.get(NamespaceSelector.hgprmNamespace+'FieldName__c');
                List<String> dbFieldList = dbField.split('\\.');
                if(dbFieldList.size() > 0) {
                    SObject relSobj = sObj;
                    Boolean breakLoop = false;
                    for(String field:dbFieldList) {
                        if(field.endsWith('__r')) {
                            if(relSobj.getSObject(field) != null) { relSobj = relSobj.getSObject(field); } else { breakLoop = true; }
                        } else {
                            if(field.contains('MailingAddress')) {
                                String address = (String)relSobj.get(field);
                                resMap.put((String)metaObj.get('DeveloperName'), address.replace('<br>',''));                            
                            } else if((String)metaObj.get(NamespaceSelector.hgprmNamespace+'DataType__c') == 'DATETIME') {
                                DateTime createdDt = (DateTime)relSobj.get(field);
                                resMap.put((String)metaObj.get('DeveloperName'), createdDt.format('M/d/yyyy h:mm a'));
                            } else if((String)metaObj.get(NamespaceSelector.hgprmNamespace+'DataType__c') == 'BOOLEAN') {
                                resMap.put((String)metaObj.get('DeveloperName'), (Boolean)relSobj.get(field));
                            } else {
                                resMap.put((String)metaObj.get('DeveloperName'),(String)relSobj.get(field));
                                String fieldNameLink = (String)metaObj.get('DeveloperName');
                                if((String)metaObj.get(NamespaceSelector.hgprmNamespace+'DataType__c') == 'URL'){
                                    fieldNameLink = fieldNameLink+'link';
                                    resMap.put(fieldNameLink,'/'+(String)relSobj.get('Id'));
                                }   
                            }
                        }
                        if(breakLoop) { break; }
                    } 
                }
            }
        }
        resMap.put('Id',(string)sObj.get('Id'));
        return resMap;
    }
    
    /********************************
    * @method: setExpandMapResult
    * @description: set Expand Field Datatable result into Map
    * Param: Sobject
    * Return type: Map
    * *****************************/
    public static Map<String, Object> setExpandMapResult(Sobject sObj){
        Map<String, Object> resMap = new Map<String, Object>();
        for(Sobject metaObj:lstObjMap){
            String dbField = (String)metaObj.get(NamespaceSelector.hgprmNamespace+'ExpandDbFieldName__c');
            List<String> dbFieldList = dbField.split('\\.');
            if(dbFieldList.size()>0){
                Sobject relSobj = sObj;
                Boolean breakLoop = false;
                for(String field:dbFieldList) {
                    if(field.endsWith('__r')) {
                        if(relSobj.getSObject(field) != null){
                            relSobj = relSobj.getSObject(field);
                        } else {
                            breakLoop = true;
                        }
                    } else {
                        if((String)metaObj.get(NamespaceSelector.hgprmNamespace+'DataType__c') == 'BOOLEAN') {
                            resMap.put((String)metaObj.get('DeveloperName'), (Boolean)relSobj.get(field));
                        } else {
                            resMap.put((String)metaObj.get('DeveloperName'),(String)relSobj.get(field));
                            String fieldNameLink = (String)metaObj.get('DeveloperName');
                            if((String)metaObj.get(NamespaceSelector.hgprmNamespace+'DataType__c') == 'url'){
                                fieldNameLink = fieldNameLink+'link';
                                resMap.put(fieldNameLink,'/'+(String)relSobj.get('Id'));
                            }
                        }
                    }
                    if(breakLoop) { break; }
                } 
            }
        }
        resMap.put('Id',(String)sObj.get('Id'));
        return resMap;
    }
    
    /********************************
    * @method: setWrapperFieldSet
    * @description: set fields to show into Datatable
    * Param: none
    * Return type: List
    * *****************************/
     public static List<TerritoryBuilderTableWrapperFldSet> setWrapperFieldSet() {
        List<TerritoryBuilderTableWrapperFldSet> flswrapper = new List<TerritoryBuilderTableWrapperFldSet>();
        for(Sobject metaObj:lstObjMap){
            String fieldNameLink = (String)metaObj.get('DeveloperName');
            if((String)metaObj.get(NamespaceSelector.hgprmNamespace+'DataType__c') == 'URL'){
                fieldNameLink = fieldNameLink+'link';
            }
            String dataType = (String)metaObj.get(NamespaceSelector.hgprmNamespace+'DataType__c');
            dataType = dataType.toLowerCase();
            flswrapper.add(new TerritoryBuilderTableWrapperFldSet((String)metaObj.get('DeveloperName'), (String)metaObj.get('MasterLabel'), dataType, fieldNameLink));
        }
        return flswrapper;
    }
    
    /********************************
    * @method: getDbFieldMeta
    * @description: get Db Field from meta Object
    * Param: String
    * Return type: String
    * *****************************/
    public static String getDbFieldMeta(String fieldName) {
         for(Sobject metaObj:lstObjMap) {
             if((String)metaObj.get(NamespaceSelector.hgprmNamespace+'DataType__c') == 'url') { fieldName = fieldName.removeEnd('link'); }
             if((String)metaObj.get('DeveloperName') == fieldName) { return (String)metaObj.get(NamespaceSelector.hgprmNamespace+'FieldName__c'); }
        }
        return null;
    }
    
    /********************************
    * @Wrapper Class Name: TerritoryBuilderTableWrapperFldSet
    * @description: This class is to hold the Territory Builder Field set
    * @Last Modified Date: 10/05/2020
    * *****************************/
    public class TerritoryBuilderTableWrapperFldSet {
        @AuraEnabled public String fieldName;
        @AuraEnabled public String label;
        @AuraEnabled public String fieldType;
        @AuraEnabled public Boolean sortable;
        @AuraEnabled public String fieldNameLink;
        
        /********************************
        * @Method: TerritoryBuilderTableWrapperFldSet
        * @description: Constructor to set the Class attribute
        * @return: N/A
        * *****************************/
        public TerritoryBuilderTableWrapperFldSet(String fieldName, String label, String fieldType, String fieldNameLink) {
            this.fieldName = fieldName;
            this.label = label;
            this.fieldType = fieldType;
            this.sortable = true;
            this.fieldNameLink = fieldNameLink;
        }
    }

}