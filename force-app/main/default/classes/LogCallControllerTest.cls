/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying 
* or sale of the above may constitute an infringement of copyright 
* and may result in criminal or other legal proceedings.
*
* @Class Name: LogCallControllerTest
* @description: This is the test class LogCallController
* @Last Modified Date: 06/29/2020
**/
@isTest
public class LogCallControllerTest {
    @testSetup 
    static void setup() {
        Account practice= new Account();
        practice.put('Name', 'Test');
        insert practice;
        
        Sobject location= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Location__c').newSObject();
        location.put('Name', 'Location 1');
        location.put(NamespaceSelector.baseNamespace+'PracticeGroup__c', practice.Id);
        insert location;
        
        Contact provider= new Contact();
        provider.put('FirstName', 'Test');
        provider.put('LastName', 'Test');
        insert provider;
        
        Sobject practitionerLocation1= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocation__c').newSObject();
        practitionerLocation1.put('Name', 'PL');
        practitionerLocation1.put(NamespaceSelector.baseNamespace+'Location__c', Location.Id);
        practitionerLocation1.put(NamespaceSelector.baseNamespace+'Provider__c', Provider.Id);
        insert practitionerLocation1;
        
        Sobject territory= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Territory__c').newSObject();
        territory.put('Name', 'Test Territory');
        insert territory;
        
        Sobject pltObj= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c').newSObject();
        pltObj.put(NamespaceSelector.baseNamespace+'Territory__c', territory.Id);
        pltObj.put(NamespaceSelector.baseNamespace+'PractitionerLocation__c', PractitionerLocation1.Id);
        insert pltObj;
    }
    
    @isTest
	static void testBehavior(){
        System.assertEquals(5, LogCallController.getPicklistValues().size(), '');
    }
    
    @isTest
    static void testTerritoryFilter(){
        string terrName  = 'Test Territory';
        List<Sobject> terrObj = database.query('SELECT Id from '+NamespaceSelector.baseNamespace+'Territory__c WHERE Name = \'' + terrName + '\'');
        if(terrObj.size()>0){ 
            System.assertNotEquals(null,LogCallController.getProviders('',false,'territory',(string)terrObj[0].get('Id'),'',''),'');
            System.assertNotEquals(null,LogCallController.getProviders2('',false,'territory',(string)terrObj[0].get('Id'),'',''),'');
        }
    }
    
    @isTest
    static void testPracticeFilter(){
        Account practice = [SELECT Id, Name FROM Account WHERE Id != null LIMIT 1];
        System.assertNotEquals(null,LogCallController.getProviders('',false,'','practice',practice.Id,''),'');
        System.assertNotEquals(null,LogCallController.getProviders2('',false,'','practice',practice.Id,''),'');
    }
    
    @isTest
    static void testSearch(){
        Account practice = [SELECT Id, Name FROM Account WHERE Id != null LIMIT 1];
        System.assertNotEquals(null,LogCallController.getProviders('T',true,'','practice',practice.Id,''),'');
        System.assertNotEquals(null,LogCallController.getProviders2('T',true,'','practice',practice.Id,''),'');
    }

    @isTest
    static void testOtherSearch(){
        Account practice = [SELECT Id, Name FROM Account WHERE Id != null LIMIT 1];
        System.assertNotEquals(null,LogCallController.getProviders('Other',true,'','practice',practice.Id,''),'');
        System.assertNotEquals(null,LogCallController.getProviders2('Other',true,'','practice',practice.Id,''),'');
    }    
    @isTest
    static void testBehaviour2(){
        // Create Test Account
        Contact providerObj = new Contact ();
        providerObj.LastName = 'Test';
        insert providerObj;
        
        // Create Test Account
        Task testTask = new Task ();
        testTask.Subject = 'Call';
        testTask.Description = 'Test Description';
        testTask.Status = 'Open';
        Task testFollowuptask = testTask.clone(false, true, false, false);
        testFollowuptask.Description = 'Test Followup Description';
        System.assertEquals(true, LogCallController.saveTask(testTask,testFollowuptask,true,true,'PRM',new List<String>{providerObj.Id}), '');
    }
    
    @isTest
    static void testBehaviour3(){
        // Create Test Account
        Contact providerObj = new Contact ();
        providerObj.LastName = 'Test';
        insert providerObj;
        
        // Create Test Account
        Contact providerObj1 = new Contact ();
        providerObj1.LastName = 'FollowUpTsk2';
        insert providerObj1;
        
        // Create Test Account
        Contact providerObj2 = new Contact ();
        providerObj2.LastName = 'FollowUpTsk3';
        insert providerObj2;
        
        // Create Test Account
        Task testTask = new Task ();
        testTask.Subject = 'Call';
        testTask.Description = 'Test Description';
        testTask.Status = 'Open';
        testTask.IsReminderSet = true;
        testTask.ReminderDateTime = DateTime.newInstance(2020, 10, 31, 7, 8, 16);
        Task testFollowuptask = testTask.clone(false, true, false, false);
        testFollowuptask.Description = 'Test Followup Description';
        System.assertEquals(true, LogCallController.saveTask(testTask, testFollowuptask,true,true,'PRM',new List<String>{providerObj.Id, providerObj1.Id, providerObj2.Id}), '');
        User currentUsr = [SELECT Id, Name FROM User WHERE Id = :UserInfo.getUserId()];
        System.assertEquals(currentUsr, LogCallController.getLoggedInUser());
        LogCallController.PractitionerLocationWrapper2 pracLocwrapper = new LogCallController.PractitionerLocationWrapper2(providerObj2);
    }
    
    @isTest
    static void testTerritoryFilterOpt(){
        system.assertNotEquals(null, LogCallController.getFilterOptions('territory'),'');
    }
    @isTest
    static void testPracticeFilterOpt(){
        system.assertNotEquals(null, LogCallController.getFilterOptions('practice'),'');
    }
    
    private class WebServiceMockImpl implements WebServiceMock 
    {
      public void doInvoke(
        Object stub, Object request, Map<String, Object> response,
        String endpoint, String soapAction, String requestName,
        String responseNS, String responseName, String responseType) 
      {
        if (request instanceof MetadataService.readMetadata_element) {

            MetadataService.PicklistValue picklistValue = new MetadataService.PicklistValue();
            picklistValue.fullName = 'Pending';

            MetadataService.RecordTypePicklistValue recordTypePicklistValue = new MetadataService.RecordTypePicklistValue();
            recordTypePicklistValue.picklist = 'cve__Status__c';
            recordTypePicklistValue.values = new MetadataService.PicklistValue[] {picklistValue};

            MetadataService.RecordType recordType = new MetadataService.RecordType();
            recordType.fullName = 'cve__BenefitClaimed__c.cve__ShortTermDisability';
            recordType.picklistValues = new MetadataService.RecordTypePicklistValue[] {recordTypePicklistValue};

            MetadataService.ReadRecordTypeResult result = new MetadataService.ReadRecordTypeResult();
            result.records = new MetadataService.RecordType[] {recordType};

            MetadataService.readRecordTypeResponse_element responseElement = new MetadataService.readRecordTypeResponse_element();
            responseElement.result = result;
            response.put('response_x', responseElement);
        }
      }
    }
    
    @isTest
    static void testPicklistValuesByRecordType(){
        Test.startTest();
        String objectName='Task';
        String fieldName='Subject';
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        MetadataService metaDataService = new MetadataService();
        system.assertNotEquals(null, LogCallController.getPicklistValuesByRecordType(objectName,fieldName),'');
        Test.stopTest(); 
    }
}