/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: MultiSelectApexController
* @description: This Class is used for multiselect component to display multiselect picklist of any object data
* @Author: Healthgrades Inc.
* @Last Modified Date: 10/04/2020
**/
public inherited sharing class MultiSelectApexController {
    
    /* **********************
    * @Method: getSelectOptions
    * @description: to get the list of distinct options to show for multiselect 
    * @param: String objectName, String filterField, String searchString, String values
    * @return: JSON wrapper of select option
    * *********************/
    @AuraEnabled
    public static List<SelectOptions> getSelectOptions(String objectName, String filterField, String searchString, String values) {
        List<SelectOptions> recordsDataList = new List<SelectOptions>();
        try {
            SObjectType selectOptionsToken = Schema.getGlobalDescribe().get(objectName);
            if( !Util.checkObjectIsReadable(selectOptionsToken) ) {
                CoreControllerException getSelectOptionsCRUDException = new CoreControllerException('MultiSelectApexController', 
                                                                                            				'Could not access sObject ==> '+objectName);
                return recordsDataList;
            }
            if( ( objectName == 'Account' || objectName == 'Contact' ) && filterField == 'Name' ) {                
                String searchquery = 'FIND \'' + searchString + '*\' IN ALL FIELDS RETURNING '+objectName+'('+filterField+') LIMIT 10';
                List<List<SObject>>searchList = search.query(searchquery);
                List<SObject> filteredList = (List<SObject>)searchList[0];
                for(SObject s : filteredList) {
                    recordsDataList.add( new SelectOptions((String)s.get(filterField), (String)s.get(filterField)) );
                }
                
            } else {
                List<String> selectedvalues = (List<String>) JSON.deserialize(values, List<String>.class);
                String query = 'SELECT ' + filterField + ' FROM '+objectName;
                if(selectedvalues != null && selectedvalues.size() > 0) {
                    query += ' WHERE '+filterField+' IN: selectedvalues ';
                } else {
                    query += ' WHERE '+filterField+ ' LIKE \'%' + String.escapeSingleQuotes(searchString.trim()) + '%\'';
                }
                query += ' GROUP BY '+filterField + ' LIMIT 10';
                
                for(AggregateResult s : Database.query(query)) {
                    recordsDataList.add( new SelectOptions((String)s.get(filterField), (String)s.get(filterField)) );
                }
            }
        } catch (Exception getSelectOptionsErr) {
            CoreControllerException getSelectOptionsException = new CoreControllerException('MultiSelectApexController', 
                                                                                          			'Could not get select options ==> '+getSelectOptionsErr.getStackTraceString());
        }
        return recordsDataList;
    }

    /********************************
    * @method: SelectOptions
    * @description: wrapper for select options
    * @return: wrapper
    * *****************************/
    public class SelectOptions {
        @AuraEnabled public String label;
        @AuraEnabled public String value;
        public SelectOptions(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }
}