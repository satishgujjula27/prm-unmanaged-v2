/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying 
* or sale of the above may constitute an infringement of copyright 
* and may result in criminal or other legal proceedings.
*
* @Class Name: LeafletMapController
* @description: This class is to lightning component which written to show Map.
* @Last Modified Date: 10/04/2020
**/
public inherited sharing class LeafletMapController {

    /********************************
    * @method: updateRecord
    * @description: aura enabled method to update record
    * @return: serialized string of records.
    * *****************************/
	@AuraEnabled
	public static SObject updateRecord(Id saveRecordId, String geoJson, String field) {
        SObject sObjUpd = saveRecordId.getSObjectType().newSObject(saveRecordId);
        sObjUpd.put(field, geoJson);
        SObjectType terrObjToken = saveRecordId.getSObjectType();
        if( Util.checkObjectIsReadable(terrObjToken) && Util.checkObjectIsUpdateable(terrObjToken) ) {
            Database.update(sObjUpd);
        } else {
            CoreControllerException errLogException = new CoreControllerException('LeafletMapController.updateRecord', 
                                                                                          'Could not update record ==> '+sObjUpd);
            return null;
        }
		return sObjUpd;
	}
}