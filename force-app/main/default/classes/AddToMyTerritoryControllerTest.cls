/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying 
* or sale of the above may constitute an infringement of copyright 
* and may result in criminal or other legal proceedings.
*
* @Class Name: AddToMyTerritoryController
* @description: This is the test class AddToMyTerritoryControllerTest
**/
@isTest
public class AddToMyTerritoryControllerTest {
    @testSetup 
    static void setup() {
        Account practice= new Account();
        practice.put('Name', 'Test');
        insert practice;
        
        Sobject Location= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Location__c').newSObject();
        Location.put('Name', 'Location 1');
        Location.put(NamespaceSelector.baseNamespace+'AddressLine1__c','TEST ADDRESS LINE');
        Location.put(NamespaceSelector.baseNamespace+'PracticeGroup__c', practice.Id);
        insert Location;
        
        Contact Provider= new Contact();
        Provider.put('FirstName', 'Test');
        Provider.put('LastName', 'Test');
        insert Provider;
        
        Sobject PractitionerLocation1= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocation__c').newSObject();
        PractitionerLocation1.put('Name', 'PLTest');
        PractitionerLocation1.put(NamespaceSelector.baseNamespace+'Location__c', Location.Id);
        PractitionerLocation1.put(NamespaceSelector.baseNamespace+'Provider__c', Provider.Id);
        insert PractitionerLocation1;
        
        Sobject territory= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Territory__c').newSObject();
        territory.put('Name', 'Test Territory');
        insert territory;
        
        Sobject pltObj= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c').newSObject();
        pltObj.put(NamespaceSelector.baseNamespace+'Territory__c', territory.Id);
        pltObj.put(NamespaceSelector.baseNamespace+'PractitionerLocation__c', PractitionerLocation1.Id);
        insert pltObj;
    }
    
    @isTest
    static void testGetPlRecords(){
        List<Sobject> provObjs = Database.query('SELECT Id FROM Contact LIMIT 1');
        System.assertNotEquals(null,AddToMyTerritoryController.getPlRecords(provObjs[0].Id));
    }
    
     
    @isTest
    static void testSaveTerrPlRecAdd() {
        List<Sobject> terrObj = Database.query('SELECT Id,Name FROM '+NamespaceSelector.baseNamespace+'Territory__c LIMIT 1');
        List<Sobject> provLocObj = Database.query('SELECT Id,Name FROM '+NamespaceSelector.baseNamespace+'PractitionerLocation__c LIMIT 1');
        string wrapper =  '[{"terrId":"'+terrObj[0].Id+'","selPlIds":["'+provLocObj[0].Id+'"],"removedPlIds":[],"newPlIds":["'+provLocObj[0].Id+'"],"isNewTerritory":false}]';
        system.assertNotEquals(null, AddToMyTerritoryController.saveTerrPlRec(wrapper));
    }
    
    @isTest
    static void testSaveTerrPlRecDelete() {
        List<Sobject> terrObj = Database.query('SELECT Id,Name FROM '+NamespaceSelector.baseNamespace+'Territory__c LIMIT 1');
        List<Sobject> provLocObj = Database.query('SELECT Id,Name FROM '+NamespaceSelector.baseNamespace+'PractitionerLocation__c LIMIT 1');
        string wrapper =  '[{"terrId":"'+terrObj[0].Id+'","selPlIds":["'+provLocObj[0].Id+'"],"removedPlIds":["'+provLocObj[0].Id+'"],"newPlIds":[],"isNewTerritory":false}]';
        system.assertNotEquals(null, AddToMyTerritoryController.saveTerrPlRec(wrapper));
    }
    
    @isTest
    static void testCreateTerritory() {
        system.assertNotEquals(null, AddToMyTerritoryController.createTerritory('TESTING'));
    }
    
}