/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: RestPRM3TaskController
* @description: This class is to expose the Recent Logged Task.
* @Author: Healthgrades Inc.
* @Last Modified Date: 10/04/2020
**/
@RestResource(urlMapping='/prm3/v1/Tasks/myTasks')
global inherited sharing class RestPRM3TaskController {
    
    /********************************
    * @method: doGet
    * @description: get method for Api
    * @return: List<PRM3uRestController.Activity>
    * *****************************/
    @HttpGet
    global static List<PRM3uRestController.Activity> doGet() {
        try {
            RestRequest request = RestContext.request;
            RestResponse response = RestContext.response;
            response.statusCode = 200;
            response.addHeader('Content-Type', 'application/json');
            Integer recordLimit = PRM3uRestController.maxRecordsParam(request);
            return performGetSearch(recordLimit);
        } catch(Exception RestPRM3TaskCtrlEx) {
            CoreControllerException errLogException = new CoreControllerException('RestPRM3TaskController.doGet', 
                                                                                          'Could not get activity records ==> '+RestPRM3TaskCtrlEx.getStackTraceString());
            return null;
        }
    }

    /********************************
    * @method: performGetSearch
    * @description: performing search to fetch Recent Task
    * @return: List
    * *****************************/
    public static List<PRM3uRestController.Activity> performGetSearch(Integer recordLimit){
        List<String> conditions = new List<String>();
        conditions.add('TaskSubtype != \'Call\' And Type != \'Expense\' AND Subject NOT IN (\'Expense\', \'Mileage\') AND Status !=\'Completed\'');
        List<PRM3uRestController.Activity> response = new List<PRM3uRestController.Activity>();
        if( Task.sObjectType.getDescribe().isAccessible() ) {
            List<Task> result = (List<Task>)Database.query(PRM3uRestController.generateCallSql(conditions, recordLimit));
            for(Task a : result) { response.add(new PRM3uRestController.Activity(a)); }
            return response;
        } else {
            CoreControllerException errLogException = new CoreControllerException('RestPRM3TaskController.performGetSearch', 
                                                                                          'User does not have access to Task Object ');
            return null;
        }
    }

}