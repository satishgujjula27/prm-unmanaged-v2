/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: PRM3uRestControllerTest
* @description: This class is to handle Activity API.
* @Last Modified Date: 07/09/2020
**/
@isTest
public class PRM3uRestControllerTest {

    /**@@
    #DESCRIPTION   : This method is used to create Test records.
    #Paramaters    : None
    @@**/
    @testSetup static void createTestTaskdata() {
        // Create Test Contact
        Contact testContact = new Contact ();
        testContact.LastName = 'Test';
        insert testContact;

        // Create Test Account
        Task testTask = new Task ();
        testTask.Subject = 'Other';
        testTask.Description = 'Test Description';
        testTask.Status = 'Open';
        Contact lstObjects = [SELECT Id, Name FROM Contact WHERE Id != null LIMIT 1];
        testTask.WhoId = lstObjects.Id;
        testTask.RecordTypeId = [SELECT Id, DeveloperName, SobjectType FROM RecordType WHERE DeveloperName = 'PRM' AND SobjectType = 'Task'].Id;
        insert testTask;
    }

    static testMethod void maxRecordsParam() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/abc/xyz/';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        System.assertEquals(5,PRM3uRestController.maxRecordsParam(req),'');
        Test.stopTest();
    }

    static testMethod void monthTest() {
        System.assertEquals('January', PRM3uRestController.Month(1),'');
        System.assertEquals('February', PRM3uRestController.Month(2),'');
        System.assertEquals('March', PRM3uRestController.Month(3),'');
        System.assertEquals('April', PRM3uRestController.Month(4),'');
        System.assertEquals('May', PRM3uRestController.Month(5),'');
        System.assertEquals('June', PRM3uRestController.Month(6),'');
        System.assertEquals('July', PRM3uRestController.Month(7),'');
        System.assertEquals('August', PRM3uRestController.Month(8),'');
        System.assertEquals('September', PRM3uRestController.Month(9),'');
        System.assertEquals('October', PRM3uRestController.Month(10),'');
        System.assertEquals('November', PRM3uRestController.Month(11),'');
        System.assertEquals('December', PRM3uRestController.Month(12),'');
    }

    static testMethod void joinTest() {
        List<Object> lstObj = new List<Object>();
        lstObj.add( 'Test1' ); lstObj.add( 'Test2' ); lstObj.add( 'Test3' );
        System.AssertEquals('Test1trueTest2trueTest3', Util.join(lstObj, 'true'),'');
    }

    static testMethod void generateCallSqlTest() {
        List<String> lstObj = new List<String>();
        lstObj.add( 'Test1' ); lstObj.add( 'Test2' ); lstObj.add( 'Test3' );
        System.assertNotEquals(null, PRM3uRestController.generateCallSql(lstObj, 20),'');
    }

    static testMethod void testResultClass() {
        Test.startTest();
        PRM3uRestController.Result prm3uResult = new PRM3uRestController.Result('Pass', 1);
        System.assertEquals(1,prm3uResult.totalCount,'');
        Test.stopTest();
    }
}