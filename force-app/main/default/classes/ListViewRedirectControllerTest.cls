/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: ListViewRedirectControllerTest
* @description: This class is to test ListViewRedirectController.
* @Author: Healthgrades Inc.
* @Last Modified Date: 23/12/2019
**/
@isTest
public class ListViewRedirectControllerTest {
	/**
    * @Name    : testRedirect
    * @Purpose : to test list View redirect vf page for task
    * @param   : none
    * @return  : none
    */
    @isTest static void testRedirectForTask(){
        PageReference pr = Page.listViewPage;
        pr.getParameters().put('req', 'task');
        Test.startTest();
        Test.setCurrentPageReference(pr);
        ListViewRedirectController  controller = new ListViewRedirectController();
        System.assertNotEquals(null,controller.redirect());
        Test.stopTest();
    }

    /**
    * @Name    : testRedirect
    * @Purpose : to test list View redirect vf page for issue
    * @param   : none
    * @return  : none
    */
    @isTest static void testRedirectForIssue(){
        PageReference pr = Page.listViewPage;
        pr.getParameters().put('req', 'issue');
        Test.startTest();
        Test.setCurrentPageReference(pr);
        ListViewRedirectController controller = new ListViewRedirectController();
        System.assertNotEquals(null,controller.redirect());
        Test.stopTest();
    }

    /**
    * @Name    : testRedirect
    * @Purpose : to test list View redirect vf page for call
    * @param   : none
    * @return  : none
    */
    @isTest static void testRedirectForCall(){
        PageReference pr = Page.listViewPage;
        pr.getParameters().put('req', 'call');
        Test.startTest();
        Test.setCurrentPageReference(pr);
        ListViewRedirectController controller = new ListViewRedirectController();
        System.assertNotEquals(null,controller.redirect());
        Test.stopTest();
    }

    /**
    * @Name    : testgetFilterId
    * @Purpose : to test list View redirect vf page for call
    * @param   : none
    * @return  : none
    */
    @isTest static void testgetFilterId(){
        Test.startTest();
        ListViewRedirectController controller = new ListViewRedirectController();
        System.assertNotEquals(null,controller.getFilterId('Account', 'All'));
        Test.stopTest();
    }
}