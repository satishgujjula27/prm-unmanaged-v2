/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: RelatedLkpFilterController
* @description: This Class is used to show Custom Related Lookup Filter Data and actions
* @Last Modified Date: 10/04/2020
**/
public inherited sharing class RelatedLkpFilterController {
    
    @AuraEnabled public List<TerritoryBuilderTableWrapper.TerritoryBuilderTableWrapperFldSet> fieldsWrapper = new List<TerritoryBuilderTableWrapper.TerritoryBuilderTableWrapperFldSet>();
    @AuraEnabled public List<Map<String,Object>> resultWrapper = new List<Map<String,Object>>();
    @AuraEnabled public String homeObjectName;
    @AuraEnabled public String parentRecName;
    @AuraEnabled public String strTitle ='';
    @AuraEnabled public Integer totalRec;
    
    /********************************
    * @Method: getPLTRecord
    * @description: auraenabled method to fetch all PLT records.
    * @return: String
    * *****************************/
    @AuraEnabled
    public static String getPLTRecord(String recordId, Integer recLimit, String sortBy, String sortDirection, String pageSize, String pageNumber) {
        RelatedLkpFilterController wrapCls = new RelatedLkpFilterController();
        List<TerritoryBuilderTableWrapper.TerritoryBuilderTableWrapperFldSet> flswrapper = new List<TerritoryBuilderTableWrapper.TerritoryBuilderTableWrapperFldSet>();
        List<PLTWrapper> practLocations = new List<PLTWrapper>();
        List<Map<String,Object>> practLocations1 = new List<Map<String,Object>>();
        TerritoryBuilderTableWrapper.getTerrTableMeta('Territory Provider', '');
        try {
            String pltRecsql = getPLTRecSql();
            List<Sobject> pltListRecord = getPLTRecord(pltRecsql, recordId, recLimit, sortBy, sortDirection, pageSize, pageNumber, wrapCls);
            if(pltListRecord.size()>0) {
                if(recLimit > 0) { wrapCls.strTitle = setTitle(pltListRecord.size()); } // for setting count in title
                Integer loopCntRelList = 1;
                Integer psize = Integer.valueOf(pageSize);
                Integer pnumber = Integer.valueOf(pageNumber);
                Integer uppperLimit = psize * pnumber;
                Integer lowerLimit = uppperLimit - (psize-1);
                for(Sobject sObj : pltListRecord) {
                    if(loopCntRelList < lowerLimit) { loopCntRelList++; continue; }
                    if(recLimit > 0 && loopCntRelList > 3 ) { break; }
                    try{
                        Map<String,Object> res = TerritoryBuilderTableWrapper.setMapResult(sObj);
                        practLocations1.add(res);
                        loopCntRelList++;
                    } catch(Exception getPLTRecordEx) {
                        CoreControllerException errLogException = new CoreControllerException('RelatedLkpFilterController.getPLTRecord', 
                                                                                                      'Could not get provider Locations ==> '+getPLTRecordEx.getStackTraceString());
                    }
                }
            }
            wrapCls.fieldsWrapper = TerritoryBuilderTableWrapper.setWrapperFieldSet();
            wrapCls.resultWrapper = practLocations1;
            wrapCls.homeObjectName = NamespaceSelector.baseNamespace+'Territory__c';
            SObjectType terrObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Territory__c');
            Schema.DescribeFieldResult terrFieldDesc = terrObjToken.getDescribe().fields.getMap().get('Name').getDescribe();
            if( Util.checkObjectIsReadable(terrObjToken) && Util.checkFieldIsReadable(terrObjToken, terrFieldDesc) ) {
                List<Sobject> TerrObj = Database.query('SELECT Name FROM '+NamespaceSelector.baseNamespace+'Territory__c WHERE Id =:recordId AND Id != null LIMIT 1');
                if(TerrObj.size() > 0) { wrapCls.parentRecName = (String)TerrObj[0].get('Name'); }
                return JSON.serialize(wrapCls);
            } else {
                return null;
            }
        } catch(Exception getPLTRecordEx) {
            CoreControllerException errLogException = new CoreControllerException('RelatedLkpFilterController.getPLTRecord', 
                                                                                          'Could not get provider Locations ==> '+getPLTRecordEx.getStackTraceString());
            return null;
        }
    }
    
    /********************************
    * @Method: getPLTRecSql
    * @description: get plt records.
    * @return: String
    * *****************************/
    public static String getPLTRecSql(){
        List<String> fields = TerritoryBuilderTableWrapper.getQueryFields();
        SObjectType pltObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c');
        if( Util.checkObjectIsReadable(pltObjToken) ) {
            return 'SELECT '+String.join(fields,',')+' From '+NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c';   
        } else {
            CoreControllerException getSelectOptionsCRUDException = new CoreControllerException('RelatedLkpFilterController.getPLTRecSql', 
                                                                                                        'Could not access sObject ==> '+NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c');
            return null;
        }
    }
    
    /********************************
    * @Method: getPLTRecord
    * @description: get PLT records.
    * @return: List<Sobject>
    * *****************************/
    public static List<Sobject> getPLTRecord(String pltRecSql, String recordId, Integer recLimit, String sortBy, String sortDirection, String pageSize, String pageNumber, RelatedLkpFilterController wrapCls){
        pltRecSql += ' WHERE '+NamespaceSelector.baseNamespace+'PractitionerLocation__c!=NULL AND '+NamespaceSelector.baseNamespace+'Territory__c= :recordId AND Id != null';
        if(String.IsNotBlank(sortBy)) {
            Boolean addSortExt = true;
            sortBy = TerritoryBuilderTableWrapper.getDbFieldMeta(sortBy);
            if(sortBy != null) {
                pltRecSql +=' ORDER BY '+sortBy;
                if(addSortExt) {
                    if(string.IsNotBlank(sortDirection)) {
                        pltRecSql +=' '+sortDirection;
                    }
                }
            }
        }
        
        if(recLimit >0) {
            pltRecSql += ' LIMIT '+recLimit;
        } else {
            Integer psize = Integer.valueOf(pageSize);
            Integer pnumber = Integer.valueOf(pageNumber);
            if(psize>0 && pnumber>=0){
                pltRecSql += ' LIMIT '+pnumber*psize;
            }                       
        }
        SObjectType pltObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c');
        Schema.DescribeFieldResult pracLocFieldDesc = pltObjToken.getDescribe().fields.getMap().get(NamespaceSelector.baseNamespace+'PractitionerLocation__c').getDescribe();
        Schema.DescribeFieldResult terrFieldDesc = pltObjToken.getDescribe().fields.getMap().get(NamespaceSelector.baseNamespace+'Territory__c').getDescribe();
        if( Util.checkObjectIsReadable(pltObjToken) && Util.checkFieldIsReadable(pltObjToken, pracLocFieldDesc) && Util.checkFieldIsReadable(pltObjToken, terrFieldDesc)) {
            String totalPltRecQuery = 'SELECT COUNT() FROM '+NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c WHERE ';
            totalPltRecQuery += NamespaceSelector.baseNamespace+'PractitionerLocation__c!=NULL AND '+NamespaceSelector.baseNamespace+'Territory__c= :recordId AND Id != null';
            wrapCls.totalRec = Database.countQuery(totalPltRecQuery);
            return Database.query(pltRecSql); 
        } else {
            CoreControllerException getSelectOptionsCRUDException = new CoreControllerException('RelatedLkpFilterController.getPLTRecord', 
                                                                                                        'Could not access sObject and fields ==> '+NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c');
            return null;
        }
    }
    
    /********************************
    * @Method: deleteRecord
    * @description: set title based on integer count
    * @return: String
    * *****************************/
    @AuraEnabled
    public static Boolean deleteRecord(Id recordId) { return Util.deleteSObjRecord(recordId); }
    
    /********************************
    * @Method: setTitle
    * @description: set title based on integer count
    * @return: String
    * *****************************/
    public static String setTitle(Integer intCount) {
        String strTitle ='(';
        if ( intCount > 3 ) { strTitle += '3+'; } else { strTitle += String.valueOf( intCount ); }
        strTitle += ')';
        return strTitle;
    }
    
    /********************************
    * @Method: PLTWrapper
    * @description: Constructor to set the Class attribute from Object using Map
    * @return: N/A
    * *****************************/
    public class PLTWrapper {
        @AuraEnabled Public String Id;
        @AuraEnabled Public String providerId;
        @AuraEnabled Public String linkId;
        @AuraEnabled Public String providerName { get { return String.isBlank(this.ProviderName) ? '' : this.ProviderName; } set; }
        @AuraEnabled public String locationName;
        @AuraEnabled public String locationAddr;
        @AuraEnabled public String locationPhone;
        @AuraEnabled public String createdDate;
        
        public PLTWrapper(Sobject sobj) {
            SObjectType thisObjToken = sobj.getSObjectType();
            this.Id = (String)sObj.get('Id');
            Schema.DescribeFieldResult pracLocProvFieldDesc = sobj.getSObjectType().getDescribe().fields.getMap().get(NamespaceSelector.baseNamespace+'PractitionerLocation__c').getDescribe();
            if(Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Location__r') !=null && Util.checkFieldIsReadable(thisObjToken, pracLocProvFieldDesc)) {
                this.locationName = (String)Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Location__r').get('Name');
                if(Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'MailingAddress__c') !=null) {
                    this.locationAddr = (String)Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'MailingAddress__c');
                    this.locationAddr = this.locationAddr.replace('<br>','');
                }
                this.locationPhone = (String)Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'Telephone__c');
            }
            dateTime createdDt = (Datetime)sObj.get('createdDate');
            this.createdDate = createdDt.format('M/d/yyyy h:mm a');
            if(Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Provider__r') !=null && Util.checkFieldIsReadable(thisObjToken, pracLocProvFieldDesc)) {
                this.linkId = '/'+(String)Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Provider__r').get('Id');
                this.providerId = (String)Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Provider__r').get('Id');
                this.providerName = (String)Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Provider__r').get('Name');
            }
        }
    }
}