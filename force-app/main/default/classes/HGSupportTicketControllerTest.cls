/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying 
* or sale of the above may constitute an infringement of copyright 
* and may result in criminal or other legal proceedings.
*
* @Class Name: HGSupportTicketControllerTest
* @description: This is the test class for HGSupportTicketController
* @Author: Healthgrades Inc.
**/
@isTest    
public class HGSupportTicketControllerTest {
    
    public static testmethod void testvalidate(){
        
        Map<String, String> mapCase = new Map<String, String>();
        mapCase.put('Subject', 'test');
        mapCase.put('Description', 'ashjkdhajsdhakjsdhjashdjkashkdjhakjdhasd');

        Test.startTest();
            system.assertEquals(true, HGSupportTicketController.sendEmail(mapCase, 'test@example.com')); 
        Test.stopTest();
    }
}