/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying 
* or sale of the above may constitute an infringement of copyright 
* and may result in criminal or other legal proceedings.
*
* @Class Name: ScheduleBuilderControllerTest
* @description: This is the test class ScheduleBuilderController
**/
@isTest
public class ScheduleBuilderControllerTest {
    @testSetup 
    static void setup() {
        Account practice= new Account();
        practice.put('Name', 'Test');
        insert practice;
        
        Sobject Location= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Location__c').newSObject();
        Location.put('Name', 'Location 1');
        Location.put(NamespaceSelector.baseNamespace+'PracticeGroup__c', practice.Id);
        insert Location;
        
        Contact Provider= new Contact();
        Provider.put('FirstName', 'Test');
        Provider.put('LastName', 'Test');
        insert Provider;
        
        Sobject PractitionerLocation1= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocation__c').newSObject();
        PractitionerLocation1.put('Name', 'PLTest');
        PractitionerLocation1.put(NamespaceSelector.baseNamespace+'Location__c', Location.Id);
        PractitionerLocation1.put(NamespaceSelector.baseNamespace+'Provider__c', Provider.Id);
        insert PractitionerLocation1;
        
        Sobject territory= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Territory__c').newSObject();
        territory.put('Name', 'Test Territory');
        insert territory;
        
        Sobject pltObj= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c').newSObject();
        pltObj.put(NamespaceSelector.baseNamespace+'Territory__c', territory.Id);
        pltObj.put(NamespaceSelector.baseNamespace+'PractitionerLocation__c', PractitionerLocation1.Id);
        insert pltObj;
        
        Event event= new Event();
        event.put('Subject', 'Test Event');
        event.put('WhoId', Provider.Id);
        event.put('WhatId', practice.Id);
        event.put('Description', 'Test Event');
        event.put('StartDateTime', System.now().addDays(1));
        event.put('EndDateTime', System.now().addDays(2));
        event.put('Location', 'Test Location');
        insert event;
        
        //List<Territory__c> territoywonerId=[SELECT Id, OwnerId, Name FROM Territory__c where id=:territory.id];
        //system.debug('wonerId:'+territoywonerId[0].OwnerId);
    }
    
    @isTest
    static void testGetPicklistValuesObjFilterOptions(){
        system.assert(Util.getPicklistValuesObj('Task', 'Subject').size()>0);
        system.assertNotEquals(null, ScheduleBuilderController.getFilterOptions('territory'));
        system.assertNotEquals(null, ScheduleBuilderController.getFilterOptions('practice'));
    }
    
    static void testGetPracticesTerritories(){
        //ScheduleBuilderController.getTerritories();
        //ScheduleBuilderController.getPractices();
        //ScheduleBuilderController.getPLSql();
    }
    
    @isTest
    static void testSaveEvents(){
        List<Event> evObj = [SELECT Id From Event LIMIT 1];
        system.assertNotEquals(null, ScheduleBuilderController.saveEvents(evObj));
    }
    
    @isTest
    static void testGetLoggedInUser(){
        system.assert(ScheduleBuilderController.getLoggedInUser()!=null);
        //ScheduleBuilderController.setWrapperFieldSet();
        //ScheduleBuilderController.getFilterString(true, 'Other');
    }
    
    @isTest
    static void testPractitionerLocationWrapper(){
        List<Territory__c> territoywonerId=[SELECT Id, OwnerId, Name FROM Territory__c];
        string ownerId=territoywonerId[0].Id;
        list<Location__c>locationList=[SELECT Id, Name, PracticeGroup__c FROM Location__c];
        string practiceGroupId=locationList[0].PracticeGroup__c;
        system.assert(ScheduleBuilderController.getProviders('Test', true, 'territory', ownerId, 'lLinkId', 'asc',0,'50','1')!=null);
        system.assert(ScheduleBuilderController.getProviders('Test', true, 'territory', ownerId, 'fLinkId', 'asc',0,'50','1')!=null);
        system.assert(ScheduleBuilderController.getProviders('Test', true, 'territory', ownerId, 'sLinkId', 'asc',0,'50','1')!=null);
        system.assert(ScheduleBuilderController.getProviders('Test', true, 'territory', ownerId, 'pngLinkId', 'asc',0,'50','1')!=null);
        system.assert(ScheduleBuilderController.getProviders('Test', true, 'territory', ownerId, 'locationMailingAddress', 'asc',0,'50','1')!=null);
        system.assert(ScheduleBuilderController.getProviders('Test', true, 'territory', ownerId, 'locationPhone','asc',0,'50','1')!=null);
        system.assert(ScheduleBuilderController.getProviders('Test', true, 'practice', practiceGroupId, 'locationPhone','asc',0,'50','1')!=null);
        
        //List<Sobject> plObjs = Database.query('SELECT Id, '+NamespaceSelector.baseNamespace+'Provider__c, '+NamespaceSelector.baseNamespace+'Provider__r.FirstName FROM '+NamespaceSelector.baseNamespace+'PractitionerLocation__c LIMIT 1');
        //ScheduleBuilderController.PractitionerLocationWrapper pLocaWrapper = new ScheduleBuilderController.PractitionerLocationWrapper(plObjs[0]);
    }
    
    @isTest
    static void testgetPicklistValues(){
        system.assert(ScheduleBuilderController.getPicklistValues().size()>0);
        
    }
}