/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: LogIssueController
* @description: This class is to Logged Issue from bulk action.
* @Author: Healthgrades Inc.
* @Last Modified Date: 02/03/2021
**/
public inherited sharing class LogIssueController {
	String objectName;
    List<IssueList> issueListWrapper = new List<IssueList>();
    List<IssueFields> defaultFldList = new List<IssueFields>();
    String defaultIssueTabs;
    Static List<Contact> contacts;


    /********************************
    * @method: getIssueList
    * @description: get Issue List to show as tabs
    * Param: none
    * Return type: String
    * *****************************/
    @AuraEnabled
    public static String getIssueList(String selectedTab, String providerId) {
        LogIssueController logIssueWrapper = new LogIssueController();
        String issueObjName = NamespaceSelector.baseNamespace + 'PractitionerIssues__c';
        logIssueWrapper.objectName = issueObjName;
        try {
            if (String.isNotEmpty(providerId) && Schema.sObjectType.Contact.isAccessible()) {
                contacts = [SELECT Id, Name FROM Contact WHERE Id = :providerId LIMIT 1];    
            }

            List<String> issueTabsFld = new List<String> {
                'MasterLabel', 'DeveloperName', 'Active__c', 'Ordering__c', 
                'IsDefault__c', 'LayoutName__c, RecordTypeName__c'
            };

            Map<String, String> allIssueRecordTypesMap = new Map<String, String>();
            List<String> allIssueRecordTypes = new List<String>();
            for (RecordTypeInfo recTypeInfo : Schema.getGlobalDescribe().get(issueObjName).getDescribe().getRecordTypeInfos()) {
                if (recTypeInfo.isAvailable()) {
                    allIssueRecordTypesMap.put(recTypeInfo.getDeveloperName(), recTypeInfo.getRecordTypeId()); 
                    allIssueRecordTypes.add(recTypeInfo.getDeveloperName());
                }
            }

            List<IssueList> issueList = new List<IssueList>();
            String defaultTabsLayout = '';
            Boolean isDefaultSet = false;
            List<IssueFields> defaultIssueFldList = new List<IssueFields>();
            List<SObject> issueConfigurations = Database.query('SELECT '+String.join(issueTabsFld,', ')+' FROM '+'ActivityConfiguration__mdt WHERE '+'Active__c = true AND '+'RecordTypeName__c IN :allIssueRecordTypes ORDER BY '+'Ordering__c');
            if (!issueConfigurations.isEmpty()) {
                for(SObject issueObj : issueConfigurations) {
                    if (selectedTab == (String)issueObj.get('Id')) {
                        defaultTabsLayout = (String)issueObj.get('LayoutName__c');
                        logIssueWrapper.defaultIssueTabs = (String)issueObj.get('Id');
                        isDefaultSet = true;
                    } else if (!isDefaultSet && (Boolean)issueObj.get('IsDefault__c')) {
                        defaultTabsLayout = (String)issueObj.get('LayoutName__c');
                        logIssueWrapper.defaultIssueTabs = (String)issueObj.get('Id');
                    }

                    Boolean hasRecordType = allIssueRecordTypesMap.containsKey((String)issueObj.get('RecordTypeName__c'));
                    String recordTypeId =  (hasRecordType == true) ? allIssueRecordTypesMap.get((String)issueObj.get('RecordTypeName__c')) : '';
                    IssueList issueLst = new IssueList((String)issueObj.get('Id'), (String)issueObj.get('MasterLabel'), (String)issueObj.get('DeveloperName'), recordTypeId, new List<IssueFields>());
                    issueList.add(issueLst);
                }
            } else {
                defaultTabsLayout = issueObjName+'-'+NamespaceSelector.baseNamespace+'Issue Layout';
                issueList.add(new IssueList('', 'Issue Layout', '', '', new List<IssueFields>()));
            }

            List<String> sysFields = new List<String>{'Id','CreatedBy','CreatedById','CreatedDate','LastModifiedBy','LastModifiedById','LastModifiedDate','RecordTypeId'};
            if (String.isNotBlank(defaultTabsLayout)) {
                for (String fld : Util.getLayoutFields(defaultTabsLayout)) {
                    if (!sysFields.contains(fld) && Schema.getGlobalDescribe().get(issueObjName).getDescribe().fields.getMap().containsKey(fld) && Schema.getGlobalDescribe().get(issueObjName).getDescribe().fields.getMap().get(fld).getDescribe().isCreateable()) {
                        Object value = '';
                        if (fld == NamespaceSelector.baseNamespace + 'Provider__c') {
                            value = providerId;
                        } else if (fld == NamespaceSelector.baseNamespace + 'DateTimeOpened__c') {
                            value = DateTime.now();

                        } else if (String.valueOf(Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace + 'PractitionerIssues__c').getDescribe().fields.getMap().get(fld).getDescribe().getType()) == 'BOOLEAN') {
                            value = false;
                        } 
                        defaultIssueFldList.add(new IssueFields(fld, value));
                    }
                }
            }

            logIssueWrapper.issueListWrapper = issueList;
            logIssueWrapper.defaultFldList = defaultIssueFldList;
        } catch(Exception ex) {
            CoreControllerException getObjFldDetailsException = new CoreControllerException('LogIssueController.getIssueList', 
                                                                                                    'Exception occured in getIssueList'+ex.getStackTraceString());
        }

        return JSON.serialize(logIssueWrapper);
    }
     
    /********************************
    * @Wrapper Class Name: IssueList
    * @description: This class is used to hold Issue List
    * *****************************/
    public class IssueList {
        String label;
        String devName;
        String Id;
        String recordTypeId;
        List<IssueFields> issueFields;

        public IssueList(String Id, String label, String devName, String recordTypeId, List<IssueFields> issueFields){
            this.label = label;
            this.devName = devName;
            this.Id = Id;
            this.issueFields = issueFields;
            this.recordTypeId = recordTypeId;
        }
    }
    
    /********************************
    * @Wrapper Class Name: IssueFields
    * @description: This class is used to hold Issue Fields
    * *****************************/
    public class IssueFields {
        String apiName;
        String fieldType;
        String label;
        Boolean isRequired;
        Boolean readOnly;
        Object defaultValue;
        Util.LookupWrapper selItem;
        
        public IssueFields(String apiName, Object defaultValue){
            this.apiName = apiName;
            Schema.DisplayType dispType = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace + 'PractitionerIssues__c').getDescribe().fields.getMap().get(apiName).getDescribe().getType();
            this.fieldType = String.valueOf(dispType);
            this.label = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace + 'PractitionerIssues__c').getDescribe().fields.getMap().get(apiName).getDescribe().getLabel();
            Schema.DescribeFieldResult field = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace + 'PractitionerIssues__c').getDescribe().fields.getMap().get(apiName).getDescribe();
            this.isRequired = (field.isCreateable() && !field.isNillable() && String.valueOf(field.getType()) != 'BOOLEAN') ? true : false;
            this.defaultValue = (defaultValue == null) ? field.getDefaultValue() : defaultValue;
            if (apiName == NamespaceSelector.baseNamespace + 'Provider__c' && contacts != null && contacts.size() > 0) {
                this.selItem = new Util.LookupWrapper(contacts[0].Name);
            }
        }
    }
}