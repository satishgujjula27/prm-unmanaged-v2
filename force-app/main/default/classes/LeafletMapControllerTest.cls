/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: LeafletMapControllerTest
* @description: This is the test class for LeafletMapController.
* @Last Modified Date: 07/09/2020
**/
@isTest
public class LeafletMapControllerTest {
    static testMethod void testBehavior(){
        Sobject territory= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Territory__c').newSObject();
        territory.put('Name', 'Test Territory');
        insert territory;
        SObject terrObj = territory.Id.getSObjectType().newSObject(territory.Id);
        System.assertEquals(terrObj.Id,LeafletMapController.updateRecord(territory.Id,'[]',NamespaceSelector.baseNamespace+'GeoJSON__c').Id);
    }
}