/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: NamespaceSelector
* @description: This Class is used to do handle the Edit Provider functionality
* @Last Modified Date: 06/15/2020
**/
public inherited sharing class NamespaceSelector {
    
    public static String baseNamespace {
        get {
            if ( System.Label.BasePkgNamespace != 'na' ) {
                baseNamespace = System.Label.BasePkgNamespace;
            } else {
                baseNamespace = '';
            }
            return baseNamespace;
        }
        private set;
    }
    
    public static String hgprmNamespace {
        get {
            if ( System.Label.HGPRMPkgNamespace != 'na' ) {
                hgprmNamespace = System.Label.HGPRMPkgNamespace;
            } else {
                hgprmNamespace = '';
            }
            return hgprmNamespace;
        }
        private set;
    }
}