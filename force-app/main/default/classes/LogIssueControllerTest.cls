/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying 
* or sale of the above may constitute an infringement of copyright 
* and may result in criminal or other legal proceedings.
*
* @Class Name: LogIssueControllerTest
* @description: This is the test class LogIssueController
* @Last Modified Date: 03/02/2021
**/
@isTest
public class LogIssueControllerTest {
    static testMethod void testGetIssueList() {
        String result = LogIssueController.getIssueList('', '');
        Map<String, Object> resultDeserializedMap = (Map<String, Object>) JSON.deserializeUntyped(result);
        String objName = (String) resultDeserializedMap.get('objectName');
        List<Object> issuesList = (List<Object>) resultDeserializedMap.get('issueListWrapper');
        System.assert(objName.contains('PractitionerIssues__c'));
        System.assert(!issuesList.isEmpty());
    }
}