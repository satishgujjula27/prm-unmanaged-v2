/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: RestPRM3IssueStatusCountController
* @description: This class is to expose the Logged in Users Recent Issue Count.
* @Author: Healthgrades Inc.
* @Last Modified Date: 10/04/2020
**/
@RestResource(urlMapping='/prm3/v1/Issues/statusWiseCount')
global inherited sharing class RestPRM3IssueStatusCountController {

    /********************************
    * @method: doGet
    * @description: get method for Api
    * @return: List<PRM3uRestController.Activity>
    * *****************************/
    @HttpGet
    global static List<PRM3uRestController.Result> doGet() {
        try {
            RestRequest request = RestContext.request;
            RestResponse response = RestContext.response;
            response.statusCode = 200;
            response.addHeader('Content-Type', 'application/json');
            Integer recordLimit = PRM3uRestController.maxRecordsParam(request);
            List<PRM3uRestController.Activity> detailResult = RestPRM3IssueController.performGetSearch(recordLimit);
            return parseDetailResult(detailResult);
        } catch(Exception RestPRM3IssueStatusCountCtrlEx) {
            CoreControllerException errLogException = new CoreControllerException('RestPRM3IssueStatusCountController.doGet', 
                                                                                          'Could not get activity records ==> '+RestPRM3IssueStatusCountCtrlEx.getStackTraceString());
            return null;
        }
    }

    /********************************
    * @method: ParseDetailResult
    * @description: parse the detail result to get the summary result
    * @return: List
    * *****************************/
    public static List<PRM3uRestController.Result> parseDetailResult(List<PRM3uRestController.Activity> result) {
        List<PRM3uRestController.Result> response = new List<PRM3uRestController.Result>();
        if(result.size()>0){
            Map<String, Integer> resultMap = new Map<String, Integer>();
            list<String> sortList = new List<String>();
            for(PRM3uRestController.Activity actvty:result) {
                if(resultMap.containsKey(actvty.status)) {
                    resultMap.put(actvty.status,resultMap.get(actvty.status)+1);
                } else {
                    resultMap.put(actvty.status,1);
                    sortList.add(actvty.status);
                }
            }
            sortList.sort();
            for(Integer i = sortList.size()-1; i>=0;i--) {
                integer m = resultMap.get(sortList.get(i));
                response.add(new PRM3uRestController.Result(sortList.get(i),m));
            }
        }
        return response;
    }
}