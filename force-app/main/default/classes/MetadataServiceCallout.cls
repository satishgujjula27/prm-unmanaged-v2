/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and EVariant corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying 
* or sale of the above may constitute an infringement of copyright 
* and may result in criminal or other legal proceedings.
*
* @Class Name: MetadataServiceCallout
* @description: 
* @Author: Healthgrades Inc.
* Last Date Modified: 06/29/2020
**/
public inherited sharing class MetadataServiceCallout {
    public static MetadataService.MetadataPort createService(){
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = Util.getSessionIdFromVF(Page.SessionID);//UserInfo.getSessionId();
        return service;
    }

    /**
        * Method Name: getDependentPicklistValuesByRecordType
        * @description: This method is used to get Picklist Values Based on RecordType
        * Param: object Name, record TypeName Name and field Name
        * Return type: List of String  
    **/
    /*Abha R: 23 Jun 2020 -: PRM3-1518 - Created this method to get Dependent Picklist Values Based on RecordType*/
    public static List<String> getPicklistValuesByRecordType(String objectName,String recordTypeName,String fieldName){
        List<String> listofavailablevalue = new List<String>();
        String objectRecordType = objectName + '.' + recordTypeName;
        MetadataService.MetadataPort service = createService();
        MetadataService.RecordType recordType = (MetadataService.RecordType) service.readMetadata('RecordType', new String[] { objectRecordType }).getRecords()[0];
        for ( MetadataService.RecordTypePicklistValue rpk : recordType.picklistValues ) {
            if ( rpk.picklist == fieldName ) {
                for ( MetadataService.PicklistValue pk : rpk.values ) {
                    listofavailablevalue.add(String.valueof(pk.fullName));
                }
            }
        }
        return listofavailablevalue;
    }
}