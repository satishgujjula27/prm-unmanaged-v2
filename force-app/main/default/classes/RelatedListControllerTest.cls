/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying 
* or sale of the above may constitute an infringement of copyright 
* and may result in criminal or other legal proceedings.
*
* @Class Name: RelatedListControllerTest
* @description: This is the test class RelatedListController
* @Last Modified Date: 01/15/2019
**/
@isTest
public class RelatedListControllerTest {
    @isTest
    static void testfetchRecs(){
        Account testAccount = new Account ();
        testAccount.Name = 'Test Account1';
        insert testAccount;
        
        List<Contact> contList = new List<Contact>();
        Contact testContact = new Contact();
        testContact.LastName = 'Test Contact';
        testContact.AccountId = testAccount.Id;
        contList.add(testContact);
        Contact testContact1 = testContact.clone(false, false, false, false);
        testContact1.LastName = 'Test Contact1';
        contList.add(testContact1);
        Contact testContact2 = testContact.clone(false, false, false, false);
        testContact2.LastName = 'Test Contact2';
        contList.add(testContact2);
        Contact testContact3 = testContact.clone(false, false, false, false);
        testContact3.LastName = 'Test Contact3';
        contList.add(testContact3);
        insert contList;
        System.assert(RelatedListController.fetchRecs(testAccount.Id,'Contact','AccountId','') !=null);
    }
    
    @isTest
    static void testgetPLTRecord(){
        Account testAccount = new Account ();
        testAccount.Name = 'Test Account1';
        insert testAccount;
        system.assertNotEquals(null, RelatedListController.getPLTRecord(testAccount.Id));
    }
    
    @isTest
    static void testdeleteRecord(){
        Account testAccount = new Account ();
        testAccount.Name = 'Test Account1';
        insert testAccount;
        system.assertEquals(true, RelatedListController.deleteRecord(testAccount.Id));
    }
}