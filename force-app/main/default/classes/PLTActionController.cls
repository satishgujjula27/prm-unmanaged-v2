/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: PLTActionController
* @description: This class is used for Provider Territory Log Call .
* @Author: Healthgrades Inc.
* @Last Modified Date: 10/05/2020   
**/
public inherited sharing class PLTActionController {

    List<ActivityList> actvtListWrapper = new List<ActivityList>();
    List<ActivityFields> defaultFldList = new List<ActivityFields>();
    string defaultActTabs;
    Static User loggedInUser;
    public static Map<String,String> fieldBehavMapInstance;
    
    /********************************
    * @method: getPicklistValues
    * @description: to get a picklist values to show in UI
    * Param: none
    * Return type: Map
    * *****************************/
    @AuraEnabled
    public static Map<String,List<String>> getPicklistValues() {
        Map<String,List<String>> picklistMap = new Map<String,List<String>>();
        try {
            picklistMap.put('intel', Util.getPicklistValuesObj('Task', NamespaceSelector.baseNamespace+'Intel__c'));
        } catch(Exception getPicklistValEx) {
            CoreControllerException getPicklistValException = new CoreControllerException('PLTActionController.getPicklistValues', 
                                                                                                  'Could not load picklist values '+getPicklistValEx.getStackTraceString());
        }
        return picklistMap;
    }
    
    /********************************
    * @method: logCall
    * @description: to log a call for provider Location Territory Record
    * Param: Task, List
    * Return type: Boolean
    * *****************************/
    @AuraEnabled
    public static Boolean logCall(Task task, String thisRecordTypeName, List<Id> providerIds) {
        Boolean logCallBoolean;
        try {
            if(Schema.sObjectType.Task.isAccessible() && Schema.sObjectType.Task.isCreateable()){
                Id recTypeId;
                if( !String.isEmpty(thisRecordTypeName) ) {
                    recTypeId = Util.getTaskRecordType(thisRecordTypeName);
                } else {
                    recTypeId = Util.getTaskRecordType('PRM');
                }
                List <Id> pltList = getPLTRecord(providerIds);
                List<Task> Tasks = new List<Task>();
                for(Id plt:pltList) {
                    Task tskObj =  task.clone(false, true, false, false);
                    tskObj.WhoId = plt;
                    tskObj.TaskSubtype = 'Call';
                    tskObj.Status = 'Completed';
                    if(recTypeId != null){ tskObj.RecordTypeId = recTypeId; }
                    Tasks.add(tskObj);
                }
                if(Tasks.size() > 0){
                    Database.insert(Tasks); logCallBoolean = true;
                }
            } else {
                CoreControllerException getQueryLogException = new CoreControllerException('PLTActionController.logCall', 
                                                                                                   'sobjects is not insertable/readable ==> Task');
                logCallBoolean = false;
            }
        }
        catch(Exception logCallEx) {
            CoreControllerException errLogException = new CoreControllerException('PLTActionController.logCall', 
                                                                                          'Could not log Calls ==> '+logCallEx.getStackTraceString());
            logCallBoolean = false;            
        }
        return logCallBoolean;
    }
    
    /********************************
    * @method: getPLTRecord
    * @description: to get the Provider Location from Provider Location Territory
    * Param: List
    * Return type: List
    * *****************************/
    public static List<Id> getPLTRecord(List<Id> providerIds){
        List<Id> proIds = new List<Id>();
        SObjectType pracLocTerrObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c');
        if(Util.checkObjectIsDeletable(pracLocTerrObjToken)) {
            String queryString = 'SELECT Id, '+NamespaceSelector.baseNamespace+'PractitionerLocation__r.'+
                				NamespaceSelector.baseNamespace+'Provider__c FROM '+NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c WHERE Id !=null AND Id IN: providerIds';
            
            List<SObject> pltProvider = Database.query(queryString);
            if(pltProvider.size()>0){
                for(SObject sObj:pltProvider){
                    proIds.add((Id)sObj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').get(NamespaceSelector.baseNamespace+'Provider__c'));
                }
            }
            return proIds;
        } else {
            CoreControllerException getQueryLogException = new CoreControllerException('PLTActionController.getPLTRecord', 
                                                                                               'sobjects is not readable ==> '+pracLocTerrObjToken);
            return null;
        }
    }
   
    /********************************
    * @method: removePLTRecords
    * @description: to remove provider Locations from Provider Location Terrtitory
    * Param: List
    * Return type: boolean
    * *****************************/
    @AuraEnabled
    public static Boolean removePLTRecords(List<Id> recordIds){
        Boolean removePlt;
        try{
            SObject sObj = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c').newSObject();
            SObjectType provLocTerrToken = sObj.getSObjectType();
            if(Util.checkObjectIsDeletable(provLocTerrToken)){
                string queryString = 'SELECT Id FROM '+NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c WHERE Id !=null AND Id IN: recordIds';
                List<Sobject> pltProvider =  Database.query(queryString);
                if(pltProvider.size()>0){
                    Database.delete(pltProvider);
                    removePlt = true;
                }
            } else {
                CoreControllerException getQueryLogException = new CoreControllerException('PLTActionController.removePLTRecords', 
                                                                                                   'sobjects is not deletable ==> '+provLocTerrToken);
                removePlt = false;
            }
        } catch(Exception removePltEx) {
            CoreControllerException errLogException = new CoreControllerException('PLTActionController.removePLTRecords', 
                                                                                          'Could not log Calls ==> '+removePltEx.getStackTraceString());
            removePlt = false;            
        }
        return removePlt;
    }
    
    /********************************
    * @method: getLoggedInUser
    * @description: get Logged In user to show as assigned to by default into UI
    * Param: none
    * Return type: User
	* *****************************/
    @AuraEnabled
    public static User getLoggedInUser() { return [SELECT Id,Name FROM User WHERE Id = :UserInfo.getUserId()]; }
    
    /********************************
    * @method: getActivityList
    * @description: get Activity List to show as tabs
    * Param: none
    * Return type: String
    * *****************************/
    @AuraEnabled
    public static string getActivityList(string selectedTab) {
        
        PLTActionController pltActnWrapper = new PLTActionController();
        try{
            List<String> actChldFlds = new List<String>{
                'MasterLabel', NamespaceSelector.hgprmNamespace+'Active__c', NamespaceSelector.hgprmNamespace+'Ordering__c', NamespaceSelector.hgprmNamespace+'FieldApiName__c'
            };
            List<String> activityTabsFld = new List<String> {
                'MasterLabel', 'DeveloperName', NamespaceSelector.hgprmNamespace+'Active__c', NamespaceSelector.hgprmNamespace+'Ordering__c', 
                NamespaceSelector.hgprmNamespace+'IsDefault__c', NamespaceSelector.hgprmNamespace+'LayoutName__c'
            };
            List<String> allTskRecordTypes = new List<String>();
            for(RecordTypeInfo recTypeInfo: Task.SObjectType.getDescribe().getRecordTypeInfos()) {
                if(recTypeInfo.isAvailable()) {
                    allTskRecordTypes.add( recTypeInfo.getDeveloperName() );   
                }
            }
			List<ActivityList> actvtList = new List<ActivityList>();
            string defaultTabsLayout = '';
            boolean isDefaultSet = false;
            List<ActivityFields> defaultActFldList = new List<ActivityFields>();
            for(SObject actObj : Database.query('SELECT '+String.join(activityTabsFld,', ')+' FROM '+NamespaceSelector.hgprmNamespace+'ActivityConfiguration__mdt WHERE '+NamespaceSelector.hgprmNamespace+'Active__c = true AND '+NamespaceSelector.hgprmNamespace+'RecordTypeName__c IN :allTskRecordTypes ORDER BY '+NamespaceSelector.hgprmNamespace+'Ordering__c')) {
                List<ActivityFields> actFldList = new List<ActivityFields>();
                if(selectedTab == (string)actObj.get('Id')){
                    defaultTabsLayout = (string)actObj.get(NamespaceSelector.hgprmNamespace+'LayoutName__c');
                    pltActnWrapper.defaultActTabs = (string)actObj.get('Id');
                    isDefaultSet = true;
                } else if(!isDefaultSet && (Boolean)actObj.get(NamespaceSelector.hgprmNamespace+'IsDefault__c') ){
                    defaultTabsLayout = (string)actObj.get(NamespaceSelector.hgprmNamespace+'LayoutName__c');
                    pltActnWrapper.defaultActTabs = (string)actObj.get('Id');
                }
                ActivityList actvt = new ActivityList((string)actObj.get('Id'), (string)actObj.get('MasterLabel'), (string)actObj.get('DeveloperName'), actFldList);
                actvtList.add(actvt);
            }
            loggedInUser = getLoggedInUser();
            List<string> sysFields = new List<String>{'Id','CreatedBy','CreatedById','CreatedDate','LastModifiedBy','LastModifiedById','LastModifiedDate','WhoId'};
            if(!string.isBlank(defaultTabsLayout)){
                fieldBehavMapInstance = Util.getLayoutFieldsBehavior(defaultTabsLayout);
                for(string fld : fieldBehavMapInstance.keySet()){
                    if(!sysFields.contains(fld) && 
                       Schema.getGlobalDescribe().get('Task').getDescribe().fields.getMap().containsKey(fld) && 
                       Schema.getGlobalDescribe().get('Task').getDescribe().fields.getMap().get(fld).getDescribe().isCreateable()) {
                        defaultActFldList.add(new ActivityFields(fld));
                    }
                }
            }
            pltActnWrapper.actvtListWrapper = actvtList;
            pltActnWrapper.defaultFldList = defaultActFldList;
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        }
        return JSON.serialize(pltActnWrapper);
    }
    
    /********************************
    * @Wrapper Class Name: ActivityList
    * @description: This class is used to hold Activity List
    * @Last Modified Date: 16/12/2020
    * *****************************/
    public class ActivityList {
        String label;
        string devName;
        string Id;
        List<ActivityFields> activityFields;
        public ActivityList(String Id, String label, String devName, List<ActivityFields> activityFields){
            this.label = label;
            this.devName = devName;
            this.Id = Id;
            this.activityFields = activityFields;
        }
    }
    
    /********************************
    * @Wrapper Class Name: ActivityFields
    * @description: This class is used to hold Activity Fields
    * @Last Modified Date: 16/12/2020
    * *****************************/
    public class ActivityFields {
        string apiName;
        string fieldType;
        string label;
        List<string> picklistValues;
        List<Util.MultiSelectWrapper> multiSelectPicklistValues;
        Util.LookupWrapper selItem;
        string lookupObj;
        string lkpObjLabel;
        Boolean isRequired;
        string defaultValue;
        List<string> relObjs = new List<string>();
        
        public ActivityFields(string apiName) {
            this.apiName = apiName;
            Schema.DisplayType dispType = Schema.getGlobalDescribe().get('Task').getDescribe().fields.getMap().get(apiName).getDescribe().getType();
            this.fieldType = String.valueOf(dispType);
            this.label = Schema.getGlobalDescribe().get('Task').getDescribe().fields.getMap().get(apiName).getDescribe().getLabel();
            switch on dispType {
                when BOOLEAN { }
                when DATE { }
                when MULTIPICKLIST { this.multiSelectPicklistValues = Util.getMultiSelectPicklistValues('Task', apiName); }
                when COMBOBOX { this.picklistValues = Util.getPicklistValuesObj('Task', apiName); }
                when Picklist { this.picklistValues = Util.getPicklistValuesObj('Task', apiName); }
                when Reference {
                    if(apiName == 'OwnerId'){
                        this.lookupObj = 'User';
                        if(loggedInUser != null){
                            this.selItem = new Util.LookupWrapper(loggedInUser.Name);
                            this.defaultValue = loggedInUser.Id;
                        }
                    } else {
                        List <Schema.sObjectType> RefObjs = Schema.getGlobalDescribe().get('Task').getDescribe().fields.getMap().get(apiName).getDescribe().getReferenceTo();
                        if(RefObjs.size() >0 ){
                            if(apiName.toLowerCase() != 'whatid')
                                this.lookupObj = String.valueOf(RefObjs[0]);
                            else{
                                this.lookupObj = String.valueOf(RefObjs[0]);
                                for(Schema.sObjectType obj : RefObjs ){
                                    this.relObjs.add(string.valueOf(obj));
                                }
                            }
                        }
                    }
                    this.lkpObjLabel = 'Assigned To';
                }
            }
            Schema.DescribeFieldResult field = Schema.getGlobalDescribe().get('Task').getDescribe().fields.getMap().get(apiName).getDescribe();
            this.isRequired = fieldBehavMapInstance.get(apiName) == 'Required' ? true : false; 
        }
    }
}