/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: RestPRM3IssueController
* @description: This class is to expose the Logged in Users Recent Issues.
* @Author: Healthgrades Inc.
* @Last Modified Date: 10/04/2020
**/
@RestResource(urlMapping='/prm3/v1/Issues/myIssues')
global inherited sharing class RestPRM3IssueController {

    /********************************
    * @method: doGet
    * @description: get method for Api
    * @return: List<PRM3uRestController.Activity>
    * *****************************/
	@HttpGet
    global static List<PRM3uRestController.Activity> doGet() {
        try {
            RestRequest request = RestContext.request;
            RestResponse response = RestContext.response;
            response.statusCode = 200;
            response.addHeader('Content-Type', 'application/json');
            Integer recordLimit = PRM3uRestController.maxRecordsParam(request);
            return performGetSearch(recordLimit);
        } catch(Exception RestPRM3IssueCtrlEx) {
            CoreControllerException errLogException = new CoreControllerException('RestPRM3IssueController.doGet', 
                                                                                          'Could not get activity records ==> '+RestPRM3IssueCtrlEx.getStackTraceString());
            return null;
        }
    }

	/********************************
    * @method: performGetSearch
    * @description: performing search to fetch Recent Issue
    * @return: List<PRM3uRestController.Activity>
    * *****************************/
    public static List<PRM3uRestController.Activity> performGetSearch(Integer recordLimit) {
        List<PRM3uRestController.Activity> issues = new List<PRM3uRestController.Activity>();
        String issueSql = getIssueSql();
        List<SObject> listIssueRecord = getIssueRecord(issueSql, recordLimit);
        if(listIssueRecord.size()>0) {
            for(SObject sObj:listIssueRecord) { issues.add(new PRM3uRestController.Activity(sObj)); }
        }
        return issues;
    }

    /********************************
    * @method: getIssueRecord
    * @description: performing search to fetch Issue filtered.
    * @return: List
    * *****************************/
    public static List<SObject> getIssueRecord(string sql, Integer recordLimit){
        sql += ' WHERE OwnerId'+' = \''+UserInfo.getUserId()+'\'';
        sql += ' ORDER BY CreatedDate desc';
        if(recordLimit >0) { sql += ' LIMIT '+recordLimit; }
        return Database.query(sql);
    }
    
    /********************************
    * @method: getIssueSql
    * @description: query issue object
    * @return: String
    * *****************************/
    public static String getIssueSql(){
        List<String> fields = new List<String>{
            'Id', 'CreatedDate',
			NamespaceSelector.baseNamespace+'Subject__c', NamespaceSelector.baseNamespace+'Status__c',
			NamespaceSelector.baseNamespace+'LongDescription__c',NamespaceSelector.baseNamespace+'DateTimeOpened__c',
			NamespaceSelector.baseNamespace+'Provider__r.Name'
		};
		SObjectType issueObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerIssues__c');
        if( Util.checkObjectIsReadable(issueObjToken) ) {
            return 'SELECT '+String.join(fields,',')+' FROM '+NamespaceSelector.baseNamespace+'PractitionerIssues__c';
        } else {
            CoreControllerException errLogException = new CoreControllerException('RestPRM3IssueController.getIssueSql', 
                                                                                          'User does not have access to ==> '+NamespaceSelector.baseNamespace+'PractitionerIssues__c');
            return null;
        }
    }
}