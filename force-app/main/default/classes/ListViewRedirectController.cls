/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: ListViewRedirectController
* @description: This class is to handle the redirection of list view for perticular request.
* @Author: Healthgrades Inc.
* @Last Modified Date: 10/04/2020
**/
public inherited sharing class ListViewRedirectController {
    public String defaultViewName = 'Recent';
    public String taskObjName = 'Task';

	/********************************
    * @method: redirect
    * @description: for redirecting to the perticular list view
    * @return: PageReference.
    * *****************************/
    public PageReference redirect() {
        String req = ApexPages.currentPage().getParameters().get('req');
        String url = '';

        switch on req {
            when 'issue'{
                url = getFilterUrl(getIssueTrgEntity(), 'IssueView', true);
            } when 'task'{
                url = getFilterUrl(taskObjName, 'TaskView', true);
            } when 'call'{
                url = getFilterUrl(taskObjName,'CallView', true);
            }
        }
        PageReference pageRef = new PageReference(url);
        pageRef.setRedirect(true);
        return pageRef;
    }

    /********************************
    * @method: getFilterUrl
    * @description: for preparing filter url
    * @param: String, String, boolean
    * @return: PageReference.
    * *****************************/
    public String getFilterUrl(String objectType, String viewName, Boolean getFilter) {
        String filterId = viewName;
        if(getFilter) {
            filterId = getFilterId(objectType, viewName);
            if(filterId == '') { filterId = defaultViewName; }
        }
        Util utilClass = new Util();
        String listViewUrl = '/'+utilClass.getObjNamePrefix(objectType)+'?fcf='+filterId;
        return listViewUrl;
    }

    /********************************
    * @method: getFilterId
    * @description: for finding the filter id
    * @param: String, String
    * @return: String.
    * *****************************/
    public String getFilterId(String objectType, String viewName) {
        SObject[] qData = Database.query('SELECT Id FROM ListView WHERE DeveloperName = :viewName AND SobjectType = :objectType');
        if(qData.size() > 0) {
            object Obj =  qData[0].get('Id');
            if(Obj !=null) { return Obj.toString(); } else { return ''; }
        } else {
            CoreControllerException errLogException = new CoreControllerException('ListViewRedirectController.getFilterId', 
                                                                                          'Could not access ListView ==> '+qData + ' for sObject ==> '+objectType);
            return '';
        }
    }

    public string getIssueTrgEntity() { return NamespaceSelector.baseNamespace+'PractitionerIssues__c'; }
}