/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying 
* or sale of the above may constitute an infringement of copyright 
* and may result in criminal or other legal proceedings.
*
* @Class Name: TerritoryProviderController
* @description: This class is to lightning component which written to show Provider into Map.
* @Last Modified Date: 10/05/2020
**/
public inherited sharing class TerritoryProviderController {
    List<TerritoryProviderController.PractitionerLocationWrapper> listProvLocWrapper;
    SObject saveObject;
    String geoFieldName;
    
    /********************************
    * @Method: getProviderLocations
    * @description: fetch the provider locations to show into Map as pin point
    * @return: String
    * *****************************/
    @AuraEnabled
    public static String getProviderLocations(Id territoryId, String filterIds, String filterByType) {
        try {
            TerritoryProviderController terrProvObject = new TerritoryProviderController();
            List<TerritoryProviderController.PractitionerLocationWrapper> practLocations = new List<TerritoryProviderController.PractitionerLocationWrapper>();
            Type fltrLst = Type.forName('List<String>');
            List<String> filterIdList = (List<String>) JSON.deserialize(filterIds, fltrLst);
            String plSql = getPLSql();
            List<SObject> listRecord = getPLRecord(plSql,filterIdList, filterByType);
            if(listRecord.size()>0) {
                for(SObject sObj:listRecord) {
                    try{
                        practLocations.add(new TerritoryProviderController.PractitionerLocationWrapper(sObj));
                    } catch(Exception getProviderLocationsEx) {
                        CoreControllerException getProviderLocationsException = new CoreControllerException('TerritoryProviderController.getProviderLocations', 
                                                                                                                    'Exception occured while iterating provider Locations'+getProviderLocationsEx.getStackTraceString());
                    }
                }
            }
            terrProvObject.listProvLocWrapper = practLocations;
            SObjectType terrObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Territory__c');
            Schema.DescribeFieldResult geoJsonFieldDesc = terrObjToken.getDescribe().fields.getMap().get(NamespaceSelector.baseNamespace+'GeoJSON__c').getDescribe();
            if( Util.checkObjectIsReadable(terrObjToken) && Util.checkFieldIsReadable(terrObjToken,geoJsonFieldDesc) ){
                List<SObject> territoryObj = Database.query('SELECT Id,'+NamespaceSelector.baseNamespace+'GeoJSON__c FROM '+NamespaceSelector.baseNamespace+'Territory__c WHERE Id = :territoryId LIMIT 1');
                if(territoryObj.size()>0) {
                    terrProvObject.saveObject = territoryObj[0];
                    terrProvObject.geoFieldName = NamespaceSelector.baseNamespace+'GeoJSON__c';
                }
            } else {
                CoreControllerException getProLocException = new CoreControllerException('TerritoryProviderController.getProviderLocations', 
                                                                                                 'sobjects is not insertable/readable ==> '+terrObjToken);
            }
            
            return JSON.serialize(terrProvObject);
        } catch(Exception getProvLocEx) {
            CoreControllerException getProvLocException = new CoreControllerException('TerritoryProviderController.getProviderLocations', 
                                                                                              'Exception occured while getProviderLocations'+getProvLocEx.getStackTraceString());
            return null;
        }
    }
    
    /********************************
    * @method: getPLSql
    * @description: generate soql to fetch Provider Locations
    * Param: none
    * Return type: String
    * *****************************/
    public static string getPLSql(){
        List<string> plFields = new List<string>{
            'Id',
                NamespaceSelector.baseNamespace+'Location__r.Name',
                NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'Zip__c',
                NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'Latitude__c',
                NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'Longitude__c',
                NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'PracticeGroup__r.Name',
                NamespaceSelector.baseNamespace+'Provider__r.Name',
                NamespaceSelector.baseNamespace+'Provider__r.FirstName',
                NamespaceSelector.baseNamespace+'Provider__r.LastName',
                NamespaceSelector.baseNamespace+'Provider__r.'+NamespaceSelector.baseNamespace+'InNetworkStatus__c'
		};
		SObjectType plObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocation__c');
        if( Util.checkObjectIsReadable(plObjToken) ) {
            return 'SELECT '+string.join(plFields,',')+' FROM '+NamespaceSelector.baseNamespace+'PractitionerLocation__c ';
        } else {
            CoreControllerException getplSlqCRUDException = new CoreControllerException('TerritoryProviderController.getPLSql', 
                                                                                                'Could not access sObject ==> '+NamespaceSelector.baseNamespace+'PractitionerLocation__c');
            return null;
        }
    }
    
    /********************************
    * @method: getPLRecord
    * @description: get Provider LOcation Records to show pinpoint on Map
    * Param: string, List, string
    * Return type: List
    * *****************************/
    public static List<SObject> getPLRecord(String plSql, List<Id> filterId, String filterByType){
        SObjectType plObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocation__c');
        Schema.DescribeFieldResult locationFieldDesc = plObjToken.getDescribe().fields.getMap().get(NamespaceSelector.baseNamespace+'Location__c').getDescribe();
        if( Util.checkObjectIsReadable(plObjToken) && Util.checkFieldIsReadable(plObjToken, locationFieldDesc) ){
            plSql += ' WHERE '+NamespaceSelector.baseNamespace+'Location__c != null ';
            if(filterId.size()>0) {
                if(filterByType == 'PracticeGroup') {
                    plSql += ' AND '+NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'PracticeGroup__c IN :filterId';
                } else {
                    plSql += ' AND Id IN: filterId';
                }
            }
            plSql += ' LIMIT '+(Limits.getLimitQueryRows() - Limits.getQueryRows() - 1);
            return Database.query(plSql);
        } else {
            CoreControllerException getplSlqCRUDException = new CoreControllerException('TerritoryProviderController.getPLRecord', 
                                                                                                'Could not access sObject ==> '+NamespaceSelector.baseNamespace+'PractitionerLocation__c');
            return new List<SObject>();
        }
    }
    
    /********************************
    * @Wrapper Class Name: PractitionerLocationWrapper
    * @description: This class is to hold the Provider Locations
    * @Last Modified Date: 10/05/2020
    * *****************************/
    public class PractitionerLocationWrapper {
        Public string Id;
        Public string providerLink;
        public string LocationName;
        public string practiceGroupName;
        public string ProviderName { get { return String.isBlank(this.ProviderName) ? '' : this.ProviderName; } set; }
        public Decimal Latitude;
        public Decimal Longitude;
        
        /********************************
    * @Method: PractitionerLocationWrapper
    * @description: Constructor to set the Class attribute from Object using Map
    * @return: N/A
    * *****************************/
        public PractitionerLocationWrapper(Sobject sobj) {
            this.LocationName = (string)Sobj.getSObject(NamespaceSelector.baseNamespace+'Location__r').get('Name');
            this.Latitude = (Double)Sobj.getSObject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'Latitude__c');
            this.Longitude = (Double)Sobj.getSObject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'Longitude__c');
            if(Sobj.getSObject(NamespaceSelector.baseNamespace+'Location__r').getSObject(NamespaceSelector.baseNamespace+'PracticeGroup__r') !=null){
                this.practiceGroupName = (string)Sobj.getSObject(NamespaceSelector.baseNamespace+'Location__r').getSObject(NamespaceSelector.baseNamespace+'PracticeGroup__r').get('Name');
            }
            if(Sobj.getSObject(NamespaceSelector.baseNamespace+'Provider__r') !=null){
                this.providerLink = (string)Sobj.getSObject(NamespaceSelector.baseNamespace+'Provider__r').get('Id');
                this.ProviderName = (string)Sobj.getSObject(NamespaceSelector.baseNamespace+'Provider__r').get('Name');
            }
            this.Id = (String)sObj.get('Id');
        }
        
    }
}