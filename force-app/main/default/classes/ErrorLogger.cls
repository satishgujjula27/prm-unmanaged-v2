// Deprecated
global with sharing class ErrorLogger {
  
 //Deprecated 
  global enum StatusCode {
    SUCCESS,
    FAILED,
    PARTIAL,
    WARN
  }

  
 // Deprecated     
  global virtual class Log {
    public String description;
    public String JSON;
   
  }
  // Deprecated
  global class SuccessLog extends Log {
    /**
     * Log entry for successful operations
     * @param  related     The related record or className
     * @param  message     Short message for this log entry
     * @param  description Extended details for this log entry
     */
    // Deprecated
    global SuccessLog(String related, String message, String description) {
      
    }
    // Deprecated
    global SuccessLog(
      String related,
      String message,
      String description,
      LoggingLevel logLevel
    ) {
      
    }
   // Deprecated
    global SuccessLog(
      String related,
      String message,
      String description,
      LoggingLevel logLevel,
      ErrorLogger.StatusCode statusCode
    ) {
      
    }
  }
  // Deprecated
  global class JSONErrorLog extends Log {
    // Deprecated  
    global JSONErrorLog(
      String relatedExternalId,
      List<String> errorList,
      String JSON
    ) {
      
    }
  }
  // Deprecated	
  global class ExceptionLog extends Log {
    // Deprecated  
    global ExceptionLog(Exception ex) {
     
    }
	// Deprecated
    global ExceptionLog(String recordId, String userMessage, Exception ex) {
     
    }
	// Deprecated
    global ExceptionLog(String recordId, String message, String detail) {
      
    }
	// Deprecated
    global ExceptionLog(String recordId, String message) {
     
    }
	// Deprecated
    global ExceptionLog(String recordId, List<Database.Error> ErrorMsg) {
    } 
  }

  // Deprecated
  global void createLog(Log l) {
    // Error Logging to database is currently deprecated
  }

  
}