/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: LookupController
* @description: This class is to lookup component
* @Author: Healthgrades Inc.
* @Last Modified Date: 12/27/2020
**/
public with sharing class LookupController {
    
    /********************************
    * @description - Returns JSON of list of ResultWrapper to Lex Components
    * @objectName - Name of SObject
    * @fldAPIText - API name of field to display to user while searching
    * @fldAPIVal - API name of field to be returned by Lookup COmponent
    * @lim   - Total number of record to be returned
    * @fldAPISearch - API name of field to be searched
    * @searchText - text to be searched
    * *****************************/
    @AuraEnabled(cacheable=true)
    public static String searchDB(String objectName, String fldAPIText, String fldAPIVal, Integer lim, String fldAPISearch, String searchText ) {
        List<ResultWrapper> lstRet = new List<ResultWrapper>();
        try {
            List<sObject> sobjList = new List<sObject>();
            SObjectType searchDBToken = Schema.getGlobalDescribe().get(objectName);
            String soslTextString = searchText;
            if( Util.checkObjectIsReadable(searchDBToken) ) {
                String query; searchText = '\'%' + String.escapeSingleQuotes(searchText.trim()) + '%\'';
                if( objectName != NamespaceSelector.baseNamespace+'Speciality__c' && (objectName == 'Account' || objectName == 'Contact') ) {
                    String searchquery;
                    if( !String.isEmpty(fldAPIText) || !String.isEmpty(fldAPIVal) ) {
                        searchquery = 'FIND {' + soslTextString + '*} IN ALL FIELDS RETURNING '+objectName+'('+fldAPIText+' ,'+fldAPIVal+') LIMIT '+lim;
                    }
                    List<List<SObject>>searchList = search.query(searchquery);
                    sobjList = (List<SObject>)searchList[0];
                } else if( objectName != NamespaceSelector.baseNamespace+'Speciality__c' && (objectName != 'Account' || objectName != 'Contact') ) {
                    if( objectName == NamespaceSelector.baseNamespace+'Location__c') {
                        query = 'SELECT '+fldAPIText+' ,'+fldAPIVal+' ,MailingAddress__c FROM '+objectName+' WHERE '+fldAPISearch+' LIKE '+searchText;
                    } else {
                        query = 'SELECT '+fldAPIText+' ,'+fldAPIVal+' FROM '+objectName+' WHERE '+fldAPISearch+' LIKE '+searchText;
                    }
                    query += ' LIMIT '+lim;
                    sobjList = Database.query(query);
                } else {
                    query = 'SELECT '+fldAPIText+' ,'+fldAPIVal+' ,'+NamespaceSelector.baseNamespace+'Classification__c ,'+NamespaceSelector.baseNamespace+'Specialization__c FROM '+objectName;
                    query += ' WHERE ( ('+NamespaceSelector.baseNamespace+'Grouping__c LIKE '+searchText+') OR ('+NamespaceSelector.baseNamespace+'Classification__c LIKE '+searchText+')';
                    query += ' OR ('+NamespaceSelector.baseNamespace+'Specialization__c LIKE '+searchText+') ) WITH SECURITY_ENFORCED';
                    query += ' LIMIT '+lim;
                    sobjList = Database.query(query);
                }
                
                for(SObject thisSObj : sobjList) {
                    ResultWrapper resultWrapperObj = new ResultWrapper();
                    resultWrapperObj.objName = objectName;
                    if( objectName == NamespaceSelector.baseNamespace+'Speciality__c' ) {
                        resultWrapperObj.text = String.valueOf(thisSObj.get(NamespaceSelector.baseNamespace+'Specialization__c'));
                        resultWrapperObj.subtext1 = String.valueOf(thisSObj.get(fldAPIText));
                        resultWrapperObj.subtext2 = String.valueOf(thisSObj.get(NamespaceSelector.baseNamespace+'Classification__c'));
                    } else if( objectName == NamespaceSelector.baseNamespace+'Location__c' ) {
                        resultWrapperObj.text = String.valueOf(thisSObj.get(fldAPIText));
                        resultWrapperObj.subtext1 = String.valueOf(thisSObj.get(NamespaceSelector.baseNamespace+'MailingAddress__c')).replaceAll('<[^>]+>',' ');
                    } else {
                        resultWrapperObj.text = String.valueOf(thisSObj.get(fldAPIText));
                    }
                    resultWrapperObj.val = String.valueOf(thisSObj.get(fldAPIVal))  ;
                    lstRet.add(resultWrapperObj);
                }
                
            } else {
                CoreControllerException searchCRUDDBException = new CoreControllerException('LookupController', 
                                                                                            		'Could not access sObject ==> '+objectName);
            } 
        } catch(Exception searchDBEx) {
            CoreControllerException searchDBException = new CoreControllerException('LookupController', 
                                                                                            'Could not get records ==> '+searchDBEx.getStackTraceString());
        }
        return JSON.serialize(lstRet);
    }
    
    /********************************
    * @method: ResultWrapper
    * @description: wrapper for lookup results
    * @return: wrapper
    * *****************************/
    public class ResultWrapper {
        public String objName {get;set;}
        public String text{get;set;}
        public String subtext1{get;set;}
        public String subtext2{get;set;}
        public String val{get;set;}
    }
}