/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: AddToMyTerritoryController
* @description: This Class is used to handle addToMyTerritory Functionality
* @Last Modified Date: 10/06/2020
**/
public inherited sharing class AddToMyTerritoryController {
    List<AddToMyTerritoryController.ProviderLocWrapper> provLocWrapper;
    List<AddToMyTerritoryController.TerritoryWrapper> territoryList;
    List<plWrapperFieldSet> fieldsWrapper;
    String providerName;
    
    /********************************
    * @method: getPlRecords
    * @description: aura enabled method to fetch all provider locations for a contact.
    * @return: serialized string of records.
    * *****************************/
    @AuraEnabled
    public static String getPlRecords(Id recordId) {
        try{
            AddToMyTerritoryController aplObject = new AddToMyTerritoryController();
            List<AddToMyTerritoryController.ProviderLocWrapper> provLocList = new List<AddToMyTerritoryController.ProviderLocWrapper>();
            List<AddToMyTerritoryController.TerritoryWrapper> territoryList = new List<AddToMyTerritoryController.TerritoryWrapper>();
            // setting table field
            List<plWrapperFieldSet> flswrapper = new List<plWrapperFieldSet>();
            flswrapper.add(new plWrapperFieldSet('LocationName', 'LOCATION NAME','url','lLinkId'));
            flswrapper.add(new plWrapperFieldSet('Primary', 'Primary','text','Primary'));
            flswrapper.add(new plWrapperFieldSet('address', 'MAILING ADDRESS','text','address'));
            flswrapper.add(new plWrapperFieldSet('phone', 'PHONE','text','phone'));
            aplObject.fieldsWrapper = flswrapper;
            
            List<SObject> provRecord = database.query('SELECT Name FROM Contact Where Id = :recordId LIMIT 1');
            if(provRecord.size() > 0 && Schema.sObjectType.Contact.fields.Name.isAccessible()){
                aplObject.providerName = (string)provRecord[0].get('Name');
            } else {
                return null;
            }
            
            String pltSql = getPltQuery();
            List<SObject> listPlRecord = getPlRecord(pltSql, recordId);
            Set<String> practLocIds = new Set<String>();
            if(listPlRecord.size()>0) {
                for(SObject sObj:listPlRecord) {
                    practLocIds.add((String)Sobj.get('Id'));
                    provLocList.add(new AddToMyTerritoryController.ProviderLocWrapper(sObj));
                }
            }
            
            aplObject.provLocWrapper = provLocList;
            
            String terrSql = getTerrQuery(practLocIds);
            List<SObject> listTerrRecord = getTerrRecord(terrSql);
            if(listTerrRecord.size()>0) {
                for(SObject sObj:listTerrRecord) {
                    territoryList.add(new AddToMyTerritoryController.TerritoryWrapper(sObj));
                }
            }
            aplObject.territoryList = territoryList;
            return JSON.serialize(aplObject);
        } catch(Exception getPlRecordsEx) {
            CoreControllerException errLogException = new CoreControllerException('AddToMyTerritoryController.getPlRecords', 
                                                                                          'Could not get provider Locations ==> '+getPlRecordsEx.getStackTraceString());
            return null;
        }
    }
    
    /********************************
    * @method: getPlRecord
    * @description: utility method to query Provider Location based on provider id.
    * @return: list of sobject.
    * *****************************/
    public static List<Sobject> getPlRecord(string sql, Id recordId){
        sql += ' WHERE '+NamespaceSelector.baseNamespace+'Provider__r.Id = :recordId';
        return Database.query(sql);
    }
    
    /********************************
    * @method: getTerrRecord
    * @description: utility method to query Territory based on ownerid.
    * @return: list of sobject.
    * *****************************/
    public static List<Sobject> getTerrRecord(string sql){
        sql += ' WHERE OwnerId = \'' + userinfo.getuserid() + '\'';
        return Database.query(sql);
    }
    
    public static string getPltQuery(){
        List<String> fields = new List<String>{
            'Id', NamespaceSelector.baseNamespace+'Location__r.Name',
			NamespaceSelector.baseNamespace+'IsPrimary__c',
			NamespaceSelector.baseNamespace+'Provider__r.Name',
			NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'Telephone__c',
			NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'AddressLine1__c',
			NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'AddressLine2__c',
			NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'AddressLine3__c',
			NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'PracticeGroup__r.Name'
		};
		return 'SELECT '+String.join(fields,',')+' FROM '+NamespaceSelector.baseNamespace+'PractitionerLocation__c';
    }
    
    public static string getTerrQuery(Set<String> practLocIds){
        string childQuery = '( SELECT '+NamespaceSelector.baseNamespace+'PractitionerLocation__c FROM '+NamespaceSelector.baseNamespace+'PracLocationTerritories__r';
        childQuery+= ' WHERE '+Util.toSqlWhere(NamespaceSelector.baseNamespace+'PractitionerLocation__c', 'IN', Util.joinForIn(practLocIds),null)+' )';
        List<String> fields = new List<String>{ 'Id', 'Name', childQuery };
        return 'SELECT '+String.join(fields,',')+' FROM '+NamespaceSelector.baseNamespace+'Territory__c';
    }
    
    public class ProviderLocWrapper {
        Public string Id;
        public string LocationName;
        public string address;
        public boolean isPrimary;
        public string lLinkId;
        
        /********************************
        * @Method: LocationWrapper
        * @description: Constructor to set the Class attribute from Object using Map
        * @return: N/A
        * *****************************/
        public ProviderLocWrapper(Sobject sobj) {
            this.Id = (String)sObj.get('Id');
            if(Sobj.getSObject(NamespaceSelector.baseNamespace+'Location__r') !=null){
                this.LocationName = (String)sObj.getSobject(NamespaceSelector.baseNamespace+'Location__r').get('Name');
                this.lLinkId = '/'+(String)sObj.getSobject(NamespaceSelector.baseNamespace+'Location__r').get('Id');
                this.isPrimary = (boolean)sObj.get(NamespaceSelector.baseNamespace+'IsPrimary__c');
                if(sObj.getSobject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'AddressLine1__c') !=null)
                    this.address = (String)sObj.getSobject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'AddressLine1__c') + ' ';
                if(sObj.getSobject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'AddressLine2__c') !=null)
                    this.address += (String)sObj.getSobject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'AddressLine2__c') + ' ';
                if(sObj.getSobject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'AddressLine3__c') !=null)
                    this.address += (String)sObj.getSobject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'AddressLine3__c');
            }
        }
    }
    
    /********************************
    * @Method: TerritoryWrapper
    * @description: Save practitioner location territory record.
    * @return: String
    * *****************************/
    public class TerritoryWrapper {
        Public String Id;
        Public String TerritoryName;
        Public List<String> pltList = new List<String>();
        
        /********************************
        * @Method: TerritoryWrapper
        * @description: Constructor to set the Class attribute from Object using Map
        * @return: N/A
        * *****************************/
        public TerritoryWrapper(SObject sobj) {
            this.Id = (String)sObj.get('Id');
            this.TerritoryName = (String)sObj.get('Name');
            for(SObject child : sObj.getSObjects(NamespaceSelector.baseNamespace+'PracLocationTerritories__r')) {
                this.pltList.add((String)child.get(NamespaceSelector.baseNamespace+'PractitionerLocation__c'));
            }
        }
    }
    
    /********************************
    * @Method: saveTerrPlRec
    * @description: Save practitioner location territory record.
    * @return: String
    * *****************************/
    @AuraEnabled
    public static String saveTerrPlRec(string terrPLRec) {
        SuccessWrapper succWrapper = new SuccessWrapper();
        try{
            SObject sObj = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c').newSObject();
            SObjectType pracLocTerrObjToken = sObj.getSObjectType();
            List<Sobject> pltObjList = new List<Sobject>();
            List<Sobject> removedPLTObj = new List<Sobject>();
            List<TerrPlWrapper> terrList = (List<TerrPlWrapper>) JSON.deserialize(terrPLRec, List<TerrPlWrapper>.class);
            if(terrList.size() > 0 && Util.checkObjectIsReadable(pracLocTerrObjToken) ) {
                for(TerrPlWrapper obj : terrList) {
                    if(obj.selPlIds.size() > 0) {
                        if(obj.isNewTerritory) {
                            obj.terrId = createTerritory(obj.terrId);
                        }
                        for(string pl:obj.newPlIds) {
                            Sobject pltObj = sObj.clone();
                            pltObj.put(NamespaceSelector.baseNamespace+'Territory__c', obj.terrId);
                            pltObj.put(NamespaceSelector.baseNamespace+'PractitionerLocation__c', pl);
                            pltObjList.add(pltObj);
                        }
                    }
                    if(obj.removedPlIds.size() > 0){
                        string terrId = obj.terrId;
                        List<string> rmIds = obj.removedPlIds;
                        List<Sobject> delPltRec = Database.query('SELECT Id FROM '+NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c WHERE Id != null AND '+NamespaceSelector.baseNamespace+'Territory__c = :terrId AND '+NamespaceSelector.baseNamespace+'PractitionerLocation__c IN :rmIds');
                        if(delPltRec.size() > 0){
                            for(Sobject dlpltObj:delPltRec) {
                                removedPLTObj.add(dlpltObj);
                            }
                        }
                    }
                }
            }
            
            if(pltObjList.size() > 0 && Util.checkObjectIsReadable(pracLocTerrObjToken) && Util.checkObjectIsInsertable(pracLocTerrObjToken) ) {
                Database.insert(pltObjList);
            }
            if(removedPLTObj.size() > 0 && Util.checkObjectIsReadable(pracLocTerrObjToken) && Util.checkObjectIsInsertable(pracLocTerrObjToken) ) {
                Database.delete(removedPLTObj);
            }
            succWrapper.created = pltObjList.size();
            succWrapper.removed = removedPLTObj.size();
            return JSON.serialize(succWrapper);
        } catch(Exception saveTerrPlRecEx) {
            CoreControllerException saveTerrPlRecErrLogException = new CoreControllerException('AddToMyTerritoryController.saveTerrPlRec', 
                                                                                                       'Could not save provider Locations territory==> '+saveTerrPlRecEx.getStackTraceString());
            return null;
        }
    }
    
    /********************************
    * @Method: createTerritory
    * @description: create new territory record.
    * @return: String
    * *****************************/
    public static String createTerritory(string terrName) {
        SObject territoryObj = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Territory__c').newSObject();
        SObjectType territoryObjToken = territoryObj.getSObjectType();
        Schema.DescribeFieldResult terrFieldDesc = territoryObjToken.getDescribe().fields.getMap().get('Name').getDescribe();
        if( Util.checkObjectIsReadable(territoryObjToken) && Util.checkFieldIsInsertable(territoryObjToken, terrFieldDesc) ) {
            territoryObj.put('Name', terrName);
            Database.insert(territoryObj);
        } else {
            CoreControllerException createTerrErrLogException = new CoreControllerException('AddToMyTerritoryController.createTerritory', 
                                                                                                    'sobject field is not accessible ==> '+territoryObjToken);
            return null;
        }
        return territoryObj.Id;
    }
    
    /********************************
    * @Method: TerrPlWrapper
    * @description: territory prac location wrapper
    * @return: wrapper class
    * *****************************/
    public class TerrPlWrapper {
        public String terrId;
        public List<String> selPlIds;
        Public List<String> newPlIds;
        public List<String> removedPlIds;
        public Boolean isNewTerritory;
    }
    
    /********************************
    * @Method: SuccessWrapper
    * @description: wrapper for success message
    * @return: wrapper class
    * *****************************/
    public class SuccessWrapper {
        public Integer created;
        public Integer removed;
    }
    
    /********************************
    * @Method: plWrapperFieldSet
    * @description: wrapper for success message
    * @return: wrapper class
    * *****************************/
    public class plWrapperFieldSet {
        @AuraEnabled public String fieldName;
        @AuraEnabled public String label;
        @AuraEnabled public String fieldType;
        @AuraEnabled public Boolean sortable;
        @AuraEnabled public String linkfieldName;
        public plWrapperFieldSet(String fieldName, String label, String fieldType, String linkfieldName) {
            this.fieldName = fieldName;
            this.label = label;
            this.fieldType = fieldType;
            this.sortable = false;
            this.linkfieldName = linkfieldName;
        }
    }
    
}