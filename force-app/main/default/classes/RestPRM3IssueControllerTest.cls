/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: RestPRM3IssueControllerTest
* @description: This is the test class for RestPRM3IssueController
* @Last Modified Date: 03/26/2019
**/
@isTest
public class RestPRM3IssueControllerTest {

	@isTest
    static void doGetTest(){
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/prm3/v1/Issues/myIssues';
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json');
        RestContext.request = req;
        RestContext.response = res;
        List<PRM3uRestController.Activity> result = RestPRM3IssueController.doGet();
        Test.stopTest();
        System.assertEquals(0, result.size());
    }

    @isTest
    static void performGetSearchTest(){
        // Create Test Contact
        Contact testContact = new Contact ();
        testContact.LastName = 'Test';
        insert testContact;
        
        // Create Issue
        Sobject issueObj= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerIssues__c').newSObject();
        issueObj.put(NamespaceSelector.baseNamespace+'Subject__c', 'Test Issue');
        issueObj.put(NamespaceSelector.baseNamespace+'Status__c', 'Working');
        issueObj.put(NamespaceSelector.baseNamespace+'Provider__c', testContact.Id);
        issueObj.put(NamespaceSelector.baseNamespace+'DateTimeOpened__c', System.now());
        issueObj.put(NamespaceSelector.baseNamespace+'LongDescription__c', 'Test');
        insert issueObj;
        
        Test.startTest();
        List<PRM3uRestController.Activity> result = RestPRM3IssueController.performGetSearch(1);
        Test.stopTest();
        System.assertEquals(1, result.size());
    }
}