/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: NamespaceSelectorTest
* @description: This test class is for NamespaceSelector.
* @Last Modified Date: 09/19/2020
**/
@isTest
private class NamespaceSelectorTest {

    /**@@
    #DESCRIPTION   : Test method for baseNamespace
    #Paramaters    : None
    @@**/
    static testMethod void testNamespace() {
        if( !String.isEmpty(NamespaceSelector.baseNamespace) ) {
            System.assertEquals(NamespaceSelector.baseNamespace.replace('__', '__'),'EDL__');
        } else {
            System.assertEquals(NamespaceSelector.baseNamespace,NamespaceSelector.class.getName().substringBefore('NamespaceSelector'));
        }
    }
    
    /**@@
    #DESCRIPTION   : Test method for hgprmNamespace
    #Paramaters    : None
    @@**/
    static testMethod void testHGPRMNamespace() {
        if( !String.isEmpty(NamespaceSelector.hgprmNamespace) ) {
            System.assertEquals(NamespaceSelector.hgprmNamespace.replace('__', '__'),NamespaceSelector.class.getName().substringBefore('NamespaceSelector').replace('.','__'));    
        } else {
            System.assertEquals(NamespaceSelector.hgprmNamespace,NamespaceSelector.class.getName().substringBefore('NamespaceSelector'));
        }
    }
}