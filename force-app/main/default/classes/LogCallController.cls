/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: LogCallController
* @description: This class is to log Calls
* @Author: Healthgrades Inc.
* @Last Modified Date: 10/05/2020
**/
public inherited sharing class LogCallController {

    @AuraEnabled Public List<LogCallWrapperFieldSet> fieldsWrapper = new List<LogCallWrapperFieldSet>();
    @AuraEnabled Public Integer totalCount;
    @AuraEnabled Public String providerEntityName;
    @AuraEnabled Public List<PractitionerLocationWrapper> resultWrapper = new List<PractitionerLocationWrapper>();
    @AuraEnabled Public List<PractitionerLocationWrapper2> resultWrapper2 = new List<PractitionerLocationWrapper2>();
    private static Map<String, List<Schema.PickListEntry>> picklists = new Map<String, List<Schema.PickListEntry>>();
    public static string sfdcBaseUrl = URL.getSalesforceBaseUrl().toExternalForm();
    List<ActivityList> actvtListWrapper = new List<ActivityList>();
    List<ActivityFields> defaultFldList = new List<ActivityFields>();
    string defaultActTabs;
    Static User loggedInUser;
    public static Map<String,String> fieldBehavMapInstance;

    /********************************
    * @method: getPicklistValues
    * @description: get the picklist values to show on UI
    * @param: none
    * @return: Map of field and picklist values
    * *****************************/
    @AuraEnabled
    public static Map<String, List<String>> getPicklistValues() {
        return Util.getPicklistValServices();
    }

    /********************************
    * @method: getProviders
    * @description: get providers to log call
    * Param: string,boolean,string,string,string,string
    * Return type: string
    * *****************************/
    @AuraEnabled
    public static String getProviders(String searchKey, Boolean isAlphaSearch, String filterBy, String filterOpt, String sortField, String sortDirection ) {
        LogCallController wrapCls = new LogCallController();
        List<LogCallWrapperFieldSet> flswrapper = new List<LogCallWrapperFieldSet>();
        List<PractitionerLocationWrapper> practLocations = new List<PractitionerLocationWrapper>();
        try {
            String plSql = Util.getPLSql();
            List<Sobject> listRecord = getProviderRecord(plSql, filterBy, filterOpt, sortField, sortDirection, isAlphaSearch, searchKey);
            Set<String> uniqueProvLocs = new Set<String>();
            for(SObject sObj:listRecord) {
                if(!uniqueProvLocs.contains((String)sObj.getSObject(NamespaceSelector.baseNamespace+'Provider__r').get('Id'))){
                    practLocations.add(new PractitionerLocationWrapper(sObj));
                    uniqueProvLocs.add((String)sObj.getSObject(NamespaceSelector.baseNamespace+'Provider__r').get('Id'));
                }
            }
            wrapCls.fieldsWrapper = setWrapperFieldSet();
            wrapCls.resultWrapper = practLocations;
            wrapCls.providerEntityName = 'Contact';
            return JSON.serialize(wrapCls);
        } catch(Exception getProvidersEx) {
            CoreControllerException getProvidersException = new CoreControllerException('LogCallController.getProviders',
                                                                                                'Could not load Provider Locations'+getProvidersEx.getStackTraceString());
            return null;
        }
    }

    /********************************
    * @method: getProviders
    * @description: get provider Locations to log call / Create Task
    * Param: string,boolean,string,string,string,string
    * Return type: string
    * *****************************/
    @AuraEnabled
    public static String getProviders2(String searchKey, Boolean isAlphaSearch, String filterBy, String filterOpt, String sortField, String sortDirection ) {
        LogCallController wrapCls = new LogCallController();
        List<PractitionerLocationWrapper2> practLocations = new List<PractitionerLocationWrapper2>();
        try {
            String plSql = getPLSql2();
            List<Sobject> listRecord = getProviderLocRecord(plSql, filterBy, filterOpt, sortField, sortDirection, isAlphaSearch, searchKey);
            for(Sobject sObj:listRecord) { practLocations.add(new PractitionerLocationWrapper2(sObj)); }
            wrapCls.fieldsWrapper = setWrapperFieldSet2();
            wrapCls.resultWrapper2 = practLocations;
            wrapCls.providerEntityName = 'Contact';
            return JSON.serialize(wrapCls);
        } catch(Exception getProviders2Ex) {
            CoreControllerException getProviders2Exception = new CoreControllerException('LogCallController.getProviders2',
                                                                                                 'Could not load Providers'+getProviders2Ex.getStackTraceString());
            return null;
        }
    }

    /********************************
    * @method: saveTask
    * @description: save Task Records
    * Param: Task, Task, boolean, boolean, List
    * Return type: Task
    * *****************************/
    @AuraEnabled
    public static Boolean saveTask(Task task, Task followupTask, Boolean isCreateTask, Boolean iscreateFollowupTask, String thisRecordTypeName, List<String> providerList) {
        Boolean finalResult;
        try{
            if(Schema.SObjectType.Task.isAccessible() && Schema.SObjectType.Task.isCreateable()) {
                List<Task> tasks = new List<Task>();
                Id recTypeId;
                if( !String.isEmpty(thisRecordTypeName) ) {
                    recTypeId = Util.getTaskRecordType(thisRecordTypeName);
                } else {
                    recTypeId = Util.getTaskRecordType('PRM');
                }
                if(isCreateTask) {
                    for(String pId:providerList) {
                        task call = task.clone(false, true, false, false);
                        call.WhoId = pId;
                        call.TaskSubtype = 'Call';
                        call.Status = 'Completed';
                        if(recTypeId != null){ call.RecordTypeId = recTypeId; }
                        tasks.add(call);
                    }
                    Database.insert(tasks);
                }
                if(iscreateFollowupTask) {
                    // directly insert the task no matter what.
                    if( providerList.size() == 1) {
                        if( !providerList.isEmpty() ) {
                            followupTask.WhoId = Id.valueOf(providerList.get(0));
                            if(recTypeId != null){ followupTask.recordTypeId = recTypeId; }
                            Database.insert(followupTask, false);
                        }
                    } else if( providerList.size() != 0 && providerList.size() > 1) {
                        for(String pId:providerList) {
                            Task flwUpTask = new Task();
                            flwUpTask.put('Subject', followupTask.Subject);
                            flwUpTask.put('ActivityDate', Date.valueof(followupTask.ActivityDate));
                            flwUpTask.put('Priority', followupTask.Priority);
                            flwUpTask.put('Status', followupTask.Status);
                            flwUpTask.put('Description', followupTask.Description);
                            flwUpTask.put('WhoId', pId);
                            if(followupTask.WhatId != null) { flwUpTask.put('WhatId', followupTask.WhatId); }
                            if(followupTask.IsReminderSet) {
                                if(followupTask.IsReminderSet != false && followupTask.ReminderDateTime != null) {
                                    flwUpTask.put('IsReminderSet', true);
                                    flwUpTask.put('ReminderDateTime', followupTask.ReminderDateTime);
                                }
                            }
                            if(recTypeId != null) { flwUpTask.put('RecordTypeId', recTypeId); }

                            tasks.add(flwUpTask);
                        }

                        Database.insert(tasks, false);
                    }
                }
                finalResult = true;
            } else {
                CoreControllerException saveTaskLogException = new CoreControllerException('LogCallController.saveTask',
                                                                                                   'sobjects is not insertable/readable ==> Task');
                finalResult = false;
            }
        } catch(Exception saveTaskEx) {
            CoreControllerException saveTaskcatchException = new CoreControllerException('LogCallController.saveTask',
                                                                                                 'Could not save Tasks'+saveTaskEx.getStackTraceString());
            finalResult = false;
        }
        return finalResult;
    }

    /********************************
    * @method: getFilterOptions
    * @description: get filter Option using territory or practice group
    * @param: string
    * @return: string
    * *****************************/
    @AuraEnabled
    public static string getFilterOptions(String filterType) {
        try {
            List<Util.FilterOptionWrapper> filterOpt = new List<Util.FilterOptionWrapper>();
            String sourceEntityName ='';
            Map<String, List<String>> reqFieldMap;
            switch on filterType {
                when 'territory'{ filterOpt = getTerritories(); }
                when 'practice'{ filterOpt = getPractices(); }
            }
            return JSON.serialize(filterOpt);
        } catch(Exception getFilterOptionsEx) {
            CoreControllerException getFilterOptionsException = new CoreControllerException('LogCallController.getFilterOptions',
                                                                                                    'Exception occured while doing change of filter Options'+getFilterOptionsEx.getStackTraceString());
            return null;
        }
    }

    /********************************
    * @method: getTerritories
    * @description: to get the territories which are associated with at least one provider and owned by logged in user
    * Param: none
    * Return type: List
    * *****************************/
    public static List<Util.FilterOptionWrapper> getTerritories() {
        List<Util.FilterOptionWrapper> filterOpt = new List<Util.FilterOptionWrapper>();
        SObjectType territoryObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Territory__c');
        if ( Util.checkObjectIsReadable(territoryObjToken) ) {
            String territorySql = 'SELECT Id, Name FROM '+NamespaceSelector.baseNamespace+'Territory__c';
            if(territorySql != null) {
                territorySql += ' WHERE '+NamespaceSelector.baseNamespace+'Total_providers__c > 0 AND OwnerId = \'' + UserInfo.getUserId() + '\' ORDER BY Name ASC';
                List<SObject> listRecord  = Database.query(territorySql);
                if(listRecord.size()>0) {
                    for(SObject sObj:listRecord) { filterOpt.add(new Util.FilterOptionWrapper(sObj)); }
                }
            }
            return filterOpt;
        } else {
            CoreControllerException methodException = new CoreControllerException('LogCallController.getTerritories',
                                                                                          'sobjects is not insertable/readable ==> '+territoryObjToken);
            return null;
        }
    }

    /********************************
    * @method: getPractices
    * @description: to get the territories owned by logged in user
    * Param: none
    * Return type: List
    * *****************************/
    public static List<Util.FilterOptionWrapper> getPractices(){
        Integer picklistLm = 1000;
        List<Util.FilterOptionWrapper> filterOpt = new List<Util.FilterOptionWrapper>();
        SObjectType pracLocTerrObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c');
        SObjectType provPracGroupObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'ProviderPracticeGroup__c');
        if (Util.checkObjectIsReadable(pracLocTerrObjToken) && Util.checkObjectIsReadable(provPracGroupObjToken)) {
            // Get providers from PractitionerLocationTerritory__c object
            String getProviderSql = 'SELECT '+NamespaceSelector.baseNamespace+'PractitionerLocation__r.'+NamespaceSelector.baseNamespace+'Provider__c providerId'+
                ' FROM '+NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c WHERE '+NamespaceSelector.baseNamespace+'Territory__r.OwnerId = \'' +
                UserInfo.getUserId() + '\''+ ' GROUP BY '+NamespaceSelector.baseNamespace+'PractitionerLocation__r.'+NamespaceSelector.baseNamespace+'Provider__c LIMIT '+picklistLm;
            List<AggregateResult> providerAgs = Database.query(getProviderSql);
            Set<Id> providerIds = new Set<Id>();
            for (AggregateResult ag : providerAgs) { providerIds.add((Id) ag.get('providerId')); }
            
            // Get Practices fron ProviderPracticeGroup__c object
            String getPracticeSql = 'SELECT '+NamespaceSelector.baseNamespace+'PracticeGroup__c practiceId FROM '+NamespaceSelector.baseNamespace+'ProviderPracticeGroup__c'+
                ' WHERE '+NamespaceSelector.baseNamespace+'Provider__C IN :providerIds GROUP BY '+NamespaceSelector.baseNamespace+'PracticeGroup__c LIMIT '+picklistLm;
            List<AggregateResult> practiceAgs = Database.query(getPracticeSql);
            Set<Id> practiceIds = new Set<Id>();
            for (AggregateResult ag : practiceAgs) { practiceIds.add((Id) ag.get('practiceId')); }
            if(!practiceIds.isEmpty()) {
                if (!Schema.sObjectType.Account.fields.Name.isAccessible() ) {
                    CoreControllerException methodException = new CoreControllerException('LogCallController.getPractices',
                                                                                                  'sobjects is not readable ==> Account');
                    return null;
                } else {
                    String practiceSql = 'SELECT Id, Name from Account WHERE Id IN :practiceIds';
                    list<SObject> listRecord = Database.query(practiceSql);
                    if(listRecord.size() > 0) {
                        for(SObject sObj:listRecord) { filterOpt.add(new Util.FilterOptionWrapper(sObj)); }
                    }
                    // Sort using the custom compareTo() method
                    filterOpt.sort();
                }
            }
            return filterOpt;
        } else {
            CoreControllerException methodException = new CoreControllerException('LogCallController.getPractices',
                                                                                          'sobjects is not insertable/readable ==> '+pracLocTerrObjToken);
            return null;
        }
    }

    /********************************
    * @method: getPLSql2
    * @description: to prepare soql for fetch Contacts/Provider
    * Param: none
    * Return type: string
    * *****************************/
    public static String getPLSql2() {
        List<String> plFields = new List<String>{ 'Id', 'FirstName', 'LastName', 'Name' };
		if ( Schema.sObjectType.Contact.fields.Name.isAccessible() ) {
            return 'SELECT '+String.join(plFields,',')+' FROM Contact';
        } else {
            CoreControllerException methodException = new CoreControllerException('LogCallController.getPLSql2',
                                                                                          'sobjects is not insertable/readable ==> Account');
            return null;
        }
    }

    /********************************
    * @method: setWrapperFieldSet
    * @description: to set columns to show into Datatable on home page log call
    * Param: none
    * Return type: List
    * *****************************/
    public static List<LogCallWrapperFieldSet> setWrapperFieldSet(){
        List<LogCallWrapperFieldSet> flswrapper = new List<LogCallWrapperFieldSet>();
        flswrapper.add(new LogCallWrapperFieldSet('firstName', 'Provider: FirstName','url','fLinkId'));
        flswrapper.add(new LogCallWrapperFieldSet('lastName', 'Provider: LastName','url','lLinkId'));
        return flswrapper;
    }

    /********************************
    * @method: setWrapperFieldSet
    * @description: to set columns to show into Datatable on Practice Record page log call
    * Param: none
    * Return type: List
    * *****************************/
    public static List<LogCallWrapperFieldSet> setWrapperFieldSet2(){
        List<LogCallWrapperFieldSet> flswrapper = new List<LogCallWrapperFieldSet>();
        flswrapper.add(new LogCallWrapperFieldSet('firstName', 'PROVIDER: FIRST NAME','url','fLinkId'));
        flswrapper.add(new LogCallWrapperFieldSet('lastName', 'PROVIDER: LAST NAME','url','lLinkId'));
        return flswrapper;
    }

    /********************************
    * @Wrapper Class Name: PractitionerLocationWrapper
    * @description: This class is used to hold provider location
    * @Last Modified Date: 10/05/2020
    * *****************************/
    public class PractitionerLocationWrapper{
        @AuraEnabled Public String Id;
        @AuraEnabled Public String firstName;
        @AuraEnabled public String lastName;
        @AuraEnabled public String fLinkId;
        @AuraEnabled public String lLinkId;
        @AuraEnabled public String locationPhone;
        @AuraEnabled public string locationMailingAddr{ get {return String.isBlank(this.locationMailingAddr) ? '':this.locationMailingAddr;} private set; }

        /********************************
        * @Method: PractitionerLocationWrapper
        * @description: Constructor to set the Class attribute from Object using Map
        * @return: N/A
        * *****************************/
        public PractitionerLocationWrapper(SObject sobj) {
            this.firstName = (String)sObj.getSObject(NamespaceSelector.baseNamespace+'Provider__r').get('FirstName');
            this.lastName = (String)sObj.getSObject(NamespaceSelector.baseNamespace+'Provider__r').get('LastName');
            this.Id = (String)sObj.getSObject(NamespaceSelector.baseNamespace+'Provider__r').get('Id');
            this.fLinkId = this.lLinkId = sfdcBaseUrl+'/'+this.Id;
        }
    }

    /********************************
    * @Wrapper Class Name: PractitionerLocationWrapper2
    * @description: This class is used to hold provider location for Activity
    * @Last Modified Date: 10/05/2020
    * *****************************/
    public class PractitionerLocationWrapper2{
        @AuraEnabled Public String Id;
        @AuraEnabled Public String pId;
        @AuraEnabled public String firstName;
        @AuraEnabled public String lastName;
        @AuraEnabled public String fLinkId;
        @AuraEnabled public String lLinkId;

        /********************************
        * @Method: PractitionerLocationWrapper2
        * @description: Constructor to set the Class attribute from Object using Map
        * @return: N/A
        * *****************************/
        public PractitionerLocationWrapper2(Sobject sobj) {
            this.Id = (String)sObj.Id;
            this.firstName =  (String)sObj.get('FirstName');
            this.lastName =  (String)sObj.get('LastName');
            this.pId = (String)sObj.get('Id');
            this.lLinkId = '/'+this.pId;
            this.fLinkId = String.isNotEmpty(this.firstName) ? '/'+this.pId : '';
        }
    }

    /********************************
    * @Wrapper Class Name: LogCallWrapperFieldSet
    * @description: This class is used to hold Log Call Wrapper fields
    * @Last Modified Date: 10/05/2020
    * *****************************/
    public class LogCallWrapperFieldSet {
        @AuraEnabled public String fieldName;
        @AuraEnabled public String label;
        @AuraEnabled public String fieldType;
        @AuraEnabled public Boolean sortable;
        @AuraEnabled public String linkfieldName;

        /********************************
        * @Method: LogCallWrapperFieldSet
        * @description: Constructor to set the Class attribute
        * @return: N/A
        * *****************************/
        public LogCallWrapperFieldSet(String fieldName, String label, String fieldType, String linkfieldName) {
            this.fieldName = fieldName;
            this.label = label;
            this.fieldType = fieldType;
            this.sortable = true;
            this.linkfieldName = linkfieldName;
        }
    }

    /********************************
    * @method: getProviderRecord
    * @description: get provider Record
    * Param: string, string, string, string, string, boolean, string
    * Return type: List
    * *****************************/
    public static List<SObject> getProviderRecord(String sql, String filterBy, String filterOpt, String sortField, String sortDirection, Boolean isAlphaSearch, String searchKey) {
        SObjectType pracLocTerrObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c');
        Schema.DescribeFieldResult pracLocTerrFieldDesc = pracLocTerrObjToken.getDescribe().fields.getMap().get(NamespaceSelector.baseNamespace+'Territory__c').getDescribe();
        Schema.DescribeFieldResult pracLocFieldDesc = pracLocTerrObjToken.getDescribe().fields.getMap().get(NamespaceSelector.baseNamespace+'PractitionerLocation__c').getDescribe();
        if( Util.checkObjectIsReadable(pracLocTerrObjToken) ) {
            String innerQuery = 'SELECT '+NamespaceSelector.baseNamespace+'PractitionerLocation__c FROM '+
                NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c WHERE Id != null AND '+NamespaceSelector.baseNamespace+'Territory__r.OwnerId = \'' + UserInfo.getUserId() + '\'';
            if(filterBy == 'territory' && Util.checkFieldIsReadable(pracLocTerrObjToken, pracLocTerrFieldDesc)) {
                innerQuery += 'AND '+NamespaceSelector.baseNamespace+'Territory__r.Id  = \'' + String.escapeSingleQuotes(filterOpt) + '\'';
            }
            sql += ' WHERE Id IN ( '+innerQuery+' )';
            if(String.isNotEmpty(searchKey)){
                String sqlFilter = getFilterString(isAlphaSearch, searchKey);
                if(String.isNotEmpty(sqlFilter)) { sql+= ' AND '+sqlFilter; }
            }

            Schema.DescribeFieldResult provFieldDesc = pracLocTerrObjToken.getDescribe().fields.getMap().get(NamespaceSelector.baseNamespace+'Provider__c').getDescribe();
            List<String> ordFields = new List<String>();
            if( Util.checkFieldIsReadable(pracLocTerrObjToken, provFieldDesc) && !SObjectType.Contact.fields.Name.isEncrypted() ) {
                if(sortField == 'lLinkId') { ordFields.add(NamespaceSelector.baseNamespace+'Provider__r.LastName');
				} else if(sortField == 'fLinkId') { ordFields.add(NamespaceSelector.baseNamespace+'Provider__r.FirstName');
				} else { ordFields.add(NamespaceSelector.baseNamespace+'Provider__r.Name'); }
                if(ordFields.size()>0) {
                    sql += ' ORDER BY '+String.join(ordFields,','); if(String.IsNotBlank(sortDirection)) { sql += ' '+sortDirection; }
                }
            }
            return Database.query(sql);
        } else {
            CoreControllerException methodException = new CoreControllerException('LogCallController.getProviderRecord',
                                                                                          'sobjects is not insertable/readable ==> '+pracLocTerrObjToken);
            return null;
        }
    }

    /********************************
    * @method: getProviderLocRecord
    * @description: get provider Location Record
    * Param: string, string, string, string, string, boolean, string
    * Return type: List
    * *****************************/
    public static List<Sobject> getProviderLocRecord(String sql, String filterBy, String filterOpt, String sortField, String sortDirection, Boolean isAlphaSearch, String searchKey) {
        SObjectType pracPracGroupObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'ProviderPracticeGroup__c');
        Schema.DescribeFieldResult provPracGrpProvFieldDesc = pracPracGroupObjToken.getDescribe().fields.getMap().get(NamespaceSelector.baseNamespace+'Provider__c').getDescribe();
        Set<Id> providerIds = new Set<Id>(); List<Sobject> returnSObjList = new List<Sobject>();
        if( Util.checkObjectIsReadable(pracPracGroupObjToken) && Util.checkFieldIsReadable(pracPracGroupObjToken, provPracGrpProvFieldDesc) ) {
            String innerQuery = 'SELECT '+NamespaceSelector.baseNamespace+'Provider__c FROM '+NamespaceSelector.baseNamespace+'ProviderPracticeGroup__c WHERE Id != null AND '+
                				NamespaceSelector.baseNamespace+'PracticeGroup__c = :filterOpt';
            List<SObject> lstSObj = Database.query(innerQuery);
            for( SObject sobjRec : lstSObj ) { providerIds.add( (Id)sobjRec.get(NamespaceSelector.baseNamespace+'Provider__c') ); }

            if( isAlphaSearch ) {

                sql += ' WHERE Id IN :providerIds';

                if(String.isNotEmpty(searchKey)){
                    String sqlFilter = Util.getFilterStringForActivity(isAlphaSearch,searchKey);
                    if(String.isNotEmpty(sqlFilter)) { sql+= ' AND '+sqlFilter; }
                }

                if( !SObjectType.Contact.fields.FirstName.isEncrypted() && !SObjectType.Contact.fields.LastName.isEncrypted() ) {
                    List<String> ordFields = new List<String>();
                    if(sortField == 'fLinkId') { ordFields.add('FirstName');}
                    else if(sortField == 'lLinkId') { ordFields.add('LastName'); }
                    else { ordFields.add('FirstName'); }

                    if(ordFields.size() > 0){
                        sql += ' ORDER BY '+String.join(ordFields,',');
                        if(String.IsNotBlank(sortDirection)) { sql += ' '+sortDirection; }
                    }
                }
                returnSObjList = Database.query(sql);
            } else {
                List<List<Sobject>> acc = new List<List<Sobject>>();
                String soslQry;
                if(String.isNotEmpty(searchKey)) {
                    soslQry = 'FIND \''+searchKey+'*\' RETURNING Contact(Id, FirstName, LastName, Name WHERE Id IN :providerIds)';
                    acc = search.query(soslQry);
                    returnSObjList = acc[0];
                } else {
                    sql += ' WHERE Id IN :providerIds';
                    returnSObjList = Database.query(sql);
                }
            }

        } else {
            CoreControllerException methodException = new CoreControllerException('LogCallController.getProviderLocRecord',
                                                                                          'sobjects is not insertable/readable ==> '+pracPracGroupObjToken);
        }
        return returnSObjList;
    }

    /********************************
    * @method: getFilterString
    * @description: get the alphbetical filter criteria for Provider Location
    * Param: boolean, string
    * Return type: string
    * *****************************/
    public static String getFilterString(Boolean isAlphaSearch, String searchKey) { return Util.getFilterStringServices(isAlphaSearch, searchKey); }

    /********************************
    * @method: getLoggedInUser
    * @description: get Logged in user to show default assigned to in UI
    * Param: none
    * Return type: User
    * *****************************/
    @AuraEnabled
    public static User getLoggedInUser() { return [SELECT Id, Name FROM User WHERE Id = :UserInfo.getUserId()]; }

    /********************************
    * @method: getPicklistValuesByRecordType
    * @description: get picklist values by Record Type
    * Param: string, string
    * Return type: list
    * *****************************/
    @AuraEnabled(cacheable=true)
    public static String[] getPicklistValuesByRecordType(String objName, String fieldName){
        String RecTypeName;
        String[] picklistValues = new String[]{};
		String[] fetchedList = new String[]{};
		try {
            List<Schema.RecordTypeInfo> rtList = Schema.getGlobalDescribe().get(objName).getDescribe().getRecordTypeInfos();
            SObjectType picklistValObjToken = Schema.getGlobalDescribe().get(objName);
            if(rtList.size()>0 && Util.checkObjectIsReadable(picklistValObjToken)) {
                for(Schema.RecordTypeInfo rti : rtList) {
                    if(rti.isDefaultRecordTypeMapping() == true && rti.getName() != 'Master') {
                        String nmspcpre;
                        if(!test.isRunningTest()){
                            nmspcpre = [SELECT NamespacePrefix FROM RecordType WHERE SobjectType =:objName AND Id =:rti.getRecordTypeId()].NamespacePrefix;
                        } else {
                            nmspcpre = NamespaceSelector.hgprmNamespace.replace('__','');
                        }
                        RecTypeName = nmspcpre+'__'+rti.getName();
                        fetchedList =  MetadataServiceCallout.getPicklistValuesByRecordType(objName,RecTypeName,fieldName);
                        for(String s : fetchedList) { picklistValues.add(s.replace('%2F', '/')); }
                    }
                    if(rti.isDefaultRecordTypeMapping() == true && rti.getName() == 'Master') {
                        String[] types = new String[]{objName};
						Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
                        for(Schema.DescribeSobjectResult res : results) {
                            for (Schema.PicklistEntry entry : res.fields.getMap().get(fieldName).getDescribe().getPicklistValues()) {
                                if (entry.isActive()) { picklistValues.add(entry.getValue().replace('%2F','/')); }
                            }
                        }
                    }
                }
            }
        } catch(Exception getPValByRecTypeex) {
            CoreControllerException getPValByRecTypeexException = new CoreControllerException('LogCallController.getPicklistValuesByRecordType',
                                                                                                      'Could not load picklist values for Record Type'+getPValByRecTypeex.getStackTraceString());
        }
        return picklistValues;
    }

    /********************************
    * @method: getActivityList
    * @description: get Activity List to show as tabs
    * Param: none
    * Return type: String
	* *****************************/
    @AuraEnabled
    public static string getActivityList(string selectedTab) {
        LogCallController pltActnWrapper = new LogCallController();
        try{
            List<String> actChldFlds = new List<String>{
                'MasterLabel', NamespaceSelector.hgprmNamespace+'Active__c', NamespaceSelector.hgprmNamespace+'Ordering__c', NamespaceSelector.hgprmNamespace+'FieldApiName__c'
            };
            List<String> activityTabsFld = new List<String> {
                'MasterLabel', 'DeveloperName', NamespaceSelector.hgprmNamespace+'Active__c', NamespaceSelector.hgprmNamespace+'Ordering__c',
                NamespaceSelector.hgprmNamespace+'IsDefault__c', NamespaceSelector.hgprmNamespace+'LayoutName__c'
            };
			List<String> allTskRecordTypes = new List<String>();
            for(RecordTypeInfo recTypeInfo: Task.SObjectType.getDescribe().getRecordTypeInfos()) {
                if(recTypeInfo.isAvailable()) {
                    allTskRecordTypes.add( recTypeInfo.getDeveloperName() );
                }
            }
			List<ActivityList> actvtList = new List<ActivityList>();
            string defaultTabsLayout = '';
            boolean isDefaultSet = false;
            List<ActivityFields> defaultActFldList = new List<ActivityFields>();
            for(SObject actObj : Database.query('SELECT '+String.join(activityTabsFld,', ')+' FROM '+NamespaceSelector.hgprmNamespace+'ActivityConfiguration__mdt WHERE '+NamespaceSelector.hgprmNamespace+'Active__c = true AND '+NamespaceSelector.hgprmNamespace+'RecordTypeName__c IN :allTskRecordTypes ORDER BY '+NamespaceSelector.hgprmNamespace+'Ordering__c')) {
                List<ActivityFields> actFldList = new List<ActivityFields>();
                if(selectedTab == (string)actObj.get('Id')){
                    defaultTabsLayout = (string)actObj.get(NamespaceSelector.hgprmNamespace+'LayoutName__c');
                    pltActnWrapper.defaultActTabs = (string)actObj.get('Id');
                    isDefaultSet = true;
                } else if(!isDefaultSet && (Boolean)actObj.get(NamespaceSelector.hgprmNamespace+'IsDefault__c')){
                    defaultTabsLayout = (string)actObj.get(NamespaceSelector.hgprmNamespace+'LayoutName__c');
                    pltActnWrapper.defaultActTabs = (string)actObj.get('Id');
                }
                ActivityList actvt = new ActivityList((string)actObj.get('Id'), (string)actObj.get('MasterLabel'), (string)actObj.get('DeveloperName'), actFldList);
                actvtList.add(actvt);
            }
            loggedInUser = getLoggedInUser();
            List<string> sysFields = new List<String>{'Id','CreatedBy','CreatedById','CreatedDate','LastModifiedBy','LastModifiedById','LastModifiedDate','WhoId'};
            if(!string.isBlank(defaultTabsLayout)){
                fieldBehavMapInstance = Util.getLayoutFieldsBehavior(defaultTabsLayout);
                for(string fld : fieldBehavMapInstance.keySet()){
                    if(!sysFields.contains(fld) && Schema.getGlobalDescribe().get('Task').getDescribe().fields.getMap().containsKey(fld) && Schema.getGlobalDescribe().get('Task').getDescribe().fields.getMap().get(fld).getDescribe().isCreateable()){
                        defaultActFldList.add(new ActivityFields(fld));
                    }
                }
            }
            pltActnWrapper.actvtListWrapper = actvtList;
            pltActnWrapper.defaultFldList = defaultActFldList;
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        }
        return JSON.serialize(pltActnWrapper);
    }

    /********************************
    * @Wrapper Class Name: ActivityList
    * @description: This class is used to hold Activity List
    * @Last Modified Date: 16/12/2020
    * *****************************/
    public class ActivityList {
        String label;
        string devName;
        string Id;
        List<ActivityFields> activityFields;
        public ActivityList(String Id, String label, String devName, List<ActivityFields> activityFields){
            this.label = label;
            this.devName = devName;
            this.Id = Id;
            this.activityFields = activityFields;
        }
    }

    /********************************
    * @Wrapper Class Name: ActivityFields
    * @description: This class is used to hold Activity Fields
    * @Last Modified Date: 16/12/2020
    * *****************************/
    public class ActivityFields {
        string apiName;
        string fieldType;
        string label;
        List<string> picklistValues;
        List<Util.MultiSelectWrapper> multiSelectPicklistValues;
        Util.LookupWrapper selItem;
        string lookupObj;
        string lkpObjLabel;
        Boolean isRequired;
        string defaultValue;
        List<string> relObjs = new List<string>();

        public ActivityFields(string apiName){
            this.apiName = apiName;
            Schema.DisplayType dispType = Schema.getGlobalDescribe().get('Task').getDescribe().fields.getMap().get(apiName).getDescribe().getType();
            this.fieldType = String.valueOf(dispType);
            this.label = Schema.getGlobalDescribe().get('Task').getDescribe().fields.getMap().get(apiName).getDescribe().getLabel();
            switch on dispType {
                when BOOLEAN { }
                when DATE { }
                when MULTIPICKLIST { this.multiSelectPicklistValues = Util.getMultiSelectPicklistValues('Task', apiName); }
                when COMBOBOX { this.picklistValues = Util.getPicklistValuesObj('Task', apiName); }
                when Picklist { this.picklistValues = Util.getPicklistValuesObj('Task', apiName); }
                when Reference {
                    if(apiName == 'OwnerId'){
                        this.lookupObj = 'User';
                        if(loggedInUser != null){
                            this.selItem = new Util.LookupWrapper(loggedInUser.Name);
                            this.defaultValue = loggedInUser.Id;
                        }
                    } else {
                        List <Schema.sObjectType> RefObjs = Schema.getGlobalDescribe().get('Task').getDescribe().fields.getMap().get(apiName).getDescribe().getReferenceTo();
                        if(RefObjs.size() >0 ){
                            if(apiName.toLowerCase() != 'whatid')
                                this.lookupObj = String.valueOf(RefObjs[0]);
                            else{
                                this.lookupObj = String.valueOf(RefObjs[0]);
                                for(Schema.sObjectType obj : RefObjs ){
                                    this.relObjs.add(string.valueOf(obj));
                                }
                            }
                        }
                    }
                    this.lkpObjLabel = 'Assigned To';
                }
            }
            Schema.DescribeFieldResult field = Schema.getGlobalDescribe().get('Task').getDescribe().fields.getMap().get(apiName).getDescribe();
            this.isRequired = fieldBehavMapInstance.get(apiName) == 'Required' ? true : false; 
        }
    }
}