/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: GenericActionOverrideController
* @description: This is for override Action Functionality
* @Last Modified Date: 12/26/2020
**/
public with sharing class GenericActionOverrideController {
    
    /********************************
    * @Method: getObjFields
    * @description: Constructor to query object fields.
    * @return: N/A
    * *****************************/
    @AuraEnabled
    public static String getObjFields(String recordId, String relObjName){
        List<FieldWrapper> fldWrapper = new List<FieldWrapper>(); List<String> listRelObjFields = new List<String>();
        ResultWrapper resWrapper = new ResultWrapper();
        if(!String.isBlank(recordId)){
            String recObjName = Id.valueOf(recordId).getSObjectType().getDescribe().getName();
            List<Sobject> provRecord = new List<Sobject>();
            Boolean disableRec = false;
            if(recObjName == 'Account') {
                if( Account.sObjectType.getDescribe().isAccessible() && Schema.sObjectType.Account.fields.Name.isAccessible() ) {
                    provRecord = Database.query('SELECT Name FROM Account WHERE Id = :recordId LIMIT 1');   
                } else {
                    return null;
                }
            } else {
                SObjectType thisObjToken = Schema.getGlobalDescribe().get(recObjName);
                if( Util.checkObjectIsReadable(thisObjToken) ) {
                    provRecord = Database.query('SELECT Name FROM '+recObjName+' WHERE Id = :recordId LIMIT 1');
                } else {
                    return null;
                }
            }
            if(provRecord.size() > 0){
                resWrapper.recName = (String)provRecord[0].get('Name');
                disableRec = true;
            }
            if(relObjName.contains('ProviderSpecialties')) {
                SObjectType provSpecObjToken = Schema.getGlobalDescribe().get(relObjName);
                if( Util.checkObjectIsReadable(provSpecObjToken) ) {
                    Map<String, Integer> fldOrderMap = new Map<String, Integer>();
                    fldOrderMap.put(NamespaceSelector.baseNamespace+'Grouping__c', 1);
                    fldOrderMap.put(NamespaceSelector.baseNamespace+'Classification__c', 2);
                    fldOrderMap.put(NamespaceSelector.baseNamespace+'Specialization__c', 3);
                    fldOrderMap.put(NamespaceSelector.baseNamespace+'Speciality__c', 4);
                    fldOrderMap.put(NamespaceSelector.baseNamespace+'BoardCertificationStatus__c', 5);
                    fldOrderMap.put(NamespaceSelector.baseNamespace+'IsPrimary__c', 6);
                    Map<String,Schema.SObjectField> fieldsMap = provSpecObjToken.getDescribe().fields.getMap();
                    for(Schema.SObjectField sObjfield : fieldsMap.Values()) {
                        Schema.DescribeFieldResult descfldResult = sObjfield.getDescribe();
                        if( descfldResult.isCustom() && !descfldResult.getName().contains('TierNode')
                           && !descfldResult.getName().contains('Creator') && !descfldResult.getName().contains('Update')
                           && !descfldResult.getName().contains('ExternalId') && !descfldResult.getName().contains('Provider') 
                          ) {
                              listRelObjFields.add(descfldResult.getName());
                              fldWrapper.add(new FieldWrapper(descfldResult.getName(), descfldResult.getLabel(), 
                                                              String.valueOf(descfldResult.getType()), '', '', resWrapper.recName, 'Contact', '', 
                                                              descfldResult.getLabel(), false, false, fldOrderMap.get( descfldResult.getName() ) ));
                          }
                    }
                    listRelObjFields.add(NamespaceSelector.baseNamespace+'Speciality__r.'+NamespaceSelector.baseNamespace+'Code__c');
                    resWrapper.title = 'Manage Specialties';
                    resWrapper.name = 'Provider Specialty';
                    resWrapper.fieldValues = getAllProviderSpeciality(relObjName, recordId, listRelObjFields);
                } else {
                    return null;
                }
            } else if(relObjName.contains('ProviderPracticeGroup')) {
                SObjectType provPracGroupObjToken = Schema.getGlobalDescribe().get(relObjName);
                if( Util.checkObjectIsReadable(provPracGroupObjToken) ) {
                    if(recObjName == 'Account'){
                        fldWrapper.add(new FieldWrapper('Provider__c', 'Provider', 'lookup', '', 'Search Provider...', '', 'Contact','', '', false, true,0));
                        fldWrapper.add(new FieldWrapper('PracticeGroup__c', 'Practice & Group', 'lookup', recordId, 'Search Practice & Group...', resWrapper.recName,'Account', Util.getRecordType('Account','Practice & Group'),'Practice & Group', disableRec, false,0));
                    } else {
                        fldWrapper.add(new FieldWrapper('Provider__c', 'Provider', 'lookup', recordId, 'Search Provider...', resWrapper.recName, 'Contact','', '', disableRec, false,0));
                        fldWrapper.add(new FieldWrapper('PracticeGroup__c', 'Practice & Group', 'lookup', '', 'Search Practice & Group...', '','Account', Util.getRecordType('Account','Practice & Group'),'Practice & Group', false, true,0));
                    }
                    fldWrapper.add(new FieldWrapper('LocationIsPrimary__c', 'Primary', 'checkbox', false, '', '','','','', false, false,0));
                    fldWrapper.add(new FieldWrapper('StaffJoinDate__c', 'Staff Join Date', 'date', '', '', '','','','',false, false,0));
                    resWrapper.title = 'New Provider Practice Group';
                    resWrapper.name = 'Provider Practice Group';
                
                } else {
                    return null;
                }
            } else if(relObjName.contains('PracticeGroupLocation')) {
                SObjectType pracGroupLocObjToken = Schema.getGlobalDescribe().get(relObjName);
                if( Util.checkObjectIsReadable(pracGroupLocObjToken) ) {
                    fldWrapper.add(new FieldWrapper('Location__c', 'Location', 'lookup', '', 'Search Location...', '', NamespaceSelector.baseNamespace+'Location__c', '', '', false, true, 0));
                    fldWrapper.add(new FieldWrapper('PracticeGroup__c', 'Practice & Group', 'lookup', recordId, 'Search Practice & Group...', resWrapper.recName, 'Account', Util.getRecordType('Account', 'Practice & Group'), 'Practice & Group', disableRec, false, 0));
                    fldWrapper.add(new FieldWrapper('IsPrimary__c', 'Primary', 'checkbox', false, '', '','','','', false, false,0));
                    resWrapper.title = 'New Practice Group Location';
                    resWrapper.name = 'Practice Group Location';
                } else {
                    return null;
                }
            }
            resWrapper.fldWrapper = fldWrapper;
        }
        return JSON.serialize(resWrapper);
    }
    
    /********************************
    * @Method: getAllProviderSpeciality
    * @description: query all provider speciality records for the viewing providers.
    * @return: N/A
    * *****************************/
    @AuraEnabled
    public static List<SObject> getAllProviderSpeciality(String relObjName, String contactRecId, List<String> relObjFieldList) {
        List<SObject> provRecord = new List<SObject>();
        try{
            SObjectType thisObjToken = Schema.getGlobalDescribe().get(relObjName);
            if( Util.checkObjectIsReadable(thisObjToken) ) {
                String relObjflds = String.join(relObjFieldList, ', ');
                provRecord = Database.query('SELECT '+relObjflds+' FROM '+relObjName+' WHERE '+NamespaceSelector.baseNamespace+'Provider__c= :contactRecId');
                for (sObject provRec: provRecord) {
                    if( provRec.getSobject(NamespaceSelector.baseNamespace+'Speciality__r') != null ) {
                        provRec.put(NamespaceSelector.baseNamespace+'Speciality__c', provRec.getSobject(NamespaceSelector.baseNamespace+'Speciality__r').get(NamespaceSelector.baseNamespace+'Code__c'));
                    }
                }
            }
            return provRecord;
        }
        catch(Exception getAllProvSpecEx){
            CoreControllerException methodException = new CoreControllerException('GenericActionOverrideController.getAllProviderSpeciality', 
                                                                                          'Exception occured while doing saving Records ==> '+getAllProvSpecEx.getStackTraceString());
            return null;
        }
    }

    @AuraEnabled
    public static string getObjNameFieldText(String relObjName, String name) {
        return Util.getObjNameFieldText(relObjName, name);
    }
    
    /********************************
    * @Method: saveRecords
    * @description: Constructor to insert records.
    * @return: N/A
    * *****************************/
    @AuraEnabled
    public static String saveRecords(String relObjName, Map<String,String> fieldVals, String name, String provList){
        try{
            Util.SaveResult result; List<SObject> bulkProvSpecLst = new List<SObject>();
            SObjectType relObjToken = Schema.getGlobalDescribe().get(relObjName);
            Schema.DescribeFieldResult primaryFieldDesc = relObjToken.getDescribe().fields.getMap().get(NamespaceSelector.baseNamespace+'IsPrimary__c').getDescribe();
            Schema.DescribeFieldResult boardCertFieldDesc = relObjToken.getDescribe().fields.getMap().get(NamespaceSelector.baseNamespace+'BoardCertificationStatus__c').getDescribe();
            if (Util.checkObjectIsReadable(relObjToken) && Util.checkObjectIsInsertable(relObjToken) && Util.checkObjectIsUpdateable(relObjToken)) {
                List<Object> provSpecObject = (List<Object>)JSON.deserializeUntyped(provList);
                for (Object eachObj: provSpecObject) {
                    sObject provSpecObj = Schema.getGlobalDescribe().get(relObjName).newSObject();
                    Map<String, Object> provSpecMap = (Map<String, Object>) eachObj;
                    Boolean isPrimary = (provSpecMap.get(NamespaceSelector.baseNamespace+'IsPrimary__c') == null || !(Boolean)provSpecMap.get(NamespaceSelector.baseNamespace+'IsPrimary__c')) ? false : true;
                    Boolean boardCertStatus = (provSpecMap.get(NamespaceSelector.baseNamespace+'BoardCertificationStatus__c') == null || !(Boolean)provSpecMap.get(NamespaceSelector.baseNamespace+'BoardCertificationStatus__c')) ? false : true;
                    Boolean isModified = (provSpecMap.get('isModified') == null || !(Boolean)provSpecMap.get('isModified')) ? false : true;
                    Boolean isNewSpecialityAdded = (provSpecMap.get('NewSpecialityAdded') == null || !(Boolean)provSpecMap.get('NewSpecialityAdded')) ? false : true;

                    if (isNewSpecialityAdded) {
                        provSpecObj.put(NamespaceSelector.baseNamespace+'Speciality__c', provSpecMap.get('Id'));
                        provSpecObj.put(NamespaceSelector.baseNamespace+'Provider__c', provSpecMap.get(NamespaceSelector.baseNamespace+'Provider__c'));
                        provSpecObj.put(NamespaceSelector.baseNamespace+'BoardCertificationStatus__c', boardCertStatus);
                        provSpecObj.put(NamespaceSelector.baseNamespace+'IsPrimary__c', isPrimary);
                        bulkProvSpecLst.add(provSpecObj);
                    } else if (isModified && !isNewSpecialityAdded && Util.checkFieldIsUpdateable(relObjToken, primaryFieldDesc) && Util.checkFieldIsUpdateable(relObjToken, boardCertFieldDesc)) {
                        provSpecObj.put('Id', provSpecMap.get('Id'));
                        provSpecObj.put(NamespaceSelector.baseNamespace+'IsPrimary__c', isPrimary);
                        provSpecObj.put(NamespaceSelector.baseNamespace+'BoardCertificationStatus__c', boardCertStatus);
                        bulkProvSpecLst.add(provSpecObj);
                    }
                }
                
                Database.UpsertResult[] provSaveResult = Database.upsert(bulkProvSpecLst);   
            }
            result = new Util.SaveResult(true, bulkProvSpecLst.size()+' Specality updates were succesfully saved');
            return JSON.serialize(result);
        }
        catch(Exception saveRecordsEx){
            CoreControllerException methodException = new CoreControllerException('GenericActionOverrideController.saveRecords', 
                                                                                          'Exception occured while doing saving Records ==> '+saveRecordsEx.getStackTraceString());
            return null;
        }
    }
    
    /********************************
    * @Method: deleteRecords
    * @description: Constructor to delete records.
    * @return: N/A
    * *****************************/
    @AuraEnabled
    public static String deleteRecords(String relObjName, String childRecordId, String parentRecId, Boolean childRecIsPrimary){
        try {
            String delResultString;
            // Since it is coming from Provider Speciality check if the object is deletable and readable.
            SObjectType relObjToken = Schema.getGlobalDescribe().get(relObjName);
            if( Util.checkObjectIsReadable(relObjToken) && Util.checkObjectIsDeletable(relObjToken) ) {
                Database.DeleteResult delResult = Database.delete(childRecordId, false);
                if (delResult.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    delResultString = 'success,'+relObjName+' record was successfully deleted.';
                } else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : delResult.getErrors()) {
                        delResultString = 'error,'+'Error occurred: '+err.getStatusCode() + ': ' + err.getMessage()+' in fields '+ err.getFields()+' and the error is '+ delResult.errors;
                    }
                }
            }
            
            // if the provider speciality is primary for the parent contact, then the parent has to be updated.
            if( childRecIsPrimary && !String.isEmpty(parentRecId) ) {
                String parentSObjectName = String.valueOf(Id.valueOf(parentRecId).getSObjectType());
                sObject recObj = Schema.getGlobalDescribe().get(parentSObjectName).newSObject();
                recObj.put('Id', parentRecId);
                recObj.put(NamespaceSelector.baseNamespace+'Speciality__c', null);
                SObjectType parentObjToken = Schema.getGlobalDescribe().get(parentSObjectName);
                if( Util.checkObjectIsReadable(parentObjToken) && Util.checkObjectIsInsertable(parentObjToken) ) {
                    update recObj;
                }
            }
            return delResultString;
        }
        catch(Exception deleteRecordsEx){
            CoreControllerException methodException = new CoreControllerException('GenericActionOverrideController.deleteRecords', 
                                                                                          'Exception occured while doing deleting Records ==> '+deleteRecordsEx.getStackTraceString());
            return null;
        }
    }
    
    /********************************
    * @Method: updateContactRecord
    * @description: Constructor to insert records.
    * @return: N/A
    * *****************************/
    @AuraEnabled
    public static Util.SaveResultWrapper updateContactRecord(String specialityId, String currSObjRecId) {
        try{
            return Util.saveProviderSpeciality(specialityId, currSObjRecId);
        }
        catch(Exception updateContactRecordEx){
            CoreControllerException methodException = new CoreControllerException('GenericActionOverrideController.updateContactRecord', 
                                                                                          'Exception occured while doing saving Records ==> '+updateContactRecordEx.getStackTraceString());
            return null;
        }
    }
    
    /********************************
    * @Method: FieldWrapper
    * @description: Constructor to find record type
    * @return: N/A
    * *****************************/
    public class FieldWrapper{
        String name;
        String label;
        Object value;
        String fieldType;
        String lookupValue;
        String placeholder;
        LookupWrapper selItem;
        String lookupObj;
        String recordTypeId;
        String lkpObjLabel;
        Boolean isDisable;
        Boolean isRequired;
        Integer columnOrder;
        
        public FieldWrapper(String name, String label, String fieldType, Object value, String placeholder, String lookupValue, String lookupObj, String recordTypeId, String lkpObjLabel, Boolean isDisable, Boolean isRequired, Integer columnOrder){
            this.name = name;
            this.label = label;
            this.value = value;
            this.fieldType = fieldType;
            this.placeholder = placeholder;
            this.lookupObj = lookupObj;
            this.recordTypeId = recordTypeId;
            this.lkpObjLabel = lkpObjLabel;
            this.isDisable = isDisable;
            this.isRequired = isRequired;
            if(!String.isBlank(lookupValue)){
                this.selItem = new LookupWrapper(lookupValue);
            }
            this.columnOrder = columnOrder;
        }
    }
    
    /********************************
    * @Method: ResultWrapper
    * @description: Constructor to find record type
    * @return: N/A
    * *****************************/
    public class ResultWrapper{
        String recName;
        List<FieldWrapper> fldWrapper;
        String title;
        String name;
        String homeObjName;
        List<SObject> fieldValues;
    }
    
    /********************************
    * @Method: LookupWrapper
    * @description: Constructor to find record type
    * @return: N/A
    * *****************************/
    public class LookupWrapper{
        String text;
        public LookupWrapper(String text){ this.text = text; }
    }
}