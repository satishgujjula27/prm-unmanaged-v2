/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: GenericActionOverrideControllerTest
* @description: This is the test class for GenericActionOverrideController
* @Last Modified Date: 07/08/2020
**/
@isTest
public class GenericActionOverrideControllerTest {
    
    @isTest
    static void testGetObjFields() {
        Contact Provider= new Contact();
        Provider.put('LastName', 'Test');
        insert Provider;
        
        Account pGroupName = new Account();
        pGroupName.put('Name', 'Test');
        insert pGroupName;
        
        Sobject provPracGrpObj= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'ProviderPracticeGroup__c').newSObject();
        provPracGrpObj.put(NamespaceSelector.baseNamespace+'Provider__c', Provider.Id);
        provPracGrpObj.put(NamespaceSelector.baseNamespace+'PracticeGroup__c', pGroupName.Id);
        insert provPracGrpObj;
        
        Sobject locObj= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Location__c').newSObject();
        locObj.put('Name', 'Test Location');
        insert locObj;
        
        Sobject pracGrpLocObj= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PracticeGroupLocation__c').newSObject();
        pracGrpLocObj.put(NamespaceSelector.baseNamespace+'Location__c', locObj.Id);
        pracGrpLocObj.put(NamespaceSelector.baseNamespace+'PracticeGroup__c', pGroupName.Id);
        insert pracGrpLocObj;
        
        System.assertNotEquals(null, GenericActionOverrideController.getObjFields(Provider.Id, NamespaceSelector.baseNamespace+'ProviderSpecialties__c'));
        System.assertNotEquals(null, GenericActionOverrideController.getObjFields(pGroupName.Id, NamespaceSelector.baseNamespace+'ProviderSpecialties__c'));
        
        System.assertNotEquals(null, GenericActionOverrideController.getObjFields(provPracGrpObj.Id, NamespaceSelector.baseNamespace+'ProviderPracticeGroup__c'));
        System.assertNotEquals(null, GenericActionOverrideController.getObjFields(pGroupName.Id, NamespaceSelector.baseNamespace+'ProviderPracticeGroup__c'));
        
        System.assertNotEquals(null, GenericActionOverrideController.getObjFields(pracGrpLocObj.Id, NamespaceSelector.baseNamespace+'PracticeGroupLocation__c'));
    }
    
    @isTest
    static void testupdateContactRecord() {
        Sobject specialityObj= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Speciality__c').newSObject();
        specialityObj.put('Name', 'TEST Specility');
        insert specialityObj;
        
        Contact Provider= new Contact();
        Provider.put('LastName', 'Test');
        insert Provider;
        
        System.assertNotEquals(null, GenericActionOverrideController.updateContactRecord(String.valueOf(specialityObj.get('Id')), String.valueOf(Provider.get('Id'))));
    }

    @isTest
    static void testgetObjNameFieldText(){
        System.assertNotEquals(null, GenericActionOverrideController.getObjNameFieldText(NamespaceSelector.baseNamespace+'ProviderPracticeGroup__c', 'Provider Practice Group'));
    }
    
    @isTest
    static void testSaveRecords(){
        Sobject specialityObj= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Speciality__c').newSObject();
        specialityObj.put('Name', 'TEST Specility');
        insert specialityObj;
        
        Contact Provider= new Contact();
        Provider.put('LastName', 'Test');
        insert Provider;
        
        Sobject provSpecialityObj= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'ProviderSpecialties__c').newSObject();
        provSpecialityObj.put('Name', 'TEST Prov Specility');
        provSpecialityObj.put(NamespaceSelector.baseNamespace+'Provider__c', Provider.get('Id'));
        provSpecialityObj.put(NamespaceSelector.baseNamespace+'Speciality__c', specialityObj.get('Id'));
        provSpecialityObj.put(NamespaceSelector.baseNamespace+'IsPrimary__c', true);
        insert provSpecialityObj;
        
        List<Map<string,string>> lstMapStr = new List<Map<string,string>>();
        Map<string,string> fieldVals = new Map<string,string>{
            'BoardCertificationStatus__c' => 'true',
                'IsPrimary__c' => 'true',
                'Id' => provSpecialityObj.Id,
                'Provider__c' => Provider.Id,
                'Speciality__c' => specialityObj.Id
                };
		lstMapStr.add(fieldVals); 
		
        List<Map<string, Object>> lstMapStr1 = new List<Map<string, Object>>();
        Map<string, Object> provSpecVals1 = new Map<string, Object>{
            'BoardCertificationStatus__c' => true,
            'IsPrimary__c' => true,
            'Id' => specialityObj.Id,
            'Provider__c' => Provider.Id,
            'Speciality__c' => specialityObj.Id,
            'isModified' => true,
            'NewSpecialityAdded' => true
        };
        lstMapStr1.add(provSpecVals1);
        Map<string, Object> provSpecVals2 = new Map<string,Object>{
            'BoardCertificationStatus__c' => true,
            'IsPrimary__c' => false,
            'Id' => provSpecialityObj.Id,
            'Provider__c' => Provider.Id,
            'Speciality__c' => specialityObj.Id,
            'isModified' => true,
            'NewSpecialityAdded' => false
        };    
        lstMapStr1.add(provSpecVals2);
        System.assertNotEquals(null, GenericActionOverrideController.saveRecords(NamespaceSelector.baseNamespace+'ProviderSpecialties__c', fieldVals, 'Provider Specialty', JSON.serialize(lstMapStr1)));
    }
    
    @isTest
    static void testDeleteRecords(){
        Sobject specialityObj= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Speciality__c').newSObject();
        specialityObj.put('Name', 'TEST Specility');
        insert specialityObj;
        
        Contact Provider= new Contact();
        Provider.put('LastName', 'Test');
        insert Provider;
        
        Sobject provSpecialityObj= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'ProviderSpecialties__c').newSObject();
        provSpecialityObj.put('Name', 'TEST Prov Specility');
        provSpecialityObj.put(NamespaceSelector.baseNamespace+'Provider__c', Provider.get('Id'));
        provSpecialityObj.put(NamespaceSelector.baseNamespace+'Speciality__c', specialityObj.get('Id'));
        provSpecialityObj.put(NamespaceSelector.baseNamespace+'IsPrimary__c', true);
        insert provSpecialityObj;
        
        Map<string,string> fieldVals = new Map<string,string>{
            'BoardCertificationStatus__c' => 'true',
                'IsPrimary__c' => 'true',
                'Provider__c' => Provider.Id,
                'Speciality__c' => specialityObj.Id
                };        
		System.assertNotEquals(null, GenericActionOverrideController.deleteRecords(NamespaceSelector.baseNamespace+'ProviderSpecialties__c', 
                                                                                   String.valueOf(provSpecialityObj.get('Id')), 
                                                                                   String.valueOf(Provider.get('Id')), true ));
    }
}