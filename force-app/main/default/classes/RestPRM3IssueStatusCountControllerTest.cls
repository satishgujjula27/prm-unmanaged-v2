/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: RestPRM3IssueStatusCountControllerTest
* @description: This is the test class for RestPRM3IssueStatusCountController
* @Last Modified Date: 03/26/2019
**/
@isTest
public class RestPRM3IssueStatusCountControllerTest {

    @isTest
    static void doGetTest(){
        // Create Test Contact
        Contact testContact = new Contact ();
        testContact.LastName = 'Test';
        insert testContact;
        
        // Create Issue
        Sobject issueObj= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerIssues__c').newSObject();
        issueObj.put(NamespaceSelector.baseNamespace+'Subject__c', 'Test Issue');
        issueObj.put(NamespaceSelector.baseNamespace+'Status__c', 'Working');
        issueObj.put(NamespaceSelector.baseNamespace+'Provider__c', testContact.Id);
        issueObj.put(NamespaceSelector.baseNamespace+'DateTimeOpened__c', System.now());
        issueObj.put(NamespaceSelector.baseNamespace+'LongDescription__c', 'Test');
        insert issueObj;
        
        
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/prm3/v1/Issues/statusWiseCount';
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json');
        RestContext.request = req;
        RestContext.response = res;
        List<PRM3uRestController.Result> response = RestPRM3IssueStatusCountController.doGet();
        Test.stopTest();
        system.assertEquals(1, response.size());
    }

    @isTest
    static void parseDetailResult(){
        Test.startTest();
        List<PRM3uRestController.activity> activities = new List<PRM3uRestController.activity>();
        List<PRM3uRestController.Result> response = RestPRM3IssueStatusCountController.ParseDetailResult(activities);
        Test.stopTest();
        system.assertEquals(0, response.size());
    }

}