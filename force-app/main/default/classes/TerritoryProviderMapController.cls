/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: TerritoryProviderMapController
* @description: This Class is used to show Provider Location Pointers into Terrtiry Record Page
* @Last Modified Date: 10/05/2020
**/
public inherited sharing class TerritoryProviderMapController {
    
    /********************************
    * @method: getPLTRecord
    * @description: get Provider Location Territory Records to show pinpoint on Map for Territory Record
    * Param: string, List, string
    * Return type: List
    * *****************************/
    @AuraEnabled
	public static String getPLTRecord(String recordId) {
        List<PLTWrapper> practLocations = new List<PLTWrapper>();
        try {
            string plRecSql = getPLRecordSql();
            List<SObject> listRecord = getPLRecord(plRecSql,recordId);
            if(listRecord.size()>0) {
                for(SObject sObj:listRecord) {
                    try {
                        practLocations.add(new PLTWrapper(sObj));
                    } catch(Exception getPLTRecordEx) {
                        CoreControllerException methodException = new CoreControllerException('TerritoryProviderMapController.getPLTRecord', 
                                                                                                      'Exception occured while iterating provider Locations '+getPLTRecordEx.getStackTraceString());
                    }
                }
            }
            return JSON.serialize(practLocations);
        } catch(Exception getPLTRecEx) {
            CoreControllerException methodException = new CoreControllerException('TerritoryProviderMapController.getPLTRecord', 
                                                                                          'Exception occured while getEventInfo'+getPLTRecEx.getStackTraceString());
            return null;
        }
    }
    
    /********************************
    * @method: getPLRecord
    * @description: get Provider Location Records
    * Param: string, string
    * Return type: List
    * *****************************/
    public static List<SObject> getPLRecord(String plRecSql, String recordId){
        SObjectType pltObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c');
        Schema.DescribeFieldResult terrFieldDesc = pltObjToken.getDescribe().fields.getMap().get(NamespaceSelector.baseNamespace+'Territory__c').getDescribe();
        Schema.DescribeFieldResult provLocFieldDesc = pltObjToken.getDescribe().fields.getMap().get(NamespaceSelector.baseNamespace+'PractitionerLocation__c').getDescribe();
        if( Util.checkObjectIsReadable(pltObjToken) ) {
            if(Util.checkFieldIsReadable(pltObjToken, terrFieldDesc) && Util.checkFieldIsReadable(pltObjToken, provLocFieldDesc) ) {
                plRecSql += ' WHERE '+NamespaceSelector.baseNamespace+'PractitionerLocation__c != NULL AND '+NamespaceSelector.baseNamespace+'Territory__c =:recordId';
            }
            plRecSql += ' ORDER BY createdDate LIMIT ' + (Limits.getLimitQueryRows() - Limits.getQueryRows());
            return Database.query(plRecSql);
        } else{
            CoreControllerException getPLRecordException = new CoreControllerException('TerritoryProviderMapController.getPLRecord', 
                                                                                                'Could not access sObject ==> '+pltObjToken);
            return new List<Sobject>();
        }
    }
    
    /********************************
    * @method: getPLRecordSql
    * @description: generate soql to fetch provider Location Teritory Records 
    * Param: none
    * Return type: String
    * *****************************/
    public static String getPLRecordSql() {
		SObjectType pracLocTerrObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c');
        if ( Util.checkObjectIsReadable(pracLocTerrObjToken) ) {
            List<String> fields = new List<String> {
                'Id','createdDate',
				NamespaceSelector.baseNamespace+'PractitionerLocation__r.'+NamespaceSelector.baseNamespace+'Provider__r.Name',
				NamespaceSelector.baseNamespace+'PractitionerLocation__r.'+NamespaceSelector.baseNamespace+'Location__r.Name',
				NamespaceSelector.baseNamespace+'PractitionerLocation__r.'+NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'PracticeGroup__r.Name',
				NamespaceSelector.baseNamespace+'PractitionerLocation__r.'+NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'Latitude__c',
				NamespaceSelector.baseNamespace+'PractitionerLocation__r.'+NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'Longitude__c',
				NamespaceSelector.baseNamespace+'PractitionerLocation__r.'+NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'AddressLine1__c',
				NamespaceSelector.baseNamespace+'PractitionerLocation__r.'+NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'AddressLine2__c',
				NamespaceSelector.baseNamespace+'PractitionerLocation__r.'+NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'AddressLine3__c',
				NamespaceSelector.baseNamespace+'PractitionerLocation__r.'+NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'City__c',
				NamespaceSelector.baseNamespace+'PractitionerLocation__r.'+NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'StateCode__c'
			};
			return 'SELECT '+String.join(fields,',')+' FROM '+NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c';
        } else {
            CoreControllerException getPLRecordSqlException = new CoreControllerException('TerritoryProviderMapController.getPLRecordSql', 
                                                                                                  'sobjects is not insertable/readable ==> '+pracLocTerrObjToken);
            return null;
        }
    }
    
    /********************************
    * @Wrapper Class Name: PLTWrapper
    * @description: This class is to hold the Provider Locations Territory Records
    * @Last Modified Date: 10/05/2020
    * *****************************/
    public class PLTWrapper{
        @AuraEnabled Public String Id;
        @AuraEnabled Public String providerLink;
        @AuraEnabled Public String providerName { get { return String.isBlank(this.ProviderName) ? '' : this.ProviderName; } set; }
        @AuraEnabled public String locationName;
        @AuraEnabled public String practiceGroupName;
        @AuraEnabled public String locationAddr { get { return String.isBlank(this.locationAddr) ? '' : this.locationAddr; } set; }
        @AuraEnabled public String locationCityState { get { return String.isBlank(this.locationCityState) ? '' : this.locationCityState; } set; }
        @AuraEnabled public Decimal Latitude;
        @AuraEnabled public Decimal Longitude;
        
        /********************************
        * @Method: PLTWrapper
        * @description: Constructor to set the Class attribute from Object using Map
        * @return: N/A
        * *****************************/
        public PLTWrapper(SObject sobj) {
            this.providerName = (String)Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Provider__r').get('Name');
            this.providerLink = (String)Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Provider__r').get('Id');
            this.locationName = (String)Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Location__r').get('Name');
            this.Latitude = (Decimal)Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'Latitude__c');
            this.Longitude = (Decimal)Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'Longitude__c');
            if(Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'AddressLine1__c') !=null) {
            	this.locationAddr = (String)Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'AddressLine1__c') + ' ';
            }
            if(Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'AddressLine2__c') !=null) {
            	this.locationAddr += (String)Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'AddressLine2__c') + ' ';
            }
            if(Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'AddressLine3__c') !=null) {
            	this.locationAddr += (String)Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'AddressLine3__c');
            }
            if(Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'City__c') !=null) {
            	this.locationCityState = (String)Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'City__c') + ',';
            }
            if(Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'StateCode__c') !=null) {
                this.locationCityState += (String)Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'StateCode__c');
            }
            if(Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r') !=null && Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Location__r') !=null && Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Location__r').getSObject(NamespaceSelector.baseNamespace+'PracticeGroup__r')!=null) { 
                this.practiceGroupName = (string)Sobj.getSObject(NamespaceSelector.baseNamespace+'PractitionerLocation__r').getSObject(NamespaceSelector.baseNamespace+'Location__r').getSObject(NamespaceSelector.baseNamespace+'PracticeGroup__r').get('Name');
            }
            this.Id = (String)sObj.get('Id');
        }
    }
}