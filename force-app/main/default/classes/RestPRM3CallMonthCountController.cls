/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: RestPRM3CallMonthCountController
* @description: This class is to expose the Monthly Logged Call.
* @Author: Healthgrades Inc.
* @Last Modified Date: 10/04/2020
**/
@RestResource(urlMapping='/prm3/v1/Calls/monthlyCall')
global inherited sharing class RestPRM3CallMonthCountController {
    
    /********************************
    * @method: doGet
    * @description: get method for Api
    * @return: List<PRM3uRestController.Call>
    * *****************************/
    @HttpGet
    global static List<PRM3uRestController.Result> doGet() {
        try {
            RestRequest request = RestContext.request;
            RestResponse response = RestContext.response;
            response.statusCode = 200;
            response.addHeader('Content-Type', 'application/json');
            Integer recordLimit = PRM3uRestController.maxRecordsParam(request);
            List<PRM3uRestController.Activity> detailResult = RestPrm3CallController.performGetSearch(recordLimit);
            return parseDetailResult(detailResult);
        } catch(Exception RestPRM3CallMonthCountCtrlEx) {
            CoreControllerException errLogException = new CoreControllerException('RestPRM3CallMonthCountController.doGet', 
                                                                                          'Could not get activity records ==> '+RestPRM3CallMonthCountCtrlEx.getStackTraceString());
            return null;
        }
    }
    
	/********************************
    * @method: ParseDetailResult
    * @description: parse the detail result to get the summary result
    * @return: List
    * *****************************/
    public static List<PRM3uRestController.Result> parseDetailResult(List<PRM3uRestController.Activity> result) {
        Map<string,integer> resultMap = new Map<string,integer>();
        Map<integer,integer> resultSortMonthMap = new Map<integer,integer>();
        list<Integer> sortList = new List<Integer>();
        for(PRM3uRestController.Activity actvty:result) {
            if(resultSortMonthMap.containsKey(actvty.monthNum)) {
                resultSortMonthMap.put(actvty.monthNum,resultSortMonthMap.get(actvty.monthNum)+1);
            } else {
                resultSortMonthMap.put(actvty.monthNum,1);
                sortList.add(actvty.monthNum);
            }
        }
        sortList.sort();
        List<PRM3uRestController.Result> response = new List<PRM3uRestController.Result>();
        for(Integer i = sortList.size()-1; i>=0;i--) {
            integer m = resultSortMonthMap.get(sortList.get(i));
            response.add(new PRM3uRestController.Result(PRM3uRestController.Month(sortList.get(i)),m));
        }
        return response;
    }
}