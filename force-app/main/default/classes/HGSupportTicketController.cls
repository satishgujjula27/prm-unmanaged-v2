/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying 
* or sale of the above may constitute an infringement of copyright 
* and may result in criminal or other legal proceedings.
*
* @Class Name: HGSupportTicketController
* @description: This class is used by HG support utility to send email from utility bar..
* @Author: Healthgrades Inc.
* @Last Modified Date: 10/04/2020
**/
public inherited sharing class HGSupportTicketController {
    
    /********************************
    * @method: sendEmail
    * @description: constructor to send support email.
    * @return: Boolean indicating if the email was sent or failed.
    * *****************************/
    @AuraEnabled
    public static Boolean sendEmail(Map<String, String> caseData, String emailId) {
        try{
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] { emailId };
            message.optOutPolicy = 'FILTER';
            message.setReplyTo('support@healthgrades.com');
            message.setSenderDisplayName('HG Salesforce Support Case');
            message.setBccSender(false);
            message.setSubject('New Healthgrades Support Ticket: '+caseData.get('Subject'));
            String emailBody = '<b>Healthgrades PRM Incident Report</b><br/><br/>';
            emailBody += 'Description of Problem: <br/>';
            emailBody += caseData.get('Description')+'<br/><br/>';
            emailBody += '<b>Client Organization Id:</b> '+UserInfo.getOrganizationId()+'<br/>';
            emailBody += '<b>Client Organization Name:</b> '+UserInfo.getOrganizationName()+'<br/>';
            emailBody += '<b>Client Time Zone:</b> '+UserInfo.getTimeZone()+'<br/>';
            emailBody += '<b>Reported by User:</b> '+UserInfo.getName()+'<br/>';
            emailBody += '<b>Current User Preferred Theme:</b> '+UserInfo.getUiTheme()+'<br/>';
            emailBody += '<b>Current Display User Theme:</b> '+UserInfo.getUiThemeDisplayed()+'<br/>';
            message.setHtmlBody(emailBody);
            
            Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            Boolean isSend = false;
            for(Messaging.SendEmailResult res:results) {
                if(res.isSuccess()) { isSend = true; }
            }
            return isSend;
        } catch(EmailException emailEx) {
            CoreControllerException errLogException = new CoreControllerException('HGSupportTicketController.sendEmail', 'Could not send email'+emailEx.getStackTraceString());
            return false;
        }
    }
}