/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: ScheduleBuilderController
* @description: This class is to Process Events
* @Author: Healthgrades Inc.
* @Last Modified Date: 10/07/2020
**/
public inherited sharing class ScheduleBuilderController {
    
    @AuraEnabled Public List<LogCallWrapperFieldSet> fieldsWrapper = new List<LogCallWrapperFieldSet>();
    @AuraEnabled Public Integer totalCount;
    @AuraEnabled Public String providerEntityName;
    @AuraEnabled Public List<PractitionerLocationWrapper> resultWrapper = new List<PractitionerLocationWrapper>();
    private static Map<String, List<Schema.PickListEntry>> picklists = new Map<String, List<Schema.PickListEntry>>();
    @AuraEnabled public String strTitle ='';
    @AuraEnabled public Integer totalRec;
    
    /********************************
    * @method: getProviders
    * @description: get providers to schedule Events
    * Param: string, boolean, string, string, string, string
    * Return type: string
    * *****************************/
    @AuraEnabled
    public static String getProviders(String searchKey, Boolean isAlphaSearch, String filterBy, String filterOpt, String sortField, String sortDirection, Integer recLimit, String pageSize, String pageNumber) {
        ScheduleBuilderController wrapCls = new ScheduleBuilderController();
        List<LogCallWrapperFieldSet> flswrapper = new List<LogCallWrapperFieldSet>();
        List<PractitionerLocationWrapper> practLocations = new List<PractitionerLocationWrapper>();
        try {
            String plSql = Util.getPLSql();
            List<Sobject> listRecord = getProviderRecord(plSql, filterBy, filterOpt, sortField, sortDirection,isAlphaSearch,searchKey,recLimit,pageSize,pageNumber,wrapCls);
            if(listRecord.size()>0) {
                Integer loopCntRelList = 1;
                // for custom pagination logic
                Integer psize = Integer.valueOf(pageSize);
                Integer pnumber = Integer.valueOf(pageNumber);
                Integer uppperLimit = psize * pnumber;
                Integer lowerLimit = uppperLimit - (psize-1);
                for(Sobject sObj:listRecord) {
                    if(loopCntRelList < lowerLimit) {
                        loopCntRelList++; continue;
                    }
                    if(recLimit > 0 && loopCntRelList >3 ) { break; }
                    try {
                        practLocations.add(new PractitionerLocationWrapper(sObj)); 
                        loopCntRelList++;
                    } catch(Exception getProvidersEx) {
                        CoreControllerException getProvidersException = new CoreControllerException('ScheduleBuilderController.getProviders', 
                                                                                                            'Exception occured while doing iteration provider Locations'+getProvidersEx.getStackTraceString());
                    }
                }
            }
            wrapCls.fieldsWrapper = setWrapperFieldSet();
            wrapCls.resultWrapper = practLocations;
            wrapCls.providerEntityName = 'Contact';
            return JSON.serialize(wrapCls);
        } catch(Exception getProvidersEx) {
            CoreControllerException getProvidersException = new CoreControllerException('ScheduleBuilderController.getProviders', 
                                                                                                'Could not load Provider Locations'+getProvidersEx.getStackTraceString());
            return null;
        }
    }
    
    /********************************
    * @method: getProviderRecord
    * @description: get providers to schedule Events
    * Param: string,string, string, string, string, boolean, string, integer
    * Return type: List
    * *****************************/
    public static List<SObject> getProviderRecord(String sql, String filterBy, String filterOpt, String sortField, String sortDirection, Boolean isAlphaSearch, String searchKey, Integer recLimit, String pageSize, String pageNumber, ScheduleBuilderController wrapCls) {
        SObjectType pracLocTerrObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c');
        Schema.DescribeFieldResult pracLocTerrFieldDesc = pracLocTerrObjToken.getDescribe().fields.getMap().get(NamespaceSelector.baseNamespace+'Territory__c').getDescribe();
        Schema.DescribeFieldResult pracLocFieldDesc = pracLocTerrObjToken.getDescribe().fields.getMap().get(NamespaceSelector.baseNamespace+'PractitionerLocation__c').getDescribe();
        if( Util.checkObjectIsReadable(pracLocTerrObjToken) ) {
            String innerQuery = 'SELECT '+NamespaceSelector.baseNamespace+'PractitionerLocation__c FROM '+NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c WHERE Id !=null AND '+
                NamespaceSelector.baseNamespace+'Territory__r.OwnerId = \'' + UserInfo.getUserId() + '\'';
            if(filterBy == 'territory' && Util.checkFieldIsReadable(pracLocTerrObjToken, pracLocTerrFieldDesc)) {
                innerQuery += 'AND '+NamespaceSelector.baseNamespace+'Territory__r.Id  = \'' + String.escapeSingleQuotes(filterOpt) + '\'';
            }
            if(filterBy == 'practice' && Util.checkFieldIsReadable(pracLocTerrObjToken, pracLocFieldDesc)) {
                innerQuery += ' AND '+NamespaceSelector.baseNamespace+'PractitionerLocation__r.'+NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'PracticeGroup__c = \'' + String.escapeSingleQuotes(filterOpt) + '\'';
            }
            sql += ' WHERE Id IN ( '+innerQuery+' ) AND '+NamespaceSelector.baseNamespace+'Location__c != NULL AND '+NamespaceSelector.baseNamespace+'Provider__c != NULL';
            if(String.isNotEmpty(searchKey)){
                String sqlFilter = getFilterString(isAlphaSearch,searchKey);
                if(String.isNotEmpty(sqlFilter)) { sql+= ' AND '+sqlFilter; }
            }
            
            Schema.DescribeFieldResult provFieldDesc = pracLocTerrObjToken.getDescribe().fields.getMap().get(NamespaceSelector.baseNamespace+'Provider__c').getDescribe();
            Schema.DescribeFieldResult locFieldDesc = pracLocTerrObjToken.getDescribe().fields.getMap().get(NamespaceSelector.baseNamespace+'Location__c').getDescribe();
            List<String> ordFields = new List<String>();
            if( Util.checkFieldIsReadable(pracLocTerrObjToken, provFieldDesc) && Util.checkFieldIsReadable(pracLocTerrObjToken, locFieldDesc) ) {
                if(sortField == 'lLinkId') { ordFields.add(NamespaceSelector.baseNamespace+'Provider__r.LastName');
                } else if(sortField == 'fLinkId') { ordFields.add(NamespaceSelector.baseNamespace+'Provider__r.FirstName');
                } else if(sortField == 'sLinkId') { ordFields.add(NamespaceSelector.baseNamespace+'Provider__r.'+NamespaceSelector.baseNamespace+'Speciality__r.Name');
                } else if(sortField == 'pngLinkId') { ordFields.add(NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'PracticeGroup__r.Name');
                } else if(sortField == 'locationMailingAddress') { ordFields.add(NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'MailingAddress__c');
                } else if(sortField == 'locationPhone') { ordFields.add(NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'Telephone__c');
                } else { ordFields.add(NamespaceSelector.baseNamespace+'Provider__r.Name'); }
                if(ordFields.size() > 0) {
                    sql += ' ORDER BY '+String.join(ordFields,',');
                    if(String.IsNotBlank(sortDirection)) { sql += ' '+sortDirection; }
                }
                if(recLimit >0) { sql += ' LIMIT '+recLimit; 
				} else {
                    Integer psize = Integer.valueOf(pageSize);
                    Integer pnumber = Integer.valueOf(pageNumber);
                    if(psize>0 && pnumber>=0) { sql+= ' LIMIT '+pnumber*psize; }
                }
                String totalPlRecQuery = 'SELECT Count() FROM '+NamespaceSelector.baseNamespace+'PractitionerLocation__c WHERE Id != null AND Id IN ( '+innerQuery+' ) AND '+NamespaceSelector.baseNamespace+'Location__c != NULL AND '+NamespaceSelector.baseNamespace+'Provider__c != NULL';
                wrapCls.totalRec = Database.countQuery(totalPlRecQuery);
            }
            return Database.query(sql);
        } else {
            CoreControllerException methodException = new CoreControllerException('ScheduleBuilderController.getProviderRecord', 
                                                                                          'sobjects is not insertable/readable ==> '+pracLocTerrObjToken);
            return null;
        }
    }
    
    /********************************
    * @method: setWrapperFieldSet
    * @description: set wrapper fields
    * Param: none
    * Return type: List
    * *****************************/
    public static List<LogCallWrapperFieldSet> setWrapperFieldSet(){
        List<LogCallWrapperFieldSet> flswrapper = new List<LogCallWrapperFieldSet>();
        flswrapper.add(new LogCallWrapperFieldSet('firstName', 'PROVIDER: FIRST NAME','url','fLinkId'));
        flswrapper.add(new LogCallWrapperFieldSet('lastName', 'PROVIDER: LAST NAME','url','lLinkId'));
        flswrapper.add(new LogCallWrapperFieldSet('primarySpeciality', 'PROVIDER: PRIMARY SPECIALITY','url','sLinkId'));
        flswrapper.add(new LogCallWrapperFieldSet('practice', 'LOCATION: PRACTICE & GROUP','url','pngLinkId'));
        flswrapper.add(new LogCallWrapperFieldSet('locationMailingAddress', 'LOCATION: MAILING ADDRESS','text','locationMailingAddress'));
        flswrapper.add(new LogCallWrapperFieldSet('locationPhone', 'LOCATION: PHONE','text','locationPhone'));
        return flswrapper;
    }
    
    /********************************
    * @method: getFilterOptions
    * @description: get Filter Option
    * Param: string
    * Return type: string
    * *****************************/
    @AuraEnabled
    public static string getFilterOptions(String filterType){
        try {
            List<Util.FilterOptionWrapper> filterOpt = new List<Util.FilterOptionWrapper>();
            string sourceEntityName ='';
            Map<String, List<String>> reqFieldMap;
            switch on filterType{
                when 'territory' { filterOpt = getTerritories(); }
                when 'practice' { filterOpt = getPractices(); }
            }
            return JSON.serialize(filterOpt);
        } catch(Exception getFilterOptionsEx) {
            CoreControllerException getFilterOptionsException = new CoreControllerException('ScheduleBuilderController.getFilterOptions', 
                                                                                                    'Exception occured while doing chnage of filter Options'+getFilterOptionsEx.getStackTraceString());
            return null;
        }
    }
    
    /********************************
    * @method: getTerritories
    * @description: get territories for logged in user
    * Param: none
    * Return type: List
    * *****************************/
    public static List<Util.FilterOptionWrapper> getTerritories(){
        return LogCallController.getTerritories();
    }
    
    /********************************
    * @method: getPractices
    * @description: get practices for logged in user
    * Param: none
    * Return type: List
    * *****************************/
    public static List<Util.FilterOptionWrapper> getPractices(){
        integer picklistLm = 100;
        List<Util.FilterOptionWrapper> filterOpt = new List<Util.FilterOptionWrapper>();
        SObjectType pracLocTerrObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c');
        if ( Util.checkObjectIsReadable(pracLocTerrObjToken) ) {
            String getDistinctPractSql = 'SELECT '+NamespaceSelector.baseNamespace+'PractitionerLocation__r.'+NamespaceSelector.baseNamespace+'Location__r.'+
                NamespaceSelector.baseNamespace+'PracticeGroup__c practiceId FROM '+NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c WHERE Id != null AND '+
                NamespaceSelector.baseNamespace+'Territory__r.OwnerId = \'' + UserInfo.getUserId() + '\''+
                ' AND '+NamespaceSelector.baseNamespace+'PractitionerLocation__r.'+NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'PracticeGroup__c !=null'+
                ' GROUP BY '+NamespaceSelector.baseNamespace+'PractitionerLocation__r.'+NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'PracticeGroup__c LIMIT '+picklistLm;
            
            List<AggregateResult> ags = database.query(getDistinctPractSql);
            Set<Id> practiceIds = new Set<Id>();
            for(AggregateResult ag : ags) { practiceIds.add((Id) ag.get('practiceId')); }
            if(practiceIds.size()>0) {
                if (!Schema.sObjectType.Account.fields.Name.isAccessible() ) {
                    CoreControllerException methodException = new CoreControllerException('ScheduleBuilderController.getPractices', 
                                                                                                  'sobjects is not readable ==> Account');
                    return null;
                } else {
                    String practiceSql = 'SELECT Id, Name from Account';
                    if(practiceSql != null) { practiceSql += ' WHERE Id IN :practiceIds ORDER BY Name ASC'; }
                    list<Sobject> listRecord = database.query(practiceSql);
                    if(listRecord.size()>0) {
                        for(Sobject sObj:listRecord) { filterOpt.add(new Util.FilterOptionWrapper(sObj)); }
                    } 
                }
            }
            return filterOpt;
		} else {
            CoreControllerException methodException = new CoreControllerException('ScheduleBuilderController.getPractices', 
                                                                                          'sobjects is not insertable/readable ==> '+pracLocTerrObjToken);
            return null;
        }
    }
    
    /********************************
    * @Wrapper Class Name: LogCallWrapperFieldSet
    * @description: This class is used to hold field wrapper
    * @Last Modified Date: 10/06/2020
    * *****************************/
    public class LogCallWrapperFieldSet {
        @AuraEnabled public String fieldName;
        @AuraEnabled public String label;
        @AuraEnabled public String fieldType;
        @AuraEnabled public Boolean sortable;
        @AuraEnabled public String linkfieldName;
        
        /********************************
        * @Method: LogCallWrapperFieldSet
        * @description: Constructor to set the Class attribute
        * @return: N/A
        * *****************************/
        public LogCallWrapperFieldSet(String fieldName,String label,string fieldType,string linkfieldName) {
            this.fieldName = fieldName;
            this.label = label;
            this.fieldType = fieldType;
            this.sortable = true;
            this.linkfieldName = linkfieldName;
        }
    }
    
    /********************************
    * @Wrapper Class Name: PractitionerLocationWrapper
    * @description: This class is used to hold provider locations
    * @Last Modified Date: 10/06/2020
    * *****************************/
    public class PractitionerLocationWrapper{
        @AuraEnabled Public String id;
        @AuraEnabled Public String pId;
        @AuraEnabled Public String pngId;
        @AuraEnabled Public String sId;
        @AuraEnabled public String firstName;
        @AuraEnabled public String lastName;
        @AuraEnabled public String primarySpeciality;
        @AuraEnabled public String practice;
        @AuraEnabled public String locationMailingAddress;
        @AuraEnabled public String locationPhone;
        @AuraEnabled public String fLinkId;
        @AuraEnabled public String lLinkId;
        @AuraEnabled public String pngLinkId;
        @AuraEnabled public String sLinkId;
        
        /********************************
        * @Method: PractitionerLocationWrapper
        * @description: Constructor to set the Class attribute from Object using Map
        * @return: N/A
        * *****************************/
        public PractitionerLocationWrapper(Sobject sobj) {
            this.id = (String)sObj.Id;
            this.firstName = (String)sObj.getSObject(NamespaceSelector.baseNamespace+'Provider__r').get('FirstName');          
            this.lastName = (String)sObj.getSObject(NamespaceSelector.baseNamespace+'Provider__r').get('LastName');            
            if(sObj.getSObject(NamespaceSelector.baseNamespace+'Provider__r').getSObject(NamespaceSelector.baseNamespace+'Speciality__r')!=null){
                this.sId = (String)sObj.getSObject(NamespaceSelector.baseNamespace+'Provider__r').getSObject(NamespaceSelector.baseNamespace+'Speciality__r').get('Id');
                this.primarySpeciality = (String)sObj.getSObject(NamespaceSelector.baseNamespace+'Provider__r').getSObject(NamespaceSelector.baseNamespace+'Speciality__r').get('Name');
            }
            if(sObj.getSObject(NamespaceSelector.baseNamespace+'Location__r').getSObject(NamespaceSelector.baseNamespace+'PracticeGroup__r')!=null){
                this.pngId = (String)sObj.getSObject(NamespaceSelector.baseNamespace+'Location__r').getSObject(NamespaceSelector.baseNamespace+'PracticeGroup__r').get('Id');
                this.practice = (String)sObj.getSObject(NamespaceSelector.baseNamespace+'Location__r').getSObject(NamespaceSelector.baseNamespace+'PracticeGroup__r').get('Name');
            }               
            this.locationMailingAddress = ((String)sObj.getSObject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'MailingAddress__c')).replaceAll('<br>', ', ');           
            this.locationPhone = (String)sObj.getSObject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'Telephone__c');
            this.pId = (String)sObj.getSObject(NamespaceSelector.baseNamespace+'Provider__r').get('Id');             
            this.fLinkId = this.lLinkId = '/'+this.pId;
            if(this.pngId != null) { this.pngLinkId = '/'+this.pngId; }
            if(this.sId != null) { this.sLinkId = '/'+this.sId; }
        }
    }
    
    /********************************
    * @method: getLoggedInUser
    * @description: get Logged in user to show default assigned to in UI
    * Param: none
    * Return type: User
    * *****************************/
    @AuraEnabled
    public static User getLoggedInUser() { return [SELECT Id,Name FROM User WHERE Id = :UserInfo.getUserId()]; }
    
    /********************************
    * @method: getFilterString
    * @description: generate filter string criteria for alphabetical search
    * Param: boolean, string
    * Return type: string
    * *****************************/
    public static String getFilterString(boolean isAlphaSearch, String searchKey) {
        return Util.getFilterStringServices(isAlphaSearch, searchKey);
    }
    
    /********************************
    * @method: getPicklistValues
    * @description: get the picklist values to show on UI
    * @param: none
    * @return: Map of field and picklist values
    * *****************************/
    @AuraEnabled
    public static Map<string,List<String>> getPicklistValues() {
        return Util.getPicklistValServices();
    }
    
    /********************************
    * @method: saveEvents
    * @description: to create events for provider locations
    * Param: List
    * Return type: string
    * *****************************/
    @AuraEnabled
    public static String saveEvents(List<Event> events) {
        try{
            if( Schema.SObjectType.Event.isAccessible() && Schema.SObjectType.Event.isCreateable()){
                Database.insert(events);
            } else {
                CoreControllerException getQueryLogException = new CoreControllerException('ScheduleBuilderController.saveEvents', 
                                                                                                   'sobjects is not insertable/readable ==> Event');
            }
        } catch(DmlException saveEventsEx) {
            CoreControllerException saveEventsException = new CoreControllerException('ScheduleBuilderController.saveEvents', 
                                                                                          'Could not save Tasks'+saveEventsEx.getStackTraceString()); 
            EvErrorWrapper err = new EvErrorWrapper(false,'The following exception has occurred: ' + saveEventsEX.getMessage());
            return JSON.serialize(err);
        }
        return null;
    }
    
    /********************************
    * @Wrapper Class Name: EvErrorWrapper
    * @description: This class is used to hold event errors
    * @Last Modified Date: 10/06/2020
    * *****************************/
    public class EvErrorWrapper {
        @AuraEnabled public Boolean success;
        @AuraEnabled public String message;
        
        /********************************
        * @Method: EvErrorWrapper
        * @description: Constructor to set the Class attribute from Object using Map
        * @return: N/A
        * *****************************/
        public EvErrorWrapper(Boolean success, String message) {
            this.success = success;
            this.message = message;
        }
    }
}