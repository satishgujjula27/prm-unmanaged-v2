/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: PRM3uRestController
* @description: This class is to handle Activity API.
* @Last Modified Date: 10/04/2020
**/
global inherited sharing class PRM3uRestController {

    public static final String EMPTY = '';

	/********************************
    * @method: maxRecordsParam
    * @description: finding maxrecords record params from request
    * @return: List<PRM3uRestController.Activity>
    * *****************************/
    public static Integer maxRecordsParam(RestRequest request) {
        string maxrecords = request.params.get('maxrecords');
        if(maxrecords != NULL && maxrecords.isNumeric()) {
            return Integer.valueOf(maxrecords);
        } else {
            return 5;
        }
    }

    /********************************
    * @method: Activity
    * @description: This class is used to hold and expose the Api attribute
    * @return: List<PRM3uRestController.Activity>
    * *****************************/
    global inherited sharing class Activity {
        public String id { get { return String.isBlank(this.id) ? PRM3uRestController.EMPTY : this.id; } set; }
        public String subject { get { return String.isBlank(this.Subject) ? PRM3uRestController.EMPTY : this.Subject; } set; }
        public String status { get { return String.isBlank(this.Status) ? PRM3uRestController.EMPTY : this.Status; } set; }
        public String comments { get { return String.isBlank(this.comments) ? PRM3uRestController.EMPTY : this.comments; } set; }
        public DateTime createdDate { get; private set; }
        public String dueDate { get; private set; }
        public ProviderLite provider { get; set; }
        public String month { get;private set; }
        public integer monthNum { get;private set; }

        /********************************
        * @Method: PractitionerIssueWrapper
        * @description: Constructor to set the Class attribute from Object using Map
        * @return: N/A
        * *****************************/
        global Activity(Sobject sobj) {
            this.Id = (String)sObj.get('Id');
            this.createdDate = (Datetime)sObj.get('createdDate');
            this.subject = (String)sObj.get(NamespaceSelector.baseNamespace+'Subject__c');
            this.status = (String)sObj.get(NamespaceSelector.baseNamespace+'Status__c');
            this.comments = (String)sObj.get(NamespaceSelector.baseNamespace+'LongDescription__c');
            this.dueDate = String.valueOf(sObj.get(NamespaceSelector.baseNamespace+'DateTimeOpened__c'));
            this.month = (sObj.get('createdDate') == null ? '' : Month(((Datetime)sObj.get('createdDate')).month()));
            this.provider = new PRM3uRestController.ProviderLite(sobj);
        }

        public Activity(Task t) {
            this.id = t.Id;
            this.subject = t.Subject;
            this.comments = t.Description;
            this.createdDate = t.CreatedDate;
            this.dueDate = (t.ActivityDate == null ? PRM3uRestController.Empty : String.valueOf(t.ActivityDate));
            this.status = t.Status;
            this.month = (t.ActivityDate == null ? PRM3uRestController.Empty : Month(t.ActivityDate.month()));
            this.monthNum = (t.ActivityDate == null ? 0 : t.ActivityDate.month());
            this.provider = new PRM3uRestController.ProviderLite(t);
        }
    }

    /********************************
    * @Method: Activity
    * @description: This class is used to hold and expose the Api attribute
    * @return: N/A
    * *****************************/
    global inherited sharing class ProviderLite {
        public String id { get { return String.isBlank(this.id) ? PRM3uRestController.EMPTY: this.id; } set; }
        public String name { get { return String.isBlank(this.name) ? PRM3uRestController.EMPTY: this.name; } set; }

        /********************************
        * @Method: ProviderLite
        * @description: for Issue to set provider with reverse mapping
        * @return: N/A
        * *****************************/
        public ProviderLite(Sobject sObj) {
            if(sObj.getSobject(NamespaceSelector.baseNamespace+'Provider__r') !=null){
                this.Id = (string)sObj.getSobject(NamespaceSelector.baseNamespace+'Provider__r').get('Id');
                this.name = (string)sObj.getSobject(NamespaceSelector.baseNamespace+'Provider__r').get('Name');
            }
        }

        /********************************
        * @Method: ProviderLite
        * @description: for Task & Call to set provider using WhoId
        * @return: N/A
        * *****************************/
        public ProviderLite(Task t) {
            this.id = t.Who.Id;
            this.name = t.Who.Name;
        }
    }

    /********************************
    * @Method: Result
    * @description: This class is used to hold and expose the Count of Activity
    * @return: N/A
    * *****************************/
    global inherited sharing class Result {
        public String label { get;private set; }
        public Integer totalCount { get;private set; }

        /********************************
        * @Method: Result
        * @description: constructor for set the status and count
        * @return: N/A
        * *****************************/
        public Result(String status, Integer count) {
            this.label = status;
            this.totalCount = count;
        }
    }

    /********************************
    * @Method: Month
    * @description: to find out the full month name from month no
    * @return: String
    * *****************************/
    public static String Month(integer month) {
        String monthName = '';
        switch on month {
            when 1 { monthName = 'January'; }
            when 2 { monthName = 'February'; }
            when 3 { monthName = 'March'; }
            when 4 { monthName = 'April'; }
            when 5 { monthName = 'May'; }
            when 6 { monthName = 'June'; }
            when 7 { monthName = 'July'; }
            when 8 { monthName = 'August'; }
            when 9 { monthName = 'September'; }
            when 10 { monthName = 'October'; }
            when 11 { monthName = 'November'; }
            when 12 { monthName = 'December'; }
        }
        return monthName;
    }


    /********************************
    * @Method: generateCallSql
    * @description: generate the sql
    * @return: String
    * *****************************/
    public static String generateCallSql(List<String> filters, Integer recordLimit) {
        if(filters == null) { filters = new List<String>(); }
        list<RecordType> recordtypeExist = [SELECT Id FROM RecordType WHERE SobjectType ='Task' AND IsActive = true AND DeveloperName IN ('Recruitment', 'RelationshipManagement')];
        // for fetching results for logged in user
        filters.add('OwnerId =\''+UserInfo.getUserId()+'\'');
        string recTypeId = Util.getTaskRecordType('PRM')+'\'';
        if( recordtypeExist.size() > 0) {
            for( RecordType recType : recordtypeExist) {
                recTypeId += ',\''+recType.Id+'\'';
            }
        }
        if(recTypeId !=null) { filters.add('recordTypeId IN (\''+recTypeId+')'); }
        list<string> taskFields = new List<string> {
            'Id', 'Owner.Id', 'Owner.Name', 'Who.Id', 'Who.Name', 'OwnerId', 'Subject', 'Description', 'CreatedDate', 'LastModifiedDate', 'ActivityDate', 'Status'
        };
		
        String taskSql = 'SELECT '+string.join(taskFields,',')+' FROM Task';
        if(filters.size() > 0 && Schema.sObjectType.Task.isAccessible() ) {
            taskSql += (' WHERE ' + Util.join(filters, ' AND '));
        } else {
            CoreControllerException methodException = new CoreControllerException('PRM3uRestController.generateCallSql', 
																						  'sobjects is not insertable/readable ==> '+taskSql);
            return null;
        }
        taskSql += ' ORDER BY CreatedDate desc';
        if(recordLimit > 0) { taskSql += ' LIMIT ' +recordLimit; }
        return taskSql;
    }

}