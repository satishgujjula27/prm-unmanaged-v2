/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: GenericActionController
* @description: This Class is used to do Action Button component 
* @Last Modified Date: 04/07/2021
**/
public inherited sharing class GenericActionController {
    public static Map<string,string> linkIconMap = new Map<string,string>();
    public static Map<string,string> compNameMap = new Map<string,string>();
    public static Map<string,boolean> isModalShowMap = new Map<string,boolean>();
    public static Map<string,boolean> isRenderSalesforce1Map= new Map<string,boolean>();
	
    /********************************
    * @Method: queryWebLinks
    * @description: constructor to query weblinks metadata
    * @return: list of weblink wrapper object
    * *****************************/
    @AuraEnabled
    public static List<WeblinkWrapper> queryWebLinks(String objApiName) {
        List<WeblinkWrapper> webLinks = new List<WeblinkWrapper>();
        getIconMetaData();
        List<string> includeWebLinks = new List<String>{ 'AddtoMyTerritories', 'PracticePrintableView','ProviderPrintableView', 'LogActivity' };
		for(Sobject webObj:Database.query('SELECT Id, Url, MasterLabel, Description, Name FROM WebLink WHERE PageOrSobjectType =: objApiName AND Name IN :includeWebLinks')){
            webLinks.add(new WeblinkWrapper(webObj));
        }
        return webLinks;
    }
    
    /********************************
    * @Method: getIconMetaData
    * @description: constructor to query actionbuttonconfig metadata for icons.
    * @return: none
    * *****************************/
    public static void getIconMetaData() {
        try {
            List<string> metaDataFields = new List<string>{
                'Id', 'DeveloperName', 'MasterLabel',
                NamespaceSelector.hgprmNamespace+'IconName__c',
                NamespaceSelector.hgprmNamespace+'ComponentName__c',
                NamespaceSelector.hgprmNamespace+'IsShowModal__c',
                NamespaceSelector.hgprmNamespace+'Salesforce1__c'
            };
            List<Sobject> lstMetaObjs = Database.query('SELECT '+string.join(metaDataFields,',')+' FROM '+NamespaceSelector.hgprmNamespace+'ActionButtonsConf__mdt');
            for(Sobject actBtnConfObj:lstMetaObjs){
                linkIconMap.put((string)actBtnConfObj.get('DeveloperName'),(string)actBtnConfObj.get(NamespaceSelector.hgprmNamespace+'IconName__c'));
                compNameMap.put((string)actBtnConfObj.get('DeveloperName'),(string)actBtnConfObj.get(NamespaceSelector.hgprmNamespace+'ComponentName__c'));
                isModalShowMap.put((string)actBtnConfObj.get('DeveloperName'),(boolean)actBtnConfObj.get(NamespaceSelector.hgprmNamespace+'IsShowModal__c'));
                isRenderSalesforce1Map.put((string)actBtnConfObj.get('DeveloperName'),(boolean)actBtnConfObj.get(NamespaceSelector.hgprmNamespace+'Salesforce1__c'));
            }   
        } catch(Exception getIconMdtEx) {
            CoreControllerException errLogException = new CoreControllerException('GenericActionController.getIconMetaData', 'Could not get icon metadata'+getIconMdtEx.getStackTraceString());
        }
    }
    
    /********************************
    * @Method: WeblinkWrapper
    * @description: wrapper to send icon metadata record to front-end.
    * @return: wrapper object
    * *****************************/
    public class WeblinkWrapper{
        @AuraEnabled public String Url;
        @AuraEnabled public String iconName;
        @AuraEnabled public String Description;
        @AuraEnabled public String MasterLabel;
        @AuraEnabled public String compName;
        @AuraEnabled public String Id;
        @auraEnabled public boolean isModal;
        @auraEnabled public boolean isRenderSalesforce1;
        
        public WeblinkWrapper(Sobject webLinkObj) {
            this.Id = (String)webLinkObj.get('Id');
            this.Url = (String)webLinkObj.get('Url');
            this.Description = (String)webLinkObj.get('Description');
            this.MasterLabel = (String)webLinkObj.get('MasterLabel');
            if(linkIconMap.containsKey((String)webLinkObj.get('Name'))) {
                this.iconName = linkIconMap.get((String)webLinkObj.get('Name'));
            }
            if(compNameMap.containsKey((String)webLinkObj.get('Name'))) {
                this.compName = compNameMap.get((String)webLinkObj.get('Name'));
            } else {
                this.iconName = 'utility:touch_action';
            }
            if(isModalShowMap.containsKey((String)webLinkObj.get('Name'))) {
                this.isModal = isModalShowMap.get((String)webLinkObj.get('Name'));
            } else {
                this.isModal = true;   
            }
            if(isRenderSalesforce1Map.containsKey((String)webLinkObj.get('Name'))) {
                this.isRenderSalesforce1 = isRenderSalesforce1Map.get((String)webLinkObj.get('Name'));
            } else {
                this.isRenderSalesforce1 = false;
            }
        }
    }
}