/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying 
* or sale of the above may constitute an infringement of copyright 
* and may result in criminal or other legal proceedings.
*
* @Class Name: LookupControllerTest
* @description: This is the test class lookupController
* @Last Modified Date: 23/12/2019
**/
@isTest
public class LookupControllerTest {
    @isTest
    static void testSearchDB(){
         // Create Test Account
        Account testAccount = new Account ();
        testAccount.Name = 'Test Account1';
        insert testAccount;
        
        string result = LookupController.searchDB('Account','Name','Id',1,'Name','Test Account1');
        List<LookupController.ResultWrapper> data = (List<LookupController.ResultWrapper>)System.JSON.deserialize(result,List<LookupController.ResultWrapper>.class);
        system.assertNotEquals(null, data.size());
    }
}