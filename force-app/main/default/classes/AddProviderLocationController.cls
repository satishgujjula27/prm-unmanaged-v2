/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: AddProviderLocationController
* @description: This Class is used to handle add provider location functionality
* @Last Modified Date: 03/20/2020
**/
public inherited sharing class AddProviderLocationController {
    List<AddProviderLocationController.LocationWrapper> listLocWrapper;
    List<TableWrapperFieldSet> listTableWrapperFset;
    boolean isPracticeAssociated;
    string provLocObjName;
    string practiceObjName;
    Map<string,string>  provLocFieldMap;
    string provPractObjName;
    Map<string,string>  provPractFieldMap;
    string providerName = '';
    string providerObjName;
    public Integer totalRec;
    
    /********************************
    * @Method: getLocations
    * @description: Constructor to get location records
    * @return: N/A
    * *****************************/
    @AuraEnabled
    public static string getLocations(Id recordId, String pageSize, String pageNumber) {
        try{
            AddProviderLocationController aplObject = new AddProviderLocationController();
            List<AddProviderLocationController.LocationWrapper> locationLst = new List<AddProviderLocationController.LocationWrapper>();
            
            // setting table field
            List<TableWrapperFieldSet> flswrapper = new List<TableWrapperFieldSet>();
            flswrapper.add(new TableWrapperFieldSet('LocationName', 'LOCATION NAME','text'));
            flswrapper.add(new TableWrapperFieldSet('address', 'MAILING STREET','text'));
            flswrapper.add(new TableWrapperFieldSet('city', 'MAILING CITY','text'));
            flswrapper.add(new TableWrapperFieldSet('state', 'MAILING  STATE','text'));
            flswrapper.add(new TableWrapperFieldSet('zip', 'MAILING POSTAL CODE','text'));
            flswrapper.add(new TableWrapperFieldSet('phone', 'PHONE','text'));
            aplObject.listTableWrapperFset = flswrapper;
            
            String locationSql = getLocationQuery();
            if(checkProviderPractAssoc(recordId)) {
                aplObject.isPracticeAssociated = true;
                List<Sobject> listRecord = getRecord(locationSql, recordId, pageSize, pageNumber, aplObject);
                if(listRecord.size()>0) {
                    Integer loopCntRelList = 1;
                    // for custom pagination logic
                    Integer psize = Integer.valueOf(pageSize);
                    Integer pnumber = Integer.valueOf(pageNumber);
                    Integer uppperLimit = psize * pnumber;
                    Integer lowerLimit = uppperLimit - (psize-1);
                    
                    for(SObject sObj:listRecord) {
                        if(loopCntRelList < lowerLimit) {
                            loopCntRelList++; continue;
                        }
                        try {
                            locationLst.add(new AddProviderLocationController.LocationWrapper(sObj));
                            loopCntRelList++;
                        } catch(Exception listRecordEx) {
                            CoreControllerException errLogException = new CoreControllerException('AddProviderLocationController.getLocations', 
                                                                                                          'Could not get Locations ==> '+listRecordEx.getStackTraceString());
                        }
                    }
                }
            } else {
                aplObject.isPracticeAssociated = false;
            }
            if (!Contact.sObjectType.getDescribe().isAccessible()) {
                CoreControllerException errLogException = new CoreControllerException('AddProviderLocationController.getLocations', 
                                                                                              'Contact object is not enabled for this user');
                return null;
            }
            List<Sobject> provRecord = Database.query('SELECT Name FROM Contact Where Id = :recordId LIMIT 1');
            if(provRecord.size() > 0 && Schema.sObjectType.Contact.fields.Name.isAccessible() ){
                aplObject.providerName = (string)provRecord[0].get('Name');
            }
            aplObject.listLocWrapper = locationLst;
            aplObject.practiceObjName = 'Account';
            return JSON.serialize(aplObject);
        } catch(Exception getLocEx) {
            CoreControllerException errLogException = new CoreControllerException('AddProviderLocationController.getLocations', 
                                                                                          'Could not get Locations ==> '+getLocEx.getStackTraceString());
            return null;
        }
    }
    
    /********************************
    * @Method: getRecord
    * @description: Constructor to get records based on location params passed
    * @return: list
    * *****************************/
    public static List<Sobject> getRecord(String sql, Id recordId, String pageSize, String pageNumber, AddProviderLocationController aplObject){
        try {
            SObjectType locObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Location__c');
            SObjectType provLocObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocation__c');
            SObjectType pracGroupLocObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PracticeGroupLocation__c');
            SObjectType provpracGroupObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'ProviderPracticeGroup__c');
            if ( !Util.checkObjectIsReadable(locObjToken) && !Util.checkObjectIsReadable(provLocObjToken) && 
                !Util.checkObjectIsReadable(provpracGroupObjToken) && !Util.checkObjectIsReadable(pracGroupLocObjToken) ) {
                CoreControllerException errLogException = new CoreControllerException('AddProviderLocationController.getRecord', 
                                                                                              'All four sobjects are not accessible ==> '+locObjToken+'--'+
                                                                                              provLocObjToken+'--'+provpracGroupObjToken+'--'+pracGroupLocObjToken);
                return null;
            }
            
            String excludePractLocQuery = ''; String includePractLocQuery = '';
            
            String totalRecQuery = 'SELECT COUNT() FROM '+NamespaceSelector.baseNamespace+'Location__c';
            excludePractLocQuery = 'SELECT '+NamespaceSelector.baseNamespace+'Location__c FROM '+NamespaceSelector.baseNamespace+'PractitionerLocation__c WHERE '+NamespaceSelector.baseNamespace+'Provider__c = :recordId AND Id != null';
            sql += ' WHERE Id NOT IN ( '+excludePractLocQuery+' )';
            totalRecQuery += ' WHERE Id NOT IN ( '+excludePractLocQuery+' )';
            
            // get the practice group of the provider from Provider Practice & group so that we can pass it to PGroupLocation
            Set<Id> pracGroupIds = new Set<Id>();
            String pGroupLocRecQuery = 'SELECT '+NamespaceSelector.baseNamespace+'PracticeGroup__c FROM '+NamespaceSelector.baseNamespace+'ProviderPracticeGroup__c WHERE '+NamespaceSelector.baseNamespace+'Provider__c = :recordId AND Id != null';
            List<SObject> pGroupLocLst = Database.query(pGroupLocRecQuery);
            for(SObject practiceGrouplocRecs : pGroupLocLst) {
                pracGroupIds.add( (Id)practiceGrouplocRecs.get(NamespaceSelector.baseNamespace+'PracticeGroup__c') );
            }
            
            includePractLocQuery = 'SELECT '+NamespaceSelector.baseNamespace+'Location__c FROM '+NamespaceSelector.baseNamespace+'PracticeGroupLocation__c WHERE '+NamespaceSelector.baseNamespace+'PracticeGroup__c = :pracGroupIds';
            sql += ' AND Id IN ('+includePractLocQuery+' )';
            totalRecQuery += ' AND Id IN ('+includePractLocQuery+' )';
            
            sql +=' ORDER BY Name ASC ';
            Integer psize = Integer.valueOf(pageSize);
            Integer pnumber = Integer.valueOf(pageNumber);
            if(psize>0 && pnumber>=0) { sql+= ' LIMIT '+pnumber*psize; }
            if( !String.isEmpty(sql) ) {
                CoreControllerException getRecordLogExption = new CoreControllerException('TerritoryManagementController.getPLData', 
                                                                                                  'Exception occured while doing iteration for filter data in SOQL => '+sql);
            }
            aplObject.totalRec = Database.countQuery(totalRecQuery);
            return Database.query(sql);
        } catch(Exception getRecordex) {
            CoreControllerException getRecordLogException = new CoreControllerException('AddProviderLocationController.getRecord', 
                                                                                                'Could not get record ==> '+getRecordex.getStackTraceString());
            return null;
        }
    }
    
    /********************************
    * @Method: LocationWrapper
    * @description: Constructor to set the Class attribute from Object using Map
    * @return: N/A
    * *****************************/
    public static string getLocationQuery() {
        List<String> fields = new List<String>{
            'Name', 
            NamespaceSelector.baseNamespace+'AddressLine1__c',
            NamespaceSelector.baseNamespace+'AddressLine2__c',
            NamespaceSelector.baseNamespace+'AddressLine3__c',
            NamespaceSelector.baseNamespace+'City__c',
            NamespaceSelector.baseNamespace+'StateCode__c',
            NamespaceSelector.baseNamespace+'Zip__c',
            NamespaceSelector.baseNamespace+'Telephone__c'
        };
        
        SObjectType locObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Location__c');
        if ( !Util.checkObjectIsReadable(locObjToken) ) {
            CoreControllerException getQueryLogException = new CoreControllerException('AddProviderLocationController.getLocationQuery', 
                                                                                               'All three sobjects are not accessible ==> '+locObjToken);
            return null;
        } else {
            return 'SELECT '+String.join(fields,',')+' FROM '+NamespaceSelector.baseNamespace+'Location__c';   
        }
    }
    
    /********************************
    * @Method: LocationWrapper
    * @description: Constructor to set the Class attribute from Object using Map
    * @return: N/A
    * *****************************/
    public static Boolean checkProviderPractAssoc(Id recordId) {
        Boolean finalResult;
        SObjectType provPracGrpObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'ProviderPracticeGroup__c');
        Schema.DescribeFieldResult provPracGrpFieldDesc = provPracGrpObjToken.getDescribe().fields.getMap().get(NamespaceSelector.baseNamespace+'Provider__c').getDescribe();
        if ( Util.checkObjectIsReadable(provPracGrpObjToken) && Util.checkFieldIsReadable(provPracGrpObjToken, provPracGrpFieldDesc) ) {
            String sql = 'SELECT COUNT() FROM '+NamespaceSelector.baseNamespace+'ProviderPracticeGroup__c WHERE '+NamespaceSelector.baseNamespace+'Provider__c = :recordId AND Id != null';
            if(Database.countQuery(sql)>0) {
                finalResult = true;
            } else {
                finalResult = false;
            }
        } else {
            CoreControllerException getQueryLogException = new CoreControllerException('AddProviderLocationController.checkProviderPractAssoc', 
                                                                                               'sobjects iss not accessible ==> '+provPracGrpObjToken);
            return null;
        }
        return finalResult;
    }
    
    /********************************
    * @Method: LocationWrapper
    * @description: Constructor to set the Class attribute from Object using Map
    * @return: N/A
    * *****************************/
    @AuraEnabled
    public static Boolean saveProviderLocation(Id recordId, List<String> locations, string isPrimaryLocId){
        Boolean finalResult;
        try {
            sObject sObj = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocation__c').newSObject();
            SObjectType provPracGrpObjToken = sObj.getSObjectType();
            List<Sobject> objList = new List<Sobject>();
            if(locations.size() > 0 && Util.checkObjectIsReadable(provPracGrpObjToken) && Util.checkObjectIsInsertable(provPracGrpObjToken)) {
                for(string loc:locations){
                    Sobject pl = sObj.clone();
                    pl.put(NamespaceSelector.baseNamespace+'Provider__c', recordId);
                    pl.put(NamespaceSelector.baseNamespace+'Location__c', loc);
                    if(loc == isPrimaryLocId) {
                        pl.put(NamespaceSelector.baseNamespace+'IsPrimary__c', true);
                    }
                    pl.put('Name', 'PL');
                    objList.add(pl);
                }
                Database.insert(objList);
                finalResult = true;
            } else {
                CoreControllerException getQueryLogException = new CoreControllerException('AddProviderLocationController.saveProviderLocation', 
                                                                                                   'sobjects is not insertable/readable ==> '+provPracGrpObjToken);
                finalResult = false;
            }
        } catch(Exception saveProvLocex) {
            CoreControllerException errLogException = new CoreControllerException('AddProviderLocationController.saveProviderLocation', 
                                                                                          'Could not save provider Locations'+saveProvLocex.getStackTraceString());
            finalResult = false;
        }
        return finalResult;
    }
    
    /********************************
    * @Method: LocationWrapper
    * @description: Constructor to set the Class attribute from Object using Map
    * @return: N/A
    * *****************************/
    @AuraEnabled
    public static Boolean saveProviderPractice(Id recordId, Map<String, String> provPractAttr){
        Boolean saveProvPracBoolean;
        try {
            sObject provPracsObjDesc = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'ProviderPracticeGroup__c').newSObject();
            SObjectType provPracObjToken = provPracsObjDesc.getSObjectType();
            List<Sobject> objList = new List<Sobject>();
            if(provPractAttr.containsKey('PracticeGroup') && String.isNotEmpty(provPractAttr.get('PracticeGroup')) && 
               Util.checkObjectIsReadable(provPracObjToken) && Util.checkObjectIsInsertable(provPracObjToken)) {
                Sobject sobjProvPractice = provPracsObjDesc;
                sobjProvPractice.put(NamespaceSelector.baseNamespace+'Provider__c', recordId);
                sobjProvPractice.put(NamespaceSelector.baseNamespace+'PracticeGroup__c', provPractAttr.get('PracticeGroup'));
                if(provPractAttr.containsKey('LocationIsPrimary')) {
                    sobjProvPractice.put(NamespaceSelector.baseNamespace+'LocationIsPrimary__c', Boolean.valueOf(provPractAttr.get('LocationIsPrimary')));
                }
                sobjProvPractice.put('Name', 'PP');
                Database.insert(sobjProvPractice);
                saveProvPracBoolean = true;
            } else {
                CoreControllerException getQueryLogException = new CoreControllerException('AddProviderLocationController.saveProviderPractice', 
                                                                                                   'sobjects is not insertable/readable ==> '+provPracObjToken);
                saveProvPracBoolean = false;
            }
        } catch(Exception saveProvPracEx) {
            CoreControllerException errLogException = new CoreControllerException('AddProviderLocationController.saveProviderPractice', 
                                                                                          'Could not save provider Locations ==> '+saveProvPracEx.getStackTraceString());
            saveProvPracBoolean = false;
        }
        return saveProvPracBoolean;
    }
    
    /********************************
    * @Method: LocationWrapper
    * @description: Constructor to set the Class attribute from Object using Map
    * @return: N/A
    * *****************************/
    public class LocationWrapper {
        public String Id;
        public String LocationName;
        public String address;
        public String city;
        public String state;
        public String zip;
        public String phone;
        
        public LocationWrapper(Sobject sobj) {
            this.Id = (String)sObj.get('Id');
            this.LocationName = (String)sObj.get('Name');
            if(sObj.get(NamespaceSelector.baseNamespace+'AddressLine1__c') !=null) {
                this.address = (String)sObj.get(NamespaceSelector.baseNamespace+'AddressLine1__c') + ' ';
            }
            if(sObj.get(NamespaceSelector.baseNamespace+'AddressLine2__c') !=null) {
                this.address += (String)sObj.get(NamespaceSelector.baseNamespace+'AddressLine2__c') + ' ';
            }
            if(sObj.get(NamespaceSelector.baseNamespace+'AddressLine3__c') !=null) {
                this.address += (String)sObj.get(NamespaceSelector.baseNamespace+'AddressLine3__c');
            }
            this.city = (String)sObj.get(NamespaceSelector.baseNamespace+'City__c');
            this.state = (String)sObj.get(NamespaceSelector.baseNamespace+'StateCode__c');
            this.zip = (String)sObj.get(NamespaceSelector.baseNamespace+'Zip__c');
            this.phone = (String)sObj.get(NamespaceSelector.baseNamespace+'Telephone__c');
        }
    }
    
    /********************************
    * @Method: TableWrapperFieldSet
    * @description: Constructor to set the Class attribute from Object using Map
    * @return: N/A
    * *****************************/
    public class TableWrapperFieldSet {
        @AuraEnabled public String fieldName;
        @AuraEnabled public String label;
        @AuraEnabled public String linkLabel;
        @AuraEnabled public String fieldType;
        public TableWrapperFieldSet(String fieldName, String label, String fieldType) {
            this.fieldName = fieldName;
            this.label = label;
            this.fieldType = fieldType;
        }
    }
}