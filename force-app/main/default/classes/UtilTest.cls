/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: UtilTest
* @description: This is the test class for Util
* @Last Modified Date: 01/27/2020
**/
@isTest
public class UtilTest {

    @testSetup static void setup() {
        // Create common test accounts
        Account testAccount = new Account ();
        testAccount.Name = 'Test Account1';
        insert testAccount;
    }

    @isTest
    static void testBehavior(){
        Account testAccount = [SELECT Id FROM Account LIMIT 1];
        System.assertEquals('Account',string.valueOf(Util.getType('Account')));
        System.assertEquals('Account',String.valueOf(Util.getType(testAccount.Id)));
        System.assertEquals('Account',Util.getName(testAccount.Id));
        system.assertEquals(Null, Util.getType('Account',testAccount.Id));
        System.assertEquals('Name', string.valueOf(Util.getType(testAccount,'Name')));
        System.assert(Util.getFields(testAccount).size()>0);
		system.assert(Util.getFieldDescribes('Account').size()>0);
        System.assert(Util.getFields('Account').size()>0);
        system.assertNotEquals(null, Util.get('Account'));
    }

    @isTest
    static void testBehaviour1()
    {
        Account testAccount = [SELECT Id FROM Account LIMIT 1];
        system.assertEquals(' (Name Equal  Test Account1) ', Util.toSqlWhere('Name','Equal','Test Account1',''));
        system.assertEquals(' (Name IN  (Test Account1)) ',Util.toSqlWhere('Name','i','Test Account1',''));
        system.assertEquals('(NOT  (Name IN  (Test Account1)))',Util.toSqlWhere('Name','n','Test Account1',''));
        system.assertEquals(' (Name LIKE  Test Account1%) ',Util.toSqlWhere('Name','s','Test Account1',''));
        system.assertEquals(' (Name LIKE  %Test Account1%) ',Util.toSqlWhere('Name','c','Test Account1',''));
        system.assertEquals('(NOT  (Name LIKE  %Test Account1%))',Util.toSqlWhere('Name','d','Test Account1',''));
        system.assertEquals(' (Name Like  Test Account1) ',Util.toSqlWhere('Name','Like','Test Account1',''));
        system.assertEquals(' (Id IN '+testAccount.Id+')',Util.toSqlWhere('Id','IN',testAccount.Id,''));
    }

    @isTest
    static void testBehaviour3() {
        system.assert(Util.getPicklistValues('Task','Subject').size()>0);
    }
    
    @isTest
    static void testBehaviour4() {
        Contact testContact = new Contact ();
        testContact.LastName = 'Test Contact1';
        insert testContact;
        
        Sobject specialityObj= Schema.getGlobalDescribe().get('Speciality__c').newSObject();
        specialityObj.put('Name', 'TEST Specility');
        insert specialityObj;
        
        Sobject provSpecialityObj= Schema.getGlobalDescribe().get('ProviderSpecialties__c').newSObject();
        provSpecialityObj.put('Name', 'TEST Prov Specility');
        provSpecialityObj.put('Provider__c', testContact.get('Id'));
        provSpecialityObj.put('Speciality__c', specialityObj.get('Id'));
        provSpecialityObj.put('IsPrimary__c', true);
        insert provSpecialityObj;
        
        system.assertNotEquals(null, Util.getContactSpeciality(testContact.Id).size());
    }
    
    @isTest
    static void testBehaviour5() {
        system.assertNotEquals(null, Util.getFieldList('Task',false).size()>0);
    }
    
    @isTest
    static void testBehaviour6() {
        system.assertNotEquals(null, Util.getExcludeFields('Task').size()>0);
        system.assertNotEquals(null, Util.getNamespacePrefix());
        system.assertNotEquals(null, Util.getRelationalField('Name'));
        system.assertNotEquals(null, Util.getRelationalField('Id'));
        system.assertNotEquals(null, Util.getTaskRecordType('PRM'));
        system.assertNotEquals(null, Util.getAvailableRecordTypes('Task'));
        system.assertNotEquals(null, Util.getRecordType('Task', 'PRM'));
    }
}