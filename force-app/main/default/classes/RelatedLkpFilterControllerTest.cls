/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying 
* or sale of the above may constitute an infringement of copyright 
* and may result in criminal or other legal proceedings.
*
* @Class Name: RelatedLkpFilterControllerTest
* @description: This is the test class RelatedLkpFilterController
* @Last Modified Date: 03/26/2020
**/
@isTest
public class RelatedLkpFilterControllerTest {
	@isTest
    static void testgetPLTRecord(){
        sObject testlocationObj = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Location__c').newSObject();
		testlocationObj.put('Name','Test Location'); 
        insert testlocationObj;
    
        Contact provider = new Contact();
        provider.put('LastName','Test Provider1');
        insert provider;
        
        sObject practitionerLocation = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocation__c').newSObject();
        practitionerLocation.put('Name','Test PLocation1'); 
        practitionerLocation.put(NamespaceSelector.baseNamespace+'Provider__c',provider.Id);
        practitionerLocation.put(NamespaceSelector.baseNamespace+'Location__c',testlocationObj.Id);
        insert practitionerLocation;
       
        sObject testTerritory = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Territory__c').newSObject();
		testTerritory.put('Name','Test Territory1'); 
        insert testTerritory;
       
        sObject testPLocTerr1 = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c').newSObject();
        testPLocTerr1.put(NamespaceSelector.baseNamespace+'Territory__c',testTerritory.Id);
        testPLocTerr1.put(NamespaceSelector.baseNamespace+'Provider__c',provider.Id);
        testPLocTerr1.put(NamespaceSelector.baseNamespace+'PractitionerLocation__c',practitionerLocation.Id);
        insert testPLocTerr1;
        sObject testPLocTerr2 = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c').newSObject();
        testPLocTerr2.put(NamespaceSelector.baseNamespace+'Territory__c',testTerritory.Id);
        testPLocTerr2.put(NamespaceSelector.baseNamespace+'Provider__c',provider.Id);
        testPLocTerr2.put(NamespaceSelector.baseNamespace+'PractitionerLocation__c',practitionerLocation.Id);
        insert testPLocTerr2;
        
        system.assertNotEquals(null, RelatedLkpFilterController.getPLTRecord(testTerritory.Id,0,'CreatedDate','desc','50','1'));
        system.assertNotEquals(null, RelatedLkpFilterController.getPLTRecord(testTerritory.Id,0,'linkId','desc','50','1'));
        system.assertNotEquals(null, RelatedLkpFilterController.getPLTRecord(testTerritory.Id,0,'locationName','asc','50','1'));
        system.assertNotEquals(null, RelatedLkpFilterController.getPLTRecord(testTerritory.Id,2,'CreatedDate','desc','50','2'));
    }
    
    @isTest
    static void testSetTitle(){
        system.assertEquals('(3)', RelatedLkpFilterController.setTitle(3));
    }
    
    @isTest
    static void testdeleteRecord(){
        Account testAccount = new Account ();
        testAccount.Name = 'Test Account1';
        insert testAccount;
        system.assertEquals(true, RelatedListController.deleteRecord(testAccount.Id));
    }
}