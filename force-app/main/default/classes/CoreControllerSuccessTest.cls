@isTest
public class CoreControllerSuccessTest {
  /**
   * This method is used to create Test records
   */

  @testSetup
  static void createTestdata() {
    Account testAccount = new Account();
    testAccount.Name = 'Test Account1';
    insert testAccount;
  }

  /**
   * CoreControllerSuccessTest Test method for CoreControllerSuccess
   */

  static testMethod void CoreControllerSuccessTest() {
    Test.startTest();
    CoreControllerSuccess e = new CoreControllerSuccess(
      'APIServiceMappingController',
      'Record created'
    );
    Integer actual = [
      SELECT COUNT()
      FROM Account
      WHERE Name = :'Test Account1'
    ];
    System.assertEquals(
      1,
      actual,
      'There should be one account named "Test Account1"'
    );
    Test.stopTest();
  }
}