/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying 
* or sale of the above may constitute an infringement of copyright 
* and may result in criminal or other legal proceedings.
*
* @Class Name: RecursiveTriggerHandler
* @description: To stop trigger from executing more than once.
* @Author: Healthgrades Inc.
**/
public inherited sharing class RecursiveTriggerHandler{
    public static Boolean isFirstTime = true;
    public static boolean isFirstTimeCall(){
        if(isFirstTime) { isFirstTime = false; return true; } return false;
    }
}