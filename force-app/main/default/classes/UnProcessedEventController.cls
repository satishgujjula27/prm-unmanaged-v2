/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: UnProcessedEventController
* @description: This Class is used to handle unprocessed Events
* @Last Modified Date: 10/05/2020
**/
public inherited sharing class UnProcessedEventController {
    
    List<UnProcessedEventController.EventWrapper> ew;
    List<Util.TableWrapperFieldSet> fieldsWrapper;
    public static final String EVENT_TIMEFRAME_PAST = 'Past';
    public static final String EVENT_TIMEFRAME_CURRENT = 'Current';
    public static final String EVENT_TIMEFRAME_FUTURE = 'Future';
    
    /********************************
    * @method: getEvents
    * @description: get using for selected timeframe
    * Param: string
    * Return type: string
    * *****************************/
    @AuraEnabled
    public static String getEvents(String eventTimeframe) {
        try{
            UnProcessedEventController evObject = new UnProcessedEventController();
            List<UnProcessedEventController.EventWrapper> eventLst = new List<UnProcessedEventController.EventWrapper>();
            // setting table field
            List<Util.TableWrapperFieldSet> flswrapper = new List<Util.TableWrapperFieldSet>();
            flswrapper.add(new Util.TableWrapperFieldSet('startdatetime', 'START DATE TIME','text'));
            flswrapper.add(new Util.TableWrapperFieldSet('enddatetime', 'END DATE TIME','text'));
            flswrapper.add(new Util.TableWrapperFieldSet('subject', 'SUBJECT','text'));
            flswrapper.add(new Util.TableWrapperFieldSet('firstname', 'FIRST NAME','text'));
            flswrapper.add(new Util.TableWrapperFieldSet('lastname', 'LAST NAME','text'));
            flswrapper.add(new Util.TableWrapperFieldSet('location', 'LOCATION','text'));
            evObject.fieldsWrapper = flswrapper;
            
            String sql = getEventQuery();
            List<Sobject> unprocEventRecs = getUnprocEvRec(sql,eventTimeframe);
            if(unprocEventRecs.size() > 0) {
                for(Sobject evObj:unprocEventRecs) { eventLst.add(new EventWrapper(evObj)); }
            }
            evObject.ew = eventLst;
            return JSON.serialize(evObject);
        } catch(Exception getEventsEx) {
            CoreControllerException getEventsException = new CoreControllerException('UnProcessedEventController.getEvents', 
                                                                                             'Exception occured while getEvent'+getEventsEx.getStackTraceString());
            return null;
        }
    }
    
    /********************************
    * @method: getEventQuery
    * @description: get primary event query to fetch events
    * Param: none
    * Return type: string
    * *****************************/
    public static String getEventQuery(){
        List<String> fields = new List<String>{
            'Id', 'Subject', 'WhoId', 'WhatId', 'Description', 'FORMAT(StartDateTime)', 'FORMAT(EndDateTime)', 'Who.FirstName', 
			'Who.LastName', 'Who.Name', 'What.Name', 'Location', NamespaceSelector.baseNamespace+'EventStatus__c'
        };   
        SObjectType eventObjToken = Schema.getGlobalDescribe().get('Event');
        if(Util.checkObjectIsReadable(eventObjToken)){
            return 'SELECT '+String.join(fields,',')+' FROM '+'Event';
        } else {
            CoreControllerException getEventQueryException = new CoreControllerException('UnProcessedEventController.getEventQuery', 
                                                                                                 'sobjects is not insertable/readable ==> '+eventObjToken);
            return null;
        }
    }
    
    /********************************
    * @method: getUnprocEvRec
    * @description: get unprocessed Event record for selected timeframe
    * Param: string, string
    * Return type: List
    * *****************************/
    public static List<SObject> getUnprocEvRec(String sql, String eventTimeframe) {
        SObjectType eventObjToken = Schema.getGlobalDescribe().get('Event');
        Schema.DescribeFieldResult evStatusFieldDesc = eventObjToken.getDescribe().fields.getMap().get(NamespaceSelector.baseNamespace+'EventStatus__c').getDescribe();
        Schema.DescribeFieldResult endDtTimeFieldDesc = eventObjToken.getDescribe().fields.getMap().get('EndDateTime').getDescribe();
        Schema.DescribeFieldResult startDtTimeFieldDesc = eventObjToken.getDescribe().fields.getMap().get('StartDateTime').getDescribe();
        if( Util.checkObjectIsReadable(eventObjToken) ) {
            sql += ' WHERE OwnerId = \'' + userinfo.getuserid() + '\' AND (WhoId != null OR WhatId != null) ';
            if( Util.checkFieldIsReadable(eventObjToken, evStatusFieldDesc) ) {
                sql += ' AND '+NamespaceSelector.baseNamespace+'EventStatus__c != \'Processed\' AND '+NamespaceSelector.baseNamespace+'EventStatus__c != \'Cancelled\' ';
            }
            DateTime dt = DateTime.now(); // used to ease dynamic query building
            if( Util.checkFieldIsReadable(eventObjToken, startDtTimeFieldDesc) && Util.checkFieldIsReadable(eventObjToken, endDtTimeFieldDesc) ) {
                sql += (
                    eventTimeframe == EVENT_TIMEFRAME_PAST
                    ? 'AND EndDateTime <: dt '
                    : eventTimeframe == EVENT_TIMEFRAME_CURRENT
                    ? 'AND StartDateTime <=: dt AND EndDateTime >=: dt '
                    : eventTimeframe == EVENT_TIMEFRAME_FUTURE
                    ? 'AND StartDateTime >: dt '
                    : ''
                );
            }
            sql += ' ORDER BY StartDateTime, Who.Name, WhoId, What.Name, WhatId, Subject';
            sql += ' LIMIT 200';
            return Database.query(sql);
        } else {
            CoreControllerException getUnprocEvRecException = new CoreControllerException('UnProcessedEventController.getUnprocEvRec', 
                                                                                                  'sobjects is not insertable/readable ==> '+eventObjToken);
            return null;
        }
    }
    
    /********************************
    * @method: removeEvents
    * @description: set event status as cancelled to remove it from UI
    * Param: List
    * Return type: none
    * *****************************/
    @AuraEnabled
    public static void removeEvents(List<String> eventIds) { setEventStatus(eventIds, 'Cancelled'); }
    
    /********************************
    * @method: setEventStatus
    * @description: set status to events
    * Param: List, String
    * Return type: none
    * *****************************/
    public static void setEventStatus(List<String> eventIds, String eventStatus){
        SObjectType eventObjToken = Schema.getGlobalDescribe().get('Event');
        Schema.DescribeFieldResult evStatusFieldDesc = eventObjToken.getDescribe().fields.getMap().get(NamespaceSelector.baseNamespace+'EventStatus__c').getDescribe();
        if(Util.checkObjectIsReadable(eventObjToken) && Util.checkObjectIsUpdateable(eventObjToken)){
            List<String> fields = new List<String>{'Id', NamespaceSelector.baseNamespace+'EventStatus__c', 'IsReminderSet'};
			List<SObject> events = Database.query('SELECT '+string.join(fields, ',')+' FROM Event WHERE Id IN :eventIds');
            if( Util.checkFieldIsReadable(eventObjToken,evStatusFieldDesc) ){
                if(events.size() >0){
                    for(SObject evObj:events){
                        evObj.put(NamespaceSelector.baseNamespace+'EventStatus__c',eventStatus);
                        evObj.put('IsReminderSet', false);
                    }
                    Database.update(events);
                }
            } else {
                CoreControllerException setEventStatusException = new CoreControllerException('UnProcessedEventController.setEventStatus', 
                                                                                                      'field is not readable ==> '+NamespaceSelector.baseNamespace+'EventStatus__c');
            }
        } else {
            CoreControllerException setEventStatusException = new CoreControllerException('UnProcessedEventController.setEventStatus', 
                                                                                                  'sobjects is not Updateable/readable ==> '+eventObjToken);
        }
    }
    
    /********************************
    * @method: getEventInfo
    * @description: set Event information for selected event
    * Param: String
    * Return type: String
    * *****************************/
    @AuraEnabled
    public static String getEventInfo(String eventId){
        EventInfoWrapper evInfoWrapper = new EventInfoWrapper();
        try{
            SObjectType eventObjToken = Schema.getGlobalDescribe().get('Event');
            if( Util.checkObjectIsReadable(eventObjToken) ) {
                String evSql = getEventQuery();
                evSql += ' WHERE Id =:eventId LIMIT 1';
                List<Sobject> event = Database.query(evSql);
                if(event.size() > 0){
                    evInfoWrapper.event = new EventWrapper(event[0]);
                    evInfoWrapper.picklistMap.put('status', Util.getPicklistValues('Task', 'Status'));
                    evInfoWrapper.picklistMap.put('priority', Util.getPicklistValues('Task', 'Priority'));
                    evInfoWrapper.loggedInUser = [SELECT Id,Name FROM User WHERE Id = :UserInfo.getUserId()];
                    List<plWrapperFieldSet> flswrapper = new List<plWrapperFieldSet>();
                    flswrapper.add(new plWrapperFieldSet('firstname', 'FIRST NAME','url','fLinkId'));
                    flswrapper.add(new plWrapperFieldSet('lastname', 'LAST NAME','url','fLinkId'));
                    flswrapper.add(new plWrapperFieldSet('practiceGroup', 'PRACTICE & GROUP','url','pLinkId'));
                    flswrapper.add(new plWrapperFieldSet('address', 'MAILING ADDRESS','text','address'));
                    flswrapper.add(new plWrapperFieldSet('phone', 'PHONE','text','phone'));
                    evInfoWrapper.flswrapper = flswrapper;
                    evInfoWrapper.selectedPLRecords = getSelPLRecords(event[0],evInfoWrapper);
                }
                return JSON.serialize(evInfoWrapper);
            } else {
                CoreControllerException getEventInfoException = new CoreControllerException('UnProcessedEventController.getEventInfo', 
                                                                                              'sobjects is not insertable/readable ==> '+eventObjToken);
                return null;
            }
        }
        catch(Exception getEventInfoEx){
            CoreControllerException getEventInfo1Exception = new CoreControllerException('UnProcessedEventController.getEventInfo', 
                                                                                                 'Exception occured while getEventInfo'+getEventInfoEx.getStackTraceString());
            return null;
        }
    }
    
    /********************************
    * @method: getSelPLRecords
    * @description: get provider Locations to process event
    * Param: Sobject, EventInfoWrapper
    * Return type: List
    * *****************************/
    public static List<provLocWrapper> getSelPLRecords(SObject evObj, EventInfoWrapper evInfoWrapper){
        List<provLocWrapper> plRecs = new List<provLocWrapper>();
        List<String> selectedRecords = new List<String>();
        SObjectType locationObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocation__c');
        if( Util.checkObjectIsReadable(locationObjToken) ){
            String plSql = getselectedPLRecQuery();
            String plSqlOther = plSql;
            String plId = (String)evObj.get('WhatId');
            plSql += ' WHERE Id =:plId LIMIT 1';
            List<SObject> selPlRecords = Database.query(plSql);
            SObject providerLocationObj;
            if(selPlRecords.size() >0) {
                for(Sobject plObj:selPlRecords){
                    selectedRecords.add((String)plObj.get('Id'));
                    plRecs.add(new provLocWrapper(plObj));
                    providerLocationObj = plObj;
                }
            }
            
            String whoId = (String)evObj.get('WhoId');
            String WhatId = (String)evObj.get('WhatId');
            String whereClause = ' WHERE '+( providerLocationObj != null ? ' Id != \''+providerLocationObj.get('Id') + '\' AND ' : '' )
                + (evObj.get('WhatId') != null && Util.objName((Id)evObj.get('WhatId')) == 'PractitionerLocation__c'
                   ? ' ( '+NamespaceSelector.baseNamespace+'Provider__c = :whoId OR '+NamespaceSelector.baseNamespace+'Location__c =\''+providerLocationObj.get(NamespaceSelector.baseNamespace+'Location__c')+'\' ) ' :
                   evObj.get('WhatId') != null && Util.objName((Id)evObj.get('WhatId')) == 'Account' 
                   ? NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'PracticeGroup__c = :WhatId' :
                   evObj.get('WhatId') != null && Util.objName((Id)evObj.get('WhatId')) == 'Location__c'
                   ? NamespaceSelector.baseNamespace+'Location__c = :WhatId' :
                   evObj.get('WhoId') != null && evObj.get('WhatId') == null
                   ? NamespaceSelector.baseNamespace+'Provider__c = :whoId' :
                   'Id = null'
                  );
            plSqlOther = plSqlOther+whereClause;
            plSqlOther += ' LIMIT 100';
            List<Sobject> selPlOtherRecords = Database.query(plSqlOther);
            if(selPlOtherRecords.size() >0){
                for(Sobject plObj:selPlOtherRecords){
                    plRecs.add(new provLocWrapper(plObj));
                }
            }
            
            evInfoWrapper.selectedRecords = selectedRecords;
        } else {
            CoreControllerException getSelPLRecordsException = new CoreControllerException('UnProcessedEventController.getSelPLRecords', 
                                                                                          'sobjects is not insertable/readable ==> '+locationObjToken);
        }
        return plRecs;
    }
    
    /********************************
    * @method: getselectedPLRecQuery
    * @description: prepare query to fetch Provider  Locations  for Events
    * Param: none
    * Return type: String
    * *****************************/
    public static String getselectedPLRecQuery(){
        List<String> fields = new List<String>{
            'Id', 'Name', NamespaceSelector.baseNamespace+'Location__r.Name',
                NamespaceSelector.baseNamespace+'IsPrimary__c',
                NamespaceSelector.baseNamespace+'Provider__r.FirstName',
                NamespaceSelector.baseNamespace+'Provider__r.LastName',
                NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'Telephone__c',
                NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'MailingAddress__c',
                NamespaceSelector.baseNamespace+'Location__r.'+NamespaceSelector.baseNamespace+'PracticeGroup__r.Name',
                '(SELECT Id FROM '+NamespaceSelector.baseNamespace+'Practitioner_Location_Territories__r WHERE '+NamespaceSelector.baseNamespace+'Territory__r.OwnerId = \'' + userinfo.getuserid() + '\' LIMIT 1)'
                };
		return 'SELECT '+String.join(fields,',')+' FROM '+NamespaceSelector.baseNamespace+'PractitionerLocation__c';
    }
    
    /********************************
    * @method: saveTask
    * @description: to create task and followp Task while Proccessing Event
    * Param: Task, Task, Boolean, Boolean, List, String, Boolean
    * Return type: String
    * *****************************/
    @AuraEnabled
    public static String saveTask(Task task, Task followupTask, Boolean isCreateTask, Boolean iscreateFollowupTask, List<String> providerList, String eventId, Boolean isEventProcessed) {
        resultWrapper rw = new resultWrapper();
        try{
            if(isEventProcessed) { setEventStatus(new List<String>{eventId}, 'Processed'); }
            if( Schema.SObjectType.Task.isAccessible() && Schema.SObjectType.Task.isCreateable()){
                List<Task> tasksList = new List<Task>();
                string recTypeId = Util.getTaskRecordType('PRM');
                Map<String,String> plMap = getPLMap(providerList);
                for(string plId:plMap.keySet()){
                    if(isCreateTask) {
                        task tsk = task.clone(false, true, false, false);
                        tsk.WhatId = plId;
                        tsk.WhoId = plMap.get(plId);
                        tsk.Status = 'Completed';
                        tsk.Priority = 'Normal';
                        if(recTypeId != null) { tsk.recordTypeId = recTypeId; }
                        tasksList.add(tsk);
                    }
                    if(iscreateFollowupTask){
                        task fTask = followupTask.clone(false, true, false, false);
                        fTask.WhatId = plId;
                        fTask.WhoId = plMap.get(plId);
                        if(recTypeId != null) { fTask.recordTypeId = recTypeId; }
                        tasksList.add(fTask);
                    }
                }
                if(tasksList.size() > 0){ Database.insert(tasksList); }
                rw.reqResult = true;
            } else {
                CoreControllerException saveTaskException = new CoreControllerException('UnProcessedEventController.saveTask', 
                                                                                                   'sobjects is not insertable/readable ==> Task');
                rw.reqResult = false;
            }
        } catch(Exception saveTaskEx){
            CoreControllerException saveTaskException = new CoreControllerException('UnProcessedEventController.saveTask', 
                                                                                            'Could not save Tasks'+saveTaskEx.getStackTraceString());
            rw.reqResult = false;
        }
        return JSON.serialize(rw);
    }
    
    /********************************
    * @method: getPLMap
    * @description: to generate provider Location Map
    * Param: List
    * Return type: Map
    * *****************************/
    public static Map<String, String> getPLMap(List<String> plIds){
        Map<String, String> plMap = new Map<String,String>();
        SObjectType locationObjToken = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocation__c');
        if( Util.checkObjectIsReadable(locationObjToken) ){
            List<SObject> providerLocations = Database.query('SELECT '+NamespaceSelector.baseNamespace+'Provider__c FROM '+NamespaceSelector.baseNamespace+'PractitionerLocation__c WHERE Id IN :plIds');
            for(SObject plObj:providerLocations){
                if(plObj.get(NamespaceSelector.baseNamespace+'Provider__c') != null) {
                    plMap.put((String)plObj.get('Id'), (String)plObj.get(NamespaceSelector.baseNamespace+'Provider__c'));
                }
            }
        } else{
            CoreControllerException getPLMapException = new CoreControllerException('UnProcessedEventController.getPLMap', 
                                                                                            'sobjects is not insertable/readable ==> '+locationObjToken);
        }
        return plMap;
    }
    
    /********************************
    * @Wrapper Class Name: EventInfoWrapper
    * @description: This class is used to hold the Event Info
    * @Last Modified Date: 10/05/2020
    * *****************************/
    public class EventInfoWrapper {
        @AuraEnabled Public EventWrapper event;
        @AuraEnabled public Map<String, List<String>> picklistMap = new Map<String, List<String>>();
        @AuraEnabled public User loggedInUser;
        @AuraEnabled public List<plWrapperFieldSet> flswrapper;
        @AuraEnabled public List<provLocWrapper> selectedPLRecords;
        @AuraEnabled public List<String> selectedRecords;
    }
    
    /********************************
    * @Wrapper Class Name: EventWrapper
    * @description: This class is used to hold the Events
    * @Last Modified Date: 10/05/2020
    * *****************************/
    public class EventWrapper {
        Public String Id;
        public String subject;
        public String firstname;
        public String lastname;
        public String location;
        public String startdatetime;
        public String enddatetime;
        public String name;
        public String whoId;
        public String whatId;
        public String decription;
        
        /********************************
		* @Method: EventWrapper
		* @description: Constructor to set the Class attribute from Object using Map
		* @return: N/A
		* *****************************/
        public EventWrapper(SObject sobj) {
            this.Id = (String)sObj.get('Id');
            this.subject = (String)sObj.get('Subject');
            this.location = (String)sObj.get('Location');
            this.startdatetime = (String)sObj.get('StartDateTime');
            this.enddatetime = (String)sObj.get('EndDateTime');
            if(sobj.getSobject('Who') !=null){
                this.firstname = (String)sObj.getSobject('Who').get('FirstName');
                this.lastname = (String)sObj.getSobject('Who').get('LastName');
                this.name = (String)sObj.getSobject('Who').get('Name');
                this.whoId = (String)sObj.get('WhoId');
            }
            this.whatId = (String)sObj.get('WhatId');
            this.decription = (String)sObj.get('Description');
        }
    }
    
    /********************************
    * @Wrapper Class Name: resultWrapper
    * @description: This class is used to hold results
    * @Last Modified Date: 10/05/2020
    * *****************************/
    public class resultWrapper{ @AuraEnabled public Boolean reqResult; }
    
    /********************************
    * @Wrapper Class Name: provLocWrapper
    * @description: This class is for Provider Location Wrapper
    * @Last Modified Date: 10/05/2020
    * *****************************/
    public class provLocWrapper{
        public String Id;
        public String firstname;
        public String lastname;
        public String practiceGroup;
        public String address;
        public String phone;
        public String fLinkId;
        public String lLinkId;
        public String pLinkId;
        public Boolean isInTerritory;
        
        /********************************
        * @Method: provLocWrapper
        * @description: Constructor to set the Class attribute from Object using Map
        * @return: N/A
        * *****************************/
        public provLocWrapper(SObject obj){
           this.Id = (String)obj.get('Id');
            if(obj.getSObject(NamespaceSelector.baseNamespace+'Provider__r') !=null){
                this.firstname = (String)obj.getSObject(NamespaceSelector.baseNamespace+'Provider__r').get('FirstName');
                this.lastname = (String)obj.getSObject(NamespaceSelector.baseNamespace+'Provider__r').get('LastName');
                this.fLinkId = this.lLinkId = '/'+(String)obj.getSObject(NamespaceSelector.baseNamespace+'Provider__r').get('Id');
            }
            if(obj.getSObject(NamespaceSelector.baseNamespace+'Location__r') !=null){
                this.address = (String)obj.getSObject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'MailingAddress__c');
                this.address = this.address.replace('<br>','');
                this.phone = (String)obj.getSObject(NamespaceSelector.baseNamespace+'Location__r').get(NamespaceSelector.baseNamespace+'Telephone__c');
                
                if(obj.getSObject(NamespaceSelector.baseNamespace+'Location__r').getSObject(NamespaceSelector.baseNamespace+'PracticeGroup__r') !=null){
                    this.practiceGroup = (String)obj.getSObject(NamespaceSelector.baseNamespace+'Location__r').getSObject(NamespaceSelector.baseNamespace+'PracticeGroup__r').get('Name');
                    this.pLinkId = '/'+(String)obj.getSObject(NamespaceSelector.baseNamespace+'Location__r').getSObject(NamespaceSelector.baseNamespace+'PracticeGroup__r').get('Id');
                }
            }
            if(obj.getSObjects(NamespaceSelector.baseNamespace+'Practitioner_Location_Territories__r') !=null){
                this.isInTerritory = true;
            } else {
                this.isInTerritory = false;
            }
        }
    }
    
    /********************************
    * @Wrapper Class Name: plWrapperFieldSet
    * @description: This class is to hold the  Provider Location Wrapper Field set
    * @Last Modified Date: 10/05/2020
    * *****************************/
    public class plWrapperFieldSet {
        @AuraEnabled public String fieldName;
        @AuraEnabled public String label;
        @AuraEnabled public String fieldType;
        @AuraEnabled public Boolean sortable;
        @AuraEnabled public String linkfieldName;
        
        /********************************
        * @Method: plWrapperFieldSet
        * @description: Constructor to set the Class attribute
        * @return: N/A
        * *****************************/
        public plWrapperFieldSet(String fieldName, String label, String fieldType, String linkfieldName) {
            this.fieldName = fieldName;
            this.label = label;
            this.fieldType = fieldType;
            this.sortable = false;
            this.linkfieldName = linkfieldName;
        }
    }
}