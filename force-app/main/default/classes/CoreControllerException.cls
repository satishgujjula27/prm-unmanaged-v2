global with sharing class CoreControllerException extends Exception {
  /**
   * Creates an exception log entry asynchronously
   * @param  className  Name of the class from where we are firing
   * @param  message    Error message about the operation
   */
  global CoreControllerException(String className, String message) {
    createLog(className, message, '');
  }

  /**
   * Creates an exception log entry asynchronously
   * @param  className  Name of the class from where we are firing
   * @param  message    Error message about the operation
   * @param  detail     Detailed error information
   */
  global CoreControllerException(
    String className,
    String message,
    String detail
  ) {
    createLog(className, message, detail);
  }

  /**
   * Creates an exception log entry asynchronously
   * @param  className  Name of the class from where we are firing
   * @param  message    Error message about the operation
   * @param  object     Object containing error information
   */
  global CoreControllerException(String className, String message, Object obj) {
    createLog(className, message, String.valueOf(obj));
  }

  private static void createLog(
    String className,
    String message,
    String detail
  ) {
    System.debug('Exception: ' + className + ': ' + message + '\n' + detail);
  }
}