/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: TerritoryManagementControllerTest
* @description: This is the test class TerritoryManagementController
* @Last Modified Date: 05/11/2020
**/
@isTest
public class TerritoryManagementControllerTest {

    @isTest
    static void testBehavior(){
        Sobject zipCode= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Zipcode__c').newSObject();
        ZipCode.put(NamespaceSelector.baseNamespace+'Zipcode__c', '123456');
        ZipCode.put(NamespaceSelector.baseNamespace+'Latitude__c', 41);
        ZipCode.put(NamespaceSelector.baseNamespace+'Longitude__c', -71);
        insert zipCode;
        
        Sobject location= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Location__c').newSObject();
        Location.put('Name', 'Location 1');
        insert location;
        
        Contact provider= new Contact();
        Provider.put('FirstName', 'Test');
        Provider.put('LastName', 'Test');
        insert provider;
        
        Sobject practitionerLocation1= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocation__c').newSObject();
        PractitionerLocation1.put('Name', 'PL');
        PractitionerLocation1.put(NamespaceSelector.baseNamespace+'Location__c', Location.Id);
        PractitionerLocation1.put(NamespaceSelector.baseNamespace+'Provider__c', Provider.Id);
        insert practitionerLocation1;
        
        Sobject territory= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Territory__c').newSObject();
        territory.put('Name', 'Test Territory');
        territory.put(NamespaceSelector.baseNamespace+'SavedFieldFilters__c', '[{"value":"TRUE","operator":"=","joinType":"","field":"' + NamespaceSelector.baseNamespace+'IsPrimary__c' + '","displayType":"BOOLEAN"}]');
        insert territory;
        
        List<string> selectedObjData = new List<string>{PractitionerLocation1.Id}; 
        String fieldConditions = '[]';
        String marketArea = '[]';
        String zipFilters = '[{"postalCodeType" : "mailing" , "mailingPostalCodeRange" : "' + ZipCode.get(NamespaceSelector.baseNamespace+'Zipcode__c') + '" , "mailingPostalCodeRadius" : "5" , "mailingPostalCodeUnit" : "m"}]';
        String getFilterDetails = TerritoryManagementController.getFilterDetails(territory.Id);
        System.assertEquals(getFilterDetails.contains('"zipCodeFieldOptions":[{"value":"Mailing","label":"Mailing","displayType":null},{"value":"Billing","label":"Billing","displayType":null}]'), True, 'getFilterDetails failure');
        String addSelectedRecords = TerritoryManagementController.addSelectedRecords(selectedObjData, territory.Id, new List<string>());
        System.assertEquals(addSelectedRecords.length() > 0, true, 'addSelectedRecords failure');
        String saveFilterDetail = TerritoryManagementController.saveFilterDetail(territory.Id,fieldConditions,marketArea,zipFilters,'[]','provider');
        System.assertEquals(String.isBlank(saveFilterDetail), true, 'saveFilterDetail failure');
        String whereClause1 = TerritoryManagementController.constructJunctionsWhereClause(true, false, 0, '');
        System.assertEquals(whereClause1.contains('Id IN'), true, 'whereClause1 failure');
        String whereClause2 = TerritoryManagementController.constructJunctionsWhereClause(true, true, 0, '');
        System.assertEquals(String.isBlank(whereClause2), false, 'whereClause2 failure');
        String whereClause3 = TerritoryManagementController.constructJunctionsWhereClause(false, true, 0, '');
        System.assertEquals(whereClause3.contains('Id IN'), true, 'whereClause3 failure');
        
    }

    @isTest
    static void testBehavior1(){
        Sobject zipCode= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Zipcode__c').newSObject();
        ZipCode.put(NamespaceSelector.baseNamespace+'Zipcode__c', '123456');
        ZipCode.put(NamespaceSelector.baseNamespace+'Latitude__c', 41);
        ZipCode.put(NamespaceSelector.baseNamespace+'Longitude__c', -71);
        insert zipCode;
        
        Sobject territory= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Territory__c').newSObject();
        territory.put('Name', 'Test Territory');
        territory.put(NamespaceSelector.baseNamespace+'ZipFilters__c', '[{"postalCodeType":"billing","PostalCodeRange" : "' + ZipCode.get(NamespaceSelector.baseNamespace+'Zipcode__c') + '","postalCodeRadius":"5","postalCodeUnit":"m"}]');
        insert territory;
    }
    
    @isTest
    static void testBehavior2(){
        system.assertNotEquals(null, TerritoryManagementController.constructFieldWhere('[]'), 'testBehavior2 failure');
    }
    
    @isTest
    static void testBehavior3(){
        system.assertNotEquals(null, TerritoryManagementController.constructLookupWhereClause('[]', 'provider', 0), 'testBehavior3 failure');
    }
    
    @isTest
    static void testBehavior4(){
        system.assertNotEquals(null, TerritoryManagementController.constructLookupWhereClause('[{"lookupType":"Specialty","value":["a0917000008r5XdAAI"],"label":["Anesthesiology"], "isAllSelected":false}]', 'provider', 0), 'testBehavior4 failure');
    }
    
    @isTest
    static void testBehavior5(){
        system.assertNotEquals(null, TerritoryManagementController.constructLookupWhereClause('[{"lookupType":"AreaOfExpertise","value":["a0917000008r5XdAAI"],"label":["Anesthesiology"], "isAllSelected":false}]', 'provider', 0), 'testBehavior5 failure');
    }
    
     @isTest
    static void testBehavior6(){
        system.assertNotEquals(null, TerritoryManagementController.constructLookupWhereClause('[{"lookupType":"Education","value":["a0917000008r5XdAAI"],"label":["Anesthesiology"], "isAllSelected":false}]', 'provider', 0), 'testBehavior6 failure');
    }
    
    @isTest
    static void testBehavior7(){
        system.assertNotEquals(null, TerritoryManagementController.constructLookupWhereClause('[{"lookupType":"Practice","value":["a0917000008r5XdAAI"],"label":["Anesthesiology"], "isAllSelected":false}]', 'provider', 0), 'testBehavior7 failure');
    }
    
     @isTest
    static void testBehavior8(){
        system.assertNotEquals(null, TerritoryManagementController.constructLookupWhereClause('[{"lookupType":"Facility","value":["a0917000008r5XdAAI"],"label":["Anesthesiology"], "isAllSelected":false}]', 'provider', 0), 'testBehavior8 failure');
    }
    
    @isTest
    static void testBehavior9(){
        system.assertNotEquals(null, TerritoryManagementController.getSquareSqlConditions(34, -71, 30, 'K', 'Longitude', 'Latitude'), 'testBehavior9 failure');
    }
    
    @isTest
    static void testBehavior10(){
        Account practice= new Account();
        practice.put('Name', 'Test');
        practice.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Practice & Group').getRecordTypeId();
        insert practice;
        
        Sobject location= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Location__c').newSObject();
        Location.put('Name', 'Location 1');
        Location.put(NamespaceSelector.baseNamespace+'Zip__c', '123456');
        insert location;

        Sobject ZipCode= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Zipcode__c').newSObject();
        ZipCode.put(NamespaceSelector.baseNamespace+'Zipcode__c', '123456');
        ZipCode.put(NamespaceSelector.baseNamespace+'Latitude__c', 41);
        ZipCode.put(NamespaceSelector.baseNamespace+'Longitude__c', -71);
        insert ZipCode;
        
        Contact provider= new Contact();
        Provider.put('FirstName', 'Test');
        Provider.put('LastName', 'Test');
        insert provider;
        
        Sobject practitionerLocation1= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocation__c').newSObject();
        PractitionerLocation1.put('Name', 'PL');
        PractitionerLocation1.put(NamespaceSelector.baseNamespace+'Location__c', Location.Id);
        PractitionerLocation1.put(NamespaceSelector.baseNamespace+'Provider__c', Provider.Id);
        insert practitionerLocation1;
        
        Sobject territory= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Territory__c').newSObject();
        territory.put('Name', 'Test Territory');
        territory.put(NamespaceSelector.baseNamespace+'SavedFieldFilters__c', '[{"value":"TRUE","operator":"=","joinType":"","field":"' + NamespaceSelector.baseNamespace+'IsPrimary__c' + '","displayType":"BOOLEAN"}]');
        insert territory;
        
        Sobject practiceGroup= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PracticeGroupLocation__c').newSObject();
        practiceGroup.put('Name', 'Test Practice Location');
        practiceGroup.put(NamespaceSelector.baseNamespace+'PracticeGroup__c', practice.Id);
        practiceGroup.put(NamespaceSelector.baseNamespace+'Location__c', location.Id);
        insert practiceGroup;
        
        Sobject providerPracticeGroup= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'ProviderPracticeGroup__c').newSObject();
        providerPracticeGroup.put('Name', 'Test Practice Location');
        providerPracticeGroup.put(NamespaceSelector.baseNamespace+'PracticeGroup__c', practice.Id);
        providerPracticeGroup.put(NamespaceSelector.baseNamespace+'Provider__c', provider.Id);
        insert providerPracticeGroup;
        
        TerritoryManagementController.TableWrapperFieldSet testWrapper = new TerritoryManagementController.TableWrapperFieldSet('Test', 'Test', 'Test', 'Test', true);
        
        //getPLData(String filterConditions, String fieldConditions, String zipFilters, String lookupFilter)
        // Search By ==> Practices & Groups
        system.assertNotEquals(null, TerritoryManagementController.getPLData(
            '{"isIncludeMyExisting":true,"isIncludeOthersExisting":true,"pageSize":"25","pageNumber":1,"sortBy":"Name","sortDirection":"asc","filterBy":"practicegroup","childSize":1000,"filterType":"ObjectAttributes,Lookup"}', 
            '[{"value":"Location 1","operator":"=","joinType":"","field":"'+NamespaceSelector.baseNamespace+'Location__r.Name","displayType":"STRING"}]', 
            '[]', '[]'));
        system.assertNotEquals(null, TerritoryManagementController.getPLData(
            '{"isIncludeMyExisting":true,"isIncludeOthersExisting":true,"pageSize":"25","pageNumber":1,"sortBy":"Name","sortDirection":"asc","filterBy":"practicegroup","childSize":1000,"filterType":"ObjectAttributes"}', 
            '[]', '[{"postalCodeType":"mailing","mailingPostalCodeRange":"123456","mailingPostalCodeRadius":"0","mailingPostalCodeUnit":"m"}]', 
            '[]'));
        system.assertNotEquals(null, TerritoryManagementController.getPLData(
            '{"isIncludeMyExisting":true,"isIncludeOthersExisting":true,"pageSize":"25","pageNumber":1,"sortBy":"Name","sortDirection":"asc","filterBy":"practicegroup","childSize":1000,"filterType":"Lookup"}', 
            '[]', '[]', 
            '[{"lookupType":"Account","value":["Test"],"label":["Test"],"isAllSelected":false}]'));
        
        // Search By ==> Providers
        system.assertNotEquals(null, TerritoryManagementController.getPLData(
            '{"isIncludeMyExisting":true,"isIncludeOthersExisting":true,"pageSize":"25","pageNumber":1,"sortBy":"Name","sortDirection":"asc","filterBy":"provider","childSize":100,"filterType":"ObjectAttributes,Lookup"}', 
            '[{"value":"Location 1","operator":"=","joinType":"","field":"'+NamespaceSelector.baseNamespace+'Location__r.Name","displayType":"STRING"}]', 
            '[]', '[]'));
        system.assertNotEquals(null, TerritoryManagementController.getPLData(
            '{"isIncludeMyExisting":true,"isIncludeOthersExisting":true,"pageSize":"25","pageNumber":1,"sortBy":"Name","sortDirection":"asc","filterBy":"provider","childSize":100,"filterType":"ObjectAttributes"}', 
            '[]', '[{"postalCodeType":"mailing","mailingPostalCodeRange":"123456","mailingPostalCodeRadius":"0","mailingPostalCodeUnit":"m"}]', 
            '[]'));
        system.assertNotEquals(null, TerritoryManagementController.getPLData(
            '{"isIncludeMyExisting":true,"isIncludeOthersExisting":true,"pageSize":"25","pageNumber":1,"sortBy":"Name","sortDirection":"asc","filterBy":"provider","childSize":100,"filterType":"Lookup"}', 
            '[]', '[]', 
            '[{"lookupType":"Account","value":["Test"],"label":["Test"],"isAllSelected":false}]'));
    }
    
	@isTest
    static void testBehavior11(){
        system.assertNotEquals(null, TerritoryManagementController.fetchAttributes(NamespaceSelector.baseNamespace+'Location__c', NamespaceSelector.baseNamespace+'Location__r'));
        system.assertNotEquals(null, TerritoryManagementController.fetchAttributes('Account', ''));
        system.assertNotEquals(null, TerritoryManagementController.fetchAttributes('Contact', ''));
        system.assertNotEquals(null, TerritoryManagementController.getObjectFields(NamespaceSelector.baseNamespace+'Location__c'));
        
        Account practice= new Account();
        practice.put('Name', 'Test');
        practice.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Practice & Group').getRecordTypeId();
        insert practice;
        
        system.assertNotEquals(null, TerritoryManagementController.getProvLocForPracticeGroup(practice.Id, false, false, 
                                                                                              'provider', 'ObjectAttributes', 
                                                                                              '('+NamespaceSelector.baseNamespace+'Location__r.Name = "Test 1")', 
                                                                                              100)
                              );
    }
}