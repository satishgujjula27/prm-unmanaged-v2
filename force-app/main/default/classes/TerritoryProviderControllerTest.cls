/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: TerritoryProviderControllerTest
* @description: This is the test class for TerritoryProviderController
* @Last Modified Date: 03/24/2020
**/
@isTest
public class TerritoryProviderControllerTest {
    static testMethod void testBehavior() {

        Sobject ZipCode= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Zipcode__c').newSObject();
        ZipCode.put(NamespaceSelector.baseNamespace+'Zipcode__c', '123456');
        ZipCode.put(NamespaceSelector.baseNamespace+'Latitude__c', 41);
        ZipCode.put(NamespaceSelector.baseNamespace+'Longitude__c', -71);
        insert ZipCode;

        Sobject Location= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Location__c').newSObject();
        Location.put('Name', 'Location 1');
        insert Location;
        
        Contact Provider= new Contact();
        Provider.put('FirstName', 'Test');
        Provider.put('LastName', 'Test');
        insert Provider;

        Sobject PractitionerLocation= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocation__c').newSObject();
        PractitionerLocation.put('Name', 'PL');
        PractitionerLocation.put(NamespaceSelector.baseNamespace+'Location__c', Location.Id);
        PractitionerLocation.put(NamespaceSelector.baseNamespace+'Provider__c', Provider.Id);
        insert PractitionerLocation;

        Sobject territory= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Territory__c').newSObject();
        territory.put('Name', 'Test Territory');
        insert territory;

        System.assertEquals(String.isBlank(TerritoryProviderController.getProviderLocations(territory.Id, '[]', 'provider')), false);
    }
}