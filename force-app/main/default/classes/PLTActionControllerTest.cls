/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying 
* or sale of the above may constitute an infringement of copyright 
* and may result in criminal or other legal proceedings.
*
* @Class Name: PLTActionControllerTest
* @description: This is the test class PLTActionController
* @Last Modified Date: 03/26/2020
**/
@isTest
public class PLTActionControllerTest {
    @testSetup 
    static void setup() {
        Sobject Location= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Location__c').newSObject();
        Location.put('Name', 'Location 1');
        insert Location;
        
        Contact Provider= new Contact();
        Provider.put('FirstName', 'Test');
        Provider.put('LastName', 'Test');
        insert Provider;
        
         Sobject PractitionerLocation1= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocation__c').newSObject();
        PractitionerLocation1.put('Name', 'PL');
        PractitionerLocation1.put(NamespaceSelector.baseNamespace+'Location__c', Location.Id);
        PractitionerLocation1.put(NamespaceSelector.baseNamespace+'Provider__c', Provider.Id);
        insert PractitionerLocation1;
        
        Sobject territory= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Territory__c').newSObject();
        territory.put('Name', 'Test Territory');
        insert territory;
        
        Sobject pltObj= Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c').newSObject();
        pltObj.put(NamespaceSelector.baseNamespace+'Territory__c', territory.Id);
        pltObj.put(NamespaceSelector.baseNamespace+'PractitionerLocation__c', PractitionerLocation1.Id);
        insert pltObj;
    }
	@isTest
	static void testBehavior(){
        System.assertEquals(1, PLTActionController.getPicklistValues().size());
    }
    
    @isTest
    static void testBehavior1(){
        test.startTest();
        List<Sobject> plObjs = Database.query('SELECT Id FROM '+NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c LIMIT 1');
        PLTActionController.removePLTRecords(new List<Id>{plObjs[0].id});
        PractitionerLocationTerritory__c deletedPLT = [SELECT Id, IsDeleted FROM PractitionerLocationTerritory__c WHERE Id = :plObjs[0].Id ALL ROWS];
        system.assertEquals(true, deletedPLT.IsDeleted);
        test.stopTest();
    }
    
     @isTest
    static void testBehavior2(){
        test.startTest();
        // Create Test Account
        Task testTask = new Task ();
        testTask.Subject = 'Call';
        testTask.Description = 'Test Description';
        testTask.Status = 'Open';
        
        List<Sobject> plObjs = Database.query('SELECT Id FROM '+NamespaceSelector.baseNamespace+'PractitionerLocationTerritory__c LIMIT 1');
        PLTActionController.logCall(testTask,'',new List<Id>{plObjs[0].Id});
        List<task> lstTask = [SELECT Id FROM task];
        system.assertEquals(1,lstTask.size());
        test.stopTest();
    }
}