/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and EVariant corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying 
* or sale of the above may constitute an infringement of copyright 
* and may result in criminal or other legal proceedings.
*
* @Class Name: CoreControllerExceptionTest
* @description: This test class is for CoreControllerException.
* @Author: Healthgrades Inc.
**/
@isTest
public class CoreControllerExceptionTest {
    
    /**@@
    #DESCRIPTION   : This method is used to create Test records.
    #Paramaters    : None
    @@**/
    @testSetup static void createTestdata() {
        // Create Test Account
        Account testAccount = new Account ();
        testAccount.Name = 'Test Account1';
        insert testAccount;
    }
    
    /**@@
    #DESCRIPTION   : Test method for CoreControllerExceptio
    #Paramaters    : None
    @@**/
    static testmethod void CoreControllerExceptionTest1() {
        Test.startTest(); 
        List<sObject> lstObjects = [SELECT Id FROM Account];
        system.assertEquals(lstObjects.size(), 1);
        DmlException dmlEx = new DmlException();
        CoreControllerException e = new CoreControllerException('APIServiceMappingController', String.valueOf(dmlEx));
        Test.stopTest();
    }
}