/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: RelatedListController
* @description: This Class is used to show Custom Related List
* @Last Modified Date: 10/04/2020
**/
public inherited sharing class RelatedListController {
    
    /********************************
    * @method: fetchRecs
    * @description: fetch records.
    * @return: RelatedListResult
    * *****************************/
    @AuraEnabled  
    public static RelatedListResult fetchRecs( Id recId, String sObjName, String parentFieldApiName, String strCriteria ) {
        RelatedListResult obj = new RelatedListResult();  
        String prObjName = parentFieldApiName;
        SObjectType fetchRecsToken = Schema.getGlobalDescribe().get(sObjName);
        Schema.DescribeFieldResult fetchRecsFieldDesc = fetchRecsToken.getDescribe().fields.getMap().get(prObjName).getDescribe();
        if(prObjName != null && Util.checkObjectIsReadable(fetchRecsToken) && Util.checkFieldIsReadable(fetchRecsToken, fetchRecsFieldDesc) ) {
            String strTitle = ' (';           
            List <sObject> listsObjects = new List <sObject>();
            String strSOQL = 'SELECT Id FROM ' + sObjName + ' WHERE ' + prObjName + ' = \'' + recid + '\'';  
            if ( String.isNotBlank( strCriteria ) ) { strSOQL += ' ' + strCriteria; }
            strSOQL += ' LIMIT 4';
            listsObjects = Database.query( strSOQL );    
            Integer intCount = listsObjects.size();  
            if ( intCount > 3 ) {
                List < sObject > tempListsObjects = new List < sObject >();  
                for ( Integer i = 0; i <3; i++ ) {
                    tempListsObjects.add( listsObjects.get( i ) );
                }
                obj.listsObject = tempListsObjects; strTitle += '3+';
            } else {
                obj.listsObject = listsObjects; strTitle += String.valueOf( intCount );
            }
            strTitle += ')';
            obj.strTitle = strTitle;
        } else {
            CoreControllerException getSelectOptionsCRUDException = new CoreControllerException('RelatedListController.fetchRecs', 
                                                                                                        'Could not access sObject ==> '+sObjName);
            return null;
        }
        return obj;
    }
    
    /********************************
    * @method: getPLTRecord
    * @description: fetch PLT records.
    * @return: String
    * *****************************/
    @AuraEnabled
    public static String getPLTRecord(String recId) { return RelatedLkpFilterController.getPLTRecord(recId, 4, 'CreatedDate', 'desc', '50', '1'); }
    
    /********************************
    * @method: deleteRecord
    * @description: delete record based on recordId.
    * @return: Boolean
    * *****************************/
    @AuraEnabled
    public static Boolean deleteRecord(Id recordId) { return RelatedLkpFilterController.deleteRecord(recordId); }
    
    /********************************
    * @method: RelatedListResult
    * @description: inner class.
    * *****************************/
    public class RelatedListResult {
        @AuraEnabled public String strTitle;  
        @AuraEnabled public List <sObject> listsObject;
    }  
}