/*
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
* @Class Name: MultiSelectApexControllerTest
* @description: This is the test class for MultiSelectApexController.
* @Last Modified Date: 01/23/2021
**/
@isTest
public class MultiSelectApexControllerTest {
    static testMethod void testBehavior(){
        Sobject territory = Schema.getGlobalDescribe().get(NamespaceSelector.baseNamespace+'Territory__c').newSObject();
        territory.put('Name', 'Test Territory');
        insert territory;

        System.assertNotEquals(null,MultiSelectApexController.getSelectOptions(NamespaceSelector.baseNamespace+'Territory__c','Name','Test','[]'));
    }
    
    static testMethod void testBehaviorAccount(){
        Sobject accountObj = Schema.getGlobalDescribe().get('Account').newSObject();
        accountObj.put('Name', 'Test Account');
        insert accountObj;

        System.assertNotEquals(null, MultiSelectApexController.getSelectOptions('Account', 'Name', 'Test', '[]'));
    }
    
    static testMethod void testBehaviorContact(){
        Sobject contactObj = Schema.getGlobalDescribe().get('Contact').newSObject();
        contactObj.put('FirstName', 'Test Contact');
        contactObj.put('LastName', 'Test Contact');
        insert contactObj;

        System.assertNotEquals(null, MultiSelectApexController.getSelectOptions('Contact', 'LastName', 'Test', '[]'));
    }
    
    static testMethod void testBehaviorNotAccessible(){
        Sobject contactObj = Schema.getGlobalDescribe().get('Contact').newSObject();
        contactObj.put('FirstName', 'Test Contact');
        contactObj.put('LastName', 'Test Contact');
        insert contactObj;

        System.assertNotEquals(null, MultiSelectApexController.getSelectOptions('Contact_Share', 'LastName', 'Test', '[]'));
    }
    
    static testMethod void testBehaviorValuesParam(){
        Sobject contactObj = Schema.getGlobalDescribe().get('Contact').newSObject();
        contactObj.put('FirstName', 'Test Contact');
        contactObj.put('LastName', 'Test Contact');
        insert contactObj;

        System.assertNotEquals(null, MultiSelectApexController.getSelectOptions('Contact', 'LastName', 'Test', '["Name":"Test"]'));
    }
}