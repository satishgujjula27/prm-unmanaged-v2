({
    rerender: function (component, helper) {
        var nodes = this.superRerender();
        var loadMap = component.get("v.loadMap");
        if (!window.L || !loadMap) return nodes;

        $('#addressSearchForm').submit(function(event) {
            event.preventDefault();
            if (event.type == 'submit') {
				helper.searchForAddress(component,event,helper);
            }
   			return false;
		});
	    helper.initMap(component,event,helper);
        return nodes;
    }
})