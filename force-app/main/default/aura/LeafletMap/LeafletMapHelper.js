({
    initMap : function(component, event, helper) {
        var map = component.get("v.map");
        if (map === null && component.get("v.isLoaded") !== "true") {
            //  map.once('focus', function() { map.scrollWheelZoom.enable(); });
            component.set("v.isLoaded","true");
            var showDraw = component.get("v.showDraw");
            var mapElement = component.find("map").getElement();
            var mapCenter = JSON.parse(component.get("v.mapCenter"));
            var latitude, longitude, zoomLevel;
            if (mapCenter !== null) {
                latitude = mapCenter.locations[0].feature.geometry.y;
                longitude = mapCenter.locations[0].feature.geometry.x;
                zoomLevel = 14;
            } else {
                latitude = '39.759998584000471';
                longitude = '-98.499996380999676';
                zoomLevel = 5;
            }
            var scrollWheelZoom = component.get("v.scrollWheelZoom");
            map = window.L.map(mapElement, {zoomControl: true, scrollWheelZoom: scrollWheelZoom, tap: false }).setView([latitude, longitude], zoomLevel);
            if (!scrollWheelZoom) {
                map.once('focus', function() { map.scrollWheelZoom.enable(); });
                map.on('blur', function() {map.scrollWheelZoom.disable();});
            }
            
            var mapTile = component.get("v.mapTile");
            var mapTileAttribution = component.get("v.mapTileAttribution");
            window.L.tileLayer(mapTile, {attribution: mapTileAttribution}).addTo(map);
            component.set("v.map", map);
            var markers = new window.L.FeatureGroup();
            component.set("v.markers", markers);
            var geoJsonLayer = L.geoJson(null, {
                style: function (feature) {
                    return { color: "#1798c1" };
                },
                pointToLayer: function (feature, latlng) {
                    return new L.Circle(latlng, feature.properties.radius, { color: "#1798c1" });
                }
            }).addTo(map);
            
            component.set("v.geoJsonLayer", geoJsonLayer);
            
            // Initialize the FeatureGroup to store editable layers
            if (showDraw) {
                var drawnItems = new L.FeatureGroup();
                component.set("v.drawnItems",drawnItems)
                map.addLayer(drawnItems);
                // Initialize the draw control and pass it the FeatureGroup of editable layers
                var drawControl = new L.Control.Draw({
                    draw: {
                        position: 'topleft',
                        polyline: false,
                        marker: false,
                        polygon: true,
                        polygon: {
                            shapeOptions: {
                                color: '#1798c1'
                            },
                            allowIntersection: true,
                            editable: true
                        },
                        rectangle: {
                            shapeOptions: {
                                color: '#1798c1'
                            },
                            allowIntersection: true,
                            editable: true
                        },
                        circle: {
                            shapeOptions: {
                                color: '#1798c1'
                            },
                            allowIntersection: true,
                            editable: true
                        }
                    },
                    edit: {
                        featureGroup: drawnItems
                    },
                });
                
                var circleToGeoJSON = L.Circle.prototype.toGeoJSON;
                L.Circle.include({
                    toGeoJSON: function() {
                        var feature = circleToGeoJSON.call(this);
                        feature.properties = {
                            shape_type: 'circle',
                            radius: this.getRadius(),
                            color: '#1798c1'
                        };
                        return feature;
                    }
                });
                
                var rectangleToGeoJSON = L.Rectangle.prototype.toGeoJSON;
                L.Rectangle.include({
                    toGeoJSON: function() {
                        var feature = rectangleToGeoJSON.call(this);
                        feature.properties = {
                            shape_type: 'rectangle',
                            color: '#1798c1'
                        };
                        return feature;
                    }
                });
                map.addControl(drawControl);
                map.on('draw:created', function (e) {
                    helper.saveDrawnItems(component,e,"created",helper,event);
                });
                map.on('draw:edited', function (e) {
                    helper.saveDrawnItems(component,e,"edited",helper,event);
                });
                map.on('draw:deleted', function (e) {
                    helper.saveDrawnItems(component,e,"deleted",helper,event);
                });
            }
            if (component.get("v.records") !== null || component.get("v.recordString") !== '') {
                this.addMarkers(component,event,helper);
            }
            //  if (callback !== null && typeof callback !== "undefined") {
            //      callback(component,event,helper);
            //  }
            var passedAddress = component.get("v.passedAddress");
            if (passedAddress !== null && typeof passedAddress !== "undefined") {
                this.searchForAddress(component,event,helper);
            }
            var passedLat = component.get("v.passedLat");
            var passedLng = component.get("v.passedLng");
            if (passedLat !== null && typeof passedLat !== "undefined"
                && passedLng !== null && typeof passedLng !== "undefined"
               ) {
                this.addAddressMarkerByLatLng(component,passedLat,passedLng);
            }
        }
        if(component.get('v.saveRecord') !=null && component.get('v.geoJsonField') !=null) {
            this.refreshMap(component,map);
        }
    },
    
    refreshMap : function(component, map, helper) {
        var saveRecord = component.get("v.saveRecord");
        var recordId = component.get("v.recordId");
        // quit if the record's id is the same...
        if (saveRecord.Id === recordId) return;
        // else set the record id (enables saving of record changes without reloading)
        component.set("v.recordId",saveRecord.Id);
        
        // clear the geoJsonLayer
        var geoJsonLayer = component.get("v.geoJsonLayer");
        if (geoJsonLayer !== null && typeof geoJsonLayer !== "undefined") {
            geoJsonLayer.clearLayers();
        }
        
        // clear the drawnItems FeatureGroup
        var drawnItems = component.get("v.drawnItems");
        if (drawnItems !== null && typeof drawnItems !== "undefined") {
            drawnItems.clearLayers();
        }
        
        // get the starting geojson from the controlling record and add them to the geoJsonLayer
        var strNamespace = component.get("v.namespacePrefix");
        var recordGeoJson = saveRecord !== null ? saveRecord[component.get("v.geoJsonField")] : null;
        if (recordGeoJson !== null) {
            var geoJsonFeatures = $.parseJSON(recordGeoJson);
            geoJsonLayer.addData(geoJsonFeatures);
        }
        
        // add each item from the geoJson layer to the drawnItems feature group
        for (var i = 0; i < geoJsonLayer.getLayers().length; i++) {
            var shape = geoJsonLayer.getLayers()[i];
            if (shape.feature.properties) {
                if (shape.feature.properties.shape_type === "rectangle") {
                    map.removeLayer(shape);
                    shape = L.rectangle(shape.getBounds(),{ color: '#1798c1'});
                    drawnItems.addLayer(shape);
                    continue;
                }
            }
            drawnItems.addLayer(shape);
        }
        
        // set them both as component attributes for future reference
        component.set("v.geoJsonLayer",geoJsonLayer);
        component.set("v.drawnItems",drawnItems);
        
        //try to bound the map by the items if possible
        if (geoJsonLayer !== null
            && typeof geoJsonLayer !== "undefined"
            && geoJsonLayer.getBounds().isValid()) {
            var bounds = geoJsonLayer.getBounds();
            map.fitBounds(geoJsonLayer.getBounds());
            component.set("v.bounds",bounds);
        } else if (saveRecord !== null) {
            // if no items, center it on the bounds set when the controlling record was created
            var seedAddressField = component.get("v.seedAddressField");
            var mapCenter = saveRecord[seedAddressField];
            if (mapCenter !== null && typeof mapCenter !== "undefined") {
                component.set("v.mapCenter",mapCenter);
            }
        }
    },
    
    saveDrawnItems : function(component,e,action,helper,event1) {
        // get all the drawn items on the map (now part of the FeatureGroup "drawnItems")
        // loop through them and save them all to the geoJsonLayer
        var drawnItems = component.get("v.drawnItems");
        var map = component.get("v.map");
        var geoJsonLayer = component.get("v.geoJsonLayer");
        var saveRecord = component.get("v.saveRecord");
        if (action === "created") {
            var type = e.layerType;
            var layer = e.layer;
            drawnItems.addLayer(layer);
            var layerGeoJson = layer.toGeoJSON();
            geoJsonLayer.getLayers().push(layer);
        }
        
        var recordBounds, saveGeoJson = {}
        if (drawnItems !== null
            && drawnItems.getLayers().length > 0
            && typeof drawnItems.getLayers()[0] !== "undefined") {
            saveGeoJson = drawnItems.toGeoJSON();
            // provider get changes 
            let enableMarkerForSelection = component.get("v.enableMarkerForSelection");
            if(enableMarkerForSelection) {
                helper.checkMarkersInSideDraw(component,saveGeoJson,helper,event1);
            }
            // end provider get changes 
            recordBounds = drawnItems.getBounds();
            map.fitBounds(recordBounds);
            component.set("v.bounds",recordBounds);
        }
        if (saveRecord !== null && typeof saveRecord !== "undefined") {
            var geoJsonField = component.get("v.geoJsonField");
            var boundsField = component.get("v.boundsField");
            if (recordBounds === null) {
                recordBounds = map.getBounds();
            }
            saveRecord[geoJsonField] = saveGeoJson !== null && typeof saveGeoJson !== "undefined" ? JSON.stringify([saveGeoJson]) : "";
            saveRecord[boundsField] = recordBounds !== null && typeof recordBounds !== "undefined" ? JSON.stringify([recordBounds]) : "";
            this.refreshMap(component,map);
        }
        component.set("v.drawnItems",drawnItems);
        component.set("v.geoJsonLayer",geoJsonLayer);
        
        if (saveRecord !== null) {
            this.saveRecord(component,saveRecord);
            component.set("v.saveRecord",saveRecord);
        }
    },
    
    searchForAddress : function(component, event, helper) {
        var searchComponent = component.find("searchAddress");
        var addressString;
        if (typeof searchComponent !== "undefined") {
            addressString = searchComponent.get("v.value");
        }
        if (typeof addressString === "undefined" || addressString === "") {
            addressString = component.get("v.passedAddress");
        }
    },
    
    recenterMap : function(component) {
        var map = component.get("v.map");
        var mapCenter = component.get("v.mapCenter");
        if (typeof mapCenter !== "object") {
            mapCenter = $.parseJSON(mapCenter);
        }
        if (mapCenter !== null && map !== null) {
            latitude = mapCenter.lat;
            longitude = mapCenter.lon;
            //OLD GEOCODING  latitude = mapCenter.locations[0].feature.geometry.y;
            //OLD GEOCODING  longitude = mapCenter.locations[0].feature.geometry.x;
            zoomLevel = 12;
            map.setView(new L.LatLng(latitude, longitude),zoomLevel);
        }
    },
    
    pointIsInside : function(xp, yp, x, y) {
        var i, j, c = 0, npol = xp.length;
        for (i = 0, j = npol-1; i < npol; j = i++) {
            if (
                (
                    ((yp[i] <= y) && (y < yp[j])) ||
                    ((yp[j] <= y) && (y < yp[i]))
                ) &&
                (x < (xp[j] - xp[i]) * (y - yp[i]) / (yp[j] - yp[i]) + xp[i])
            ) {
                c =!c;
            }
        }
        return c;
    },
    
    addMarkerByPlace: function(component,place) {
        this.clearMarkers(component);
        var lat = place.lat;
        var lng = place.lon;
        // OLD GEOCODING var lat = place.locations[0].feature.geometry.y;
        // OLD GEOCODING var lng = place.locations[0].feature.geometry.x;
        var addressString = place.address;
        // OLD GEOCODING var addressString = place.locations[0].name;
        var map = component.get("v.map");
        var markers = component.get("v.markers");
        var latLng = [lat, lng];
        //TODO: this should be the same helper method to get content of the marker
        var marker = window.L.marker(latLng, { lat: lat, lon:lng}).bindPopup(addressString); 
        markers.addLayer(marker);
        map.addLayer(markers);
    },
    
    addAddressMarkerByLatLng : function(component,lat,lng) {
        this.clearMarkers(component);
        var map = component.get("v.map");
        var markers = component.get("v.markers");
        var latLng = [lat, lng];
        var marker = window.L.marker(latLng, { lat: lat, lon:lng}).bindPopup('Latitude: ' + lat + '<br/>Longitude:' + lng);
        markers.addLayer(marker);
        map.addLayer(markers);
        map.setView(new L.LatLng(lat, lng),12)
    },
    
    clearMarkers : function(component) {
        var markers = component.get("v.markers");
        markers.clearLayers();
    },
    
    clearClusters : function(component) {
        var clusters = component.get("v.clusters");
        clusters.clearLayers();
    },
    
    addMarkers: function(component,event,helper) {
        var map = component.get("v.map");
        var markers = component.get("v.markers");
        var records = component.get("v.records");
        //  var clusters = component.get("v.clusters");
        
        var CustomIcon = helper.getCustomIconObject(component);
        var blueIcon = new CustomIcon({iconUrl: component.get("v.mapIconUncheckedBlueUrl")});
        
        if (typeof records === "undefined" || records.length < 1) {
            var recordString = component.get("v.recordString");
            if( typeof recordString !== "undefined" ) {
                if (recordString.substr(0,1) === '"') {
                    recordString = recordString.substring(1,recordString.length-1);
                }
                records = JSON.parse(recordString);
                //  component.set("v.records",records);
            }
        }
        var drawnItems = component.get("v.drawnItems");
        // Remove existing markers
        this.clearMarkers(component);
        
        // Add Markers
        if (map && records && records.length > 0) {
            var allLatLngs = [];
            var latLngNameArray = [];
            var latFieldName = component.get("v.latField");
            var lngFieldName = component.get("v.lngField");
            var clusters = L.markerClusterGroup();
            latLngNameArray.push.apply(latLngNameArray,[latFieldName,lngFieldName]);
            for (var i = 0; i < records.length; i++) {
                var record = records[i];
                var recordId = typeof record.Id !== "undefined" ? record.Id : 'record' + i; // sometimes the record may not be an sobject
                var lat,lng
                for (var j = 0;j < 2; j++) {
                    var value = latLngNameArray[j].split(".").reduce(function(a, b) { return a !== null ? a[b] : a; },record);
                    if (latLngNameArray[j] === latFieldName) {
                        lat = value;
                    } else if (latLngNameArray[j] === lngFieldName) {
                        lng = value;
                    }
                }
                // TODO: make getting marker content a helper function
                var markerFields = component.get("v.markerFields");
                var markerTemplate = component.get("v.markerTemplate");
                var markerContent = "";
                var enableMarkerForSelection = component.get("v.enableMarkerForSelection");
                var showMarkerOnPopup = component.get("v.showMarkerOnPopup");
                
                if (typeof markerTemplate !== "undefined" && typeof markerFields !== "undefined") {
                    for (var k = 0; k < markerFields.length; k++) {
                        var markerFieldApiName = markerFields[k];
                        var markerFieldValue = markerFieldApiName.split(".").reduce(function(c, d) { return c != null ? c[d] : c; },record);
                        
                        if (markerFieldValue === null || markerFieldValue === undefined) markerFieldValue = "  ";
                        var re = new RegExp(markerFieldApiName,"g");     // replace all occurrences
                        markerTemplate = markerTemplate.replace(re,markerFieldValue);
                    }
                    markerContent = markerTemplate;
                } else {
                    var nameField = component.get("v.nameField");
                    var nameFieldValue = nameField.split(".").reduce(function(c, d) { return c !== null ? c[d] : c; },record);
                    markerContent = nameFieldValue;
                }
                if (typeof lat !== "undefined" && lng !== "undefined" &&
                    lat !== null && lng !== null) {
                    //   if (1 === 1) { // later, replace this with the logic outlined below
                    // loop through all the layers on the map geoJsonLayer.getLayers()
                    // for each layer, get the geoJson property that stores the lat/lng array
                    // as soon as the lat/lng returns true for pointIsInside, continue
                    // if it doesn't, quit without drawing the marker
                    var latLng = [lat, lng];
                    var markerDiv = document.createElement("div"); // creating a div allows the marker state to persist if the content alters!
                    // strings do not, and will reset every time.
                    markerDiv.innerHTML = markerContent;
                    var selectOrUndo = "select";
                    var marker = window.L.marker(latLng, { lat: lat, lon:lng, draggable:true, id: recordId + 'marker', title:"Click to " + selectOrUndo })
                    .bindPopup(markerDiv);
                    if(enableMarkerForSelection) {
                        let isSelected = helper.checkSelected(component,recordId);
                        let iconUrl = component.get("v.mapIconUncheckedBlueUrl");
                        let shadowUrl = component.get("v.mapIconUncheckedShadowUrl");
                        if(isSelected) {
                            iconUrl = component.get("v.mapIconCheckedBlueUrl");
                            shadowUrl = component.get("v.mapIconCheckedShadowUrl");
                        }
                        marker.setIcon(new CustomIcon({iconUrl: iconUrl, shadowUrl: shadowUrl}));
                        var addOnClick = function(recordId) {
                            marker.on('click', function (e) {
                                // toggleSelectRecord(e,recordId);
                                // highlightSelected();
                                helper.handleToggleSelectedRecord(component, e, recordId, helper);
                            });
                        };
                        addOnClick(recordId);
                    }
                    if(showMarkerOnPopup){
                        marker.on('mouseover', function (e) { this.openPopup(); });
                    }
                    markers.addLayer(marker);
                    clusters.addLayer(marker);
                    allLatLngs.push(latLng);
                    //   }
                }
                
            }
            /* LOCKERSERVICE ERROR: this section causes an invalid bounds error */
            if (allLatLngs.length > 0 && (drawnItems === null || drawnItems.getLayers().length < 1)) {
                var bounds = new window.L.LatLngBounds(allLatLngs);
                if (bounds.isValid()) {
                    map.fitBounds(bounds);
                    component.set("v.bounds", bounds)
                }
            }
            /* LOCKERSERVICE: cluster causes a locker service error as well, having to do with an animation event */
            if (component.get("v.useCluster")) {
                map.addLayer(clusters);
                component.set("v.clusters", clusters);
            } else {
                map.addLayer(markers);
                component.set("v.markers", markers);
            }
        }
    },
    
    saveRecord : function(component,record) {
        var action1 = component.get("c.updateRecord");
        action1.setParams({ 'saveRecordId' : record['Id'],'geoJson' : record[component.get("v.geoJsonField")],'field':component.get('v.geoJsonField')});
        $A.enqueueAction(action1);
    },
    
    handleToggleSelectedRecord : function(component,e,id,helper){
        var selectedIds = component.get("v.selectedIds");
        var CustomIcon1 = helper.getCustomIconObject(component);
        if(!selectedIds.includes(id)) {
            e.target.setIcon(new CustomIcon1({iconUrl: component.get("v.mapIconCheckedBlueUrl"), shadowUrl: component.get("v.mapIconCheckedShadowUrl")}));
            helper.addSelected(component, id);
        } else {
            e.target.setIcon(new CustomIcon1({iconUrl: component.get("v.mapIconUncheckedBlueUrl"), shadowUrl: component.get("v.mapIconUncheckedShadowUrl")}));
            helper.removeSelected(component, id);
        }
    },
    
    removeSelected : function(component,id){
        let selectedIds = component.get("v.selectedIds");
        let index = selectedIds.indexOf(id);
        if (index > -1) {
            selectedIds.splice(index, 1);
            component.set("v.selectedIds", selectedIds);
            var cmpEvent = component.getEvent("selectedNodesInMap");
            cmpEvent.setParams({"toggleId" : id, "opperation": "removeSelected"}); 
            cmpEvent.fire();
        }
    },
    addSelected : function(component,id){
        let selectedIds = component.get("v.selectedIds");
        selectedIds.push(id);
        component.set("v.selectedIds", selectedIds);
        
        var cmpEvent = component.getEvent("selectedNodesInMap");
        cmpEvent.setParams({"toggleId" : id, "opperation": "addSelected"}); 
        cmpEvent.fire();
    },
    checkSelected : function(component,id){
        var selectedIds = component.get("v.selectedIds");
        if(selectedIds.includes(id)) {
            return true;
        } else {
            return false;
        }
    },
    getCustomIconObject : function(component){
        return L.Icon.extend({
            options: {
                shadowUrl: component.get("v.mapIconCheckedBlueUrl"),
                iconSize:     [30, 32],
                shadowSize:   [36, 44],
                iconAnchor:   [0, 24],
                shadowAnchor: [-2, 36],
                popupAnchor: [10,-10]
            }
        });
    },
    checkMarkersInSideDraw : function(component,saveGeoJson,helper,event1){
        var searchResultData = component.get("v.searchResultData");
        for(var i in searchResultData) {
            var pObj = searchResultData[i];
            let longVarName = component.get("v.longitudeVarName");
            let latVarName = component.get("v.latitudeVarName");
            let lngLat = [pObj[longVarName], pObj[latVarName]]; 
            let latLng = L.latLng(pObj[latVarName], pObj[longVarName]);
            helper.checkInside(saveGeoJson, helper, lngLat, latLng, pObj, component, event1);
        }
        
        // calling this function to reploating map for showing selected 
        this.addMarkers(component, event1, helper);
    },
    
    checkInside : function(saveGeoJson,helper,lngLat,latLng,record,component,event1){
        for (var i = 0; i < saveGeoJson.features.length; i++) {
            var feature = saveGeoJson.features[i];
            if (feature.geometry.type == 'Polygon') {
                var featurePolygon = feature.geometry.coordinates[0];
                if (helper.isPointInside(lngLat,featurePolygon)) {
                    //addSelectedId(pl.record.Id);
                   // helper.updateMarkerAfterDraw(component, helper, latLng, lngLat, record);
                    
                    helper.addSelected(component, record.Id);
                }
            } else if (feature.geometry.type == 'Point') {
                var radius = feature.properties.radius;
                var centerPoint = L.latLng(feature.geometry.coordinates[1],feature.geometry.coordinates[0]);
                if (latLng != null && latLng.distanceTo(centerPoint) < radius) {
                    helper.addSelected(component,record.Id);
                }
            }
        }
    },
    
    isPointInside : function(point,vs){
        var x = point[0], y = point[1];
        
        var inside = false;
        for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
            var xi = vs[i][0], yi = vs[i][1];
            var xj = vs[j][0], yj = vs[j][1];
            
            var intersect = ((yi > y) != (yj > y))
            && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
            if (intersect) inside = !inside;
        }
        
        return inside;
    },
    
    updateMarkerAfterDraw : function(component, helper, latLng, lngLat, record) {
        var CustomIcon = helper.getCustomIconObject(component);
        var map = component.get("v.map");
        var markers = component.get("v.markers");
        
        var markerFields = component.get("v.markerFields");
        var markerTemplate = component.get("v.markerTemplate");
        var markerContent = "";
        
        if (typeof markerTemplate !== "undefined" && typeof markerFields !== "undefined") {
            for (var k = 0; k < markerFields.length; k++) {
                var markerFieldApiName = markerFields[k];
                var markerFieldValue = markerFieldApiName.split(".").reduce(function(c, d) { return c != null ? c[d] : c; },record);
                
                if (markerFieldValue === null || markerFieldValue === undefined) markerFieldValue = "  ";
                var re = new RegExp(markerFieldApiName,"g");     // replace all occurrences
                markerTemplate = markerTemplate.replace(re,markerFieldValue);
            }
            markerContent = markerTemplate;
        } else {
            var nameField = component.get("v.nameField");
            var nameFieldValue = nameField.split(".").reduce(function(c, d) { return c !== null ? c[d] : c; },record);
            markerContent = nameFieldValue;
        }
        
        var markerDiv = document.createElement("div");
        markerDiv.innerHTML = markerContent;
        //TODO: this should be the same helper method to get content of the marker
        var marker = window.L.marker(latLng, { lat: lngLat[1], lon:lngLat[1]}).bindPopup(markerDiv); 
        iconUrl = component.get("v.mapIconCheckedBlueUrl");
        shadowUrl = component.get("v.mapIconCheckedShadowUrl");
        marker.setIcon(new CustomIcon({iconUrl: iconUrl ,shadowUrl: shadowUrl}));
        marker.on('mouseover', function (e) { this.openPopup(); });
        markers.addLayer(marker);
        map.addLayer(markers);
    }
    
})