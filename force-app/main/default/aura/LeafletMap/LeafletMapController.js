({
    jsLoaded: function(component, event, helper) {
        helper.initMap(component, event, helper);
        component.set("v.showDraw", false);
        //component.rerender();
    },

    refreshMap : function(component, event, helper) {
		var map = component.get("v.map");
        if (map == null || typeof map == "undefined") return;
	    helper.refreshMap(component, map, helper);
    },

	recordChangeHandler: function(component, event, helper) {
        helper.addMarkers(component);
	},

	formPress: function(component, event, helper) {
        if (event.type === 'submit' && event.which === "13") {
			helper.searchForAddress(component,event,helper);
        }
	},

    buttonPress: function(component, event, helper) {
		var map = component.get("v.map");
        if (map === null) {
            helper.initMap(component, event, helper);
        } else {
	        helper.searchForAddress(component,event,helper);
        }
	},

    loadMapHandler: function(component, event, helper) {
        helper.initMap(component,event,helper);
    },

    recenterMap : function(component, event, helper) {
		var map = component.get("v.map");
        if (map == null) {
            helper.initMap(component, event, helper);
        } else {
	        helper.recenterMap(component, map);
        }
    }
})