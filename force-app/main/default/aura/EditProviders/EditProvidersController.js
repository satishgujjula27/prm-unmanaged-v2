({
    onPageReferenceChange: function(component, evt, helper) {
        let myPageRef = component.get("v.pageReference");
        let provRecordId = myPageRef.state.c__recordId;
        component.set("v.recordId", provRecordId);
        component.set("v.currentView", "Filter");
        component.set("v.tbController", {"fieldConditions":[], "marketArea":[],"lookupObject":[], "postalCodeRange":"", "mailingPostalCodeRange":"","MailingZipCondition":{}, "BillingZipCondition":{}, "isIncludeMyExisting":true, "isIncludeOthersExisting":true});
        component.set("v.lookupNames", []);
        component.set("v.mailingPostalCodeRange","");
        component.set("v.mailingPostalCodeUnit","m");
        component.set("v.mailingPostalCodeRadius","0");
        helper.doInitHelper(component);
    },
    doInit : function(component, event, helper) {
        let pageReference = component.get("v.pageReference");
        let device = $A.get("$Browser.formFactor");
        if(pageReference != null && pageReference.state != null) {
            component.set("v.recordId", pageReference.state.c__recordId);
        }
        if (device != "PHONE") {
            component.set("v.isMobileDevice",false);
        }
        let pageSize = (component.get("v.filterByType") == 'practicegroup') ? '50' : '25';
        component.set("v.pageSize", pageSize);
        component.set("v.Territory", {"Name":"Test"});
        component.set("v.lookupNames", []);
        component.set("v.tbController", {"fieldConditions":[], "marketArea":[], "postalCodeRange":"", "mailingPostalCodeRange":"", "providerScores":[{"isApplied":false}], "providerScoreVolumes":[], "providerScoreLoyalties":[], "isIncludeMyExisting":true, "isIncludeOthersExisting":true});
        helper.doInitHelper(component);
    },
    handleFTypValueChange : function(component, event, helper) {
        let ftpoptions = component.get("v.filtertypeoptions");
        for(let i in ftpoptions){
            if(component.get("v.SelectedFilterType") == ftpoptions[i].value) {
             	component.set("v.SelectedFilterTypeLabel", ftpoptions[i].label);   
            }
        }
    },
    changeFilterBy : function(component, event, helper){
        component.set("v.SelectedFilterBy" , event.getParam("value"));
        let fbyoptions = component.get("v.filterbyoptions");
        for(let i in fbyoptions){
            if(fbyoptions[i].value == component.get("v.SelectedFilterBy")) {
             	component.set("v.SelectedFilterByLabel" ,fbyoptions[i].label.replace(/s\b/ig,""));   
            }
        }
        let fltByList = component.get("v.filterByList");
        for(let i in fltByList){
            if(fltByList[i].devName == component.get("v.SelectedFilterBy")){
                component.set("v.filterByType", fltByList[i].filterByType);
            }
        }
        
        helper.resetCriteria(component);
        helper.resetZipFilters(component);
        helper.resetLookupFilter(component);
        helper.fetchFilterTypes(component ,component.get("v.SelectedFilterBy"));
    },
    gotoSobject : function(component, event, helper) {
        let navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({ "recordId": component.get("v.recordId") });
        navEvt.fire();
    },
    showFilter : function(component, event, helper) {
        component.set("v.currentView", "Filter");
    },
    showMap : function(component, event, helper) {
        component.set("v.currentView", "Map");
        helper.loadProviderMapFilterForTerritory(component);
    },
    showTable : function(component, event, helper) {
        component.set("v.currentView", "Table");
    }, 
    handleSelectedNodesInMapEvent : function(component, event, helper) {
        let id = event.getParam("toggleId");
        let opperation = event.getParam("opperation");
        let selectedIds = component.get('v.selectedRows');
        if(opperation == "removeSelected"){
            let index = selectedIds.indexOf(id);
            if (index > -1) { selectedIds.splice(index, 1); }
        } else {
            let index = selectedIds.indexOf(id);
            if (index == -1) { selectedIds.push(id); }
        }
        component.set("v.selectedRows", selectedIds);
        let selectedRowstoShow = selectedIds;
        let data = component.get("v.data");
        if(selectedIds.length > 0){
            data.forEach(function(dataRow) {
                if(dataRow['_children'] != undefined && dataRow['_children'] != '[]' && dataRow['_children'].length > 0)  {
                    let totalChildSelected = 0;
                    dataRow['_children'].forEach(function(children) {
                         if(selectedIds.includes(children.Id)){
                                totalChildSelected++;
                            }
                    });
                    if(totalChildSelected == dataRow['_children'].length) {
                        if(!selectedRowstoShow.includes(dataRow.Id)) {
                            selectedRowstoShow.push(dataRow.Id);
                        }
                        /*if(!selectedParentIds.includes(dataRow.Id)) {
                            selectedParentIds.push(dataRow.Id);
                        } */
                    }
                    else{
                        selectedRowstoShow = selectedRowstoShow.filter(function(ele){ return ele != dataRow.Id; });
                    }
                }
                
            });
        }
        component.set("v.selectedRowstoShow",selectedRowstoShow);
        component.set("v.currentView", 'Table');
        //$A.get('e.force:refreshView').fire();
    },
    handleSelectChange : function(component, event, helper) {
        let values = event.getParam("values");
        let labels = event.getParam("labels");
        let isAllSelected = event.getParam("isAllSelected");
        if(isAllSelected){
            component.set("v.isAllSelected", isAllSelected);
        }
        component.set("v.selectedMultiSelectLabel", labels);
        component.set("v.selectedMultiSelectValues", values);
        component.set("v.isAllSelected", isAllSelected);
    }, 
    search_onClick: function(component, event, helper) {
        helper.searchHelper(component);
        if(component.get("v.isFilterChanged") == true){
            helper.resetDatatableAttribute(component);
        }
    }, 
    addMarketAreas: function(component, event, helper) {
        let tbController=component.get("v.tbController");
        tbController.marketArea.push({ 'zipCodeField':component.get("v.zipCodeField"), "maName":component.get("v.selectedMultiSelectValues"), "maLabel":component.get("v.selectedMultiSelectLabel") });
        let dataVal = [];
        if(component.get("v.maNames") != undefined) {
            dataVal = component.get("v.maNames");
            for(let i=0;i<component.get("v.selectedMultiSelectLabel").length;i++){
                dataVal.push(component.get("v.selectedMultiSelectLabel")[i]);
            }
        } else {
            dataVal = component.get("v.selectedMultiSelectLabel");
        }
        
        component.set("v.maNames", dataVal);
        component.set("v.tbController", tbController);
        component.set("v.selectedMultiSelectLabel", []);
        component.set("v.selectedMultiSelectValues", []);
        helper.resetPaginationParam(component);
    },
    addlookupFilter: function(component, event, helper) {
        
        let lookupObj = new Object();
        lookupObj["lookupType"] = component.get("v.SelectedFilterType").split(',')[0];
        lookupObj["value"] = component.get("v.selectedMultiSelectValues");
        lookupObj["label"] = component.get("v.selectedMultiSelectLabel");
        lookupObj["isAllSelected"] = component.get("v.isAllSelected");
        
        if( component.get("v.isSpecialtyFilterType") && component.get("v.selectedMultiSelectValues").length >0 ) {
            let selSpecLkpVal = component.get("v.selectedFilterLevel");
            let existLkLabel = component.get("v.lookupLabel");
            let existLkLblArr = existLkLabel.split(',');
            existLkLblArr.push(' Specialties ('+selSpecLkpVal[0].label+')');
            component.set("v.lookupLabel", existLkLblArr.join());
            
            if( component.get("v.isSpecialtyLevel") ) {
                lookupObj["lookupLevel"] = component.get("v.EDLnamespace")+'Speciality__r.'+component.get("v.EDLnamespace")+'Code__c';
            } else {
                lookupObj["lookupLevel"] = selSpecLkpVal[0].value;
            }
            
            component.set("v.lookupFilterComponent", []);
            component.set("v.fieldConditionsField", "");
        }
        
        let tbController = component.get("v.tbController");
        tbController.lookupObject = [];
        tbController.lookupObject.push(lookupObj);
        
        let dataVal = [];
        if(component.get("v.lookupNames") != undefined) {
            for(let i=0;i<component.get("v.selectedMultiSelectLabel").length;i++){
                dataVal.push(component.get("v.selectedMultiSelectLabel")[i]);
            }
        } else {
            dataVal = component.get("v.selectedMultiSelectLabel");
        }
        component.set("v.lookupNames", dataVal);
        component.set("v.tbController", tbController);    
        component.set("v.selectedMultiSelectLabel", []);
        component.set("v.selectedMultiSelectValues", []);
        component.set("v.selAdvSpecDataObj", []);
        helper.resetPaginationParam(component);
    },
    addSelected :  function(component, event, helper) {
        helper.addSelectedHelper(component, event, helper);
    },
    resetMarketAreas : function(component, event, helper) {
        let tbController = component.get("v.tbController");
        tbController.marketArea = [];
        let dataVal = [];
        component.set("v.maNames", dataVal);
        component.set("v.tbController", tbController);
        helper.resetPaginationParam(component);
    },
    resetLookupFilter : function(component, event, helper) {
        helper.resetLookupFilter(component);
    },
    resetCriteria : function(component, event, helper) {
        helper.resetCriteria(component);
    },
    addMailingZipCriteria : function(component, event, helper) {
        let tbController = component.get("v.tbController");
        tbController.MailingZipCondition = {"postalCodeType":"mailing", 'mailingPostalCodeRange':component.get("v.mailingPostalCodeRange"), "mailingPostalCodeRadius":component.get("v.mailingPostalCodeRadius"), "mailingPostalCodeUnit":component.get("v.mailingPostalCodeUnit")};
        tbController.mailingPostalCodeRange = component.get("v.mailingPostalCodeRange");
        tbController.mailingPostalCodeUnit = component.get("v.mailingPostalCodeUnit");
        tbController.mailingPostalCodeRadius = component.get("v.mailingPostalCodeRadius");
        component.set("v.tbController", tbController);
        helper.resetPaginationParam(component);
    },
    addBillingZipCriteria: function(component, event, helper) { 
        let tbController = component.get("v.tbController");
        tbController.BillingZipCondition = {"postalCodeType":"billing", 'postalCodeRange':component.get("v.postalCodeRange"), "postalCodeRadius":component.get("v.postalCodeRadius"), "postalCodeUnit":component.get("v.postalCodeUnit")};
        tbController.postalCodeRange = component.get("v.postalCodeRange");
        tbController.postalCodeUnit = component.get("v.postalCodeUnit");
        tbController.postalCodeRadius = component.get("v.postalCodeRadius");
        component.set("v.tbController", tbController);
        helper.resetPaginationParam(component);
    }, 
    clearZipCodeFilters : function(component, event, helper) {
        helper.resetZipFilters(component);
    },
    addCriteria : function(component, event, helper) {
        let newCondition = component.get("v.fieldConditionsField");
        if(newCondition == null || newCondition == ''){
            helper.showMessage('Warning!!', 'please select field', 'warning');
            return;
        }
        newCondition = component.get("v.fieldConditionsOperator");
        if(newCondition == null || newCondition == ''){
            helper.showMessage('Warning!!', 'please select Operator', 'warning');
            return;
        }
        let data = {}; 
        data.field = component.get("v.fieldConditionsField").split('-')[0];
        data.displayType = component.get("v.fieldConditionsField").split('-')[1];
        data.operator = component.get("v.fieldConditionsOperator");
        data.value = component.get("v.fieldConditionsValue");
        
        let tbController = component.get("v.tbController");
        tbController.fieldConditions.push(data);
        component.set("v.tbController", tbController);
        component.set("v.fieldConditionsField", "");
        component.set("v.fieldConditionsOperator", "");
        component.set("v.fieldConditionsValue", "");
        helper.resetPaginationParam(component);
    },
    saveCurrentFilters:function(component, event, helper) {
        helper.saveFilter(component);
    },
    loadProviderMap:function(component, event, helper) {
        helper.loadProviderMapFilterForTerritory(component);
    },
    filterTypeChange :  function(component, event, helper) {
        component.set("v.lookupFilterComponent", []);
        component.set("v.fieldConditionsField", "");
        component.set("v.SelectedFilterType" , event.getParam("value"));
        let apitype = component.get("v.SelectedFilterType").split(',');
        component.set("v.filtertypeTYPE" , apitype[1]);
        if(component.get("v.filtertypeTYPE")=='Lookup'){
            let apiname = apitype[0] ;
            let namefield = 'Name' ;
            let label = component.get("v.SelectedFilterTypeLabel");
            if( apitype[0].indexOf('Specialit') < 0 ) {
                component.set("v.isSpecialtyFilterType", false);
                helper.loadMultiSelectOption(component,component.get("v.SelectedFilterType"),apiname,namefield, label);   
            } else {
                component.set("v.isSpecialtyFilterType", true);
                helper.fetchObjAttributes(component,component.get("v.SelectedFilterType"));
            }
        }
        if(component.get("v.filtertypeTYPE")=='ObjectAttributes'){
            helper.fetchObjAttributes(component,component.get("v.SelectedFilterType"));
        }
    },
    levelTypeChange :  function(component, event, helper) {
        let apitype = component.get("v.SelectedFilterType").split(',');
        let apiname = apitype[0];
        let namefield = event.getParam("value");
        let label = component.get("v.SelectedFilterTypeLabel");
        let lvlFlds = JSON.parse(JSON.stringify(component.get("v.fields")));
        let selLvlObjArr = lvlFlds.filter(obj => { return obj.value === namefield });
        if( namefield != 'Specialty' ) {
            component.set("v.isSpecialtyLevel", false);
            component.set("v.selAdvSpecDataObj", []);
            helper.loadMultiSelectOption(component, component.get("v.SelectedFilterType"), apiname, namefield, label);
        } else {
            component.set("v.isSpecialtyLevel", true);
            component.set("v.lookupFilterComponent", []);
            component.set("v.showModal", true);
            //helper.loadLookupIndexSearchField(component, event, helper);
        }
        component.set("v.selectedFilterLevel", selLvlObjArr);
    },
    selectedSpecValue : function(component, event, helper) {
        let selRows = JSON.parse(JSON.stringify(event.getParam('selectedRows')));
        component.set("v.showSpecialityPill", true);
        let existingProvSpec = component.get("v.selAdvSpecDataObj");
        if( existingProvSpec.length > 0) {
            for( let j=0; j<existingProvSpec.length; j++) {
                selRows.push(existingProvSpec[j]);
            }
        }
        selRows = selRows.filter((v,i,a)=>a.findIndex(t=>(t.Id === v.Id))===i);
        component.set("v.selAdvSpecDataObj", selRows);
        let selRowsValArr = []; let selRowsLblArr = [];
        if( selRows.length > 0) {
            for( let g=0; g<selRows.length; g++) {
                let filterlbl = selRows[g][component.get("v.EDLnamespace")+'Specialization__c']+'-'+
                    selRows[g][component.get("v.EDLnamespace")+'Classification__c'];
                selRowsLblArr.push(filterlbl);
                let filterVal = selRows[g][component.get("v.EDLnamespace")+'Code__c'];
                selRowsValArr.push(filterVal);
            }
        }
        component.set("v.selectedMultiSelectLabel", selRowsLblArr);
        component.set("v.selectedMultiSelectValues", selRowsValArr);
    },
    selectAll : function(component, event, helper){
        let data = component.get("v.data");
        let filterIds = [];
        let selectedRowstoShow = [];
        let selectedParentIds = [];
        if(data !=null && data.length >0) {
            data.forEach(function(dataRow){
                selectedRowstoShow.push(dataRow.Id);
                if(dataRow['_children'] != undefined && typeof dataRow['_children'] == 'object' && (dataRow['_children'] == '[]' || dataRow['_children'].length > 0)){
                    selectedParentIds.push(dataRow.Id);
                    if(dataRow['_children'] != '[]' && dataRow['_children'].length > 0){
                        dataRow['_children'].forEach(function(children){
                            if(!filterIds.includes(children.Id))
                                filterIds.push(children.Id);
                            if(!selectedRowstoShow.includes(children.Id))
                                selectedRowstoShow.push(children.Id);
                        });
                    }
                    return;
                }
                filterIds.push(dataRow.Id);
                if(filterIds.includes(dataRow.Id) && dataRow['isExcludeExpand'] != undefined && dataRow['isExcludeExpand'] == true){
                    filterIds = filterIds.filter(function(ele){ return ele != dataRow.Id; }); 
                    selectedRowstoShow = selectedRowstoShow.filter(function(ele){ return ele != dataRow.Id; });
                }
                
            });
        }
        component.set("v.selectedRows", filterIds);
        component.set("v.selectedRowstoShow",selectedRowstoShow);
        component.set("v.selectedParentIds",selectedParentIds);
    },
    deselectAll : function(component, event, helper){
        helper.clearSelectedRwAttribute(component);
    },
    handleNext : function(component, event, helper) {
        let pageNumber = component.get("v.pageNumber");
        component.set("v.pageNumber", pageNumber+1);
        component.set("v.hasPageChanged", true);
        helper.searchHelper(component);
    },
    handleFirst : function(component, event, helper) { 
        component.set("v.pageNumber", 1);
        component.set("v.hasPageChanged", true);
        helper.searchHelper(component);
    },
    handleLast : function(component, event, helper) { 
        let totalRecords = component.get("v.totalRecords");
        let pageSize = component.get("v.pageSize");
        component.set("v.hasPageChanged", true);
        if(totalRecords > 0 && pageSize>0) {
            component.set("v.pageNumber", Math.ceil(totalRecords/pageSize));
        }
        helper.searchHelper(component);
    },
    handlePrev : function(component, event, helper) {        
        let pageNumber = component.get("v.pageNumber");
        component.set("v.pageNumber", pageNumber-1);
        component.set("v.hasPageChanged", true);
        helper.searchHelper(component);
    },
    changePageSize : function(component, event, helper) {        
        let pageSize = event.getParam("value");
        component.set("v.pageSize", pageSize);
        component.set("v.pageNumber", 1);
        helper.searchHelper(component);
    },
    handleRowAction :function(component,event,helper){
        let selRows = event.getParam('selectedRows');
        let allSelRows = component.get("v.selectedRows");
        let gridExpandedRows = component.get("v.gridExpandedRows");
        let data = component.get("v.data");
        let alreadySelParentIds = component.get("v.selectedParentIds");
        
        let allSelectedRows = []; let selectedRowstoShow = [];
        let selectedChildIds = component.get("v.selectedChildIds");
        allSelRows.forEach(function(Id) {
            if(!allSelectedRows.includes(Id)) { allSelectedRows.push(Id); }
            if(!selectedRowstoShow.includes(Id)) { selectedRowstoShow.push(Id); }
        });
        let pageSelIds = []; let alreadySelChild = [];
        let expandedParents = []; let selectedParentIds = [];
        selRows.forEach(function(row) {
            pageSelIds.push(row.Id);
            if(!allSelectedRows.includes(row.Id)) { allSelectedRows.push(row.Id); }
            if(!selectedRowstoShow.includes(row.Id)) { selectedRowstoShow.push(row.Id); }
            if(row['level'] != undefined && row['level'] == 2) {
                if(!selectedChildIds.includes(row.Id)) { selectedChildIds.push(row.Id); }
                alreadySelChild.push(row.Id);
            }
            if(row['level'] != undefined && row['level'] == 1 && row['isExpanded'] != undefined && row['isExpanded'] == true) {
                expandedParents.push(row.Id);
                selectedParentIds.push(row.Id);
            }
        });
        
        if(selRows.length > 0){
            data.forEach(function(dataRow) {
                if(!pageSelIds.includes(dataRow.Id)) {
                    allSelectedRows = allSelectedRows.filter(function(ele){ return ele != dataRow.Id; }); 
                    selectedRowstoShow = selectedRowstoShow.filter(function(ele){ return ele != dataRow.Id; });
                }
                if(selectedRowstoShow.includes(dataRow.Id) && dataRow['_children'] != undefined && typeof dataRow['_children'] == 'object') {
                    let skipParent = false;
                    if(dataRow['_children'] != '[]' && dataRow['_children'].length > 0) {
                        dataRow['_children'].forEach(function(children){
                            if(selectedChildIds.includes(children.Id) && !alreadySelChild.includes(children.Id) && expandedParents.includes(dataRow.Id)) {
                                skipParent = true;
                            } else {
                                if(!allSelectedRows.includes(children.Id)) { allSelectedRows.push(children.Id); }
                                if(!selectedRowstoShow.includes(children.Id)) { selectedRowstoShow.push(children.Id); }
                                if(!alreadySelChild.includes(children.Id)) { alreadySelChild.push(children.Id); }
                                pageSelIds.push(children.Id);
                            }
                        });
                    }
                    if(!selectedParentIds.includes(dataRow.Id)) { selectedParentIds.push(dataRow.Id); }
                    allSelectedRows = allSelectedRows.filter(function(ele){ return ele != dataRow.Id; });
                    if(skipParent == true) {
                        selectedRowstoShow = selectedRowstoShow.filter(function(ele){ return ele != dataRow.Id; });
                    }
                } else if(!selectedRowstoShow.includes(dataRow.Id) && dataRow['_children'] != undefined && typeof dataRow['_children'] == 'object' && dataRow['_children'].length > 0) {
                   
                    let totalChildSelected = 0;
                    let parentRemoved = false;
                    if(alreadySelParentIds.includes(dataRow.Id)) {
                        parentRemoved = true;
                        selectedParentIds = selectedParentIds.filter(function(ele){ return ele != dataRow.Id; });
                    }
                    dataRow['_children'].forEach(function(children) {
                        if(parentRemoved == true){
                            allSelectedRows = allSelectedRows.filter(function(ele){ return ele != children.Id; }); 
                            selectedRowstoShow = selectedRowstoShow.filter(function(ele){ return ele != children.Id; });
                            selectedChildIds = selectedChildIds.filter(function(ele){ return ele != children.Id; }); 
                            alreadySelChild = alreadySelChild.filter(function(ele){ return ele != children.Id; }); 
                        } else {
                            if(alreadySelChild.includes(children.Id)){
                                totalChildSelected++;
                            }
                        }
                    });
                    if(totalChildSelected == dataRow['_children'].length) {
                        if(!selectedRowstoShow.includes(dataRow.Id)) {
                            selectedRowstoShow.push(dataRow.Id);
                        }
                        if(!selectedParentIds.includes(dataRow.Id)) {
                            selectedParentIds.push(dataRow.Id);
                        }
                    }
                }
                if(allSelectedRows.includes(dataRow.Id) && dataRow['isExcludeExpand'] != undefined && dataRow['isExcludeExpand'] == true){
                    allSelectedRows = allSelectedRows.filter(function(ele){ return ele != dataRow.Id; }); 
                    selectedRowstoShow = selectedRowstoShow.filter(function(ele){ return ele != dataRow.Id; });
                }
            });
        } else if(allSelRows.length > 0 && !component.get("v.hasPageChanged")){
            data.forEach(function(dataRow) {
                if(allSelRows.includes(dataRow.Id)) {
                    allSelectedRows = allSelectedRows.filter(function(ele){ return ele != dataRow.Id; });
                    selectedRowstoShow = selectedRowstoShow.filter(function(ele){ return ele != dataRow.Id; });
                }
            });
        }
        selectedChildIds.forEach(function(childId) {
            if(!pageSelIds.includes(childId)) {
                allSelectedRows = allSelectedRows.filter(function(ele){ return ele != childId; }); 
                selectedRowstoShow = selectedRowstoShow.filter(function(ele){ return ele != childId; }); 
            }
        });
        if(component.get("v.hasPageChanged") == true){
            component.set("v.hasPageChanged", false);
        }
        component.set("v.selectedRows",allSelectedRows);
        component.set("v.selectedRowstoShow",selectedRowstoShow);
        component.set("v.selectedParentIds",selectedParentIds);
        component.set("v.selectedChildIds",alreadySelChild);
    },
    handleInclude : function(component, event, helper){
        helper.resetPaginationParam(component);
    },
    updateColumnSorting : function(component,event,helper){
        let fieldName = event.getParam('fieldName');
        let sortDirection = event.getParam('sortDirection');
        component.set("v.sortedBy", fieldName);
        component.set("v.sortedDirection", sortDirection);
        helper.searchHelper(component);
    },
    handleRowToggle : function(component,event,helper){
        let rowName = event.getParam('name');
        let row = event.getParam('row');
        let isExpanded = event.getParam('isExpanded');
        let hasChildrenContent = event.getParam('hasChildrenContent');
        if (hasChildrenContent === true) {
            let selectedRowstoShow = component.get("v.selectedRowstoShow");
            component.set("v.selectedRowstoShow",selectedRowstoShow);
        } else {
            // call a method to retrieve the updated data tree that includes the missing children
            if( component.get("v.filterByType") == 'practicegroup' ) {
                if( typeof row._children != 'undefined' ) {
                    component.set('v.isLoading', true);
                    let filterCondition = JSON.parse(row._children);                  
                    helper.retrieveUpdatedData(component, row.Id, rowName, filterCondition[0].queryCondition);
                } else {
                    row.isExpanded = false;
                    component.set('v.isLoading', false);
                }
            }
        }
    },
    handleShowAllRecords : function(component,event,helper) {
        let childIds = component.get("v.childIds");
        if( childIds.length <= component.get("v.childSize") ) {
            component.set("v.childSize", component.get("v.childSize")+100);
            helper.searchHelper(component);
        } else {
            component.set("v.childSize", childIds.length);
        }
    },
    openAdvSearchModal: function(component, event, helper){
        component.set("v.showModal", true);
    },
    closeAdvSearchModal: function(component, event, helper){
        component.set("v.showModal", false);
    },
    saveAdvSearchModal: function(component, event, helper) {
        component.set("v.showModal", false);
    },
    removePill : function( component, event, helper ) {
        let recordId = event.target.name;
        let selectedDataObj = component.get("v.selAdvSpecDataObj");
        const index = selectedDataObj.indexOf(recordId);
        if (recordId > -1) {
            selectedDataObj.splice(recordId, 1);
            component.set('v.selAdvSpecDataObj', selectedDataObj);
        }
        
        if( selectedDataObj.length < 1 ) {
            component.set('v.selAdvSpecDataObj', []);
            component.set("v.selectedMultiSelectValues", []);
            component.set("v.selectedMultiSelectLabel", []);
        }
    },
    removeFilterPill : function(component, event, helper) {
        let pillItemName = event.target.name;
        let pillItemTitle = event.target.title; let fldCondinnerArr;
        let lookupNameObj = JSON.parse(JSON.stringify(component.get("v.lookupNames")));
        let tbController = JSON.parse(JSON.stringify(component.get("v.tbController")));
        for(let fldCond in tbController) {
            if( fldCond == pillItemTitle ) {
                fldCondinnerArr = tbController[fldCond];
            }
        }
        
        // for loop is repeated twice, 
        // because of the structure of the tbController inner objects
        if( pillItemTitle.startsWith('fieldConditions') ) {
            for(let fldCondinnerVal in fldCondinnerArr) {
                if( pillItemName.startsWith(fldCondinnerArr[fldCondinnerVal].value) ) {
                    tbController.fieldConditions.splice(fldCondinnerVal, 1);
                }
            }
        } else if( pillItemTitle.startsWith('lookupObject') ) {
            for(let fldCondinnerVal in fldCondinnerArr) {
                tbController.lookupObject[0].label = fldCondinnerArr[fldCondinnerVal].label.filter(function(item) { return item !== pillItemName });
                tbController.lookupObject[0].value = fldCondinnerArr[fldCondinnerVal].value.filter(function(item) { return item !== pillItemName });
                for( let lkpNameObjVal in lookupNameObj) {
                    if( lookupNameObj[lkpNameObjVal] == pillItemName ) {
                        lookupNameObj.splice(lkpNameObjVal, 1);
                    }
                }
                component.set("v.lookupNames", lookupNameObj);
            }
        }
        component.set("v.tbController", tbController);
    }
})