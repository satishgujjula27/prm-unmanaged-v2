({
    doInitHelper : function(component) { 
        let self = this, doInitArray = new Object();
        component.set("v.spinner",true);
        doInitArray['recordId'] = component.get("v.recordId");
        self.doServerCall(component, 'getFilterDetails', doInitArray, 'doInitHelper', false).then(
            function(doInitHelperdetail) {
                var detail = JSON.parse(doInitHelperdetail);
                component.set("v.operatorSelectOptions", detail.friendlyOperators);
                component.set("v.zipCodeFieldOptions", detail.zipCodeFieldOptions);
                component.set("v.TerritoryName", detail.sobjectName);
                
                if(detail.FieldNames!=null){
                    var columns=[];
                    for(var i=0;i<detail.FieldNames.length;i++){
                        columns.push({label: detail.FieldNames[i], fieldName: detail.FieldNames[i], type: 'text'});
                    }
                    component.set('v.columns', columns);
                    component.set('v.data', detail.listRecord);
                }
                
                let filterByOps = [];
                if(detail.filterByWrapper != undefined && detail.filterByWrapper.length > 1){
                    component.set("v.showSearchBy", true);
                }
                component.set("v.filterByList", detail.filterByWrapper);
                for(var opnum in detail.filterByWrapper) {
                    let o = { label:detail.filterByWrapper[opnum].label, value:detail.filterByWrapper[opnum].devName };
                    filterByOps.push(o);
                    if(detail.filterByWrapper[opnum].isDefault == true) {
                        component.set("v.SelectedFilterBy", detail.filterByWrapper[opnum].devName); 
                        component.set("v.SelectedFilterByLabel", detail.filterByWrapper[opnum].label.replace(/s\b/ig,""));
                        component.set("v.filterByType", detail.filterByWrapper[opnum].filterByType);
                        self.loadFilterTypes(component, detail.filterByWrapper[opnum].filterTypeWrapper);
                    }
                }
                component.set("v.filterbyoptions", filterByOps);
                component.set("v.spinner",false);
            }
        );
    },
    loadFilterTypes : function(component, filterTypeDetail) {
        var self = this; let result = JSON.parse(filterTypeDetail);
        let filterByTypeDetail = result.filterTypeOpt;
        let filterTypeOps = [];
        component.set("v.filterTypeList", filterByTypeDetail);
        self.changeAppliedFilterLabels(component, component.get("v.filterTypeList"));
        for(let opnum in filterByTypeDetail) {
            let o = {label:filterByTypeDetail[opnum].label,value:filterByTypeDetail[opnum].apiName+','+filterByTypeDetail[opnum].fType+','+filterByTypeDetail[opnum].relName};
            filterTypeOps.push(o);
            if(filterByTypeDetail[opnum].isDefault == true) {
                component.set("v.SelectedFilterType", filterByTypeDetail[opnum].apiName+','+filterByTypeDetail[opnum].fType+','+filterByTypeDetail[opnum].relName);
                component.set("v.filtertypeTYPE", filterByTypeDetail[opnum].fType);
                component.set("v.SelectedFilterTypeLabel", filterByTypeDetail[opnum].label);                     
            }
        }
        component.set("v.filtertypeoptions", filterTypeOps);
        if(component.get("v.filtertypeTYPE") == "ObjectAttributes") {
            self.fetchObjAttributes(component,component.get("v.SelectedFilterType"));
        }
        if(component.get("v.filtertypeTYPE") == "Lookup"){
            var namefield = 'Name';
            var apiname = component.get("v.SelectedFilterType").split(',')[0] ;
            var label = component.get("v.SelectedFilterTypeLabel");
            self.loadMultiSelectOption(component,component.get("v.SelectedFilterType"), apiname, namefield, label);
        }
        
        if(result.loadSavedFieldCondition != null) {
            self.loadSavedFieldCondition(component, result.loadSavedFieldCondition);
        }
        
        if(result.loadSavedLookupFilter != null){
            self.loadSavedLookupFilter(component, result.loadSavedLookupFilter);
        }
        
        if(result.loadSavedZipFilter!=null) {
            self.loadSavedZipFilter(component, result.loadSavedZipFilter);
        }
        self.clearSelectedRwAttribute(component);
    },
    fetchFilterTypes : function(component, filterByName) {
        let self = this; let result = JSON.parse(JSON.stringify(component.get("v.filterByList")));
        let filterByTypeDetail;
        for(let fltrWrapperObj in result) {
            var fltrTypeWrapperObj = JSON.parse(result[fltrWrapperObj].filterTypeWrapper);
            if( filterByName == result[fltrWrapperObj].devName ) {
                filterByTypeDetail = fltrTypeWrapperObj.filterTypeOpt;
                
                if(fltrTypeWrapperObj.loadSavedFieldCondition != null) {
                    self.loadSavedFieldCondition(component, fltrTypeWrapperObj.loadSavedFieldCondition);
                }
                if(fltrTypeWrapperObj.loadSavedLookupFilter != null){
                    self.loadSavedLookupFilter(component, fltrTypeWrapperObj.loadSavedLookupFilter);
                }
                if(fltrTypeWrapperObj.loadSavedZipFilter!=null) {
                    self.loadSavedZipFilter(component, fltrTypeWrapperObj.loadSavedZipFilter);
                }
            }
        }
        let filterTypeOps = [];
        component.set("v.filterTypeList", filterByTypeDetail);
        self.changeAppliedFilterLabels(component, component.get("v.filterTypeList"));
        for(let opnum in filterByTypeDetail) {
            let o = {label:filterByTypeDetail[opnum].label,value:filterByTypeDetail[opnum].apiName+','+filterByTypeDetail[opnum].fType+','+filterByTypeDetail[opnum].relName};
            filterTypeOps.push(o);
            if(filterByTypeDetail[opnum].isDefault == true) {
                component.set("v.SelectedFilterType",filterByTypeDetail[opnum].apiName+','+filterByTypeDetail[opnum].fType+','+filterByTypeDetail[opnum].relName);
                component.set("v.filtertypeTYPE", filterByTypeDetail[opnum].fType);
                component.set("v.SelectedFilterTypeLabel", filterByTypeDetail[opnum].label);                     
            }
        }
        component.set("v.filtertypeoptions", filterTypeOps);
        if(component.get("v.filtertypeTYPE") == "ObjectAttributes") {
            self.fetchObjAttributes(component,component.get("v.SelectedFilterType"));
        }
        if(component.get("v.filtertypeTYPE") == "Lookup"){
            var namefield = 'Name';
            var apiname = component.get("v.SelectedFilterType").split(',')[0] ;
            var label = component.get("v.SelectedFilterTypeLabel");
            self.loadMultiSelectOption(component,component.get("v.SelectedFilterType"), apiname, namefield, label);
        }
       
        self.clearSelectedRwAttribute(component);
    },
    fetchObjAttributes : function(component , objType) {
        let self = this; let filtertype = objType.split(','); let fetchObjAttribs = new Object();
        fetchObjAttribs["objName"] = filtertype[0];
        fetchObjAttribs["relName"] = filtertype[2];
        self.doServerCall(component, 'fetchAttributes', fetchObjAttribs, 'fetchObjAttributes', false).then(
            function(fetchObjAttribdetail) {
                let detail = JSON.parse(fetchObjAttribdetail);
                if( filtertype[0].indexOf('Specialit') > 0) {
                    for (var i = 0; i < detail.fields.length; i++) {
                        if( detail.fields[i].label != "--None--" ) {
                            detail.fields[i].label = detail.fields[i].label.split(':')[1].trim();   
                        }
                    }
                    const lastEl = Object.assign({}, detail.fields[1]);
                    lastEl.value = 'Specialty';
                    lastEl.label = 'Specialty';
                    lastEl.displayType = '';
                    detail.fields.splice(1, 0, lastEl);
                }
                component.set("v.fields", detail.fields);
                component.set("v.operatorSelectOptions", detail.friendlyOperators);
            }
        );
    },
    changeAppliedFilterLabels : function(component , ftypList) {
        let fRelatedFTypes = ftypList;
        let lkplabels  = [];
        let attrblabels = [] ;
        for(let i in fRelatedFTypes){
            if(fRelatedFTypes[i].fType == "Lookup" && fRelatedFTypes[i].label != 'Specialities') {
                lkplabels.push(fRelatedFTypes[i].label);
            }
            if(fRelatedFTypes[i].fType == "ObjectAttributes"){
                attrblabels.push(fRelatedFTypes[i].label);
            }
        }
        let lkplbl = lkplabels.join(", ");
        let attlbl = attrblabels.join(", ");
        component.set("v.lookupLabel", lkplbl);
        component.set("v.attributeLabel", attlbl);
    },
    retrieveUpdatedData : function(component, parentId, rowId, queryCondition) {
        let self = this;
        let tbController = component.get("v.tbController");
        let tbCtrlFldConditions = tbController.fieldConditions;
        
        if( queryCondition.indexOf(component.get("v.EDLnamespace")+'Location__r.Name') < 0 ) {
            queryCondition.replace('Name', component.get("v.EDLnamespace")+'Location__r.Name')
        }
        
        let selectedFilterType;
        if(tbController.MailingZipCondition != null && tbController.MailingZipCondition.mailingPostalCodeRange != null ){
            selectedFilterType = 'ObjectAttributes';
        }
        if(tbController.BillingZipCondition != null && tbController.BillingZipCondition.postalCodeRange != null ){
            selectedFilterType = 'ObjectAttributes';
        }
        
        if( tbCtrlFldConditions.length > 0 && tbController.lookupObject.length > 0) {
            selectedFilterType = 'ObjectAttributes,Lookup';
        } else if( tbCtrlFldConditions.length == 0 && tbController.lookupObject.length > 0) {
            selectedFilterType = 'Lookup';
        } else if( tbCtrlFldConditions.length > 0 && tbController.lookupObject.length == 0) {
            selectedFilterType = 'ObjectAttributes';
        }
        
        let filterConditionObj = new Object();
        filterConditionObj["parentId"] = parentId;
        filterConditionObj["isIncludeMyExisting"] = tbController.isIncludeMyExisting;
        filterConditionObj["isIncludeOthersExisting"] = tbController.isIncludeOthersExisting;
        filterConditionObj["filterBy"] = component.get("v.filterByType");
        filterConditionObj["filterByType"] = selectedFilterType;
        filterConditionObj["practiceTpQueryCond"] = queryCondition;
        filterConditionObj["childSize"] = component.get("v.childSize");
        
        self.doServerCall(component, 'getProvLocForPracticeGroup', filterConditionObj, 'searchHelper', false).then(
            function(searchHelperdetail) {
                let detail = JSON.parse(searchHelperdetail);
                if ( Object.keys(detail).length === 0 && detail.constructor === Object ) {
                    self.showMessage("Warning!", 'No provider locations available' , 'warning');
                    component.set('v.isLoading', false);
                } else {
                    let originalData = component.get("v.data");
                    component.set('v.data', self.addChildrenToRow(originalData, rowId, detail));
                    component.set('v.isLoading', false);
                }
            }
        );
    },
    addChildrenToRow: function(data, rowName, children) {
        let newData = JSON.parse(JSON.stringify(data));
        newData.forEach(function(rowVal) {
            if (rowVal.Id === rowName) {
                rowVal._children = children[rowVal.Id];
            }
        });
        return newData;
    },
    searchHelper : function(component) {
        let self = this; let pagesize;
        component.set("v.currentView", "Table");
        component.set("v.spinner",true);
        let tbController = component.get("v.tbController");
        let tbCtrlFldConditions = tbController.fieldConditions;
        let tbCtrlFldOperators = JSON.stringify(tbCtrlFldConditions);
        if(tbCtrlFldOperators.includes('"operator":"c"') || tbCtrlFldOperators.includes('"operator":"n"')) {
            pagesize = "25";
        } else {
            pagesize = component.get("v.pageSize");
        }
        let zipFilters = []; let selectedFilterType;
        if(tbController.MailingZipCondition != null && tbController.MailingZipCondition.mailingPostalCodeRange != null ){
            zipFilters.push(tbController.MailingZipCondition);
            selectedFilterType = (tbController.lookupObject.length > 0) ? 'ObjectAttributes,Lookup' : 'ObjectAttributes';
        }
        if(tbController.BillingZipCondition != null && tbController.BillingZipCondition.postalCodeRange != null ){
            zipFilters.push(tbController.BillingZipCondition);
            selectedFilterType = (tbController.lookupObject.length > 0) ? 'ObjectAttributes,Lookup' : 'ObjectAttributes';
        }
        
        if( tbCtrlFldConditions.length > 0 && tbController.lookupObject.length > 0) {
            selectedFilterType = 'ObjectAttributes,Lookup';
        } else if( tbCtrlFldConditions.length == 0 && tbController.lookupObject.length > 0 && tbController.MailingZipCondition == null && tbController.BillingZipCondition == null ) {
            selectedFilterType = 'Lookup';
        } else if( tbCtrlFldConditions.length > 0 && tbController.lookupObject.length == 0) {
            selectedFilterType = 'ObjectAttributes';
        }

        let basicSrchConditions = new Object();
        basicSrchConditions["isIncludeMyExisting"] = tbController.isIncludeMyExisting;
        basicSrchConditions["isIncludeOthersExisting"] = tbController.isIncludeOthersExisting;
        basicSrchConditions["pageSize"] = pagesize;
        basicSrchConditions["pageNumber"] = component.get("v.pageNumber");
        basicSrchConditions["sortBy"] = component.get("v.sortedBy");
        basicSrchConditions["sortDirection"] = component.get("v.sortedDirection");
        basicSrchConditions["filterBy"] = component.get("v.filterByType");
        basicSrchConditions["childSize"] = component.get("v.childSize");
        basicSrchConditions["filterType"] = selectedFilterType;
        
        let filterConditionObj = new Object();
        filterConditionObj["filterConditions"] = JSON.stringify(basicSrchConditions);
        filterConditionObj["fieldConditions"] = JSON.stringify(tbController.fieldConditions);
        filterConditionObj["zipFilters"] = JSON.stringify(zipFilters);
        filterConditionObj["lookupFilter"] = JSON.stringify(tbController.lookupObject);
        
        self.doServerCall(component, 'getPLData', filterConditionObj, 'searchHelper', false).then(
            function(searchHelperdetail) {
                let detail = JSON.parse(searchHelperdetail);
                if(detail.fieldsWrapper != null){
                    let columns = [];
                    var actions = [{ label: 'Show details', name: 'show_details' }];
                    for(let key in detail.fieldsWrapper) {
                        columns.push({label: detail.fieldsWrapper[key].label, fieldName: detail.fieldsWrapper[key].fieldNameLink, type: detail.fieldsWrapper[key].fieldType,typeAttributes: {
                            label: { fieldName: detail.fieldsWrapper[key].fieldName }
                        }, });
                    }
                    component.set('v.columns', columns);
                    component.set('v.totalRecords',detail.totalRec);
                    self.setPaginationParams(component,detail.listPLWRecord);
                    component.set('v.data', detail.listPLWRecord);
                    if(component.get("v.currentView") == 'Map') {
                        self.loadProviderMapFilterForTerritory(component);
                    }
                }
                component.set("v.selectedRowstoShow",component.get("v.selectedRowstoShow"));
                component.set("v.spinner", false);
            }
        );
    },
    saveFilter : function(component) {
        let self = this;
        component.set("v.spinner", true);
        let tbController = component.get("v.tbController");
        let zipFilters = [];
        if(tbController.MailingZipCondition != null && tbController.MailingZipCondition.mailingPostalCodeRange != null ){
            zipFilters.push(tbController.MailingZipCondition);
        }
        if(tbController.BillingZipCondition != null && tbController.BillingZipCondition.postalCodeRange != null ){
            zipFilters.push(tbController.BillingZipCondition);
        }
        
        let saveFilterObj = new Object();
        saveFilterObj["recordId"] = component.get("v.recordId");
        saveFilterObj["fieldConditions"] = JSON.stringify(tbController.fieldConditions);
        saveFilterObj["marketArea"] = JSON.stringify(tbController.marketArea);
        saveFilterObj["zipFilters"] = JSON.stringify(zipFilters);
        saveFilterObj["lookupFilter"] = JSON.stringify(tbController.lookupObject);
        saveFilterObj["filterByType"] = component.get("v.filterByType");
        self.doServerCall(component, 'saveFilterDetail', saveFilterObj, 'saveFilter', false).then(
            function(saveFilterdetail) {
                component.set("v.spinner",false);
                let message = "Applied "+component.get("v.SelectedFilterByLabel")+" Filters have been stored successfully to Territory";
                self.showMessage("Success!", message , 'success');
            }
        );
    },
    loadMultiSelectOption : function(component, type, apiName, nameField, label){
        $A.createComponent(
            "c:MultiSelect", { "objectName" : apiName, "fieldName" : nameField, "labelAll" : label},
            function(lMap, status, errorMessage) {
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    let body = component.get("v.lookupFilterComponent");
                    body = lMap;
                    component.set("v.lookupFilterComponent", body);
                } else if (status === "INCOMPLETE") {
                    console.log("Could not load MultiSelect component as there was no response from server.")
                    // Show offline error
                } else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },
    loadLookupIndexSearchField : function(component, event, helper){
		let lookupCompObj = new Object();
        lookupCompObj["objectName"] = component.get("v.EDLnamespace")+'Speciality__c';
        lookupCompObj["fieldAPIText"] = component.get("v.EDLnamespace")+'Grouping__c';
        lookupCompObj["fieldAPIVal"] = 'Id'; lookupCompObj["limit"] = '4';
        lookupCompObj["fieldAPISearch"] = component.get("v.EDLnamespace")+'Grouping__c';
        lookupCompObj["lookupIcon"] = 'custom:custom94'; lookupCompObj["objectLabel"] = '';
        lookupCompObj["selItem"] = ''; lookupCompObj["placeholder"] = 'Add Specialties...';
        lookupCompObj["lookupFieldName"] = component.get("v.EDLnamespace")+'Grouping__c'; 
        lookupCompObj["isNewCreationEnable"] = false; lookupCompObj["isDisable"] = false;
        lookupCompObj["isSpecialityLookup"] = true; lookupCompObj["labelAll"] = 'Specialties';
        $A.createComponent(
            "c:LookupComponent", lookupCompObj,
            function(lMap, status, errorMessage) {
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    let body = component.get("v.lookupFilterComponent");
                    body = lMap;
                    component.set("v.lookupFilterComponent", body);
                } else if (status === "INCOMPLETE") {
                    console.log("Could not load LookupComponent[loadLookupIndexSearchField] as there was no response from server.")
                    // Show offline error
                } else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },
    createMap : function(component , map){
        let emap = []; for(let key in map) { emap.push({ key : key, value : map[key]}); } return emap;
    },
    loadProviderMapFilterForTerritory : function(component) {
        let data = component.get("v.data");
        let filterIds = [];
        let childIds = component.get("v.childIds");
        if(childIds != null && childIds.length > 0){
            childIds.forEach(function(child){
                filterIds.push(child);
            });
        } else if(data !=null && data.length >0) {
            for(let i in data) { filterIds.push(data[i].Id); }
        }
        //component.set("v.selectedRows", filterIds);
        $A.createComponent(
            "c:ProviderMapFilterForTerritory", { "recordId" : component.get("v.recordId"),"filterByType":component.get("v.SelectedFilterBy"),"selectedIds":component.get("v.selectedRows"),"filterIds":filterIds },
            function(lMap, status, errorMessage) {
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    let body = component.get("v.TerritoryMap");
                    body = lMap;
                    component.set("v.TerritoryMap", body);
                    component.set("v.mapLoaded",true);
                } else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                } else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },
    addSelectedHelper : function(component, event, helper) {
        let self = this; component.set("v.spinner", true);
        if(component.find('availablePT') === undefined){
            component.set("v.currentView", "Table");
            component.set("v.spinner", false);
        } else {
            let selectedRows = component.get('v.selectedRows');
            let selectedParentIds = component.get('v.selectedParentIds');
            if(selectedRows.length > 0 || selectedParentIds.length > 0) {
                let addSelectedHelperObj = new Object();
                addSelectedHelperObj["selectedData"] = JSON.stringify(selectedRows);
                addSelectedHelperObj["territoryId"] = component.get("v.recordId");
                addSelectedHelperObj["selectedParentIds"] = component.get("v.selectedParentIds");
                self.doServerCall(component, 'addSelectedRecords', addSelectedHelperObj, 'addSelectedHelper', false).then(
                    function(addSelHelperdetail) {
                        component.set("v.spinner", false);
                        let resp = JSON.parse(addSelHelperdetail);
                        if(resp !=null) {
                            if(resp.status == true) {
                                let message = resp.totalRecAdded+" items were successfully added to "+component.get("v.TerritoryName");
                                if(resp.totalDuplicate >0) {
                                    message += " ("+resp.totalDuplicate+" duplicate item skipped.)";
                                }
                                self.showMessage("Success!",message , 'success');
                            } else {
                                self.showMessage("Error!", resp.msg, 'error');
                            }
                        }
                    }
                );
            } else {
                self.showMessage("No Record Selected!", 'First Select some record!', 'warning');
                component.set("v.spinner",false);
            }
        }
    },
    setPaginationParams : function(component,resultData){
        if(resultData.length < parseInt(component.get("v.pageSize")) || parseInt(component.get("v.pageSize")) == component.get('v.totalRecords') ) {
            component.set("v.isLastPage", true);
        } else {
            component.set("v.isLastPage", false);
        }
        component.set("v.dataSize", resultData.length);
        if(component.get("v.pageNumber") >0  && component.get("v.pageSize") >0) {
            let start = ((component.get("v.pageNumber") * component.get("v.pageSize"))- component.get("v.pageSize"))+1;
            let end = '';
            if(component.get("v.pageSize") > component.get("v.totalRecords")) {
                end = component.get("v.totalRecords");   
            } else {
                end = component.get("v.pageNumber") * component.get("v.pageSize");
                if(end > component.get("v.totalRecords")) {
                    end = start + component.get("v.dataSize") -1;
                }
            }
            if(parseInt(component.get("v.totalRecords")) == 0) {
                start = 0;
            }
            component.set("v.showRecCountText",start+" - "+end+" of "+component.get("v.totalRecords"));
        }
    },
    resetPaginationParam : function(component) {
        let pageSize = (component.get("v.filterByType") == 'practicegroup') ? '50' : '25';
        component.set("v.pageSize", pageSize);
        component.set("v.pageNumber", 1);
        component.set("v.childSize",100);
        this.trackFilterChange(component);
    },
    doServerCall : function(component, apexAction, params, callFromMethod, pushToEvent) {
        let self = this;
        try {
            return new Promise(function(resolve, reject) {
                let action = component.get("c."+apexAction+"");
                if( params != null || typeof params != "undefined" ) {
                    action.setParams( params );
                }
                action.setCallback(this, function(response) {
                    let state = response.getState();
                    if (state === "SUCCESS" && response.getReturnValue() != null) {
                        resolve( response.getReturnValue() );
                    } else {
                        let errors = response.getError();
                        let message = 'Unknown error'; // Default error message
                        // Retrieve the error message sent by the server
                        if (errors && Array.isArray(errors) && errors.length > 0) {
                            message = errors[0].message;
                        }
                        // Display the message                        
                        self.showMessage("Error!", message , 'error');
                        component.set("v.spinner", false);
                    }
                });
                $A.enqueueAction(action);
            });
        } catch(e) {
            console.log("Failed at enqueueAction - "+callFromMethod);
        }
    },
    showMessage : function(messageTitle,messageBody,messageType){
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": messageTitle, "type":messageType, "message": messageBody
        });
        toastEvent.fire();
    },
    resetLookupFilter : function(component) {
        let self = this;
        let tbController = component.get("v.tbController");
        tbController.lookupObject = [];
        let dataVal = [];
        component.set("v.lookupNames", dataVal);
        component.set("v.tbController", tbController);
        component.set("v.lookupLabel", 'Area Of Expertise, Education, Credentials, Practices');
        self.resetPaginationParam(component);
    },
    resetCriteria : function(component){
        let self = this;
        let tbController = component.get("v.tbController");
        tbController.fieldConditions = [];
        component.set("v.tbController", tbController);
        self.resetPaginationParam(component);
    },
    resetZipFilters : function(component) {
        let self = this;
        let tbController = component.get("v.tbController");
        tbController.MailingZipCondition = {};
        tbController.BillingZipCondition = {};
        tbController.mailingPostalCodeRange = '';
        tbController.mailingPostalCodeUnit = '';
        tbController.mailingPostalCodeRadius = '';
        tbController.postalCodeRange = '';
        tbController.postalCodeUnit = '';
        tbController.postalCodeRadius = '';
        component.set("v.tbController", tbController);
        self.resetPaginationParam(component);
    },
    loadSavedFieldCondition : function(component, loadSavedFieldCondition){
        let tbController = component.get("v.tbController");
        tbController.fieldConditions = loadSavedFieldCondition;
        component.set("v.tbController", tbController);
    },
    loadSavedLookupFilter : function(component, loadSavedLookupFilter){
        let tbController = component.get("v.tbController");
        try{
            tbController.lookupObject = loadSavedLookupFilter;
            component.set("v.tbController", tbController);
        } catch(e) { }
        let dataVal=[];
        for(let i=0;i<tbController.lookupObject.length;i++) {
            for(let j=0;j<tbController.lookupObject[i].label.length;j++) {
                dataVal.push(tbController.lookupObject[i].label[j]);
            }
        }
        component.set("v.lookupNames", dataVal);
    },
    loadSavedZipFilter : function(component, zipFilter){
        try{
            var tbController = component.get("v.tbController");
            for(let i=0;i<zipFilter.length;i++) {
                if(zipFilter[i].postalCodeType == "mailing") {
                    tbController.MailingZipCondition = zipFilter[i];
                    tbController.mailingPostalCodeRange = zipFilter[i].mailingPostalCodeRange;
                    tbController.mailingPostalCodeUnit = zipFilter[i].mailingPostalCodeUnit;
                    tbController.mailingPostalCodeRadius = zipFilter[i].mailingPostalCodeRadius;
                    
                }
                if(zipFilter[i].postalCodeType == "billing") {
                    tbController.BillingZipCondition = zipFilter[i];
                    tbController.postalCodeRange = zipFilter[i].postalCodeRange;
                    tbController.postalCodeUnit = zipFilter[i].postalCodeUnit;
                    tbController.postalCodeRadius = zipFilter[i].postalCodeRadius;
                }
            }
        } catch(e) { }
        component.set("v.tbController", tbController);
    },
    clearSelectedRwAttribute : function(component){
        component.set("v.selectedRows", []);
        component.set("v.selectedRowstoShow",[]);
        component.set("v.selectedParentIds",[]);
        component.set("v.selectedChildIds",[]);
    },
    trackFilterChange : function(component){
        if(component.get("v.isFilterChanged") == false){
            component.set("v.isFilterChanged", true);
        }
    },
    resetDatatableAttribute : function(component){
        component.set("v.selectedRows",[]);
        component.set("v.selectedRowstoShow",[]);
        component.set("v.selectedParentIds",[]);
        component.set("v.selectedChildIds",[]);
        component.set("v.childIds",[]);
        component.set("v.isFilterChanged", false);
    }
})