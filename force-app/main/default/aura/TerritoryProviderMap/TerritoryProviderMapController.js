({
    doInit : function(component, event, helper) {
        let action = component.get("c.getPLTRecord");
        let recordId = component.get("v.recordId");
        let map = component.get("v.map");
        let starterZoomLevel = 5;
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                let providers = JSON.parse(a.getReturnValue());
                let device = $A.get("$Browser.formFactor");
                let markerFields = [];
                markerFields.push("Id");
                markerFields.push("providerName");
                markerFields.push("Latitude");
                markerFields.push("providerLink");
                markerFields.push("locationName");
                markerFields.push("Longitude");
                markerFields.push("locationAddr");
                markerFields.push("locationCityState");
                markerFields.push("practiceGroupName");
                
                let markerTemplate = '<b><a href="/' + 'providerLink" target="_blank">' + 'providerName </a></b>'+ 
                    					'<br/>practiceGroupName'+
                						'<br/>locationAddr'+','+
                						'<br/>locationCityState';
                
                component.set("v.recordString", JSON.stringify(providers));
                component.set("v.markerFields", markerFields);
                component.set("v.markerTemplate", markerTemplate);
                
                if (device === "DESKTOP") {
                    $A.createComponent(
                        "c:LeafletMap", {
                            "recordString" : JSON.stringify(providers),
                            "latField" : "Latitude", "lngField" : "Longitude",
                            "showSearch" : false, "showDraw" : false,
                            "recordId" : recordId, 
                            "geoJsonField" : component.get("v.BasePkgNamespace")+"GeoJSON__c",
                            "useCluster" : true, "markerFields" : markerFields,
                            "markerTemplate" : markerTemplate, 
                            "showMarkerOnPopup": true, "latitudeVarName": "Latitude", "longitudeVarName": "Longitude",
                        },
                        function(lMap, status, errorMessage){
                            //Add the new button to the body array
                            if (status === "SUCCESS") {
                                let body = component.get("v.body");
                                body.push(lMap);
                                component.set("v.body", body);
                            } else if (status === "INCOMPLETE") {
                                console.log("No response from server or client is offline.")
                                // Show offline error
                            } else if (status === "ERROR") {
                                console.log("Error: " + errorMessage);
                                // Show error message
                            }
                        }
                    );
                } else {
                    // set loadMapInIpad attribute to true, which will render the map in phone/ipads
                    component.set("v.loadMapInIpad", true);
                }
            }
        });
        $A.enqueueAction(action);
    }
})