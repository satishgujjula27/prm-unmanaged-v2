({
	doInit : function(component, event, helper) {
		var record = component.get('v.record');
        var FieldName = component.get('v.fieldName');
        component.set("v.fieldValue",record[FieldName]);
        if(component.get("v.ordering") == 0){
            component.set("v.linkId",record[FieldName+'link']);
        }
	},
    viewRecord: function (component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        var recId = event.getSource().get( "v.value" );
        recId = recId.replace('/','');
        navEvt.setParams({
            "recordId": recId
        });
        navEvt.fire();
    },
    handleMenu: function(component, event, helper) {
        let recordId = event.getSource().get("v.class");
        var menuValue = event.detail.menuItem.get("v.value");
        switch(menuValue) {
            case "edit": helper.relatedListDetailEvent(component, 'edit',recordId); break;
            case "delete": helper.relatedListDetailEvent(component, 'delete',recordId); break;
        }
    },
})