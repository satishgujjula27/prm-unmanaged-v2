({
    relatedListDetailEvent : function(component, actionType, recordId){
        var relListDetEvent = component.getEvent("relatedListEvent");
        relListDetEvent.setParams({
            "actionType" :  actionType,
            "recordId" :  recordId
        });
        relListDetEvent.fire();
    }
})