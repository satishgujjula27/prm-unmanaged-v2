({
    onPageReferenceChange: function(component, event, helper) {
        var myPageRef = component.get("v.pageReference");
        var provRecordId = myPageRef.state.c__recordId;
        var relObjName = myPageRef.state.c__relObj;
        component.set("v.recordId", provRecordId);
        component.set("v.relObjName", relObjName);
        helper.doInitHelper(component);
        helper.loadLookupIndexSearchField(component, event, helper);
        component.set("v.providerSpecValues", "[]");
        component.set("v.providerSpecAdvModalData", "[]");
    },

	doInit : function(component, event, helper) {
        var pageReference = component.get("v.pageReference");
        let device = $A.get("$Browser.formFactor");
        if(pageReference != null && pageReference.state != null) {
            component.set("v.recordId", pageReference.state.c__recordId);
            component.set("v.relObjName", pageReference.state.c__relObj);
        }
        helper.doInitHelper(component);
        helper.loadLookupIndexSearchField(component, event, helper);
        component.set("v.providerSpecValues", "[]");
        component.set("v.providerSpecAdvModalData", "[]");
        if( device == "PHONE" ) {
            component.set("v.isMobileDevice", true);
        } else {
            component.set("v.isMobileDevice", false);
        }
	},

    handleLookupCmpEvent: function (component, event, helper) {
        let selectedObj = event.getParam("selItem");
        let selecteObjArr = [];
        let existingProvSpec = component.get("v.providerSpecValues");
        if( selectedObj != null ) {
            let lookupFieldName = event.getParam("lookupFieldName");
            let selVal = '';
            if (selectedObj != null) {
                selVal = selectedObj.val;
            }
            let fieldVals = component.get("v.fieldVals");
            if(fieldVals[lookupFieldName] !== undefined ) {
                if( lookupFieldName.indexOf(component.get("v.EDLnamespace")+'Grouping__c') >= 0 ) {
                    fieldVals[component.get("v.EDLnamespace")+'Speciality__c'] = selVal;
                } else {
                    fieldVals[lookupFieldName] = selVal;
                }
            }
            component.set("v.fieldVals", fieldVals);
            if( component.get("v.relObjName") == component.get("v.EDLnamespace")+'ProviderSpecialties__c' ) { 
                selecteObjArr.push(JSON.parse(JSON.stringify(fieldVals)));
                if( existingProvSpec.length > 0) {
                    for( let j=0; j<existingProvSpec.length; j++){
                        selecteObjArr.push(existingProvSpec[j]);
                    }
                }
                component.set("v.providerSpecValues", selecteObjArr);
                helper.addSelectedRecdsToTable(component, selectedObj.subtext2, selectedObj.subtext1, selectedObj.text, selectedObj.val);
            }
        }
        component.set("v.isprovSpecDataUpdated", true);
    },

    selectedSpecValue : function(component, event, helper) {
        let selectedRowsArr = []; let existingProvSpec = JSON.parse(JSON.stringify(component.get("v.providerSpecValues")));
        let eventSelRows = JSON.parse(JSON.stringify(event.getParam('selectedRows')));
        if( eventSelRows.length > 0) {
            // Then the user selected more than 1 
            // without grouping or classification drop-down change
            // and also by using type ahead search.
            selectedRowsArr = eventSelRows;
            // the below attribute is iterated because the user can change the grouping.
            // or he can type ahead and perform search
            if( existingProvSpec.length > 0 ) {
                for( let epval=0; epval<existingProvSpec.length; epval++) {
                    selectedRowsArr.push(existingProvSpec[epval]);
                }
            }
        } else if( eventSelRows.length == 0 ) {
            if( existingProvSpec.length > 0 ) {
                for( let epval=0; epval<existingProvSpec.length; epval++) {
                    selectedRowsArr.push(existingProvSpec[epval]);
                }
            }
        }
        var uniqueSelectedRowsArr = selectedRowsArr.reduce((unique, o) => {
            if(!unique.some(obj => obj.Id === o.Id)) { unique.push(o); } return unique; 
		},[]);
        selectedRowsArr = uniqueSelectedRowsArr;
        component.set("v.providerSpecValues", selectedRowsArr);
        component.set("v.providerSpecAdvModalData", selectedRowsArr);
    },

    saveAdvSearchModal : function(component, event, helper) {
        let selAdvModalProvSpecData = JSON.parse(JSON.stringify(component.get("v.providerSpecAdvModalData")));
        selAdvModalProvSpecData.pop();
        component.set("v.showModal", false);
        for( let k=0; k<selAdvModalProvSpecData.length; k++) {
            if( selAdvModalProvSpecData[k].length != 0 ) {
                let checkDuplicatesAndAdd = helper.addSelectedRecdsToTable(component, selAdvModalProvSpecData[k][component.get("v.EDLnamespace")+'Classification__c'], 
                                               selAdvModalProvSpecData[k][component.get("v.EDLnamespace")+'Grouping__c'], 
                                               selAdvModalProvSpecData[k][component.get("v.EDLnamespace")+'Specialization__c'], 
                                               selAdvModalProvSpecData[k]['Id']);
                if( checkDuplicatesAndAdd ) {
                    helper.showMessage("Warning!", 'Duplicate found, selected speciality is already added', 'warning');
                }
            }
        }
    },

    saveRecord : function(component, event, helper){
        helper.handleSaveRecord(component);
    },

    handleSubmit : function(component, event, helper) {
        event.preventDefault();
        let fields = event.getParam('fields');

        component.set("v.spinner", true);
        let nameFieldText = component.get("c.getObjNameFieldText");
        nameFieldText.setParams({
            relObjName : component.get("v.relObjName"),
            name : component.get("v.name")
        });
        nameFieldText.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let result = response.getReturnValue();
                if (result != null) {
                    fields.Name = result;
                    component.find('recordEditForm').submit(fields);
                }
            } else {
                component.set("v.spinner", false);
                console.log("Failed to load data from server");
            }
        });
        $A.enqueueAction(nameFieldText);
    },

    handleSuccess : function(component, event, helper) {
        component.set("v.spinner", false);
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": 'Success!', "type": 'success',
            "message": 'The record has been created/updated successfully.'
        });
        toastEvent.fire();
        window.setTimeout(function(){
            helper.navigateToProviderPage(component);
        }, 1000);
    },

    handleBack : function(component, event, helper){
        helper.navigateToProviderPage(component);
    },

    openAdvSearchModal: function(component, event, helper){
        component.set("v.showModal", true);
    },

    closeAdvSearchModal: function(component, event, helper){
        component.set("v.showModal",false);
    },

    handleBoardCertificationClick : function(component, event, helper) {
        var source = event.getSource(); 
        var tabIndex = source.get("v.tabindex");
        var data = component.get("v.providerSpecData");
        var nameSpace = component.get("v.EDLnamespace");

        data = data.map(function(rowData) {
            if (tabIndex === rowData.Id) {
                var boardCertfieldName = nameSpace + 'BoardCertificationStatus__c';
                rowData[boardCertfieldName] = (rowData[boardCertfieldName] == true) ? false : true;
                rowData['isModified'] = true;
            }
            return rowData;
        });
        component.set("v.providerSpecData", data);
    },

    handlePrimaryClick : function(component, event, helper) {
        var source = event.getSource(); 
        var tabIndex = source.get("v.tabindex");
        var data = component.get("v.providerSpecData");
        var nameSpace = component.get("v.EDLnamespace");
        var primaryfieldName = nameSpace + 'IsPrimary__c';

        data = data.map(function(rowData) {
            if (tabIndex === rowData.Id) {
                rowData[primaryfieldName] = (rowData[primaryfieldName] == true) ? false : true;
                rowData['isModified'] = true;
            } else if (tabIndex != rowData.Id && rowData[primaryfieldName]) {
                rowData[primaryfieldName] = false;
                rowData['isModified'] = true;
            }
            return rowData;
        });
        component.set("v.providerSpecData", data);
    },

    handleDeleteRowAction : function(component, event, helper) {
        var source = event.getSource();
        var tabIndex = source.get("v.tabindex");
        var data = component.get("v.providerSpecData");
        var nameSpace = component.get("v.EDLnamespace");
        var isPrimary;
        var isNewSpecialityAdded = false;

		data = data.map(function(rowData) {
        	if (tabIndex === rowData.Id) {
                isNewSpecialityAdded = rowData['NewSpecialityAdded'] === undefined ? false : true;
            	var primaryfieldName = nameSpace + 'IsPrimary__c';
                isPrimary = rowData[primaryfieldName];
            }
            return rowData;
        });
        
        if (isNewSpecialityAdded) {
            data = data.filter(item => (item.Id != tabIndex));
            component.set("v.providerSpecData", data);
        } else {
           helper.handleDeleteRecord(component, tabIndex, isPrimary);
        }
    }
})