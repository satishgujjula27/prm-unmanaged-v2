({
	doInitHelper : function(component) { 
        var self = this; component.set("v.spinner",true);
        var getObjFields = component.get("c.getObjFields");
        getObjFields.setParams({
            recordId : component.get("v.recordId"),
            relObjName : component.get("v.relObjName")
        });
        getObjFields.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = JSON.parse(response.getReturnValue());
                let columnsArr = [];
                
                if (result !=null) {
                    if (result.recName != null) {
                        component.set("v.recName", result.recName);
                    }
                    if (result.fldWrapper != null) {
                        component.set("v.objFields", result.fldWrapper);
                        let fieldValMap = {};
                        for (let i in result.fldWrapper) {
                            let columnsObj = new Object();
                            if (result.fldWrapper[i].label.indexOf('Speciality') == 0 && result.fldWrapper[i].fieldType.toLowerCase() == 'reference') {
                                columnsObj["label"] = 'Taxonomy Code';
                                columnsObj["name"] = 'TaxonomyCode';
                            } else {
                                columnsObj["label"] = result.fldWrapper[i].label;
                                columnsObj["name"] = result.fldWrapper[i].name;
                            }

                            if (result.fldWrapper[i].name.includes('BoardCertificationStatus') || result.fldWrapper[i].name.includes('IsPrimary')) {
                                columnsObj["label"] = "";
                                columnsObj["name"] = result.fldWrapper[i].name;
                            }
							
                            columnsObj["columnOrder"] = result.fldWrapper[i].columnOrder;
                            columnsArr.push(columnsObj);
                            fieldValMap[result.fldWrapper[i].name] = result.fldWrapper[i].value;
                        }

                        columnsArr.sort((a, b) => (a.columnOrder > b.columnOrder) ? 1 : -1);
                        component.set("v.fieldVals", fieldValMap);
                        component.set('v.providerSpecColumns', columnsArr);
                        var fieldVals = component.get("v.fieldVals");
                    }

                    if (result.title != null) {
                        component.set("v.title", result.title);
                    }

                    if (result.name != null) {
                        component.set("v.name", result.name);
                    }

                    if (result.fieldValues != null) {
                        result.fieldValues.sort((a, b) => (a[component.get("v.EDLnamespace")+'IsPrimary__c'] < b[component.get("v.EDLnamespace")+'IsPrimary__c']) ? 1 : -1);
                        component.set("v.providerSpecData", result.fieldValues);
                    }
                }
            } else {
                console.log("Failed to load data from server");
            }
            component.set("v.spinner",false);
        });
        $A.enqueueAction(getObjFields);
    },

    loadLookupIndexSearchField : function(component, event, helper){
		let lookupCompObj = new Object();
        lookupCompObj["objectName"] = component.get("v.EDLnamespace")+'Speciality__c';
        lookupCompObj["fieldAPIText"] = component.get("v.EDLnamespace")+'Grouping__c';
        lookupCompObj["fieldAPIVal"] = 'Id'; lookupCompObj["limit"] = '10';
        lookupCompObj["fieldAPISearch"] = component.get("v.EDLnamespace")+'Grouping__c';
        lookupCompObj["lookupIcon"] = 'custom:custom94'; lookupCompObj["objectLabel"] = '';
        lookupCompObj["selItem"] = ''; lookupCompObj["placeholder"] = 'Search Specialties...';
        lookupCompObj["lookupFieldName"] = component.get("v.EDLnamespace")+'Grouping__c'; 
        lookupCompObj["isNewCreationEnable"] = false; lookupCompObj["isDisable"] = false;
        $A.createComponent(
            "c:LookupComponent", lookupCompObj,
            function(lMap, status, errorMessage) {
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    let body = component.get("v.provSpecLkpFilterComponent");
                    body = lMap;
                    component.set("v.provSpecLkpFilterComponent", body);
                } else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                } else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },

    handleSaveRecord : function(component) {
        let self = this; let existingProvSpec;
        let fieldVals = component.get("v.fieldVals");
        let parsedFldVals = JSON.parse(JSON.stringify(fieldVals));
        if( component.get("v.relObjName") == component.get("v.EDLnamespace")+'ProviderSpecialties__c') {
            fieldVals[component.get("v.EDLnamespace")+"Provider__c"] = component.get("v.recordId");
            existingProvSpec = component.get("v.providerSpecData");
            for(let k=0; k<existingProvSpec.length; k++) {
                existingProvSpec[k][component.get("v.EDLnamespace")+"Provider__c"] = component.get("v.recordId");
            }
        }

        component.set("v.spinner",true);
        let retObjFields = JSON.parse(JSON.stringify(component.get("v.objFields")));
        let fldName; let fldLabel; let fldisRequired; let validationCheck;
        for( let objFld = 0; objFld < retObjFields.length; objFld++ ) {
            fldName = retObjFields[objFld].name;
            fldisRequired = retObjFields[objFld].isRequired;
            if( fldisRequired) {
                fldLabel = retObjFields[objFld].label;
                if( parsedFldVals[fldName].length == 0 ) {
                    validationCheck = true;
                } else {
                    validationCheck = false;
                }
            }
        }
        if( validationCheck ) {
            self.showMessage("Error!", fldLabel+' is required field', 'error');
            component.set("v.spinner", false);
            return false;
        } else {
            var saveRecords = component.get("c.saveRecords");
            saveRecords.setParams({
                relObjName : component.get("v.relObjName"),
                fieldVals : fieldVals,
                provList: JSON.stringify(existingProvSpec),
                name : component.get("v.name")
            });
            saveRecords.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = JSON.parse(response.getReturnValue());
                    if(result != null) {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": 'Success!', "type": 'success',
                            "message": 'The record has been created/updated successfully.'
                        });
                        toastEvent.fire();
                        window.setTimeout(function(){
                            self.navigateToProviderPage(component);
                        }, 3000);
                    }
                } else {
                    console.log("Failed to load data from server");
                }
                component.set("v.spinner",false);
            });
            $A.enqueueAction(saveRecords);
        }
    },

    handleDeleteRecord : function(component, thisProvSpecId, thisProvSpecIsPrimary){
        component.set("v.spinner",true);
        var deleteRecords = component.get("c.deleteRecords");
        deleteRecords.setParams({
            relObjName : component.get("v.relObjName"),
            childRecordId : thisProvSpecId,
            parentRecId : component.get("v.recordId"),
            childRecIsPrimary : thisProvSpecIsPrimary
        });
        deleteRecords.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.split(',')[0] == 'success'){
                    component.set("v.spinner", false);
                    var provSpecData = component.get("v.providerSpecData");
                    provSpecData = provSpecData.filter(item => (item.Id != thisProvSpecId));
                    component.set("v.providerSpecData", provSpecData);
                }
            } else {
                console.log("Failed to load data from server");
            }
            component.set("v.spinner",false);
        });
        $A.enqueueAction(deleteRecords);
    },

    addSelectedRecdsToTable : function(component, column1Val, column2Val, column3Val, column4Val) {
        let existingDTableData = JSON.parse(JSON.stringify(component.get("v.providerSpecData")));
        let duplicateFound;
        for( let existVal=0; existVal<existingDTableData.length; existVal++) {
            if( (component.get("v.EDLnamespace")+'Speciality__r' in existingDTableData[existVal]) ) {
                if( existingDTableData[existVal][component.get("v.EDLnamespace")+'Speciality__r'].Id == column4Val ) {
                    duplicateFound = true;
                }   
            }
        }
        if( !duplicateFound ) {
            let newDtableRow = new Object();
            newDtableRow[component.get("v.EDLnamespace")+'Classification__c'] = column1Val;
            newDtableRow[component.get("v.EDLnamespace")+'Grouping__c'] = column2Val;
            newDtableRow[component.get("v.EDLnamespace")+'Specialization__c'] = column3Val;
            newDtableRow['Id'] = column4Val;
            newDtableRow['NewSpecialityAdded'] = true;
            existingDTableData.unshift(newDtableRow);
            var uniqueExistingDTableData = existingDTableData.reduce((unique, o) => { 
                if(!unique.some(obj => obj.Id === o.Id)) { unique.push(o); } return unique; 
                                                                     },[]);
            component.set("v.providerSpecData", uniqueExistingDTableData);
            duplicateFound = false;
        }
        return duplicateFound;
    },

    showMessage : function(messageTitle,messageBody,messageType){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ "title": messageTitle, "type":messageType, "message": messageBody });
        toastEvent.fire();
    },
    
    navigateToProviderPage : function(component){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({ "recordId": component.get("v.recordId"), "slideDevName": "Detail" });
        navEvt.fire();
    }
})