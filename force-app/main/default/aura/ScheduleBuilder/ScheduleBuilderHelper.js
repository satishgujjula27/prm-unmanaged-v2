({
    quickSearch: function (component, event, helper) {
        helper.getProvider(component, event, component.find("providerSearchBox").get("v.value"), false);
    },
    validateEvents: function (component, ewList) {
        //validate event fields
        var val = { isVal: false, errmessage: '', valevlist: [] };
        var inval = { strtgrt: false, invalstrt: false, invalend: false, invalsub: false, invaldur: false };
        ewList.forEach(function (ew) {
            let flag = 0;
            if (ew.ev.StartDateTime > ew.ev.EndDateTime) {
                flag++; inval.strtgrt = true;
            }
            if (ew.ev.StartDateTime === "" || ew.ev.StartDateTime == undefined) {
                flag++; inval.invalstrt = true;
            }
            if (ew.ev.EndDateTime === "" || ew.ev.EndDateTime == undefined) {
                flag++; inval.invalend = true;
            }
            if (((Date.parse(ew.ev.EndDateTime) - Date.parse(ew.ev.StartDateTime)) / (1000 * 60 * 60 * 24)) > 14) {
                flag++; inval.invaldur = true;
            }
            if (ew.ev.Subject === "" || ew.ev.Subject == undefined) {
                flag++; inval.invalsub = true;
            }
            if (flag == 0) { val.valevlist.push(ew.ev); }
        });
        for (var prop in inval) {
            var f = inval[prop];
            if (f == true) {
                if (prop == 'strtgrt') {
                    val.errmessage = val.errmessage + 'Event must start before it ends and must end after it starts\n';
                } else if (prop == 'invalstrt') {
                    val.errmessage = val.errmessage + 'Event Start Date Time: You must enter a value\n';
                } else if (prop == 'invalend') {
                    val.errmessage = val.errmessage + 'Event End Date Time: You must enter a value\n';
                } else if (prop == 'invalsub') {
                    val.errmessage = val.errmessage + 'Event Subject: You must enter a value\n ';
                } else if (prop == 'invaldur') {
                    val.errmessage = val.errmessage + 'Event cannot last longer than 14 days\n';
                } else {
                    val.errmessage = '';   
                }
            }
        }
        if (val.valevlist.length != ewList.length) {
            val.errmessage = val.errmessage + 'Please fill or remove unfilled events.\n ';
        } else {
            val.isVal = true;
        }
        return val;
    },
    getProvider: function (component, event, searchKey, isAlphaSearch) {
        var self = this;
        component.set("v.spinner", true);
        return new Promise($A.getCallback(function (resolve, reject) {
            var getProvider = component.get("c.getProviders");
            getProvider.setParams({
                "searchKey": searchKey,
                "isAlphaSearch": isAlphaSearch,
                "filterBy": component.get("v.filterBy"),
                "filterOpt": component.get("v.selectedTerritory"),
                "sortField": component.get("v.sortedBy"),
                "sortDirection": component.get("v.sortedDirection"),
                "recLimit": 0,
                "pageSize": component.get("v.pageSize"),
                "pageNumber": component.get("v.pageNumber")
            });
            getProvider.setCallback(this, function (response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    var result = JSON.parse(response.getReturnValue());
                    resolve(response.getReturnValue());
                    if (result != null) {
                        let fieldObj = result.fieldsWrapper;
                        let resultData = result.resultWrapper;
                        let headerFields = [];
                        for (var i in fieldObj) {
                            let v = {
                                label: fieldObj[i].label, fieldName: fieldObj[i].linkfieldName, type: fieldObj[i].fieldType, sortable: fieldObj[i].sortable,
                                typeAttributes: {
                                    label: { fieldName: fieldObj[i].fieldName }
                                }
                            };
                            headerFields.push(v);
                        }
                        component.set('v.mycolumns', headerFields);
                        component.set("v.totalRecords", result.totalRec);
                        component.set("v.dataSize", resultData.length);
                        self.setPaginationParam(component, resultData);
                        var providerListIds = component.get("v.providerList");
                        var idArr = [];
                        if (providerListIds != '') {
                            idArr = providerListIds.split(",");
                        }
                        // start of sorting data on provider selection
                        var tempArray = [];
                        for (i = 0; i < idArr.length; i++) {
                            var obj = resultData.find(x => x.Id == idArr[i]);
                            if (obj !== undefined)
                                tempArray.push(obj);
                        }
                        tempArray.filter((item, index) => {
                            var index = resultData.indexOf(item);
                            resultData.splice(index, 1)
                        });
                        var newArr = tempArray.concat(resultData);
                        newArr.forEach(function (res) {
                            res.adsrc = $A.get('$Resource.eventadd');
                        });
                        component.set("v.providerLocations", newArr);
                        // end of sorting data on provider selection
                        if (idArr.length > 0) {
                            console.log('idArr*******' + idArr);
                            component.set("v.keepSelection", idArr);
                            component.set("v.selectedProvider", idArr);
                        }
                    }
                    /*  component.set("v.dataSize", resultData.length);
                component.set("v.currentCount", component.get("v.initialRows"));
                */
                } else {
                    console.log("Failed with state " + state);
                }
                component.set("v.spinner", false);
            });
            $A.enqueueAction(getProvider);
        }));
    },
    getPicklistValues: function (component, event) {
        var getPicklist = component.get("c.getPicklistValues");
        getPicklist.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                if (response.getReturnValue() != null) {
                    let res = response.getReturnValue();
                    component.set("v.filterString", res.filterString);
                }
            } else {
                console.log("Failed with state " + state);
            }
        });
        $A.enqueueAction(getPicklist);
    },
    getMoreProviders: function (component, rows) {
        return new Promise($A.getCallback(function (resolve, reject) {
            var getMoreProvider = component.get("c.getProviders");
            var recordOffset = 3;
            var recordLimit = component.get("v.initialRows");
            getMoreProvider.setParams({
                "searchKey": '',
                "isAlphaSearch": false,
                /*"sortField":'',
                "sortDirection":'ASC',
                "recordLimit": recordLimit,
                "recordOffset": recordOffset  */
            });
            getMoreProvider.setCallback(this, function (response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    var resultData = response.getReturnValue();
                    resolve(resultData);
                    recordOffset = recordOffset + recordLimit;
                    component.set("v.currentCount", recordOffset);
                }
            });
            $A.enqueueAction(getMoreProvider);
        }));
    },
    getTotalNumberOfProvider: function (component) {
        var action = component.get("c.getTotalProviders");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var resultData = response.getReturnValue();
                component.set("v.totalNumberOfRows", resultData);
            }
        });
        $A.enqueueAction(action);
    },
    setParams: function (component, event) {
        var today = $A.localizationService.formatDate(new Date(), "MM/d/yyyy");
        component.set("v.today", today);
    },
    insertDate: function (component, field) {
        var today = $A.localizationService.formatDate(new Date(), "MMM d, yyyy");
        component.set(field, $A.localizationService.formatDate(new Date(), "yyyy-MM-dd"));
    },
    getFilterOptions: function (component, helper) {
        var filterType = component.get("v.filterBy");
        var getFilterOptions = component.get("c.getFilterOptions");
        getFilterOptions.setParams({ 'filterType': filterType });
        getFilterOptions.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                let filterOptRes = JSON.parse(response.getReturnValue());
                if (filterOptRes != null) {
                    let filterOptions = [];
                    for (var i in filterOptRes) {
                        let v = { label: filterOptRes[i].label, value: filterOptRes[i].value };
                        filterOptions.push(v);
                    }
                    component.set("v.filterOptions", filterOptions);
                    helper.setFilterByLabel(component, filterType);
                }
            } else {
                console.log("Failed with state " + state);
            }
        });
        $A.enqueueAction(getFilterOptions);
    },
    setFilterByLabel: function (component, filterType) {
        var fLabel = '';
        component.get("v.filterByOption").filter((item) => {
            if (item.value == filterType) {
                fLabel = item.label;
            }
        });
        component.set("v.filterLabel", fLabel);
    },
    showToast: function (component, message, messageType, title) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            message: message,
            type: messageType,
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    sortHelper: function (component, event) {
        var currentDir = component.get("v.arrowDirection");
        if (currentDir == 'arrowdown') {
            // set the arrowDirection attribute for conditionally rendred arrow sign  
            component.set("v.arrowDirection", 'arrowup');
            // set Assending order.  
            component.set("v.sortedDirection", 'asc');
        } else {
            component.set("v.arrowDirection", 'arrowdown');
            component.set("v.sortedDirection", 'desc');
        }
        // call the onLoad function for call server side method with pass sortFieldName 
        this.getProvider(component, event, '', false);
    },
    setPaginationParam: function (component, resultData) {
        var paginationCmp = component.find("paginationCmp");
        var showRecCountTextMsg = '';
        if (component.get("v.pageNumber") > 0 && component.get("v.pageSize") > 0) {
            let start = ((component.get("v.pageNumber") * component.get("v.pageSize")) - component.get("v.pageSize")) + 1;
            let end = '';
            if (component.get("v.pageSize") * component.get("v.pageNumber")>=component.get("v.totalRecords")) {
                end = component.get("v.totalRecords");
            }else {
                end = component.get("v.pageNumber") * component.get("v.pageSize");
            }
            showRecCountTextMsg = start + " - " + end + " of " + component.get("v.totalRecords");
        }

        if (resultData.length < parseInt(component.get("v.pageSize")) || parseInt(component.get("v.pageSize")) == component.get('v.totalRecords') || (component.get("v.pageNumber") * component.get("v.pageSize")) == component.get('v.totalRecords')) {
            component.set("v.isLastPage", true);
            paginationCmp.paginationMethod(true, showRecCountTextMsg);
        } else {
            component.set("v.isLastPage", false);
            paginationCmp.paginationMethod(false, showRecCountTextMsg);
        }
    },
    resetPaginationParams: function (component) {
        var paginationCmp = component.find("paginationCmp");
        paginationCmp.resetParams(component.get("v.pageNumber"), component.get("v.pageSize"));
    }
})