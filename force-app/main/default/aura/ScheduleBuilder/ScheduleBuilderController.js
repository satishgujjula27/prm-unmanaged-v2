({
    doInit: function (component, event, helper) {
        helper.getPicklistValues(component, event);
        helper.getFilterOptions(component, helper);
        helper.getProvider(component,event, '', false);
       // helper.setParams(component, event);
    },
    delayedKeyupEvent: function (component, event, helper) {
        var timer = component.get('v.timer');
        clearTimeout(timer);

        var timer = setTimeout(function () {
            var newlst = [];
            helper.quickSearch(component, event, helper);
            clearTimeout(timer);
            component.set('v.timer', null);
        }, 300);

        component.set('v.timer', timer);
    },
    clickAlphaSearch: function (component, event, helper) {
        component.set("v.initialRows", component.get("v.defaultLimit"));
        component.set("v.rowNumberOffset", component.get("v.defaultInitialOffset"));
        let alphaSearchKey = event.currentTarget.getAttribute("data-attr");
        helper.getProvider(component,event, alphaSearchKey, true);
    },
    insertEventDateTime: function (component, event, helper) {
        helper.insertDate(component, "v.newTask.ActivityDate");
    },
    handleFilterChange: function (component, event, helper) {
        helper.getFilterOptions(component, helper);
    },
    handleFilterOptionChange: function (component, event, helper) {
        if (component.find("filterOpt").get("v.value") != null) {
            component.set("v.selectedTerritory", component.find("filterOpt").get("v.value"));
            component.set("v.pageNumber",1);
            component.set("v.pageSize","50");
            helper.resetPaginationParams(component);
            helper.getProvider(component, event, '', false);
        }
    },
    handleNext: function (component, event, helper) {
        var pageNumber = component.get("v.pageNumber");
        component.set("v.pageNumber", pageNumber + 1);
    },
    handlePrev: function (component, event, helper) {
        var pageNumber = component.get("v.pageNumber");
        component.set("v.pageNumber", pageNumber - 1);
    },
    handleComponentEvent: function (component, event, helper) {
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");
        var lookupFieldName = event.getParam("lookupFieldName");
        if (lookupFieldName == 'AssignedTo') {
            component.set("v.newFollowupTask.OwnerId", selectedAccountGetFromEvent.Id);
        }
    },
    handleLoadMoreProvider: function (component, event, helper) {
        event.getSource().set("v.isLoading", true);
        component.set('v.loadMoreStatus', 'Loading....');
        helper.getMoreProviders(component, component.get('v.rowsToLoad')).then($A.getCallback(function (data) {
            if (component.get('v.providerLocations').length == component.get('v.totalNumberOfRows')) {
                component.set('v.enableInfiniteLoading', false);
                component.set('v.loadMoreStatus', 'No more data to load');
            } else {
                var currentData = component.get('v.providerLocations');
                var newData = currentData.concat(data);
                component.set('v.providerLocations', newData);
                component.set('v.loadMoreStatus', 'Please scroll down to load more data');
            }
            event.getSource().set("v.isLoading", false);
        }));
    },
    handleEventReceived: function (component, event, helper) {
        var ewList = component.get("v.eventsWrapped");
        var evt = event.getParam("event");
        ewList[event.getParam("indxno")].ev = Object.assign({}, evt);;
        component.set("v.eventsWrapped", ewList);
    },
    saveSchedule: function (component, event, helper) {
        var ewList = component.get("v.eventsWrapped");
        if (ewList.length > 0) {
            var valid = helper.validateEvents(component, ewList);
            if (valid.isVal) {
                var eventsfinal = component.get('v.eventsfinal');
                eventsfinal = valid.valevlist;
                var action = component.get("c.saveEvents");
                action.setParams({ "events": eventsfinal });
                action.setCallback(this, function (response) {
                    var state = response.getState();
                    if (component.isValid() && state === "SUCCESS") {
                        var result = JSON.parse(response.getReturnValue());
                        if (result != null) {
                            helper.showToast(component, result.message, 'error', 'Error');
                        } else {
                            helper.showToast(component, 'Total new events created: ' + eventsfinal.length + '.', 'success', 'Success');
                            component.set('v.eventsWrapped',[]);
                        }
                    } else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " +
                                    errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    } else {
                        console.log("Failed with state " + state);
                    }
                });
                $A.enqueueAction(action);
            } else {
                helper.showToast(component, valid.errmessage, 'error', 'Error');
            }
        } else {
            helper.showToast(component, 'Please create atleast one event', 'warning', '')
        }

    },
    addevent: function (component, event, helper) {
        var selectedItem = event.currentTarget;
        //Get the selected item index
        var plId = selectedItem.dataset.plocrec;
        var whoId = selectedItem.dataset.prorec;
        var rmsrc = $A.get('$Resource.eventremove');
        var ewList = component.get("v.eventsWrapped");
        ewList.push({ 'ev': { 'WhoId': whoId, 'WhatId': plId }, 'plId': plId, 'rmsrc': rmsrc });
        component.set("v.eventsWrapped", ewList);
    },
    removeevent: function (component, event, helper) {
        var ewList = component.get("v.eventsWrapped");
        //Get the target object
        var selectedItem = event.currentTarget;
        //Get the selected item index
        var index = selectedItem.dataset.record;
        ewList.splice(index, 1);
        component.set("v.eventsWrapped", ewList);
    },
    addover: function (component, event, helper) {
        var selectedItem = event.currentTarget;
        var imgno = selectedItem.dataset.addimgno;
        var ploc = component.get('v.providerLocations');
        ploc[imgno].adsrc = $A.get('$Resource.eventaddover');
        component.set('v.providerLocations', ploc);
    },
    addout: function (component, event, helper) {
        var selectedItem = event.currentTarget;
        var imgno = selectedItem.dataset.addimgno;
        var ploc = component.get('v.providerLocations');
        ploc[imgno].adsrc = $A.get('$Resource.eventadd');
        component.set('v.providerLocations', ploc);
    },
    removeover: function (component, event, helper) {
        var selectedItem = event.currentTarget;
        var imgno = selectedItem.dataset.rmvimgno;
        var events = component.get('v.eventsWrapped');
        events[imgno].rmsrc = $A.get('$Resource.eventremoveover');
        component.set('v.eventsWrapped', events);
    },
    removeout: function (component, event, helper) {
        var selectedItem = event.currentTarget;
        var imgno = selectedItem.dataset.rmvimgno;
        var events = component.get('v.eventsWrapped');
        events[imgno].rmsrc = $A.get('$Resource.eventremove');
        component.set('v.eventsWrapped', events);
    },
    sortByField: function (component, event, helper) {
        // set current selected header field on sortedBy attribute.     
        component.set("v.sortedBy", event.currentTarget.dataset.hdr);  
        helper.sortHelper(component, event);
    },
    handlePaginationEvent : function(component,event,helper){
        component.set("v.pageNumber",event.getParam("pageNumber"));
        component.set("v.pageSize",event.getParam("pageSize"));
        helper.getProvider(component,event, '', false);
    }
})