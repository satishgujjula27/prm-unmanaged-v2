<!--
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
@Component Name: RelatedLkpFilterComponent
@description: This component is to create events for providers and log them in calendar
@Author: Healthgrades Inc.
-->
<aura:component controller="ScheduleBuilderController" implements="force:appHostable"
                description="This component is to create events for providers and log them in calendar" access="global">
    <ltng:require styles="{!$Resource.multilineToast}" />
    <!--Attribute definitions-->
    <aura:attribute name="eventsWrapped" type="Object[]" />
    <aura:attribute name="eventsfinal" type="Event[]" />
    <aura:attribute name="providerLocations" type="Sobject[]" />
    <aura:attribute name="mycolumns" type="List" />
    <aura:attribute name="keepRowSelected" type="boolean" default="true" />
    <aura:attribute name="filterString" type="List" default="[]" />
    <aura:attribute name="providerList" type="string" default="" />
    <aura:attribute name="selectedLookUpRecord" type="sObject" default="{}" />
    <aura:attribute name="keepSelection" type="List" default="[]" />
    <aura:attribute name="timer" type="Integer" />
    <aura:attribute name="selectedProvider" type="List" />
    <aura:attribute name="sortedBy" type="String" default="FirstName" />
    <aura:attribute name="sortedDirection" type="String" default="asc" />
    <aura:attribute name="selectedRecord" type="sObject" />
    <aura:attribute name="enableInfiniteLoading" type="Boolean" default="true" />
    <aura:attribute name="initialRows" type="Integer" default="3" />
    <aura:attribute name="rowsToLoad" type="Integer" default="3" />
    <aura:attribute name="totalNumberOfRows" type="Integer" default="11" />
    <aura:attribute name="defaultLimit" type="Integer" default="50" />
    <aura:attribute name="defaultInitialOffset" type="Integer" default="0" />
    <aura:attribute name="loadMoreStatus" type="String" default="Please scroll down to load more data" />
    <aura:attribute name="rowNumberOffset" type="Integer" default="0" />
    <aura:attribute name="filterByOption" type="List" default="[
                                                               {'label': 'Territory', 'value': 'territory'},
                                                               {'label': 'Practice', 'value': 'practice'}
                                                               ]" />
    <aura:attribute name="filterBy" type="String" default="territory" />
    <aura:attribute name="filterOptions" type="List" default="[]" />
    <aura:attribute name="filterLabel" type="String" default="Territory" />
    <aura:attribute name="selectedTerritory" type="String" default="" />
    <aura:attribute name="providerEntityName" type="String" />
    <aura:attribute name="showAccompained" type="boolean" default="false" />
    <aura:attribute name="today" type="date" />
    <aura:attribute name="namespace" type="string" default="{!$Label.c.BasePkgNamespace}" />
    <aura:attribute name="spinner" type="boolean" default="false" />
    <aura:attribute name="adsrc" type="boolean" default="false" />
    <aura:attribute name="arrowDirection" type="string" default="arrowup" />
    <aura:attribute name="isAsc" type="boolean" default="true" />
    <aura:attribute name="totalRecords" type="Integer" default="0"/>
    <aura:attribute name="pageNumber" type="Integer" default="1"/>
    <aura:attribute name="pageSize" type="String" default="50"/>
    <aura:attribute name="dataSize" type="Integer" default="0"/>
    <!--handlers-->
    <aura:handler name="init" action="{!c.doInit}" value="{!this}" />
    <aura:handler name="eventevt" event="c:EventRecordevt" action="{!c.handleEventReceived}"></aura:handler>
    <aura:handler name="paginationEvent" event="c:PaginationComponentEvent" action="{!c.handlePaginationEvent}"/>
    <!--component begins-->
    <div class="slds">
        <div class="wk_static">
            <div class="slds-page-header" role="banner">
                <div class="slds-grid ">
                    <div class="slds-col slds-has-flexi-truncate slds-size_8-of-12">
                        <h1 class="slds-page-header__title slds-align-middle slds-truncate slds-m-top_xx-small"
                            title="Schedule Builder">Schedule Builder</h1>
                    </div>
                    <div class="slds-col slds-grid slds-grid_vertical slds-m-left_xx-large">
                        <div class="slds-col">
                            <div style="text-align:right; padding-right:50px">
                                <div class="slds-m-top_x-small">
                                    <lightning:layout>
                                        <lightning:layoutItem class="slds-m-right_small">Filter by:
                                        </lightning:layoutItem>
                                        <lightning:layoutItem>
                                            <lightning:radioGroup name="filterByRadio" label="Filter by:"
                                                                  variant="label-hidden" options="{! v.filterByOption }"
                                                                  value="{! v.filterBy }" type="radio" class="customRadioCls"
                                                                  onchange="{! c.handleFilterChange }" />
                                        </lightning:layoutItem>
                                    </lightning:layout>
                                </div>
                            </div>
                        </div>
                        <div class="slds-col">
                            <div style="text-align:right; padding-right:50px">
                                <div>
                                    <lightning:layout>
                                        <lightning:layoutItem class="slds-m-right_small slds-m-top_large">
                                            {!v.filterLabel+': '}
                                        </lightning:layoutItem>
                                        <lightning:layoutItem class="slds-size_1-of-2">
                                            <lightning:select name="filterOpt" label="{!v.filterLabel+': '}"
                                                              variant="label-hidden" aura:id="filterOpt"
                                                              onchange="{! c.handleFilterOptionChange }">
                                                <option text="None" value="" />
                                                <aura:iteration items="{!v.filterOptions}" var="option">
                                                    <option text="{!option.label}" value="{!option.value}"
                                                            selected="{!option.value == v.selectedTerritory}" />
                                                </aura:iteration>
                                            </lightning:select>
                                        </lightning:layoutItem>
                                    </lightning:layout>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="slds-col slds-no-flex slds-grid slds-align-top">
                        <div class="slds-m-top_xx-large slds-m-right_medium">
                            <lightning:button label="Save Schedule" title="Save Schedule" onclick="{!c.saveSchedule}"
                                              variant="neutral">
                            </lightning:button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="slds-align_absolute-center slds-m-top_xx-small slds-m-bottom_medium">
            <lightning:input aura:id="providerSearchBox" name="providerSearch" type="search" class="slds-size_3-of-12"
                             placeholder="Find Provider..." onkeyup="{!c.delayedKeyupEvent}" />
        </div>
        <div class="">
            <c:PaginationComponent totalRecords="{!v.totalRecords}" dataSize="{!v.dataSize}" aura:id="paginationCmp"/>
        </div>
        <div class="slds-border_top slds-m-top_small">
            <article class="slds-card">
                <div class="slds-card__header slds-grid">
                    <header class="slds-media slds-media_center slds-has-flexi-truncate">
                        <div class="slds-media__body" align="right">
                            <aura:iteration items="{!v.filterString}" var="fChar">
                                <a class="listItem" data-attr="{!fChar}" onclick="{!c.clickAlphaSearch}">
                                    <span class="listItemPad">{!fChar}</span>
                                </a>
                            </aura:iteration>
                        </div>
                    </header>
                </div>
                <div class="slds-card__body slds-card__body_inner" style="height:12rem;">
                    <aura:if isTrue="{! v.mycolumns.length > 0 }">
                        <table class="slds-table slds-table--bordered slds-table--cell-buffer">
                            <thead>
                                <tr class="slds-text-title--caps" style="height:2.0rem;">
                                    <th class="headerRow" scope="col">
                                        <div class="slds-truncate">
                                            Add &nbsp;&nbsp;
                                        </div>
                                    </th>
                                    <aura:iteration items="{!v.mycolumns}" var="h">
                                        <th class="slds-is-sortable slds-text-title--caps" scope="col"
                                            onclick="{!c.sortByField}" data-hdr="{!h.fieldName}">
                                            <div class="slds-truncate" title="{! h.label }"><a href="">{! h.label }</a>
                                                <span>
                                                    <aura:if
                                                             isTrue="{! and(v.sortedDirection == 'asc', v.sortedBy == h.fieldName)}">
                                                        <lightning:icon iconName="utility:arrowup" size="xx-small"
                                                                        alternativeText="Indicates approval" />
                                                    </aura:if>
                                                    <aura:if
                                                             isTrue="{! and(v.sortedDirection == 'desc', v.sortedBy == h.fieldName)}">
                                                        <lightning:icon iconName="utility:arrowdown" size="xx-small"
                                                                        alternativeText="Indicates approval" />
                                                    </aura:if>
                                                </span>
                                            </div>
                                        </th>
                                    </aura:iteration>
                                </tr>
                            </thead>
                            <tbody>
                                <aura:iteration items="{!v.providerLocations}" var="pl" indexVar="in">
                                    <tr class="dataRow">
                                        <th scope="row" data-label="Add" style="width:1%;">
                                            <a href="" onclick="{!c.addevent}" data-plocrec="{!pl.id}"
                                               data-prorec="{!pl.pId}">
                                                <img data-addimgno="{!in}" src="{!pl.adsrc}" alt="Add Event"
                                                     title="Add Event" onmouseover="{!c.addover}"
                                                     onmouseout="{!c.addout}" />
                                            </a>
                                        </th>
                                        <td data-label="{!pl.firstName}">
                                            <div class="slds-truncate" title="{!pl.firstName}"><a
                                                                                                  href="{!pl.fLinkId}">{!pl.firstName}</a></div>
                                        </td>
                                        <td data-label="{!pl.lastName}">
                                            <div class="slds-truncate" title="{!pl.lastName}"><a
                                                                                                 href="{!pl.lLinkId}">{!pl.lastName}</a></div>
                                        </td>
                                        <td data-label="{!pl.primarySpeciality}">
                                            <div class="slds-truncate" title="{!pl.primarySpeciality}"><a
                                                                                                          href="{!pl.sLinkId}">{!pl.primarySpeciality}</a></div>
                                        </td>
                                        <td data-label="{!pl.practice}">
                                            <div class="slds-truncate" title="{!pl.practice}"><a
                                                                                                 href="{!pl.pngLinkId}">{!pl.practice}</a></div>
                                        </td>
                                        <td data-label="{!pl.locationMailingAddress}">
                                            <div class="slds-truncate" title="{!pl.locationMailingAddress}">
                                                {!pl.locationMailingAddress}</div>
                                        </td>
                                        <td data-label="{!pl.locationPhone}">
                                            <div class=" slds-truncate" title="{!pl.locationPhone}">{!pl.locationPhone}
                                            </div>
                                        </td>
                                    </tr>
                                    
                                    <tr class="dataRow">
                                        <td colspan="1000" class="dataCell"
                                            style="background-color:#EFFFFF !important; padding:0px;">
                                            <aura:iteration items="{!v.eventsWrapped}" var="ew" indexVar="index">
                                                <aura:if isTrue="{!ew.plId == pl.id}">
                                                    <table style="border:0px; border-spacing:0px; width:100%;">
                                                        <tr>
                                                            <td
                                                                style="border-bottom:1px solid #D4DADC; vertical-align:middle; width:50px; padding-left:15px;">
                                                                <a href="" onclick="{!c.removeevent}"
                                                                   data-record="{!index}">
                                                                    <img data-rmvimgno="{!index}" src="{!ew.rmsrc}"
                                                                         alt="Remove Event" title="Remove Event"
                                                                         onmouseover="{!c.removeover}"
                                                                         onmouseout="{!c.removeout}" />
                                                                </a>
                                                            </td>
                                                            <td
                                                                style="border-bottom:1px solid #D4DADC; vertical-align:top;">
                                                                <div aura:id="eventFieldsDiv" style="width:300px">
                                                                    <c:EventFormComp ev="{!ew.ev}"
                                                                                     location="{!pl.locationMailingAddress}"
                                                                                     indxno="{!index}">
                                                                    </c:EventFormComp>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </aura:if>
                                            </aura:iteration>
                                        </td>
                                    </tr>
                                </aura:iteration>
                            </tbody>
                        </table>
                    </aura:if>
                </div>
            </article>
        </div>
    </div>
    <aura:if isTrue="{!v.spinner}">
        <lightning:spinner alternativeText="Loading" />
    </aura:if>
</aura:component>