({
    doInit : function(component, event, helper) {
        helper.initProviderIds(component,event);
		helper.doInitHelper(component,'');
        helper.loadPicklist(component, event);
        var today = $A.localizationService.formatDate(new Date(), "MM/d/yyyy");
        component.set("v.today",today);
        helper.getLoggedInUser(component,event);
	},
    handleCallSub: function (component, event, helper) {
        component.set("v.newTask.Subject", event.getParam("text"));
    },
    tabSelected : function(component, event, helper) {
        helper.doInitHelper(component,component.get("v.defaultActTabs"));
    },
    logCall2 : function(component, event, helper) {
        var newTask = component.get("v.newTask");
        var logCall2Obj = new Object();
        if(helper.validateRequiredField(component,newTask)){
            logCall2Obj["task"] = newTask;
            logCall2Obj["thisRecordTypeName"] = component.get("v.currentActiveTabName");
            logCall2Obj["providerIds"] = component.get("v.providerIds");
            helper.doServerCall(component, 'logCall', logCall2Obj, 'logCall2', false).then(
                function(reslogCall2) {
                    let message = component.get("v.providerIds").length + ' Calls logged successfully.';
                    message += " This page will be redirected back to Territory Providers";
                    helper.showMessage("Success!", message, "success");
                    setTimeout(function(){ 
                        helper.redirect(component, event, helper);
                    }, 5000);
                }
            );
        } else {
            helper.showMessage("Required field missing", 'Please fill the required fields', 'error');
        }
    },
    handleBack : function(component,event,helper) { window.history.back(); },
    insertDate : function(component,event,helper) {
        var today = $A.localizationService.formatDate(new Date(), "MMM d, yyyy");
        component.set("v.newTask.ActivityDate",$A.localizationService.formatDate(new Date(), "yyyy-MM-dd"));
    },
    changeAsset : function(component,event,helper){
        if(event.getParam("value") != null){
            if(component.get("v.namespace") !=null && component.get("v.namespace") !='') {
                component.set("v.newTask."+component.get("v.namespace")+"Asset__c",event.getParam("value"));
            } else {
                component.set("v.newTask.Asset__c",event.getParam("value"));
            }
        }
    },
    handleLookupCmpEvent : function(component, event, helper){
        var selectedObj = event.getParam("selItem");
        var lookupFieldName = event.getParam("lookupFieldName");
        var selVal = '';
        if(selectedObj !=null) { selVal = selectedObj.val; }
        if(lookupFieldName == 'AccompaniedPhysician') {
            if(component.get("v.namespace") !=null && component.get("v.namespace") !='') {
                helper.setLookupFieldVal(component,"v.newTask."+component.get("v.namespace")+"AccompaniedPhysician__c",selVal);
            } else {
                helper.setLookupFieldVal(component,"v.newTask.AccompaniedPhysician__c",selVal);
            }
        } else if(lookupFieldName == 'AssignedTo') { helper.setLookupFieldVal(component,"v.newTask.OwnerId",selVal); }
            else {
                helper.setLookupFieldVal(component,"v.newTask."+lookupFieldName,selVal);
            }
    },
    handlefieldChanges : function(component, event, helper) {
        if(event.getSource().get('v.value') != null){
            if(event.getSource().get('v.name') == 'IntelDescription'){
                if (component.get("v.namespace") != null && component.get("v.namespace") != '') {
                    component.set("v.newTask." + component.get("v.namespace") + "IntelDescription__c", event.getSource().get('v.value'));
                } else {
                    component.set("v.newTask.IntelDescription__c", event.getSource().get('v.value'));
                }
            } else if(event.getSource().get('v.name') == 'Department'){
                if (component.get("v.namespace") != null && component.get("v.namespace") != '') {
                    component.set("v.newTask." + component.get("v.namespace") + "Department__c", event.getSource().get('v.value'));
                } else {
                    component.set("v.newTask.Department__c", event.getSource().get('v.value'));
                }
            } else if(event.getSource().get('v.name') == 'Intel'){
                if (component.get("v.namespace") != null && component.get("v.namespace") != '') {
                    component.set("v.newTask." + component.get("v.namespace") + "Intel__c", event.getSource().get('v.value'));
                } else {
                    component.set("v.newTask.Intel__c", event.getSource().get('v.value'));
                }
            }
        }
    },
    handleDateChange : function(component, event, helper){
        component.set("v.newTask."+event.getSource().get("v.name"), event.getParam("value"));
    },
    handleBooleanChange : function(component, event, helper){
        component.set("v.newTask."+event.getSource().get("v.name"),  event.getSource().get("v.checked"));
    },
    handlePicklistChange : function(component, event, helper){
        component.set("v.newTask."+event.getSource().get("v.name"), event.getSource().get("v.value"));
    },
    handleMultiPicklistChange: function(component, event, helper){
        component.set("v.newTask."+event.getSource().get("v.name"), event.getSource().get("v.value"));
    },
    
    setObjects : function(component, event, helper){
        var defaultFldList = component.get("v.defaultFldList");
        for(var key in defaultFldList){
            if(defaultFldList[key].relObjs.length > 0){
                defaultFldList[key].selItem = {};
            }
        }
        component.set("v.defaultFldList",defaultFldList);        
    }
})