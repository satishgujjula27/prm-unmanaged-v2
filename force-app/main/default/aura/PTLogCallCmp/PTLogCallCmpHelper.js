({
    loadPicklist : function(component, event) {
        var self= this; var dummyObj = new Object();
        self.doServerCall(component, 'getPicklistValues', dummyObj, 'loadPicklist', false).then(
            function(fetchPicklistVals) {
                if(fetchPicklistVals !=null) {
                    let res = fetchPicklistVals;
                    let intelOptions = [];
                    for(var i in res.intel) {
                        let v = {label:res.intel[i],value:res.intel[i]};
                        intelOptions.push(v);
                    }
                    component.set("v.intelOptions", intelOptions);
                }
            }
        );
    },
    redirect: function(component, event, helper){
        var url = window.location.href; 
        var value = url.substr(0,url.lastIndexOf('/') + 1);
        window.history.back();
        return false;
    },
    initProviderIds : function(component, event){
        var ids = component.get("v.ids");
        if(ids !='') {
            let idArr = ids.split(",");
            if(idArr.length >0) {
                component.set("v.providerIds",idArr);
                let logCallbtnTitle = 'Log '+idArr.length + ' Calls';
                component.set("v.logCallButtonTitle",logCallbtnTitle);
                component.set("v.isProviderSelected",true)
            }
        }
    },
    setLookupFieldVal : function(component, field, val) { component.set(field,val); },
    getLoggedInUser : function(component){
        var self= this; var dummyObj = new Object();
        self.doServerCall(component, 'getLoggedInUser', dummyObj, 'getLoggedInUser', false).then(
            function(fetchLoggedInUsr) {
                if(fetchLoggedInUsr !=null){
                    component.set("v.selItem1",{'text':fetchLoggedInUsr.Name});
                    self.setLookupFieldVal(component, "v.newTask.OwnerId",fetchLoggedInUsr.Id);
                }
            }
        );
    },
    
    validateRequiredField : function(component,newTask){
        let actList = component.get("v.activityLists");
        var isValid = true;
        if(actList.length == 0) {
            ($A.util.isEmpty(newTask.OwnerId) || $A.util.isEmpty(newTask.Subject)) ? isValid = false : isValid = true;
            return isValid;
        }
        var fieldApiName = [];
        var fields = component.get("v.defaultFldList");
        fields.forEach(function (field) {
            if(field.isRequired && $A.util.isEmpty(component.get("v.newTask."+field.apiName))){
                fieldApiName.push(field.apiName);
            }
        })
        fieldApiName.length !== 0 ? isValid = false : isValid = true;
        return isValid; 
    },

    showMessage : function(messageTitle,messageBody,messageType){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ "title": messageTitle, "type":messageType, "message": messageBody });
        toastEvent.fire();
    },
    doInitHelper : function(component, selectedTab) {
        let self = this, doInitArray = new Object();
        component.set("v.spinner",true);
        doInitArray['selectedTab'] = selectedTab;
        self.doServerCall(component, 'getActivityList', doInitArray, 'doInitHelper', false).then(
            function(doInitHelperdetail) {
                var detail = JSON.parse(doInitHelperdetail);
                if(detail.defaultFldList != null){
                    for(var i=0;i<detail.defaultFldList.length;i++){
                        if(detail.defaultFldList[i].defaultValue != null){
                            component.set("v.newTask."+detail.defaultFldList[i].apiName, detail.defaultFldList[i].defaultValue);
                        }
                    }
                }
                for(var key in detail.defaultFldList){
                    if(detail.defaultFldList[key].relObjs != '' && detail.defaultFldList[key].relObjs != null && detail.defaultFldList[key].relObjs.length != null && detail.defaultFldList[key].relObjs.length > 0){
                        component.set("v.selectedObject",detail.defaultFldList[key].relObjs[0]);
                        break;
                    }                   
                }
                component.set("v.activityLists",detail.actvtListWrapper);
                if(detail.actvtListWrapper === undefined || detail.actvtListWrapper < 1){
                    component.set("v.showCallScreen", true);
                } else {
                    Object.keys(detail.actvtListWrapper).forEach(function(prop) {
                        if( detail.actvtListWrapper[prop]["Id"] == detail.defaultActTabs ) {
                            component.set("v.currentActiveTabName", detail.actvtListWrapper[prop]["label"]);
                        }
                    });
                }
                
                component.set("v.defaultFldList",detail.defaultFldList);
                component.set("v.defaultActTabs",detail.defaultActTabs);
                component.set("v.defaultActTabsSet",detail.defaultActTabs);
                
                component.set("v.spinner",false);
            }
        );
    },
    doServerCall : function(component, apexAction, params, callFromMethod, pushToEvent) {
        var self = this;
        try {
            return new Promise(function(resolve, reject) {
                var action = component.get("c."+apexAction+"");
                if( params != null || typeof params != "undefined" ) {
                    action.setParams( params );
                }
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        resolve( response.getReturnValue() );
                    }
                });
                $A.enqueueAction(action);
            });
        } catch(e) {
            console.log("Failed to load data from server - "+callFromMethod);
        }
    }
})