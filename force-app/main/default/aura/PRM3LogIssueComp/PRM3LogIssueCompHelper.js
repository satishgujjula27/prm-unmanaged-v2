({
    doInitHelper : function(component, selectedTab) { 
        let self = this, doInitArray = new Object();
        doInitArray['selectedTab'] = selectedTab;
        doInitArray['providerId'] = component.get("v.providerId");
        self.doServerCall(component, 'getIssueList', doInitArray, 'doInitHelper', false).then(
            function(doInitHelperdetail) {
                var detail = JSON.parse(doInitHelperdetail);
                component.set("v.objectName", detail.objectName);
                component.set("v.issuesLists", detail.issueListWrapper);
                component.set("v.defaultFldList", detail.defaultFldList);
                
                Object.keys(detail.defaultFldList).forEach(function(prop) {
                    let fieldList = detail.defaultFldList[prop];
                    if (fieldList.apiName === component.get("v.baseNamespace")+'Provider__c' && fieldList.selItem && fieldList.selItem.text) {

                        let obj = new Object();
                        obj['text'] = fieldList.selItem.text;
                        obj['val'] = fieldList.defaultValue;
                        component.set("v.selItem", obj);
                    }
                });
                
                if (detail.issueListWrapper === undefined || detail.issueListWrapper < 1) { } 
                else {
                    Object.keys(detail.issueListWrapper).forEach(function(prop) {
                        if( detail.issueListWrapper[prop]["Id"] == detail.defaultIssueTabs ) {
                            component.set("v.recordTypeId", detail.issueListWrapper[prop]["recordTypeId"]);
                        }
                    });
                }
                
                if (detail.defaultIssueTabs != null) {
                    component.set("v.defaultIssueTabs", detail.defaultIssueTabs);
                    component.set("v.defaultIssueTabsSet", detail.defaultIssueTabs);
                }
            }
        );
    },
    
    doServerCall : function(component, apexAction, params, callFromMethod, pushToEvent) {
        var self = this;
        try {
            return new Promise(function(resolve, reject) {
                var action = component.get("c."+apexAction+"");
                if ( params != null || typeof params != "undefined" ) {
                    action.setParams( params );
                }
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        resolve( response.getReturnValue() );
                    }
                });
                $A.enqueueAction(action);
            });
        } catch(e) {
            console.log("Failed to load data from server - "+callFromMethod);
        }
    }
})