({
    doInit : function(component, event, helper) {
        helper.doInitHelper(component, '');
        let device = $A.get("$Browser.formFactor");
        let isDesktop = (device === 'DESKTOP' ? true : false);
        component.set("v.isDesktop", isDesktop);
    },

    tabSelected : function(component, event, helper) {
        helper.doInitHelper(component, component.get("v.defaultIssueTabs"));
    },

    handleSubmit: function(component, event, helper) {
        event.preventDefault();
        let baseNamespace = component.get("v.baseNamespace");
        let providerId = component.get("v.providerId");
        let fields = event.getParam('fields');
        if (providerId) { fields[baseNamespace+'Provider__c'] = providerId; }
        component.find('recordEditForm').submit(fields);
    },

    handleSuccess : function(component, event, helper) {      	
        component.set("v.isSuccess",true);
        setTimeout(function(){ component.destroy(); }, 5000);
    },
    
    handleCancel : function(component, event, helper) {
        component.destroy();
    },

    handleLookupCmpEvent: function (component, event, helper) {
        let baseNamespace = component.get("v.baseNamespace");
        let selectedObj = event.getParam("selItem");
        let lookupFieldName = event.getParam("lookupFieldName");
        let selVal = '';
        if (selectedObj != null) { selVal = selectedObj.val; }
        if (lookupFieldName == baseNamespace+'Provider__c') {
            component.set("v.providerId", selVal);
        } 
    }
})