({
    doInit : function(component, event, helper) {
        helper.doInitHelper(component,'');
        helper.getProvider(component, event, '', false);
        helper.getPicklistValues(component, event);
        helper.getLoggedInUser(component);
        helper.setParams(component, event);
        helper.getFilterOptions(component, helper);
    },
    tabSelected : function(component, event, helper) {
        helper.doInitHelper(component,component.get("v.defaultActTabs"));
    },
    handleCallSub: function (component, event, helper) {
        component.set("v.newTask.Subject", event.getParam("text"));
        component.set("v.newFollowupTask.Subject", event.getParam("text"));
    },
    handleTaskSub: function (component, event, helper) {
        component.set("v.newFollowupTask.Subject", event.getParam("text"));
    },
    createNewTask: function (component, event, helper) {
        let valid; let validinp;
        let selectedProvider = component.get("v.selectedProvider");
        if (selectedProvider.length > 0) {
            let actList = component.get("v.activityLists");
            if(actList.length > 0){
                validinp = true;
                valid = helper.validateRequiredActivityField(component);
            } else {
                validinp = component.find('validinp').reduce(function (validSoFar, inputCmp) {
                    // Displays error messages for invalid fields
                    inputCmp.showHelpMessageIfInvalid();
                    try {
                        return validSoFar && inputCmp.get('v.validity').valid;
                    } catch(err) {
                        return true;
                    }
                }, true);
                valid = helper.validateRequiredField(component, component.get("v.newTask"), component.get("v.isLogCall"), component.get("v.newFollowupTask"), component.get("v.isFollowUpTask"));
            }
            if (valid && validinp) {
                let selectedIdProvider = [];
                for (let key in selectedProvider) {
                    // First if condition for checking if it is from Activities component
                    if (JSON.parse(component.get("v.isLogCall"))) {
                        // if activity component, below will work because it returns
                        // location object to front-end.
                        if (typeof selectedProvider[key] == 'object' && selectedProvider[key].Id != "undefined") {
                            selectedIdProvider.push(selectedProvider[key].Id);
                            // this use-case applies when it is called from Bulk Action.
                        } else {
                            selectedIdProvider.push(selectedProvider[key]);
                        }
                    } else {
                        if (typeof selectedProvider[key] == 'object' && selectedProvider[key].Id != "undefined") {
                            selectedIdProvider.push(selectedProvider[key].Id);
                            // this use-case applies when it is called from Bulk Action.
                        } else {
                            selectedIdProvider.push(selectedProvider[key]);
                        }
                    }
                }
                let action = component.get("c.saveTask");
                let newTask = component.get("v.newTask");
                let newFollowupTask = component.get("v.newFollowupTask");
                let newFollowupTaskparsed = JSON.parse(JSON.stringify(newFollowupTask));
                if( component.get("v.isLogCall") == false && actList.length > 0 ) {
                    if( newFollowupTaskparsed['Status'].length == 0 || newFollowupTaskparsed['Status'] == null ) {
                        newFollowupTask['Status'] = 'High';
                    }
                    if( newFollowupTaskparsed['Priority'].length == 0 || newFollowupTaskparsed['Priority'] == null ) {
                        newFollowupTask['Priority'] = 'In Progress';
                    }   
                }
                action.setParams({
                    "task": newTask,
                    "followupTask": newFollowupTask,
                    "isCreateTask": component.get("v.isLogCall"),
                    "iscreateFollowupTask": component.get("v.isFollowUpTask"),
                    "thisRecordTypeName": component.get("v.currentActiveTabName"),
                    "providerList": selectedIdProvider
                });
                action.setCallback(this, function (response) {
                    let state = response.getState();
                    if (component.isValid() && state === "SUCCESS") {
                        let message = '';
                        if (component.get("v.isLogCall")) {
                            message = "Total new calls logged:" + selectedProvider.length;
                        }
                        if (component.get("v.isFollowUpTask")) {
                            message += " Follow-up task created:" + selectedProvider.length;
                        }
                        helper.showMessage(component, true, message, 'success');
                        if (component.get("v.keepRowSelected") == false) {
                            component.find("ProviderRecordTable").set("v.selectedRows", "[]");
                            component.set("v.selectedProvider", []);
                        }
                    } else if (state === "ERROR") {
                        let errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                helper.showMessage(component, true, errors[0].message, 'error');
                                console.log("Error message: " +
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    } else {
                        console.log("Failed with state " + state);
                    }
                });
                $A.enqueueAction(action);
            } else {
                if(!valid && !validinp) {
                    helper.showMessage(component, true, 'Please Fill Required Fields And Review Errors On This Page', 'error');
                } else if(!validinp) {
                    helper.showMessage(component, true, 'Review Errors On This Page', 'error');
                } else {
                    helper.showMessage(component, true, 'Please Fill Required Fields', 'error');
                }
            }
        } else {
            helper.showMessage(component, true, 'Nothing to process', 'warning');
        }
    },
    logCall: function (component, event, helper) {
        if (component.get("v.isLogCall") == true) {
            component.set("v.isLogCall", false);
        } else {
            component.set("v.isLogCall", true);
        }
    },
    handleIsReminder : function (component, event, helper) {
        if (component.get("v.isReminder") == true) {
            component.set("v.isReminder", false);
        } else {
            component.set("v.isReminder", true);
        }
        component.set("v.newFollowupTask.IsReminderSet",component.get("v.isReminder"));
    },
    createFollowUpTask: function (component, event, helper) {
        if (component.get("v.isFollowUpTask") == true) {
            component.set("v.isFollowUpTask", false);
        } else {
            component.set("v.isFollowUpTask", true);
        }
    },
    delayedKeyupEvent: function (component, event, helper) {
        var timer = component.get('v.timer');
        clearTimeout(timer);
        
        var timer = setTimeout(function () {
            var newlst = [];
            helper.quickSearch(component, event, helper);
            clearTimeout(timer);
            component.set('v.timer', null);
        }, 500);
        component.set('v.timer', timer);
    },
    clickAlphaSearch: function (component, event, helper) {
        component.set("v.initialRows", component.get("v.defaultLimit"));
        component.set("v.rowNumberOffset", component.get("v.defaultInitialOffset"));
        let alphaSearchKey = event.currentTarget.getAttribute("data-attr");
        helper.getProvider(component, event, alphaSearchKey, true);
    },
    handleRowAction: function (component, event, helper) {
        var selRows = event.getParam('selectedRows');
        component.set("v.selectedProvider", selRows);
        var allSelectedRows = [];
        selRows.forEach(function (row) {
            if (!allSelectedRows.includes(row.Id)) {
                allSelectedRows.push(row.Id);
            }
        });
        component.set("v.keepSelection", allSelectedRows);
    },
    updateColumnSorting: function (component, event, helper) {
        let fieldName = event.getParam('fieldName');
        let sortDirection = event.getParam('sortDirection');
        component.set("v.sortedBy", fieldName);
        component.set("v.sortedDirection", sortDirection);
        helper.getProvider(component, event, '', false);
    },
    handleNext: function (component, event, helper) {
        var pageNumber = component.get("v.pageNumber");
        component.set("v.pageNumber", pageNumber + 1);
    },
    handlePrev: function (component, event, helper) {
        var pageNumber = component.get("v.pageNumber");
        component.set("v.pageNumber", pageNumber - 1);
    },
    handleComponentEvent: function (component, event, helper) {
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");
        var lookupFieldName = event.getParam("lookupFieldName");
        if (lookupFieldName == 'AssignedTo') {
            component.set("v.newFollowupTask.OwnerId", selectedAccountGetFromEvent.Id);
        }
    },
    handleLoadMoreProvider: function (component, event, helper) {
        event.getSource().set("v.isLoading", true);
        component.set('v.loadMoreStatus', 'Loading....');
        helper.getMoreProviders(component, component.get('v.rowsToLoad')).then($A.getCallback(function (data) {
            if (component.get('v.providerLocations').length == component.get('v.totalNumberOfRows')) {
                component.set('v.enableInfiniteLoading', false);
                component.set('v.loadMoreStatus', 'No more data to load');
            } else {
                var currentData = component.get('v.providerLocations');
                var newData = currentData.concat(data);
                component.set('v.providerLocations', newData);
                component.set('v.loadMoreStatus', 'Please scroll down to load more data');
            }
            event.getSource().set("v.isLoading", false);
        }));
    },
    insertCallDate: function (component, event, helper) {
        helper.insertDate(component, "v.newTask.ActivityDate");
    },
    insertFollowupTaskDate: function (component, event, helper) {
        helper.insertDate(component, "v.newFollowupTask.ActivityDate");
    },
    handleKeepSelected: function (component, event, helper) {
        if (component.get("v.keepRowSelected")) {
            component.set("v.keepRowSelected", false);
        } else {
            component.set("v.keepRowSelected", true);
        }
    },
    handleLookupCmpEvent: function (component, event, helper) {
        var selectedObj = event.getParam("selItem");
        var lookupFieldName = event.getParam("lookupFieldName");
        var selVal = '';
        if (selectedObj != null) {
            selVal = selectedObj.val;
        }
        if (lookupFieldName == 'AccompaniedPhysician') {
            if (component.get("v.namespace") != null && component.get("v.namespace") != '') {
                helper.setLookupFieldVal(component, "v.newTask." + component.get("v.namespace") + "AccompaniedPhysician__c", selVal);
            }
            else {
                helper.setLookupFieldVal(component, "v.newTask.AccompaniedPhysician__c", selVal);
            }
        } else if (lookupFieldName == 'AssignedTo') {
            helper.setLookupFieldVal(component, "v.newFollowupTask.OwnerId", selVal);
        } else if (lookupFieldName == 'CallAssignedTo') {
            helper.setLookupFieldVal(component, "v.newTask.OwnerId", selVal);
        } else {
            helper.setLookupFieldVal(component,"v.newTask."+lookupFieldName,selVal);
        }
    },
    handleFilterChange: function (component, event, helper) {
        helper.getFilterOptions(component, helper);
    },
    handleFilterOptionChange: function (component, event, helper) {
        if (component.find("filterOpt").get("v.value") != null) {
            component.set("v.selectedTerritory", component.find("filterOpt").get("v.value"));
            helper.getProvider(component, event, '', false);
        }
    },
    changeAsset: function (component, event, helper) {
        if (component.find("taskAssetSelectItem").get("v.value") != null) {
            if (component.get("v.namespace") != null && component.get("v.namespace") != '') {
                component.set("v.newTask." + component.get("v.namespace") + "Asset__c", component.find("taskAssetSelectItem").get("v.value"));
            } else {
                component.set("v.newTask.Asset__c", event.getParam("value"));
            }
        }
    },
    changeFollowUpAsset: function (component, event, helper) {
        if (component.find("assetSelectItem").get("v.value") != null) {
            if (component.get("v.namespace") != null && component.get("v.namespace") != '') {
                component.set("v.newFollowupTask." + component.get("v.namespace") + "Asset__c", component.find("assetSelectItem").get("v.value"));
            } else {
                component.set("v.newFollowupTask.Asset__c", event.getParam("value"));
            }
        }
    },
    handlefieldChanges : function(component, event, helper) {
        if(event.getSource().get('v.value') != null){
            if(event.getSource().get('v.name') == 'IntelDescription'){
                if (component.get("v.namespace") != null && component.get("v.namespace") != '') {
                    component.set("v.newTask." + component.get("v.namespace") + "IntelDescription__c", event.getSource().get('v.value'));
                } else {
                    component.set("v.newTask.IntelDescription__c", event.getSource().get('v.value'));
                }
            } else if(event.getSource().get('v.name') == 'Department'){
                if (component.get("v.namespace") != null && component.get("v.namespace") != '') {
                    component.set("v.newTask." + component.get("v.namespace") + "Department__c", event.getSource().get('v.value'));
                } else {
                    component.set("v.newTask.Department__c", event.getSource().get('v.value'));
                }
            } else if(event.getSource().get('v.name') == 'Intel'){
                if (component.get("v.namespace") != null && component.get("v.namespace") != '') {
                    component.set("v.newTask." + component.get("v.namespace") + "Intel__c", event.getSource().get('v.value'));
                } else {
                    component.set("v.newTask.Intel__c", event.getSource().get('v.value'));
                }
            } else {
                component.set("v.newFollowupTask."+event.getSource().get("v.name"), event.getSource().get("v.value"));
            }
        }
    },
    handleDateChange : function(component, event, helper){
        component.set("v.newTask."+event.getSource().get("v.name"), event.getParam("value"));
        component.set("v.newFollowupTask."+event.getSource().get("v.name"), event.getParam("value"));
    },
    handleBooleanChange : function(component, event, helper){
        component.set("v.newTask."+event.getSource().get("v.name"),  event.getSource().get("v.checked"));
        component.set("v.newFollowupTask."+event.getSource().get("v.name"),  event.getSource().get("v.checked"));
    },
    handlePicklistChange : function(component, event, helper){
        component.set("v.newTask."+event.getSource().get("v.name"), event.getSource().get("v.value"));
        component.set("v.newFollowupTask."+event.getSource().get("v.name"), event.getSource().get("v.value"));
    },
    handleMultiPicklistChange: function(component, event, helper){
        component.set("v.newTask."+event.getSource().get("v.name"), event.getSource().get("v.value"));
        component.set("v.newFollowupTask."+event.getSource().get("v.name"), event.getSource().get("v.value"));
         },
    
    setObjects : function(component, event, helper){
        var defaultFldList = component.get("v.defaultFldList");
        for(var key in defaultFldList){
            if(defaultFldList[key].relObjs.length > 0){
                defaultFldList[key].selItem = {};
            }
        }
        component.set("v.defaultFldList",defaultFldList);   
    }
})