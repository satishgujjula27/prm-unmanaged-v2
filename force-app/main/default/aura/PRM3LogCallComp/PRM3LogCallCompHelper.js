({
    doInitHelper : function(component, selectedTab) { 
        let self = this, doInitArray = new Object();
        component.set("v.spinner",true);
        doInitArray['selectedTab'] = selectedTab;
        self.doServerCall(component, 'getActivityList', doInitArray, 'doInitHelper', false).then(
            function(doInitHelperdetail) {
                var detail = JSON.parse(doInitHelperdetail);
                if(detail.defaultFldList != null){
                    for(var i=0;i<detail.defaultFldList.length;i++){
                        if(detail.defaultFldList[i].defaultValue != null){
                            component.set("v.newTask."+detail.defaultFldList[i].apiName, detail.defaultFldList[i].defaultValue);
                        }
                    }
                }
                 for(var key in detail.defaultFldList){
                    if(detail.defaultFldList[key].relObjs != '' && detail.defaultFldList[key].relObjs != null && detail.defaultFldList[key].relObjs.length != null && detail.defaultFldList[key].relObjs.length > 0){
                        component.set("v.selectedObject",detail.defaultFldList[key].relObjs[0]);
                        break;
                    }                   
                }
                
                component.set("v.activityLists",detail.actvtListWrapper);
                if(detail.actvtListWrapper === undefined || detail.actvtListWrapper < 1) { } 
                else {
                    Object.keys(detail.actvtListWrapper).forEach(function(prop) {
                        if( detail.actvtListWrapper[prop]["Id"] == detail.defaultActTabs ) {
                            component.set("v.currentActiveTabName", detail.actvtListWrapper[prop]["label"]);
                        }
                    });
                }
                component.set("v.defaultFldList",detail.defaultFldList);
                component.set("v.defaultActTabs",detail.defaultActTabs);
                component.set("v.defaultActTabsSet",detail.defaultActTabs);
                
                component.set("v.spinner",false);
            }
        );
    },
    getPicklistValues: function (component, event) {
        var getPicklist = component.get("c.getPicklistValues");
        getPicklist.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                if (response.getReturnValue() != null) {
                    let res = response.getReturnValue();
                    let assetoptions = [];
                    for (var i in res.asset) {
                        let v = { label: res.asset[i], value: res.asset[i] };
                        assetoptions.push(v);
                    }
                    component.set("v.assetOptions", assetoptions);

                    let statusoptions = [];
                    for (var i in res.status) {
                        let v = { label: res.status[i], value: res.status[i] };
                        statusoptions.push(v);
                    }
                    component.set("v.statusOptions", statusoptions);

                    let priorityoptions = [];
                    for (var i in res.priority) {
                        let v = { label: res.priority[i], value: res.priority[i] };
                        priorityoptions.push(v);
                    }
                    component.set("v.priorityOptions", priorityoptions);
                    
                     let intelOptions = [];
                    for(var i in res.intel) {
                        let v = {label:res.intel[i],value:res.intel[i]};
                        intelOptions.push(v);
                    }
                    component.set("v.intelOptions", intelOptions);
                    component.set("v.filterString", res.filterString);
                }
            }
            else {
                console.log("Failed with state " + state);
            }
        });
        $A.enqueueAction(getPicklist);
    },
    quickSearch: function (component, event, helper) {
        helper.getProvider(component, event, component.find("providerSearchBox").get("v.value"), false);
    },
    validateRequiredField: function (component, newTask, isLogCall, newFollowUpTask, isFollowUpTask) {
        if(isLogCall){
            if (newTask.OwnerId === "") 
                return false;
        }
        if (isFollowUpTask) {
            if (newFollowUpTask.OwnerId === "" || newFollowUpTask.Status == undefined || newFollowUpTask.Status === "" ||
                newFollowUpTask.Priority == undefined || newFollowUpTask.Priority === "") 
                return false;
        }
        return true;
    },
    getProvider: function (component, event, searchKey, isAlphaSearch) {
        component.set("v.spinner", true);
        return new Promise($A.getCallback(function (resolve, reject) {
            let getProvider;
            let filterBy =  component.get("v.filterBy");
            if (filterBy == 'territory') { getProvider = component.get("c.getProviders"); }
            if (filterBy == 'practice') { getProvider = component.get("c.getProviders2"); }
            getProvider.setParams({
                "searchKey": searchKey,
                "isAlphaSearch": isAlphaSearch,
                "filterBy": filterBy,
                "filterOpt": component.get("v.selectedTerritory"),
                "sortField": component.get("v.sortedBy"),
                "sortDirection": component.get("v.sortedDirection")
            });
            getProvider.setCallback(this, function (response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    var result = JSON.parse(response.getReturnValue());
                    resolve(response.getReturnValue());
                    if (result != null) {
                        let fieldObj = result.fieldsWrapper;
                        let resultData;
                        if (filterBy == 'territory') { resultData = result.resultWrapper; } 
                        if (filterBy == 'practice') { resultData = result.resultWrapper2; }
                        let headerFields = [];
                        for (var i in fieldObj) {
                            let v = {
                                label: fieldObj[i].label, fieldName: fieldObj[i].linkfieldName, type: fieldObj[i].fieldType, sortable: fieldObj[i].sortable,
                                typeAttributes: {
                                    label: { fieldName: fieldObj[i].fieldName }
                                }
                            };
                            headerFields.push(v);
                        }
                        component.set('v.mycolumns', headerFields);
                        if (result.providerEntityName != null && result.providerEntityName != 'null') {
                            component.set('v.showAccompained', true);
                            component.set('v.providerEntityName', result.providerEntityName);
                        }
                        var providerListIds = component.get("v.providerList");
                        var idArr = [];
                        if (providerListIds != '') {
                            idArr = providerListIds.split(",");
                        }
                        // start of sorting data on provider selection
                        var tempArray = [];
                        for (i = 0; i < idArr.length; i++) {
                            var obj = resultData.find(x => x.Id == idArr[i]);
                            if (obj !== undefined)
                                tempArray.push(obj);
                        }
                        tempArray.filter((item, index) => {
                            var index = resultData.indexOf(item);
                            resultData.splice(index, 1)
                        });
                        var newArr = tempArray.concat(resultData);
                        component.set("v.providerLocations", newArr);
                        // end of sorting data on provider selection
                        if (idArr.length > 0) {
                            component.set("v.keepSelection", idArr);
                            component.set("v.selectedProvider", idArr);
                        }
                    }
                } else {
                    console.log("Failed with state " + state);
                }
                component.set("v.spinner", false);
            });
            $A.enqueueAction(getProvider);
        }));
    },
    getMoreProviders: function (component, rows) {
        return new Promise($A.getCallback(function (resolve, reject) {
            var getMoreProvider = component.get("c.getProviders");
            var recordOffset = 3;
            var recordLimit = component.get("v.initialRows");
            getMoreProvider.setParams({
                "searchKey": '', "isAlphaSearch": false
            });
            getMoreProvider.setCallback(this, function (response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    var resultData = response.getReturnValue();
                    resolve(resultData);
                    recordOffset = recordOffset + recordLimit;
                    component.set("v.currentCount", recordOffset);
                }
            });
            $A.enqueueAction(getMoreProvider);
        }));
    },
    getTotalNumberOfProvider: function (component) {
        var action = component.get("c.getTotalProviders");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var resultData = response.getReturnValue();
                component.set("v.totalNumberOfRows", resultData);
            }
        });
        $A.enqueueAction(action);
    },
    setParams: function (component, event) {
        var today = $A.localizationService.formatDate(new Date(), "MM/d/yyyy");
        component.set("v.today", today);

        //accepting isTask from bulk action
        let isTask = component.get("v.isTask");
        if (isTask == 1) {
            component.set("v.isFollowUpTask", true);
            component.set("v.isLogCall", false);
        }
    },
    insertDate: function (component, field) {
        var today = $A.localizationService.formatDate(new Date(), "MMM d, yyyy");
        component.set(field, $A.localizationService.formatDate(new Date(), "yyyy-MM-dd"));
    },
    setLookupFieldVal: function (component, field, val) {
        component.set(field, val);
    },
    setMessage: function (component, showMessage, message, messageType) {
        component.set("v.showMessage", showMessage);
        component.set("v.message", message);
        component.set("v.messageType", messageType);
    },
    showMessage: function (component, showMessage, message, messageType) {
        component.set("v.showToast", showMessage); component.set("v.toastTheme", 'slds-theme_'+messageType);
        component.set("v.toastIcon", 'utility:'+messageType); component.set("v.toastMessage", message);
        window.setTimeout(function() { component.set("v.showToast", false); }, 2000);
    },
    getFilterOptions: function (component, helper) {
        var filterType = component.get("v.filterBy");
        var getFilterOptions = component.get("c.getFilterOptions");
        getFilterOptions.setParams({ 'filterType': filterType });
        getFilterOptions.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                let filterOptRes = JSON.parse(response.getReturnValue());
                if (filterOptRes != null) {
                    let filterOptions = [];
                    for (var i in filterOptRes) {
                        let v = { label: filterOptRes[i].label, value: filterOptRes[i].value };
                        filterOptions.push(v);
                    }
                    component.set("v.filterOptions", filterOptions);
                    helper.setFilterByLabel(component, filterType);
                }
            }
            else {
                console.log("Failed with state " + state);
            }
        });
        $A.enqueueAction(getFilterOptions);
    },
    setFilterByLabel: function (component, filterType) {
        var fLabel = '';
        component.get("v.filterByOption").filter((item) => {
            if (item.value == filterType) {
                fLabel = item.label;
            }
        });
        component.set("v.filterLabel", fLabel);
    },
    getLoggedInUser: function (component) {
        var self = this;
        var getLoggedInUserAction = component.get("c.getLoggedInUser");
        getLoggedInUserAction.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                if (response.getReturnValue() != null) {
                    let res = response.getReturnValue();
                    component.set("v.selItem2", { 'text': res.Name });
                    self.setLookupFieldVal(component, "v.newTask.OwnerId", res.Id);
                    component.set("v.selItem1", { 'text': res.Name });
                    self.setLookupFieldVal(component, "v.newFollowupTask.OwnerId", res.Id);
                }
            }

        });
        $A.enqueueAction(getLoggedInUserAction);
    },
    doServerCall : function(component, apexAction, params, callFromMethod, pushToEvent) {
        var self = this;
        try {
            return new Promise(function(resolve, reject) {
                var action = component.get("c."+apexAction+"");
                if( params != null || typeof params != "undefined" ) {
                    action.setParams( params );
                }
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        resolve( response.getReturnValue() );
                    }
                });
                $A.enqueueAction(action);
            });
        } catch(e) {
            console.log("Failed to load data from server - "+callFromMethod);
        }
    },

    validateRequiredActivityField : function(component){
        var isValid = true;
        var fieldApiName = [];
        var fields = component.get("v.defaultFldList");
        fields.forEach(function (field) {
            if(field.isRequired && $A.util.isEmpty(component.get("v.newTask."+field.apiName))){
                fieldApiName.push(field.apiName);
            }
        })
        fieldApiName.length !== 0 ? isValid = false : isValid = true;
        return isValid; 
    }
})