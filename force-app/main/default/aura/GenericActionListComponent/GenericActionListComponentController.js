({
	handleActionClick : function(component, event, helper) {
        let name = component.get("v.compName");
        let device = $A.get("$Browser.formFactor");
        let isModal = component.get("v.isModal");
        if ( device === "PHONE" && component.get("v.isRenderSalesforce1") == true ) {
            let evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef : name,
                componentAttributes: {
                    recordId : component.get("v.recordId"),
                    isRenderSalesforce1 : true
                }
            });
            evt.fire();
        } else if ( isModal == false ) {
            let urlEvent = $A.get("e.force:navigateToURL");
            let url = component.get('v.url');
            url = url.replace('{c__recordId}',component.get("v.recordId"));
            urlEvent.setParams({
                "url": url
            });
            urlEvent.fire();
        } else {
            component.set("v.showModal", true);
            name = name.replace('HGPRM:logActivityModal', 'HGPRM:logActivityModal');
            $A.createComponent(name,{'recordId':component.get("v.recordId"),onclosemodal:component.getReference('c.closeModal')},
                               function(comp, status, errorMessage) {
                                   if (component.isValid() && status === 'SUCCESS') {
                                       component.set("v.dynamicComp",comp);
                                   }
                               });
        }
	},
    closeModal : function(component, event, helper){
        component.set("v.showModal",false);
    }
})