({
	handleCancel : function(component, event, helper) {
		component.find("overlayLib").notifyClose();
	},
    handleDelete : function(component, event, helper) {
        var appEvent = $A.get("e.c:ConfirmDeleteModalEvent");
        appEvent.setParams({ "deleteId" : component.get("v.recId") });
        appEvent.fire();
		component.find("overlayLib").notifyClose();
	}
})