({
    doInit : function(component, event, helper) {
        var action = component.get("c.getProviderLocations");
        var recordId = component.get("v.recordId");
        var map = component.get("v.map");
        var starterZoomLevel = 5;
        let filterIds = component.get("v.filterIds");
        action.setParams({ 
            territoryId : recordId, 
            "filterIds":JSON.stringify(filterIds),
            "filterByType":component.get("v.filterByType")
        });
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                var result = JSON.parse(a.getReturnValue());
                if(result !=null){
                    var providers = result.listProvLocWrapper;
                    var markerFields = [];
                    markerFields.push("Id");
                    markerFields.push("ProviderName");
                    markerFields.push("Latitude");
                    markerFields.push("providerLink");
                    markerFields.push("LocationName");
                    markerFields.push("Longitude");
                    markerFields.push("practiceGroupName");
                    
                    var markerTemplate = '<b><a href="/' + 'providerLink" target="_blank">' + 'ProviderName </a></b>'+ 
                        '<br/>practiceGroupName'+
                    	'<br/>LocationName';
                    $A.createComponent(
                        "c:LeafletMap", {
                            "mapHeight" : "600vh", "recordString" : JSON.stringify(providers),
                            "latField" : "Latitude", "lngField" : "Longitude",
                            "showSearch" : false, "showDraw" : true,
                            "recordId" : recordId, "geoJsonField" :result.geoFieldName,
                            "saveRecord" : result.saveObject,
                            "useCluster" : true, "markerFields" : markerFields,
                            "markerTemplate" : markerTemplate, "enableMarkerForSelection":true,
                            "showMarkerOnPopup": true, "searchResultData" : providers,
                            "latitudeVarName":"Latitude", "longitudeVarName":"Longitude",
                            "selectedIds":component.get("v.selectedIds")
                        },
                        function(lMap, status, errorMessage){
                            //Add the new button to the body array
                            if (status === "SUCCESS") {
                                var body = component.get("v.body");
                                body.push(lMap);
                                component.set("v.body", body);
                            } else if (status === "INCOMPLETE") {
                                console.log("No response from server or client is offline.")
                                // Show offline error
                            } else if (status === "ERROR") {
                                console.log("Error: " + errorMessage);
                                // Show error message
                            }
                        }
                    );
                }
            }
        });
        $A.enqueueAction(action);
    }
})