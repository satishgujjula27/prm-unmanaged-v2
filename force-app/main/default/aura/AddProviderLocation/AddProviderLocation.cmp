<!--
* Healthgrades, inc. claims copyright in this software, its screen
* display designs and supporting documentation. Healthgrades and Healthgrades corporation
* are trademarks of Healthgrades, inc. Any unauthorized use, copying
* or sale of the above may constitute an infringement of copyright
* and may result in criminal or other legal proceedings.
*
@Component Name: AddProviderLocation
@description: This component is used to Associate Location to Provider
@Author: Healthgrades Inc.
@Last Modified Date: 01/19/2021
-->
<aura:component controller="AddProviderLocationController"
                implements="force:appHostable,force:hasRecordId,lightning:isUrlAddressable" description="This component is used in Provider 360 Layout's Provider Location related list to add locations to provider." access="global" >
    
    <!--Attribute definitions-->
    <aura:attribute name="recordId" type="String" />
    <aura:attribute name="data" type="Object"/>
    <aura:attribute name="columns" type="List"/>
    <aura:attribute name="pageReference" type="Object"/>
    <aura:attribute name="spinner" type="boolean" default="false" />
    <aura:attribute name="isPracticeAssociated" type="boolean" default="true" />
    <aura:attribute Name="selItem" type="object"  access="public" description="to pass this selection attribute for accompained loopkup field"/>
    <aura:attribute name="ProvPractGroupObj" type="Sobject"/>
    <aura:attribute name="selectedRows" type="List" default="[]"/>
    <aura:attribute name="provLocObjName" type="string"/>
    <aura:attribute name="provPractObjName" type="string"/>
    <aura:attribute name="practiceObjName" type="String" />
    <aura:attribute name="providerName" type="String" />
    <aura:attribute name="selectedPracticeId" type="string"/>
    <aura:attribute name="isPrimaryLocId" type="string"/>
    <aura:attribute name="isPracticePrimary" type="boolean" default="false"/>
    <aura:attribute name="totalRecords" type="Integer" default="0"/>
    <aura:attribute name="pageNumber" type="Integer" default="1"/>
    <aura:attribute name="pageSize" type="String" default="50"/>
    <aura:attribute name="dataSize" type="Integer" default="0"/>
    
    <!--handlers-->
    <aura:handler name="init" value="{!this}" action="{!c.doInit}"/>
    <aura:handler name="change" value="{!v.pageReference}" action="{!c.onPageReferenceChange}"/>
    <aura:handler name="olookupSelectedItemEvent" event="c:lookupSelectedItemEvent" action="{!c.handleLookupCmpEvent}"/>
    <aura:handler name="paginationEvent" event="c:PaginationComponentEvent" action="{!c.handlePaginationEvent}"/>
    
    <div class="main-content-apl">        
        <!-- header -->
        <div class="slds-col slds-has-flexi-truncate headerFixed">
            <div class="slds-page-header">
                <div class="slds-grid">
                    <div class="slds-media slds-no-space slds-grow">
                        <div class="slds-media__body slds-var-m-top_medium">
                            <nav role="navigation" aria-label="Breadcrumbs">
                                <ol class="slds-breadcrumb slds-list_horizontal">
                                    <li class="slds-breadcrumb__item slds-text-title_caps">
                                        <a onclick="{!c.gotoSobject}">
										PROVIDERS</a>
                                    </li>
                                    <li class="slds-breadcrumb__item slds-text-title_caps">
                                        <a href="{!'/lightning/r/'+v.recordId+'/view'}">{!v.providerName}</a>
                                    </li>
                                </ol>
                            </nav>
                            <h1 class="slds-page-header__title slds-var-m-right_small slds-align-middle slds-truncate" title="Add Provider Locations">Add Provider Locations</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <aura:if isTrue="{!v.isPracticeAssociated}">
            <div class="slds-wrap slds-theme_default">
                <div class="slds-align_absolute-center btn-section">
                    <lightning:button label="Save" onclick="{!c.handleAddProviderLocation}" variant="brand" disabled="{!not(v.selectedRows.length)}"/>
                    <lightning:button label="Cancel" onclick="{!c.handleCancel}"/>
                </div>
                <div class="paginationComp slds-var-p-bottom_small">
                    <c:PaginationComponent totalRecords="{!v.totalRecords}" dataSize="{!v.dataSize}" aura:id="paginationCmp"/>
                </div>
                <div>
                    <lightning:datatable class="slds-table--bordered slds-max-medium-table--stacked" aura:id="AddProvLoc" keyField="Id" loadMoreOffset="50"
                                         data="{! v.data }" columns="{! v.columns }"
                                         onrowselection="{! c.handleRowAction }"
                                         onrowaction = "{! c.handleRowAction1}" />
                </div>
            </div>
            <aura:set attribute="else">
                <span>
                    <h1 class="header-position slds-text-heading_small">Before adding locations, you must associate this provider with at least one practice.</h1>
                </span>
                <div class="slds-grid slds-wrap slds-theme_default pp-form">
                    <div class="slds-col slds-var-p-horizontal_medium slds-var-p-bottom_small slds-size_2-of-3">
                        <label for="input-20" class="slds-form-element__label">Practice &amp; Group<span class="required">*</span></label>
                        <c:LookupComponent objectName="{!v.practiceObjName}" fieldAPIText="Name" fieldAPIVal="Id" limit="4"
                                           fieldAPISearch="Name" lookupIcon="standard:contact"
                                           selItem="{!v.selItem}" placeholder="Search Practice &amp; Group..."
                                           lookupFieldName="PracticeGroup" />
                    </div>
                    <div class="slds-col slds-var-p-horizontal_medium slds-var-p-bottom_small slds-size_2-of-3">
                        <lightning:input class="checkbox-align" label="Primary"
                                         name="Primary" type="checkbox" onchange="{!c.handlePrimaryCheck}" />
                    </div>
                    <div class="slds-col slds-var-p-horizontal_medium slds-size_2-of-3 slds-align_absolute-center btn-section">
                        <lightning:button label="Cancel" onclick="{!c.handleBack}"/>
                        <lightning:button label="Save" onclick="{!c.associatePracticeGroup}" variant="brand"/>
                    </div>
                </div>
            </aura:set>
        </aura:if>
    </div>
    <aura:if isTrue="{!v.spinner}">
        <div style="position:relative; margin: 0 auto; top: 3vh;" class="slds-align_absolute-center">
            <lightning:spinner alternativeText="Loading" size="small" variant="brand" />
        </div>
    </aura:if>
</aura:component>