({
    doInitHelper : function(component, event, helper) { 
        let self = this;
        component.set("v.spinner",true);
        let getLocationsObj = new Object();
        getLocationsObj["recordId"] = component.get("v.recordId");
        getLocationsObj["pageSize"] = component.get("v.pageSize");
        getLocationsObj["pageNumber"] = component.get("v.pageNumber");
        self.doServerCall(component, 'getLocations', getLocationsObj, 'doInitHelper', true).then(
            function(getLocationdetail) {
                var result = JSON.parse(getLocationdetail);
                if(result != null) {
                    if(result.practiceObjName !=null) {
                        component.set("v.practiceObjName", result.practiceObjName);
                    }
                    if(result.providerName !=null) {
                        component.set("v.providerName",result.providerName);
                    }
                    if(result.isPracticeAssociated !=null){
                        component.set('v.isPracticeAssociated',result.isPracticeAssociated);
                    }
                    if(result.listTableWrapperFset!=null){
                        var columns=[];
                        columns.push({type: 'button-icon', label : 'PRIMARY', typeAttributes: {name: 'primary', iconName: {fieldName: 'iconName'}, disabled: {fieldName: 'disabledValue'},  alternativeText: 'check to set Provider Location Primary'}});
                        for(var i=0;i<result.listTableWrapperFset.length;i++){
                            columns.push({label: result.listTableWrapperFset[i].label, fieldName: result.listTableWrapperFset[i].fieldName, type: 'text'});
                        }
                        
                        component.set('v.columns',columns);
                        component.set('v.data',result.listLocWrapper);
                        component.set("v.totalRecords", result.totalRec);
                        component.set("v.dataSize", result.listLocWrapper.length);
                        self.setPaginationParam(component,result.listLocWrapper);
                        self.resetIsPrimary(component);
                    }
                    component.set("v.spinner",false);
                }
            }
        );
    },
    handleSave : function(component){
        var self = this; var selLoc = component.get("v.selectedRows");
        if(selLoc.length >0){
            component.set("v.spinner", true);
            var saveProvLocActionObj = new Object();
            saveProvLocActionObj["recordId"] = component.get("v.recordId");
            saveProvLocActionObj["locations"] = selLoc;
            saveProvLocActionObj["isPrimaryLocId"] = component.get("v.isPrimaryLocId");
            self.doServerCall(component, 'saveProviderLocation', saveProvLocActionObj, 'handleSave', true).then(
                function(handleSavedetail) {
                    self.showMessage("Success!", 'Provider Locations added successfully', 'success');
                    self.navigateToProviderPage(component);
                    component.set("v.spinner",false);
                }
            );
        } else {
            self.showMessage("No Record Selected!", 'First Select some record!', 'warning');
        }
    },
    associatePracticeGroup : function(component){
        var self = this;
        if(component.get("v.selectedPracticeId") !=null && component.get("v.selectedPracticeId") !=''){
            component.set("v.spinner",true);
            var provPractAttr = {};
            provPractAttr['PracticeGroup'] = component.get("v.selectedPracticeId");
            provPractAttr['LocationIsPrimary'] = component.get("v.isPracticePrimary");
            
            var saveProvPractActionObj = new Object();
            saveProvPractActionObj["recordId"] = component.get("v.recordId");
			saveProvPractActionObj["provPractAttr"] = provPractAttr;
            self.doServerCall(component, 'saveProviderPractice', saveProvPractActionObj, 'handleSave', true).then(
                function(handleSavedetail) {
                    self.doInitHelper(component);
                    component.set("v.spinner",false);
                }
            );
        } else {
            self.showMessage("Practice & Group not selected!", 'Select Practice & Group', 'error');
        }
    },
    showMessage : function(messageTitle,messageBody,messageType){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ "title": messageTitle, "type":messageType, "message": messageBody });
        toastEvent.fire();
    },
    navigateToProviderPage : function(component){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({ "recordId": component.get("v.recordId"), "slideDevName": "Detail" });
        navEvt.fire();
    },
    changeActionIcons : function (component, rowId) {
        var data = component.get('v.data');
        var selectedRows = component.get('v.selectedRows');
        data = data.map(function(rowData) {
            if (rowData.Id === rowId) {
                rowData.iconName = 'utility:check';
                rowData.disabledValue = false;
                component.set("v.isPrimaryLocId",rowData.Id);
            } else {
                rowData.iconName = '';
                if(selectedRows.includes(rowData.Id)) {
                    rowData.disabledValue = false;
                } else {
                    rowData.disabledValue = true;
                }
            }
            return rowData;
        });
        component.set("v.data", data);
    },
    unCheckIsPrimary : function (component, rowId) {
        var data = component.get('v.data');
        data = data.map(function(rowData) {
            if (rowData.Id === rowId) { rowData.iconName = ''; rowData.disabledValue = true; }
            return rowData;
        });
        component.set("v.data", data);
    },
    resetIsPrimary : function (component) {
        var data = component.get('v.data');
        data = data.map(function(rowData) {
            rowData.iconName = ''; rowData.disabledValue = true; return rowData;
        });
        component.set("v.data", data);
    },
    enableIsPrimary : function (component,selectedRow) {
        var isPrimaryId = component.get("v.isPrimaryLocId");
        var data = component.get('v.data');
        data = data.map(function(rowData) {
            if(rowData.Id === isPrimaryId) {
                rowData.iconName = 'utility:check';
            } else {
                rowData.iconName = '';
            }
            if(selectedRow.includes(rowData.Id)) {
                rowData.disabledValue = false;
            } else {
                rowData.disabledValue = true;
            }
            return rowData;
        });
        component.set("v.data", data);
    },
    doServerCall : function(component, apexAction, params, callFromMethod, resolveResponse) {
        var self = this;
        try {
            return new Promise(function(resolve, reject) {
                var action = component.get("c."+apexAction+"");
                if( params != null || typeof params != "undefined" ) {
                    action.setParams( params );
                }
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        if( resolveResponse && response.getReturnValue() != null ) {
                            resolve( response.getReturnValue() );   
                        } else if( response.getReturnValue() != null ) {
                            response.getReturnValue();
                        } else {
                            component.set("v.spinner",false);
                        }
                    } else {
                        console.log('response.getState() - response.getError()*******', response.getError());
                    }
                });
                $A.enqueueAction(action);
            });
        } catch(e) {
            console.log("Failed to load data from server - "+callFromMethod);
        }
    },
    setPaginationParam : function(component,resultData){
        var paginationCmp = component.find("paginationCmp");
        var showRecCountTextMsg = '';
        if(component.get("v.pageNumber") >0  && component.get("v.pageSize") >0) {
            let start = ((component.get("v.pageNumber") * component.get("v.pageSize"))- component.get("v.pageSize"))+1;
            let end = '';
            if(component.get("v.pageSize") > component.get("v.totalRecords")) {
                end = component.get("v.totalRecords");   
            } else {
                end = component.get("v.pageNumber") * component.get("v.pageSize");
                if(end > component.get("v.totalRecords")) {
                    end = start + component.get("v.dataSize") -1;
                }
            }
            showRecCountTextMsg = start+" - "+end+" of "+component.get("v.totalRecords");
        }
        
        if(resultData.length < parseInt(component.get("v.pageSize")) || parseInt(component.get("v.pageSize")) == component.get('v.totalRecords') || (component.get("v.pageNumber") * component.get("v.pageSize")) == component.get('v.totalRecords') ){
            component.set("v.isLastPage", true);
            paginationCmp.paginationMethod(true,showRecCountTextMsg);
        } else {
            component.set("v.isLastPage", false);
            paginationCmp.paginationMethod(false,showRecCountTextMsg);
        }
    }
})