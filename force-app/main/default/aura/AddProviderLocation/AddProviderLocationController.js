({
    onPageReferenceChange: function(component, event, helper) {
        var myPageRef = component.get("v.pageReference");
        var provRecordId = myPageRef.state.c__recordId;
        component.set("v.pageNumber", "1");
        component.set("v.pageSize", "50");
        component.set("v.recordId", provRecordId);
        helper.doInitHelper(component, event, helper);
    },
	doInit : function(component, event, helper) {
		let pageReference = component.get("v.pageReference");
        if(pageReference != null && pageReference.state != null) {
            component.set("v.recordId", pageReference.state.c__recordId);
        }
        helper.doInitHelper(component, event, helper);
        component.set("v.spinner",true);
	},
    handleBack : function(component, event, helper){
        window.history.back();
    },    
    handleCancel : function(component,event,helper){
        helper.navigateToProviderPage(component);
    },
    handleRowAction :function(component,event,helper){
        var selRows = event.getParam('selectedRows');
        var allSelectedRows = [];   
        selRows.forEach(function(row) {
            if(!allSelectedRows.includes(row.Id)) {
                allSelectedRows.push(row.Id);
            }
        });
        if(component.get("v.isPrimaryLocId") !=null && !allSelectedRows.includes(component.get("v.isPrimaryLocId"))) {
            helper.unCheckIsPrimary(component,component.get("v.isPrimaryLocId"));
            component.set("v.isPrimaryLocId","");
        }
        if(allSelectedRows.length > 0) {
            helper.enableIsPrimary(component,allSelectedRows);
        } else if(allSelectedRows.length === 0) {
            helper.resetIsPrimary(component);
        }
        component.set("v.selectedRows",allSelectedRows);
    },
    handleAddProviderLocation : function(component, event, helper){
        helper.handleSave(component);
    },
    associatePracticeGroup : function(component, event, helper){
        helper.associatePracticeGroup(component);
    },
    handleLookupCmpEvent : function(component, event, helper){
        var selectedObj = event.getParam("selItem");
        var lookupFieldName = event.getParam("lookupFieldName");
        var selVal = '';
        if(selectedObj !=null) { selVal = selectedObj.val; }
        if(lookupFieldName == 'PracticeGroup') {
           component.set("v.selectedPracticeId",selVal);
        }
    },
    handleRowAction1: function (component, event, helper){
        var action = event.getParam('action');
        var row = event.getParam('row');
        helper.changeActionIcons(component,row.Id);
    },
    gotoSobject : function(component, event, helper){
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({ "scope": component.get("v.providerObjName") });
        homeEvent.fire();
    },
    handlePrimaryCheck : function(component, event, helper){
        if(component.get("v.isPracticePrimary") == true) {
            component.set("v.isPracticePrimary", false);
        } else {
            component.set("v.isPracticePrimary", true);
        }
    },
    handlePaginationEvent : function(component,event,helper){
        component.set("v.pageNumber",event.getParam("pageNumber"));
        component.set("v.pageSize",event.getParam("pageSize"));
        helper.doInitHelper(component);
    }
})