({
    firePaginationEvent : function(component) {
        var pgEvent = component.getEvent("paginationEvent");
        pgEvent.setParams({ "pageNumber" :  component.get("v.pageNumber"), "pageSize" :  component.get("v.pageSize") });
        pgEvent.fire();
    }
})