({
    handleNext : function(component, event, helper) { 
        var pageNumber = component.get("v.pageNumber");
        component.set("v.pageNumber", pageNumber+1);
        helper.firePaginationEvent(component);
    },
    handleFirst : function(component, event, helper) { 
        component.set("v.pageNumber", 1);
        helper.firePaginationEvent(component);
    },
    handleLast : function(component, event, helper) {
        let totalRecords = component.get("v.totalRecords");
        let pageSize = component.get("v.pageSize");
        if(totalRecords > 0 && pageSize>0) { component.set("v.pageNumber", Math.ceil(totalRecords/pageSize)); }
        helper.firePaginationEvent(component);
    },
    handlePrev : function(component, event, helper) {        
        var pageNumber = component.get("v.pageNumber");
        component.set("v.pageNumber", pageNumber-1);
        helper.firePaginationEvent(component);
    },
    getpagiParam : function(component, event) {
        var params = event.getParam('arguments');
        if (params) {
            var param1 = params.isLastPage;
            var param2 = params.showRecCountText;
            component.set("v.isLastPage", param1);
            component.set("v.showRecCountText", param2);
        }
    },
    resetPageParam: function(component, event) {
        var params = event.getParam('arguments');
        if (params) {
            var param1 = params.pageNumber;
            var param2 = params.pageSize;
            component.set("v.pageNumber", param1);
            component.set("v.pageSize", param2);
        }
    }
})