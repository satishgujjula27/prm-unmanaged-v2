({
    doInit : function(component,event,helper){
        helper.getRecords(component);
    },
    onPageReferenceChange: function(component,event,helper) {
        var myPageRef = component.get("v.pageReference");
        var provRecordId = myPageRef.state.c__recordId;
        component.set("v.pageNumber", "1");
        component.set("v.pageSize", "50");
        component.set("v.isLastPage", false);
        $A.get('e.force:refreshView').fire();
        helper.getRecords(component);
    },
    navigateToObject : function(component,event,helper){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({ "recordId": component.get("v.recordId") });
        navEvt.fire();  
    },
    navigateToObjectHome : function(component,event,helper){
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({ "scope": component.get("v.homeObjectName") });
        homeEvent.fire();
    },
	logCall : function(component, event, helper) {
        let idList = component.get("v.keepSelection");
        let ids = '';
        if(idList.length >0) { ids = idList.join(','); }
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:PTLogCallCmp",
            componentAttributes: { territoryId : component.get( "v.recordId" ), ids : ids }
        });
        evt.fire();
	},
    removeSelected : function(component, event, helper) {
        let idList = component.get("v.keepSelection");
        let ids = '';
        if(idList.length >0) { ids = idList.join(','); }
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:PLTRemoveCmp",
            componentAttributes: { territoryId : component.get( "v.recordId" ), ids : ids }
        });
        evt.fire();
	},
    handleRowSelection :function(component,event,helper){
        var selRows = event.getParam('selectedRows');
        component.set("v.selectedProvider",selRows);
        var allSelectedRows = [];   
        selRows.forEach(function(row) {
            if(!allSelectedRows.includes(row.Id)) { allSelectedRows.push(row.Id); }
        });
        component.set("v.keepSelection",allSelectedRows);
    },
    handleRowAction :function(component,event,helper){
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch(action.name) {
            case "edit": helper.doEdit(row.Id); break;
            case "delete": helper.doDelete(component,row.Id); break;
        }
    },
    handleDeleteEvent : function(component,event,helper){
        var deleteAction = component.get("c.deleteRecord");
        deleteAction.setParams({ recordId : event.getParam("deleteId") });
        deleteAction.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") { $A.get('e.force:refreshView').fire(); }
        });
        $A.enqueueAction(deleteAction); 
    },
    updateColumnSorting : function(component,event,helper){
        let fieldName = event.getParam('fieldName');
        let sortDirection = event.getParam('sortDirection');
        component.set("v.sortedBy", fieldName);
        component.set("v.sortedDirection", sortDirection);
        helper.getRecords(component);
    },
    handlePaginationEvent : function(component,event,helper){
        component.set("v.pageNumber",event.getParam("pageNumber"));
        component.set("v.pageSize",event.getParam("pageSize"));
        helper.getRecords(component);
    }
})