({
    getRecords : function(component) {
        $A.util.removeClass(component.find("pageSpinner"), "slds-hide");
        let self = this; let getRecordsActionobj = new Object();
		getRecordsActionobj["recordId"] = component.get("v.recordId");
		getRecordsActionobj["recLimit"] = 0;
		getRecordsActionobj["sortBy"] = component.get("v.sortedBy");
		getRecordsActionobj["sortDirection"] = component.get("v.sortedDirection");
		getRecordsActionobj["pageSize"] = component.get("v.pageSize");
		getRecordsActionobj["pageNumber"] = component.get("v.pageNumber");
        self.doServerCall(component, 'getPLTRecord', getRecordsActionobj, 'getRecords', false).then(
            function(getRecordsdetail) {
                let result = JSON.parse(getRecordsdetail);
                if(result!=null) {
                    let resultData = result.resultWrapper;
                    let headerFields = [];
                    let actions = [
                        { label: 'Edit', name: 'edit' },
                        { label: 'Delete', name: 'delete' }
                    ];
                    if(result.fieldsWrapper != null) {
                        for(let key in result.fieldsWrapper) {
                            let columnLabel = result.fieldsWrapper[key].label;
                            if ($A.get("$Browser.formFactor") === 'PHONE') {
                                if (columnLabel === 'Location Address' || columnLabel === 'Location Phone' || columnLabel === 'Created Date') {
                                    continue;
                                }
                            }
                            let v = {label: columnLabel, fieldName: result.fieldsWrapper[key].fieldNameLink, type: result.fieldsWrapper[key].fieldType,typeAttributes: {
                                label: { fieldName: result.fieldsWrapper[key].fieldName }
                            }, sortable : result.fieldsWrapper[key].sortable};
                            headerFields.push(v);
                        }
                    }
                    let actn = { type: 'action', typeAttributes: { rowActions: actions } };
                    headerFields.push(actn);
                    component.set('v.mycolumns', headerFields);
                    component.set("v.parentRecName", result.parentRecName);
                    component.set("v.homeObjectName", result.homeObjectName);
                    component.set("v.providerLocations", resultData);
                    component.set("v.totalRecords", result.totalRec);
                    component.set("v.dataSize", resultData.length);
                    $A.util.addClass(component.find("pageSpinner"), "slds-hide");
                    self.setPaginationParam(component, resultData);
                }
            }
        );
    },
    doServerCall : function(component, apexAction, params, callFromMethod, pushToEvent) {
        var self = this;
        try {
            return new Promise(function(resolve, reject) {
                var action = component.get("c."+apexAction+"");
                if( params != null || typeof params != "undefined" ) {
                    action.setParams( params );
                }
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        resolve( response.getReturnValue() );
                    }
                });
                $A.enqueueAction(action);
            });
        } catch(e) {
            console.log("Failed to load data from server - "+callFromMethod);
        }
    },
    doEdit : function(recordId){
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({ "recordId": recordId });
        editRecordEvent.fire();
    },
    doDelete : function(component,recId){
        $A.createComponent("c:ConfirmMessageCmp", {recId:recId},
                           function(content, status) {
                               if (status === "SUCCESS") {
                                   var modalBody = content;
                                   component.find('overlayLib').showCustomModal({
                                       header: "Delete Provider Location Territory",
                                       body: 'Are you sure you want to delete this Provider Location Territory?',
                                       footer:modalBody,
                                       showCloseButton: true,
                                       cssClass : 'detail slds-text-align_center',
                                       closeCallback: function(ovl) { }
                                   }).then(function(overlay){ });
                               }
                           });
    },
    setPaginationParam : function(component, resultData){
        var paginationCmp = component.find("paginationCmp");
        var showRecCountTextMsg = '';
        if(component.get("v.pageNumber") >0 && component.get("v.pageSize") >0) {
            let start = ((component.get("v.pageNumber") * component.get("v.pageSize"))- component.get("v.pageSize"))+1;
            let end = '';
            if(component.get("v.pageSize") > component.get("v.totalRecords")) {
                end = component.get("v.totalRecords");   
            } else {
                end = component.get("v.pageNumber") * component.get("v.pageSize");
                if(end > component.get("v.totalRecords")) {
                    end = start + component.get("v.dataSize") -1;
                }
            }
            showRecCountTextMsg = start+" - "+end+" of "+component.get("v.totalRecords");
        }

        if(resultData.length < parseInt(component.get("v.pageSize")) || parseInt(component.get("v.pageSize")) == component.get('v.totalRecords') || 
           (component.get("v.pageNumber") * component.get("v.pageSize")) == component.get('v.totalRecords') ) {
            component.set("v.isLastPage", true);
            paginationCmp.paginationMethod(true, showRecCountTextMsg);
        } else{
            component.set("v.isLastPage", false);
            paginationCmp.paginationMethod(false, showRecCountTextMsg);
        }
    }
})