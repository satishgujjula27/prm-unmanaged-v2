({
    /**
    * Method Name: closeMessage
    * @description: for closing Success Message Modal
    * Param: Component, event, helper
    * Return type: N/A
    **/
	closeMessage : function(component, event, helper) {
        var closeMessageModalEvent = component.getEvent("closeMessageModal");
        closeMessageModalEvent.fire();
 		component.destroy();
	}
})