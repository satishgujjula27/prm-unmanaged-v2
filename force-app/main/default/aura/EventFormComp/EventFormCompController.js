({
    doInit: function (component, event, helper) {
        component.set("v.evlocation", component.get("v.location"));
        component.set("v.formdate",new Date());
        var timezone = $A.get("$Locale.timezone");
        component.set("v.tmzone",timezone);
    },
    handleCallSub: function (component, event, helper) {
        component.set("v.ev.Subject", event.getParam("text"));
    },
    insertStart: function (component, event, helper) {
        var d = component.get("v.formdate");
        var n = d.toISOString();
        component.set("v.ev.StartDateTime","");
        component.set("v.ev.StartDateTime",n);
    },
    insertEnd: function (component, event, helper) {
        var d = component.get("v.formdate");
        var n = d.toISOString();
        component.set("v.ev.EndDateTime","");
        component.set("v.ev.EndDateTime",n);
    },
    sendEvent: function (component, event, helper) {
        if (event.getSource().get("v.name") == 'location') {
            component.set("v.evlocation", event.getSource().get("v.value"));
            component.set("v.ev.Location", event.getSource().get("v.value"));
        }
        var compEvent = component.getEvent("eventevt");// getting the Instance of event
        compEvent.setParams({ "event": component.get("v.ev"), "indxno": component.get("v.indxno") });// setting the attribute of event
        compEvent.fire();// firing the event.
    }
})
