({
	itemSelected : function(component, event, helper) {
		helper.itemSelected(component, event, helper);
	},
    serverCall : function(component, event, helper) {
		helper.serverCall(component, event, helper);
	},
    clearSelection : function(component, event, helper){
        helper.clearSelection(component, event, helper);
    },
    openAdvSearchModal: function(component, event, helper){
        component.set("v.showModal", true);
    },
    closeAdvSearchModal: function(component, event, helper){
        component.set("v.showModal", false);
    },
    saveAdvSearchModal: function(component, event, helper) {
        component.set("v.showModal", false);
    },
    selectedSpecValue : function(component, event, helper) {
        let selRows = JSON.parse(JSON.stringify(event.getParam('selectedRows')));
        let selectedDataObj = JSON.parse(JSON.stringify(component.get('v.selectedDataObj'))) || [];
        let selObj = new Object(); let chkIndexVal;
        if( selRows.length > 1 && selRows.length != 0 ) {
            for( let indRow = 0; indRow < selRows.length; indRow++ ) {
                selObj["label"] = selRows[indRow][component.get("v.EDLnamespace")+'Grouping__c'];
                selObj["value"] = selRows[indRow].Id;
                chkIndexVal = selectedDataObj.findIndex(idx => idx.value === selRows[indRow].Id);
            }
            selectedDataObj.push(selObj);
        } else if( selRows.length != 0 ) {
            selObj["label"] = selRows[0][component.get("v.EDLnamespace")+'Grouping__c'];
            selObj["value"] = selRows[0].Id;
            chkIndexVal = selectedDataObj.findIndex(idx => idx.value === selRows[0].Id);
            selectedDataObj.push(selObj);
        }
        if(chkIndexVal != -1) {selectedDataObj.splice(chkIndexVal, 1);}
        component.set('v.selectedDataObj', selectedDataObj);
        helper.dispatchSelectChangeEvent(component, selRows, selRows, false);
    },
    handleSelectAll : function( component, event, helper ) {
        component.set("v.isDisable", true);
        let selObj = new Object(); let selectedDataObj = [];
        selObj["label"] = 'All '+component.get("v.labelAll");
        selObj["value"] = 'ALLSELECTED';
        selectedDataObj.push(selObj);
        component.set('v.selectedDataObj', selectedDataObj);
        helper.dispatchSelectChangeEvent(component,['ALLSELECTED'],['All Specialities'], true);
    },
    removePill : function( component, event, helper ) {
        var recordId = event.getSource().get('v.name');
        var selectedDataObj = component.get('v.selectedDataObj');
        
        var chkIndexVal = selectedDataObj.findIndex(idx => idx.value === recordId);
        if(chkIndexVal != -1) { selectedDataObj.splice(chkIndexVal, 1); }
        
        component.set('v.selectedDataObj', selectedDataObj);
        component.set("v.isDisable", false);
    }
})