({
	itemSelected : function(component, event, helper) {
        var target = event.target;   
        var SelIndex = helper.getIndexFrmParent(target,helper,"data-selectedIndex");
        if(SelIndex){
            var serverResult = component.get("v.serverResult");
            var selItem = serverResult[SelIndex];
            if(selItem.val == 'new'){
                helper.createRecord(component);
                component.set("v.showListBoxOptions", false);
            }
            else{
                component.set("v.showListBoxOptions", false);
                if(selItem.val){
                    component.set("v.selItem",selItem);
                    component.set("v.lastServerResult",serverResult);
                    helper.fireSelBackEvent(component);
                } 
                component.set("v.serverResult",null);
            }
        } 
	}, 
    serverCall : function(component, event, helper) {  
        var target = event.target;  
        var searchText = target.value; 
        var last_SearchText = component.get("v.lastSearchText");
        //Escape button pressed 
        if (event.keyCode == 27 || !searchText.trim()) { 
            helper.clearSelection(component, event, helper);
        }else if(searchText.trim() != last_SearchText ){ 
            //Save server call, if last text not changed
            //Search only when space character entered
         
            var objectName = component.get("v.objectName");
            var fieldAPItext = component.get("v.fieldAPIText");
            var fieldAPIval = component.get("v.fieldAPIVal");
            var fieldAPIsearch = component.get("v.fieldAPISearch");
            var limit = component.get("v.limit");
            
            var action = component.get('c.searchDB');
            action.setStorable();
            
            action.setParams({
                objectName : objectName,
                fldAPIText : fieldAPItext,
                fldAPIVal : fieldAPIval,
                lim : limit, 
                fldAPISearch : fieldAPIsearch,
                searchText : searchText
            });
    
            action.setCallback(this,function(a){
                this.handleResponse(a,component,helper);
            });
            
            component.set("v.lastSearchText",searchText.trim());
            $A.enqueueAction(action); 
            component.set("v.showListBoxOptions", true);
        }else if(searchText && last_SearchText && searchText.trim() == last_SearchText.trim()){ 
            component.set("v.serverResult",component.get("v.lastServerResult"));
        }         
	},
    handleResponse : function (res,component,helper){
        if (res.getState() === 'SUCCESS') {
            var retObj = JSON.parse(res.getReturnValue());
            if(retObj.length <= 0){
                var noResult = JSON.parse('[]');
                if(component.get("v.isNewCreationEnable") == true) {
                    noResult.push({'text' : '+ New '+component.get("v.objectLabel"),'val':'new','objName':'Account'});
                } else {
                    noResult.push({"text" : "No Results Found"});
                }
                component.set("v.serverResult",noResult); 
            	component.set("v.lastServerResult",noResult);
            } else {
                if(component.get("v.isNewCreationEnable") == true) {
                    retObj.push({'text' : '+ New '+component.get("v.objectLabel"),'val':'new','objName':'Account'});
                }
                component.set("v.serverResult",retObj); 
            	component.set("v.lastServerResult",retObj);
            }  
        } else if (res.getState() === 'ERROR') {
            var errors = res.getError();
            if (errors) {
                if (errors[0] && errors[0].message) {
                    alert(errors[0].message);
                }
            } 
        }
    },
    getIndexFrmParent : function(target,helper,attributeToFind){
        //User can click on any child element, so traverse till intended parent found
        var SelIndex = target.getAttribute(attributeToFind);
        while(!SelIndex){
            target = target.parentNode ;
			SelIndex = helper.getIndexFrmParent(target,helper,attributeToFind);           
        }
        return SelIndex;
    },
    clearSelection: function(component, event, helper){
        component.set("v.selItem",null);
        component.set("v.serverResult",null);
        helper.fireSelBackEvent(component);
    },
    fireSelBackEvent: function(component){
        // call the event
        var compEvent = component.getEvent("olookupSelectedItemEvent");
        // set the Selected sObject Record to the event attribute.  
        compEvent.setParams({"selItem" : component.get("v.selItem"),"lookupFieldName":component.get("v.lookupFieldName") });  
        compEvent.fire();
    },
    dispatchSelectChangeEvent: function(component,values,labels, isAllSelected){
        var appEvent = $A.get("e.c:SelectChange");
        appEvent.setParams({ "values": values, "labels" : labels, "isAllSelected" : isAllSelected });
        appEvent.fire();
    },
    createRecord : function(component){
        var createRecordEvent = $A.get("e.force:createRecord");
        var params = {
                        "entityApiName": component.get("v.objectName"),
                        "navigationLocation":"LOOKUP" 	
                     };
        if(component.get("v.recordTypeId") != null){
          //  params["recordTypeId"] = component.get("v.recordTypeId")
        }
        createRecordEvent.setParams(params);
        createRecordEvent.fire();
    }
})