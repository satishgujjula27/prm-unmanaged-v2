({
	handleSuccess : function(component, event, helper) {
        if(component.get("v.isEnable") == 'Yes'){
            var fieldData = component.get("v.fieldData");
            var sendEmailAction = component.get("c.sendEmail");
            sendEmailAction.setParams({
                caseData : fieldData,
                emailId : component.get("v.emailId")
            });
            $A.enqueueAction(sendEmailAction);
        }
        component.set("v.showSuccess", true);
	},
    onRecordSubmit : function(component, event, helper) {
        if(component.get("v.isEnable") == 'Yes'){
            event.preventDefault(); // stop form submission
            var eventFields = event.getParam("fields");
            eventFields["Origin"] = "Email";
            component.set("v.fieldData",eventFields);
            component.find('ticketForm').submit(eventFields);
        }
    },
    raiseAnotherTicket : function(component, event, helper) {
       component.set("v.showSuccess", false);
    }
    
})