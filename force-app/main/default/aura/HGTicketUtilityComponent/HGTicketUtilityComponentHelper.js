({
    showSuccessMessage : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "The Ticket Raised successfully."
        });
        toastEvent.fire();
    }
})