({
    removeSelectedRecHelper : function(component) {
        $A.util.addClass(component.find("spinner"), "slds-show");
        // call the apex class method 
        var removePLTRecAction = component.get("c.removePLTRecords"); var self = this;
        // set param to method  
        removePLTRecAction.setParams({ 'recordIds': JSON.stringify(component.get('v.selectedRecId')), });
        // set a callBack    
        removePLTRecAction.setCallback(this, function(response) {
            $A.util.removeClass(component.find("spinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                var message = component.get("v.selectedsize") + ' Territory Provider(s) removed successfully.';
                message += " This page will be redirected back to Territory Providers";
                self.showMessage("Success!", message, "success");
                setTimeout(function(){ 
                    var url = window.location.href; 
                    var value = url.substr(0,url.lastIndexOf('/') + 1);
                    window.history.back();
                    return false;
                }, 5000);
            }
        });
        // enqueue the Action  
        $A.enqueueAction(removePLTRecAction);
    },
    initProviderIds : function(component){
        var ids = component.get("v.ids");
        let idArr = ids.split(",");
        if(ids!=null &&  ids!="" && idArr.length >0) {
            component.set("v.selectedRecId",idArr);
            component.set("v.selectedsize",idArr.length);
        }
    },
    showMessage : function(messageTitle,messageBody,messageType){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ "title": messageTitle, "type":messageType, "message": messageBody });
        toastEvent.fire();
    }
})