({
    doInit : function(component, event, helper) {
        component.set("v.selectedRecId",[]);
        component.set("v.selectedsize",0);
        helper.initProviderIds(component)
    },
	goBack : function(component, event, helper) { window.history.back(); },
    goBackToRelatedRecord : function(component, event, helper) { window.history.back(); },
    removeSelectedRec : function(component, event, helper) { helper.removeSelectedRecHelper(component); }
})