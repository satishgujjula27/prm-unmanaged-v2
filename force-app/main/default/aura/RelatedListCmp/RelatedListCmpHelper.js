({
	fetchRelatedRec : function(component,temObjName) {
        var self = this; var fetchRelatedRecObj = new Object();
        fetchRelatedRecObj["recId"] = component.get("v.recordId");
        fetchRelatedRecObj["sObjName"] = temObjName;
        fetchRelatedRecObj["parentFieldApiName"] = component.get( "v.parentFieldApiName" );
        fetchRelatedRecObj["strCriteria"] = component.get( "v.criteria" );
        self.doServerCall(component, 'fetchRecs', fetchRelatedRecObj, 'fetchRelatedRec', false).then(
            function(fetchRelatedRecdetail) {
                let tempTitle = component.get( "v.title" );
                component.set( "v.listRecords", fetchRelatedRecdetail.listsObject );
                component.set( "v.title", tempTitle + fetchRelatedRecdetail.strTitle );
            }
        );
	},
    fetchPLTRec : function(component) {
        var self = this; var fetchPLTRecObj = new Object();
        fetchPLTRecObj["recId"] = component.get("v.recordId");
        self.doServerCall(component, 'getPLTRecord', fetchPLTRecObj, 'fetchPLTRec', false).then(
            function(fetchPLTRecdetail) {
                component.set("v.spinner", false);
                let result = JSON.parse(fetchPLTRecdetail);
                if(result !=null){
                    component.set("v.listRecords", result.resultWrapper);
                    var tempTitle = component.get( "v.tempTitle" );
                    component.set("v.title", tempTitle +" "+ result.strTitle);
                    component.set("v.fields", result.fieldsWrapper);
                }
            }
        );
	},
	doEdit : function(recordId){
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({ "recordId": recordId });
        editRecordEvent.fire();
    },
    doDelete : function(component,recId){
        $A.createComponent("c:ConfirmMessageCmp", {recId:recId},
			function(content, status) {
                if (status === "SUCCESS") {
                    var modalBody = content;
                    component.find('overlayLib').showCustomModal({
                        header: "Delete Provider Location Territory",
                        body: 'Are you sure you want to delete this Provider Location Territory?',
                        footer:modalBody,
                        showCloseButton: true,
                        cssClass : 'detail slds-text-align_center',
                        closeCallback: function(ovl) { }
                    }).then(function(overlay){ });
                }
            });
    },
    handleDeleteEvent : function(component,event,helper){
        var deleteAction = component.get("c.deleteRecord");
        deleteAction.setParams({
            recordId : event.getParam("deleteId")
        });
        deleteAction.setCallback(this,function(response){
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS"){
                helper.fetchPLTRec(component);
            }
        });
        $A.enqueueAction(deleteAction);
    },
    doServerCall : function(component, apexAction, params, callFromMethod, pushToEvent) {
        var self = this;
        try {
            return new Promise(function(resolve, reject) {
                var action = component.get("c."+apexAction+"");
                if( params != null || typeof params != "undefined" ) {
                    action.setParams( params );
                }
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        resolve( response.getReturnValue() );
                    }
                });
                $A.enqueueAction(action);
            });
        } catch(e) {
            console.log("Failed to load data from server - "+callFromMethod);
        }
    }
})