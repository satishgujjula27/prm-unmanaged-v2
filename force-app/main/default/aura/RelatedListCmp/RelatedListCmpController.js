({
    fetchRecords : function(component, event, helper) {
        component.set("V.tempTitle",component.get( "v.title" ));
        var temObjName = component.get( "v.sobjectName" );
        component.set( "v.ObjectName", temObjName.replace( "__c", "" ).replace( "_", " " ) );
        if(temObjName.includes('PractitionerLocationTerritory')) {
            component.set("v.isPLTObj",true);
            helper.fetchPLTRec(component);
        } else {
            helper.fetchRelatedRec(component,temObjName);
        }
    },
    viewRelatedList: function (component, event, helper) {
        if(component.get("v.isPLTObj") == true){
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef : "c:RelatedLkpFilterComponent",
                componentAttributes: {
                    recordId : component.get( "v.recordId" ),
                    title : component.get( "v.tempTitle" )
                }
            });
            evt.fire();
        } else {
            var navEvt = $A.get("e.force:navigateToRelatedList");
            navEvt.setParams({
                "relatedListId": component.get("v.ObjectName")+'s__r',
                "parentRecordId": component.get( "v.recordId" )
            });
            navEvt.fire();
        }
    },
    viewRecord: function (component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        var recId = event.getSource().get( "v.value" )
        navEvt.setParams({ "recordId": recId });
        navEvt.fire();
    },
    handleMenu: function(component, event, helper) {
        let recordId = event.getSource().get("v.class");
        var menuValue = event.detail.menuItem.get("v.value");
        switch(menuValue) {
            case "edit": helper.doEdit(recordId); break;
            case "delete": helper.doDelete(component,recordId); break;
        }
    },
    handleDeleteEvent : function(component,event,helper){
        var deleteAction = component.get("c.deleteRecord");
        deleteAction.setParams({
            recordId : event.getParam("deleteId")
        });
        deleteAction.setCallback(this,function(response){
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS"){
                helper.fetchPLTRec(component);
            }
        });
        $A.enqueueAction(deleteAction);
    },
    handleRelListDetailEvent : function(component,event,helper) {
        var menuValue = event.getParam("actionType");
        switch(menuValue) {
            case "edit": helper.doEdit(event.getParam("recordId")); break;
            case "delete": helper.doDelete(component,event.getParam("recordId")); break;
        }
    }
})