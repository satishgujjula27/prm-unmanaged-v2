({
	doServerSideCall : function(component, apexAction, params, json, callFromMethod) {
        var self = this;
        try {
            return new Promise(function(resolve, reject) {
                var action = component.get("c."+apexAction+"");
                if( params != null || typeof params != "undefined" ) {
                    action.setParams( params );
                }
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        resolve(response.getReturnValue());
                    } else {
                        console.log(response.getError());
                    }
                });
                $A.enqueueAction(action);
            });
        } catch(e) {
            console.log(e);
        }
    }
})