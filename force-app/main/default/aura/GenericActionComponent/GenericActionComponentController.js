({
	doInit : function(component, event, helper) {
        let device = $A.get("$Browser.formFactor");
        let searchRecordParams = new Object();
        searchRecordParams["objApiName"] = component.get("v.sObjectName");
        helper.doServerSideCall(component, 'queryWebLinks', searchRecordParams, 'doInit').then(
            function(response) {
                if(response === null ) { } else {
                    if( response.length > 0 ) {
                        component.set("v.showBlankCard", true);
                    }
                    if (device === "PHONE") {
                        let res = [];
                        for (let links of response ) {
                            if (links.isRenderSalesforce1 == true) {
                                res.push(links);
                            }
                        }
                        component.set("v.webLinkItems", res);
                    } else {
                        component.set("v.webLinkItems", response);
                    }
                }
            }
        );
	}
})